package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.WeekDays;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;

import org.apache.log4j.Logger;

/**
 * Home object for domain model class WeekDays.
 * @see kg.enesaitech.entity.WeekDays
 * @author Hibernate Tools
 */
public class WeekDaysHome {

	private static final Logger log = Logger.getLogger(WeekDaysHome.class);
	private static WeekDaysHome weekDaysHome = null;

	public static WeekDaysHome getInstance(){
		if(weekDaysHome == null){
			weekDaysHome = new WeekDaysHome();
		}
		return weekDaysHome;
	}

	public void persist(WeekDays transientInstance) {
		log.debug("persisting WeekDays instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(WeekDays persistentInstance) {
		log.debug("removing WeekDays instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public WeekDays merge(WeekDays detachedInstance) {
		log.debug("merging WeekDays instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				WeekDays result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public WeekDays findById(Integer id) {
		log.debug("getting WeekDays instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				WeekDays instance = entityManager.find(WeekDays.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public boolean deleteById(int id) {
		boolean result = true;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager.createQuery("DELETE FROM WeekDays WHERE id = :id");
				query.setParameter("id", id);
				
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("Get failed", re);
				
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				
				result = false;
				throw re;
			}
		}finally{entityManager.close();}
		
		
		return result;
	}
	public List<WeekDays> getList(SearchParameters searchParameters) {
		
		List<WeekDays> result = null;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery("FROM WeekDays");

				@SuppressWarnings("unchecked")
				List<WeekDays> weekDays = query.getResultList();
				result = weekDays;

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
			}
		}finally{entityManager.close();}
		

		return result;
	}
}
