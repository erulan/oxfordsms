package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.UsersRole;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class UsersRole.
 * @see kg.enesaitech.entity.UsersRole
 * @author Hibernate Tools
 */
public class UsersRoleHome {

	private static final Logger log = Logger.getLogger(UsersRoleHome.class);
	private static UsersRoleHome usersRoleHome = null;
	
	public static UsersRoleHome getInstance(){
		if(usersRoleHome == null){
			usersRoleHome = new UsersRoleHome();
		}
		return usersRoleHome;
	}

	public void persist(UsersRole transientInstance) {
		log.debug("persisting UsersRole instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(UsersRole persistentInstance) {
		log.debug("removing UsersRole instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public UsersRole merge(UsersRole detachedInstance) {
		log.debug("merging UsersRole instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				UsersRole result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
	
	}

	public UsersRole findById(Integer id) {
		log.debug("getting UsersRole instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				UsersRole instance = entityManager.find(UsersRole.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public boolean deleteById(int id) {
		boolean result = true;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager.createQuery("DELETE FROM UsersRole WHERE id = :id");
				query.setParameter("id", id);
				
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("Get failed", re);
				
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				
				result = false;
				throw re;
			}
		}finally{entityManager.close();}
		
		
		return result;
	}
	
	public boolean deleteByRoleName(String name) {
		boolean result = true;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager.createQuery("DELETE FROM UsersRole AS ur WHERE ur.role.id = (select r.id from Role r where r.name = :name)");
				query.setParameter("name", name);
				
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("Get failed", re);
				
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				
				result = false;
				throw re;
			}
		}finally{entityManager.close();}
		
		
		return result;
	}
	
	public boolean deleteByUserName(String name) {
		boolean result = true;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager.createQuery("DELETE FROM UsersRole AS ur WHERE ur.users.id = (select u.id from Users u where u.userName = :name)");
				query.setParameter("name", name);
				
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("Get failed", re);
				
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				
				result = false;
				throw re;
			}
		}finally{entityManager.close();}
		
		
		return result;
	}
	public SearchResult<UsersRole> getList(SearchParameters searchParameters) {

		//Variable Decleration part
		SearchResult<UsersRole> searchResult = new SearchResult<UsersRole>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}

				
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<UsersRole> usersRoles = 
						query.getResultList();
				searchResult.setResultList(usersRoles);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return searchResult;
	}
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from UsersRole b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){
			if(searchParameters.getSearchParameters().get("userName") != null){
				searchStringList.add(" upper(b.users.userName) like :userName ");
				queryParamIsString.put("userName", true);
			}
			if(searchParameters.getSearchParameters().get("role") != null){
				searchStringList.add(" upper(b.role.name) like :role ");
				queryParamIsString.put("role", true);
			}
			if(searchParameters.getSearchParameters().get("userId") != null){
				searchStringList.add(" cast(b.users.id as string) = :userId ");
				queryParamIsString.put("userId", false);
			}
			if(searchParameters.getSearchParameters().get("roleId") != null){
				searchStringList.add(" cast(b.role.id as string) = :roleId ");
				queryParamIsString.put("roleId", false);
			}
			

			
			if(searchStringList.size() > 0){
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			

			
			if(searchParameters.getOrderParamDesc().get("userName") != null){
				orderList.add(" b.users.userName " + (searchParameters.getOrderParamDesc().get("userName") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("role") != null){
				orderList.add(" b.role.name " + (searchParameters.getOrderParamDesc().get("role") ? " DESC " : " ASC "));
			}
						
			
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	};
}