package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.Teacher;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class Teacher.
 * @see kg.enesaitech.entity.Teacher
 * @author Hibernate Tools
 */

public class TeacherHome {

	private static final Logger log = Logger.getLogger(TeacherHome.class);
	private static TeacherHome teacherHome = null;

	public static TeacherHome getInstance(){
		if(teacherHome == null){
			teacherHome = new TeacherHome();
		}
		return teacherHome;
	}

	public void persist(Teacher transientInstance) {
		log.debug("persisting Teacher instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
	
	}

	public void remove(Teacher persistentInstance) {
		log.debug("removing Teacher instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Teacher merge(Teacher detachedInstance) {
		log.debug("merging Teacher instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Teacher result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Teacher findById(Integer id) {
		log.debug("getting Teacher instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Teacher instance = entityManager.find(Teacher.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from Teacher where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;			
			}
		}finally{entityManager.close();}
		
		return true;
	}
	public SearchResult<Teacher> getList(SearchParameters searchParameters) {

		//Variable Decleration part
		SearchResult<Teacher> searchResult = new SearchResult<Teacher>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}

				
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<Teacher> teachers = 
						query.getResultList();
				searchResult.setResultList(teachers);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				
			}
		}finally{entityManager.close();}
		

		return searchResult;
	}
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from Teacher b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){
			if(searchParameters.getSearchParameters().get("name") != null){
				searchStringList.add(" (upper(b.nameEn) like :name OR upper(b.nameRu) like :name ) ");
				queryParamIsString.put("name", true);
			}
			if (searchParameters.getSearchParameters().get("teacher") != null) {
				searchStringList
						.add(" (upper(b.nameEn) like :teacher OR upper(b.nameRu) like :teacher OR upper(b.nameRu) like :teacher "
								+ " OR upper(b.surnameEn) like :teacher OR upper(b.surnameRu) like :teacher "
								+ " OR concat(upper(b.nameEn), ' ', upper(b.surnameEn)) like :teacher "
								+ " OR concat(upper(b.nameRu), ' ', upper(b.surnameRu)) like :teacher )  ");

				queryParamIsString.put("teacher", true);
			}
			if(searchParameters.getSearchParameters().get("nationality") != null){
				searchStringList.add(" upper(b.nationality.nationality) like :nationality ");
				queryParamIsString.put("nationality", true);
			}
			if(searchParameters.getSearchParameters().get("surname") != null){
				searchStringList.add(" (upper(b.surnameEn) like :surname OR upper(b.surnameRu) like :surname ) ");
				queryParamIsString.put("surname", true);
			}
			if(searchParameters.getSearchParameters().get("teacherNum") != null){
				searchStringList.add(" upper(b.teacherNum) like :teacherNum ");
				queryParamIsString.put("teacherNum", true);
			}			
			if(searchParameters.getSearchParameters().get("nationalityId") != null){
				searchStringList.add(" cast(nationality.id as string) = :nationalityId ");
				queryParamIsString.put("nationalityId", false);
			}
			if(searchParameters.getSearchParameters().get("countryId") != null){
				searchStringList.add(" cast(country.id as string) = :countryId ");
				queryParamIsString.put("countryId", false);
			}
			if(searchParameters.getSearchParameters().get("oblastId") != null){
				searchStringList.add(" cast(oblast.id as string) = :oblastId ");
				queryParamIsString.put("oblastId", false);
			}
			if(searchParameters.getSearchParameters().get("regionId") != null){
				searchStringList.add(" cast(region.id as string) = :regionId ");
				queryParamIsString.put("regionId", false);
			}
			if(searchParameters.getSearchParameters().get("status") != null){
				searchStringList.add(" cast(status.id as string) = :status ");
				queryParamIsString.put("status", false);
			}
			if(searchParameters.getSearchParameters().get("bloodId") != null){
				searchStringList.add(" cast(blood.id as string) = :bloodId ");
				queryParamIsString.put("bloodId", false);
			}
			

			
			if(searchStringList.size() > 0){
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			

			
			if(searchParameters.getOrderParamDesc().get("name") != null){
				orderList.add(" b.nameEn " + (searchParameters.getOrderParamDesc().get("name") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("surname") != null){
				orderList.add(" b.surnameEn " + (searchParameters.getOrderParamDesc().get("surname") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("country") != null){
				orderList.add(" b.country.id " + (searchParameters.getOrderParamDesc().get("country") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("region") != null){
				orderList.add(" b.region.id " + (searchParameters.getOrderParamDesc().get("region") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("oblast") != null){
				orderList.add(" b.oblast.id " + (searchParameters.getOrderParamDesc().get("oblast") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("nationality") != null){
				orderList.add(" b.nationality.id " + (searchParameters.getOrderParamDesc().get("nationality") ? " DESC " : " ASC "));
			}
			
			
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	};
	
	public Set<String> teacherNumList() {
		Set<String> teacherNumbers = new HashSet<String>();
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				
				teacherNumbers = new HashSet<String>(entityManager.createQuery(" select teacherNum from Teacher t ").getResultList());
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
	
		return teacherNumbers;
	}
	
	public Teacher getByTeacherNum(String teacherNum) {
		Teacher teacher = null;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				List<Teacher> teachers = entityManager.createQuery("from Teacher t where t.teacherNum = :teacherNum").setParameter("teacherNum", teacherNum).getResultList();
				if(!teachers.isEmpty()){
					teacher = (Teacher) teachers.get(0);
				}
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if(entityManager.getTransaction().isActive()){
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		
		return teacher;
	};

}
