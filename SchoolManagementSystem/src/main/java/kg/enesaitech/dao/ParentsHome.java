package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.Parents;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class Parents.
 * @see kg.enesaitech.entity.Parents
 * @author Hibernate Tools
 */
public class ParentsHome {

	private static final Logger log = Logger.getLogger(ParentsHome.class);
	private static ParentsHome parentsHome = null;

	public static ParentsHome getInstance(){
		if(parentsHome == null){
			parentsHome = new ParentsHome();
		}
		return parentsHome;
	}

	public Integer persist(Parents transientInstance) {
		Integer id = null;
		log.debug("persisting Parents instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				entityManager.flush();
				id = transientInstance.getId();
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
		return id;
	}

	public void remove(Parents persistentInstance) {
		log.debug("removing Parents instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Parents merge(Parents detachedInstance) {
		log.debug("merging Parents instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Parents result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Parents findById(Integer id) {
		log.debug("getting Parents instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Parents instance = entityManager.find(Parents.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from Parents where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;			
			}
		}finally{entityManager.close();}
		
		return true;
	}
	
	public SearchResult<Parents> getList(SearchParameters searchParameters) {

		//Variable Decleration part
		SearchResult<Parents> searchResult = new SearchResult<Parents>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}

				
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<Parents> parents = 
						query.getResultList();
				searchResult.setResultList(parents);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return searchResult;
	}private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from Parents b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){
			if(searchParameters.getSearchParameters().get("parent1") != null){
				searchStringList.add(" upper(b.parent1) like :parent1 ");
				queryParamIsString.put("parent1", true);
			}
			if(searchParameters.getSearchParameters().get("parent2") != null){
				searchStringList.add(" upper(b.parent2) like :parent2 ");
				queryParamIsString.put("parent2", true);
			}
			if(searchParameters.getSearchParameters().get("parent1Number") != null){
				searchStringList.add(" upper(b.parent1Number) like :parent1Number ");
				queryParamIsString.put("parent1Number", true);
			}
			if(searchParameters.getSearchParameters().get("parent2Number") != null){
				searchStringList.add(" upper(b.parent2Number) like :parent2Number ");
				queryParamIsString.put("parent2Number", true);
			}
			
			if(searchStringList.size() > 0){
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			
			if(searchParameters.getOrderParamDesc().get("parent1") != null){
				orderList.add(" parent1 " + (searchParameters.getOrderParamDesc().get("parent1") ? " DESC " : " ASC "));
			}
			
			if(searchParameters.getOrderParamDesc().get("parent2") != null){
				orderList.add(" parent2 " + (searchParameters.getOrderParamDesc().get("parent2") ? " DESC " : " ASC "));
			}
			
			
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	}; 
}
