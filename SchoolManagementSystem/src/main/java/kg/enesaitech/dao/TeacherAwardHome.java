package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.TeacherAward;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;

import org.apache.log4j.Logger;

/**
 * Home object for domain model class Oblast.
 * 
 * @see kg.enesaitech.entity.Oblast
 * @author Hibernate Tools
 */
public class TeacherAwardHome {

	private static final Logger log = Logger.getLogger(TeacherAwardHome.class);
	private static TeacherAwardHome teacherAwardHome = null;

	public static TeacherAwardHome getInstance() {
		if (teacherAwardHome == null) {
			teacherAwardHome = new TeacherAwardHome();
		}
		return teacherAwardHome;
	}

	public Integer persist(TeacherAward transientInstance) {
		Integer teacherAwardId = null;
		log.debug("persisting TeacherAward instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				entityManager.flush();
				teacherAwardId = transientInstance.getId();
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		return teacherAwardId;
	}

	public void remove(TeacherAward persistentInstance) {
		log.debug("removing TeacherAward instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public TeacherAward merge(TeacherAward detachedInstance) {
		log.debug("merging TeacherAward instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				TeacherAward result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public TeacherAward findById(Integer id) {
		log.debug("getting TeacherAward instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				TeacherAward instance = entityManager.find(TeacherAward.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	
	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from TeacherAward where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;			
			}
		}finally{entityManager.close();}
		
		return true;
	}
	public List<TeacherAward> getList(SearchParameters searchParameters) {
		List<TeacherAward> result = null;

		String searchString = " from TeacherAward b ";
		List<String> searchStringList = new ArrayList<String>();
		Map<String, String> queryParam = new HashMap<String, String>();
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		

		if (searchParameters.getSearchParameters().get("gramota") != null) {
			searchStringList.add(" upper(b.gramota) like :gramota ");
			queryParam.put("gramota",
					searchParameters.getSearchParameters().get("gramota"));
		}

		if (searchStringList.size() > 0) {
			searchString += " where ";
		}

		for (int i = 0; i < searchStringList.size(); i++) {
			searchString += searchStringList.get(i);
			if (i + 1 != searchStringList.size()) {
				searchString += " and ";
			}
		}
		if (!searchParameters.getOrderParamDesc().isEmpty()) {
			searchString += " order by ";

			if (searchParameters.getOrderParamDesc().get("gramota") != null) {
				searchString += " gramota "
						+ (searchParameters.getOrderParamDesc().get("gramota") ? " DESC "
								: " ASC ");
			}

		}
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);

				for (Map.Entry<String, String> entry : queryParam.entrySet()) {
					query.setParameter(entry.getKey(), "%"
							+ entry.getValue().toUpperCase() + "%");
				}

				@SuppressWarnings("unchecked")
				List<TeacherAward> oblasts = query.getResultList();
				result = oblasts;

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return result;
	}
}
