package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.ClassHours;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class ClassHours.
 * 
 * @see kg.enesaitech.entity.ClassHours
 * @author Hibernate Tools
 */
public class ClassHoursHome {

	private static final Logger log = Logger.getLogger(ClassHoursHome.class);
	private static ClassHoursHome classHoursHome = null;

	public static ClassHoursHome getInstance() {
		if (classHoursHome == null) {
			classHoursHome = new ClassHoursHome();
		}
		return classHoursHome;
	}

	public void persist(ClassHours transientInstance) {
		log.debug("persisting ClassHours instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(ClassHours persistentInstance) {
		log.debug("removing ClassHours instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
	
	}

	public ClassHours merge(ClassHours detachedInstance) {
		log.debug("merging ClassHours instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				ClassHours result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public ClassHours findById(int id) {
		log.debug("getting ClassHours instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				ClassHours instance = entityManager.find(ClassHours.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
	
	}

	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from ClassHours where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
		return true;
	}
	
	public SearchResult<ClassHours> getList(SearchParameters searchParameters) {
		// Variable Decleration part
		SearchResult<ClassHours> searchResult = new SearchResult<ClassHours>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();

		String searchString = prepareQueryString(searchParameters,
				queryParamIsString);
		
		// Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) "
						+ searchString);

				// Set Parameters after creating Query.
				// Parameters(queryParamIsString key->paramName, value->isString)
				// prepared in Prepare Parameters part of prepareQueryString
				// function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
					query.setParameter(
							varName.getKey(),
							varName.getValue() ? "%"
									+ searchParameters.getSearchParameters()
											.get(varName.getKey()).toUpperCase()
									+ "%" : searchParameters.getSearchParameters()
									.get(varName.getKey()));
					queryCount.setParameter(
							varName.getKey(),
							varName.getValue() ? "%"
									+ searchParameters.getSearchParameters()
											.get(varName.getKey()).toUpperCase()
									+ "%" : searchParameters.getSearchParameters()
									.get(varName.getKey()));
				}

				Integer totalData = ((Number) queryCount.getSingleResult())
						.intValue();
				searchResult.setTotalRecords(totalData);
				if (searchParameters.getStartIndex() != null) {
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if (searchParameters.getResultQuantity() != null) {
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<ClassHours> classHours = query.getResultList();
				searchResult.setResultList(classHours);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();

				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return searchResult;
	}

	private String prepareQueryString(SearchParameters searchParameters,
			Map<String, Boolean> queryParamIsString) {
		String searchString = " from ClassHours b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();

		// Prepare Parameters Part
		if (!searchParameters.getSearchParameters().isEmpty()) {
			if(searchParameters.getSearchParameters().get("orderNum") != null){
				searchStringList.add(" cast(b.orderNum as string) = :orderNum ");
				queryParamIsString.put("orderNum", false);
			}
			if(searchParameters.getSearchParameters().get("classHourTypeId") != null){
				searchStringList.add(" cast(b.classHourType.id as string) = :classHourTypeId ");
				queryParamIsString.put("classHourTypeId", false);
			}

			if (searchStringList.size() > 0) {
				searchString += " where ";
			}

			searchString += StringUtils.join(searchStringList, " and ");
		}

		// Set order by part
		if (!searchParameters.getOrderParamDesc().isEmpty()) {
			if (searchParameters.getOrderParamDesc().get("orderNum") != null) {
				orderList
						.add(" b.orderNum "
								+ (searchParameters.getOrderParamDesc().get(
										"orderNum") ? " DESC " : " ASC "));
			}			

			if (orderList.size() > 0) {
				searchString += " order by ";
			}

			searchString += StringUtils.join(orderList, " , ");
		}

		return searchString;
	};
}
