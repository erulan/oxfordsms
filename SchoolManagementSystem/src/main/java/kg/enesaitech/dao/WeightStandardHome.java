package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.WeightForHeight;
import kg.enesaitech.entity.WeightStandard;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class Lesson.
 * 
 * @see kg.enesaitech.entity.Lesson
 * @author Hibernate Tools
 */
public class WeightStandardHome {

	private static final Logger log = Logger.getLogger(WeightStandardHome.class);
	private static WeightStandardHome weightStandardHome = null;

	public static WeightStandardHome getInstance() {
		if (weightStandardHome == null) {
			weightStandardHome = new WeightStandardHome();
		}
		return weightStandardHome;
	}

	public void persist(WeightStandard transientInstance) {
		log.debug("persisting WeightStandard instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(WeightStandard persistentInstance) {
		log.debug("removing WeightStandard instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public WeightStandard merge(WeightStandard detachedInstance) {
		log.debug("merging WeightStandard instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				WeightStandard result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public WeightStandard findById(Integer id) {
		log.debug("getting WeightStandard instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				WeightStandard instance = entityManager.find(WeightStandard.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from WeightStandard where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
		return true;
	}

	public SearchResult<WeightStandard> getList(SearchParameters searchParameters) {

		//Variable Decleration part
		SearchResult<WeightStandard> searchResult = new SearchResult<WeightStandard>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}

				
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<WeightStandard> weightStandards = 
						query.getResultList();
				searchResult.setResultList(weightStandards);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
	

		return searchResult;
	}
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from WeightStandard b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){					
			if(searchParameters.getSearchParameters().get("periodId") != null){
				searchStringList.add(" cast(b.period.id as string) = :periodId ");
				queryParamIsString.put("periodId", false);
			}
			if(searchParameters.getSearchParameters().get("month") != null){
				searchStringList.add(" cast(b.month as string) = :month");
				queryParamIsString.put("month", false);
			}
			if(searchParameters.getSearchParameters().get("isMale") != null){
				searchStringList.add(" cast(b.isMale as string) = :isMale");
				queryParamIsString.put("isMale", false);
			}	
			

			
			if(searchStringList.size() > 0){
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			

//			if (searchParameters.getOrderParamDesc().get("month") != null) {
//				searchString += " month "
//						+ (searchParameters.getOrderParamDesc().get("month") ? " DESC "
//								: " ASC ");
//			}
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	};
	
	public WeightStandard getWeightStandartByMonth(int month, int isMale) {
		WeightStandard weightStandard = null;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				
				Query query = entityManager.createQuery("from WeightStandard where cast(month as string) = :month and cast(isMale as string) = :isMale");
				query.setParameter("month", month+"");
				query.setParameter("isMale", isMale+"");
				weightStandard = (WeightStandard) query.getSingleResult();
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("get Role ByName error", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
			}
		}finally{entityManager.close();}
		
		return weightStandard;
	}

}
