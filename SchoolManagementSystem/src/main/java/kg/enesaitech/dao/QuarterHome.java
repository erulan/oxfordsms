package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.Quarter;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;

import org.apache.log4j.Logger;

/**
 * Home object for domain model class Quarter.
 * @see kg.enesaitech.entity.Quarter
 * @author Hibernate Tools
 */

public class QuarterHome {

	private static final Logger log = Logger.getLogger(QuarterHome.class);
	private static QuarterHome quarterHome = null;

	public static QuarterHome getInstance(){
		if(quarterHome == null){
			quarterHome = new QuarterHome();
		}
		return quarterHome;
	}
	public void persist(Quarter transientInstance) {
		log.debug("persisting Quarter instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(Quarter persistentInstance) {
		log.debug("removing Quarter instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Quarter merge(Quarter detachedInstance) {
		log.debug("merging Quarter instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Quarter result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Quarter findById(Integer id) {
		log.debug("getting Quarter instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Quarter instance = entityManager.find(Quarter.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	
	public Quarter getCurrentQuarter() {
		log.debug("getting Quarter instance ");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager.createQuery("from Quarter where isCurrent = 1 ");
				Quarter instance = (Quarter) query.getSingleResult();
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	
	public boolean deleteById(int id) {
		boolean result = true;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager.createQuery("DELETE FROM Quarter WHERE id = :id");
				query.setParameter("id", id);
				
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("Get failed", re);
				
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				
				result = false;
				throw re;
			}
		}finally{entityManager.close();}
		
		
		return result;
	}
	public List<Quarter> getList(SearchParameters searchParameters) {
		List<Quarter> result = null;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery("FROM Quarter order by id");

				@SuppressWarnings("unchecked")
				List<Quarter> quarter = query.getResultList();
				result = quarter;

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
			}
		}finally{entityManager.close();}
		

		return result;
	}
}
