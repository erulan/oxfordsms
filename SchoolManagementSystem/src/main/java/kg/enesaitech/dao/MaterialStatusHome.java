package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.MaterialStatus;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;

import org.apache.log4j.Logger;

/**
 * Home object for domain model class Status.
 * 
 * @see kg.enesaitech.entity.Status
 * @author Hibernate Tools
 */
public class MaterialStatusHome {

	private static final Logger log = Logger.getLogger(MaterialStatusHome.class);
	private static MaterialStatusHome materialHome = null;

	public static MaterialStatusHome getInstance() {
		if (materialHome == null) {
			materialHome = new MaterialStatusHome();
		}
		return materialHome;
	}

	public void persist(MaterialStatus transientInstance) {
		log.debug("persisting MaterialStatus instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
	
	}

	public void remove(MaterialStatus persistentInstance) {
		log.debug("removing MaterialStatus instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public MaterialStatus merge(MaterialStatus detachedInstance) {
		log.debug("merging MaterialStatus instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				MaterialStatus result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public MaterialStatus findById(Integer id) {
		log.debug("getting MaterialStatus instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				MaterialStatus instance = entityManager.find(MaterialStatus.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from MaterialStatus where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
		return true;
	}
	
	public List<MaterialStatus> getList(SearchParameters searchParameters) {
		List<MaterialStatus> result = null;
		String searchString = " from MaterialStatus b ";
		List<String> searchStringList = new ArrayList<String>();
		Map<String, String> queryParam = new HashMap<String, String>();
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		

		if (searchParameters.getSearchParameters().get("name") != null) {
			searchStringList.add(" upper(b.name) like :name ");
			queryParam.put("name",
					searchParameters.getSearchParameters().get("name"));
		}
		if (searchParameters.getSearchParameters().get("description") != null) {
			searchStringList.add(" upper(b.description) like :description ");
			queryParam.put("description", searchParameters.getSearchParameters()
					.get("description"));
		}

		if (searchStringList.size() > 0) {
			searchString += " where ";
		}

		for (int i = 0; i < searchStringList.size(); i++) {
			searchString += searchStringList.get(i);
			if (i + 1 != searchStringList.size()) {
				searchString += " and ";
			}
		}
		if (!searchParameters.getOrderParamDesc().isEmpty()) {
			searchString += " order by ";

			if (searchParameters.getOrderParamDesc().get("name") != null) {
				searchString += " name "
						+ (searchParameters.getOrderParamDesc().get("name") ? " DESC "
								: " ASC ");
			}

		}
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);

				for (Map.Entry<String, String> entry : queryParam.entrySet()) {
					query.setParameter(entry.getKey(), "%"
							+ entry.getValue().toUpperCase() + "%");
				}

				@SuppressWarnings("unchecked")
				List<MaterialStatus> materialStatuses = query.getResultList();
				result = materialStatuses;

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return result;
	}
}