package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.BookType;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class Book.
 * 
 * @see kg.enesaitech.entity.Book
 * @author Hibernate Tools
 */
public class BookTypeHome {

	private static final Logger log = Logger.getLogger(BookTypeHome.class);
	private static BookTypeHome bookTypeHome = null;

	public static BookTypeHome getInstance() {
		if (bookTypeHome == null) {
			bookTypeHome = new BookTypeHome();
		}
		return bookTypeHome;
	}

	public void persist(BookType transientInstance) {
		log.debug("persisting BookType instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{
			entityManager.close();
		}
	}

	public void remove(BookType persistentInstance) {
		log.debug("removing BookType instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{
			entityManager.close();
		}
	}

	public BookType merge(BookType detachedInstance) {
		log.debug("merging BookType instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				BookType result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{
			entityManager.close();
		}
		
	}

	public BookType findById(Integer id) {
		log.debug("getting BookType instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				BookType instance = entityManager.find(BookType.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{
			entityManager.close();
		}
	}

	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from BookType where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{
			entityManager.close();
		}
		
		return true;
	}
	public SearchResult<BookType> getList(SearchParameters searchParameters) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		//Variable Decleration part
		SearchResult<BookType> searchResult = new SearchResult<BookType>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}

				
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<BookType> books = 
						query.getResultList();
				searchResult.setResultList(books);

				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("list failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{
			entityManager.close();
		}
		
		return searchResult;
	}
	
	
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from BookType b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){
			if(searchParameters.getSearchParameters().get("name") != null){
				searchStringList.add(" upper(b.name) like :name ");
				queryParamIsString.put("name", true);
			}
			if(searchParameters.getSearchParameters().get("author") != null){
				searchStringList.add(" upper(b.author) like :author ");
				queryParamIsString.put("author", true);
			}
			if(searchParameters.getSearchParameters().get("categoryId") != null){
				searchStringList.add(" cast(category.id as string) = :categoryId ");
				queryParamIsString.put("categoryId", false);
			}
			if(searchParameters.getSearchParameters().get("categoryName") != null){
				searchStringList.add(" upper(b.category.name) like :categoryName ");
				queryParamIsString.put("categoryName", true);
			}
			
			if(searchStringList.size() > 0){
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			
			if(searchParameters.getOrderParamDesc().get("name") != null){
				orderList.add(" name " + (searchParameters.getOrderParamDesc().get("name") ? " DESC " : " ASC "));
			}
			
			if(searchParameters.getOrderParamDesc().get("author") != null){
				orderList.add(" author " + (searchParameters.getOrderParamDesc().get("author") ? " DESC " : " ASC "));
			}
			
			if(searchParameters.getOrderParamDesc().get("categoryId") != null){
				orderList.add(" category.id " + (searchParameters.getOrderParamDesc().get("categoryId") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("categoryName") != null){
				orderList.add(" category.name " + (searchParameters.getOrderParamDesc().get("categoryName") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("publishYear") != null){
				orderList.add(" publishYear " + (searchParameters.getOrderParamDesc().get("publishYear") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("cost") != null){
				orderList.add(" cost " + (searchParameters.getOrderParamDesc().get("cost") ? " DESC " : " ASC "));
			}
			
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	}; 
}
