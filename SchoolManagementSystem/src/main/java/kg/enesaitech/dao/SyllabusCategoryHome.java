package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.SyllabusCategory;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class Syllabus.
 * @see kg.enesaitech.entity.Syllabus
 * @author Hibernate Tools
 */
public class SyllabusCategoryHome {

	private static final Logger log = Logger.getLogger(SyllabusCategoryHome.class);
	private static SyllabusCategoryHome syllabusCategoryHome = null;

	public static SyllabusCategoryHome getInstance(){
		if(syllabusCategoryHome == null){
			syllabusCategoryHome = new SyllabusCategoryHome();
		}
		return syllabusCategoryHome;
	}

	public Integer persist(SyllabusCategory transientInstance) {
		Integer syllabusCategoryId = null;
		log.debug("persisting SyllabusCategory instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				entityManager.flush();
				syllabusCategoryId = transientInstance.getId();
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}

		return syllabusCategoryId;
	}

	public void remove(SyllabusCategory persistentInstance) {
		log.debug("removing SyllabusCategory instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
	
	}

	public SyllabusCategory merge(SyllabusCategory detachedInstance) {
		log.debug("merging SyllabusCategory instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				SyllabusCategory result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public SyllabusCategory findById(Integer id) {
		log.debug("getting SyllabusCategory instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				SyllabusCategory instance = entityManager.find(SyllabusCategory.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from SyllabusCategory where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;			
			}
		}finally{entityManager.close();}
		
		return true;
	}
	
	public SearchResult<SyllabusCategory> getList(SearchParameters searchParameters) {

		//Variable Decleration part
		SearchResult<SyllabusCategory> searchResult = new SearchResult<SyllabusCategory>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}

				
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<SyllabusCategory> stuffs = 
						query.getResultList();
				searchResult.setResultList(stuffs);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return searchResult;
	}
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from SyllabusCategory b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){
			if(searchParameters.getSearchParameters().get("notQuarterId") != null){
				searchStringList.add(" cast(b.quarter.id as string) != :notQuarterId ");
				queryParamIsString.put("notQuarterId", false);
			}
			if(searchParameters.getSearchParameters().get("courseId") != null){
				searchStringList.add(" cast(b.course.id as string) = :courseId ");
				queryParamIsString.put("courseId", false);
			}
			if(searchParameters.getSearchParameters().get("quarterId") != null){
				searchStringList.add(" cast(b.quarter.id as string) = :quarterId ");
				queryParamIsString.put("quarterId", false);
			}
			if(searchParameters.getSearchParameters().get("topic") != null){
				searchStringList.add(" upper(b.topic) like :topic ");
				queryParamIsString.put("topic", true);
			}
			if(searchParameters.getSearchParameters().get("courseLesson") != null){
				searchStringList.add(" upper(b.course.lesson.name) like :courseLesson ");
				queryParamIsString.put("courseLesson", true);
			}
			if(searchParameters.getSearchParameters().get("teacherName") != null){
				searchStringList.add(" (upper(b.course.teacher.nameEn) like :teacherName OR upper(b.course.teacher.nameRu) like :teacherName ) ");
				queryParamIsString.put("teacherName", true);
			}
			if(searchParameters.getSearchParameters().get("teacherSurname") != null){
				searchStringList.add(" (upper(b.course.teacher.surnameEn) like :teacherSurname OR upper(b.course.teacher.surnameRu) like :teacherSurname ) ");
				queryParamIsString.put("teacherSurname", true);
			}
			if(searchParameters.getSearchParameters().get("cGroupId") != null){
				searchStringList.add(" cast(b.course.CGroup.id as string) = :cGroupId ");
				queryParamIsString.put("cGroupId", false);
			}
			if(searchParameters.getSearchParameters().get("teacherId") != null){
				searchStringList.add(" cast(b.course.teacher.id as string) = :teacherId ");
				queryParamIsString.put("teacherId", false);
			}
			if (searchParameters.getSearchParameters().get("yearId") != null) {
				searchStringList.add(" cast(b.course.year.id as string) = :yearId ");
				queryParamIsString.put("yearId", false);
			}
			
			if (searchParameters.getSearchParameters().get("courseCode") != null) {
				searchStringList.add(" upper(b.course.code) like upper(:courseCode) ");
				queryParamIsString.put("courseCode", true);
			}
			if (searchParameters.getSearchParameters().get("bookName") != null) {
				searchStringList.add(" upper(b.course.book.name) like :bookName ");
				queryParamIsString.put("bookName", true);
			}
			if (searchParameters.getSearchParameters().get("lessonName") != null) {
				searchStringList.add(" upper(b.course.lesson.name) like :lessonName ");
				queryParamIsString.put("lessonName", true);
			}
			if (searchParameters.getSearchParameters().get("bookAuthor") != null) {
				searchStringList.add(" upper(b.course.book.author) like :bookAuthor ");
				queryParamIsString.put("bookAuthor", true);
			}
	

			
			if(searchStringList.size() > 0){
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			

			
			if(searchParameters.getOrderParamDesc().get("topic") != null){
				orderList.add(" b.topic " + (searchParameters.getOrderParamDesc().get("topic") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("lessonNo") != null){
				orderList.add(" b.lessonNo " + (searchParameters.getOrderParamDesc().get("lessonNo") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("lessonName") != null){
				orderList.add(" b.course.lesson.name " + (searchParameters.getOrderParamDesc().get("lessonName") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("teacherName") != null){
				orderList.add(" b.course.teacher.nameEn " + (searchParameters.getOrderParamDesc().get("teacherName") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("teacherSurname") != null){
				orderList.add(" b.course.teacher.surnameEn " + (searchParameters.getOrderParamDesc().get("teacherSurname") ? " DESC " : " ASC "));
			}
				
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	}
}
