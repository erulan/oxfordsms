package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.ClassLetter;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;

import org.apache.log4j.Logger;

/**
 * Home object for domain model class ClassLetter.
 * @see kg.enesaitech.entity.ClassLetter
 * @author Hibernate Tools
 */
public class ClassLetterHome {

	private static final Logger log = Logger.getLogger(ClassLetterHome.class);
	private static ClassLetterHome classLetterHome = null;

	public static ClassLetterHome getInstance(){
		if(classLetterHome == null){
			classLetterHome = new ClassLetterHome();
		}
		return classLetterHome;
	}


	public void persist(ClassLetter transientInstance) {
		log.debug("persisting ClassLetter instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if(entityManager.contains(transientInstance)){
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(ClassLetter persistentInstance) {
		log.debug("removing ClassLetter instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public ClassLetter merge(ClassLetter detachedInstance) {
		log.debug("merging ClassLetter instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				ClassLetter result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public ClassLetter findById(Integer id) {
		log.debug("getting ClassLetter instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				ClassLetter instance = entityManager.find(ClassLetter.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	public List<ClassLetter> getList() {
		List<ClassLetter> result = null;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery("FROM ClassLetter");
				
				@SuppressWarnings("unchecked")
				List<ClassLetter> classLetters = query.getResultList();
				result = classLetters;
				
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
			}
		}finally{entityManager.close();}
		
		
		return result;
	}
	public List<ClassLetter> getList(SearchParameters searchParameters) {
		List<ClassLetter> result = null;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery("FROM ClassLetter where id < 9");

				@SuppressWarnings("unchecked")
				List<ClassLetter> classLetters = query.getResultList();
				result = classLetters;

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
			}
		}finally{entityManager.close();}
	

		return result;
	}
}
