package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.NoteType;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class NoteType.
 * @see kg.enesaitech.entity.NoteType
 * @author Hibernate Tools
 */
public class NoteTypeHome {

	private static final Logger log = Logger.getLogger(NoteTypeHome.class);
	private static NoteTypeHome noteTypeHome = null;

	public static NoteTypeHome getInstance(){
		if(noteTypeHome == null){
			noteTypeHome = new NoteTypeHome();
		}
		return noteTypeHome;
	}

	public Integer persist(NoteType transientInstance) {
		Integer id = null;
		log.debug("persisting NoteType instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				entityManager.flush();
				id = transientInstance.getId();
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
		return id;
	}

	public void remove(NoteType persistentInstance) {
		log.debug("removing NoteType instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public NoteType merge(NoteType detachedInstance) {
		log.debug("merging NoteType instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				NoteType result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public NoteType findById(Integer id) {
		log.debug("getting NoteType instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				NoteType instance = entityManager.find(NoteType.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from NoteType where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;			
			}
		}finally{entityManager.close();}
		
		return true;
	}
	
	public SearchResult<NoteType> getList(SearchParameters searchParameters) {

		//Variable Decleration part
		SearchResult<NoteType> searchResult = new SearchResult<NoteType>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}

				
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<NoteType> NoteType = 
						query.getResultList();
				searchResult.setResultList(NoteType);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return searchResult;
	}private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from NoteType b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){
			if(searchParameters.getSearchParameters().get("name") != null){
				searchStringList.add(" upper(b.name) like :name ");
				queryParamIsString.put("name", true);
			}			
			if(searchParameters.getSearchParameters().get("weight") != null){
				searchStringList.add(" cast(weight as string) = :weight ");
				queryParamIsString.put("weight", false);
			}

			if(searchParameters.getSearchParameters().get("status") != null){
				searchStringList.add(" upper(b.status.name) like :status ");
				queryParamIsString.put("status", true);
			}
			
			if(searchStringList.size() > 0){
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			
			if(searchParameters.getOrderParamDesc().get("name") != null){
				orderList.add(" b.name " + (searchParameters.getOrderParamDesc().get("name") ? " DESC " : " ASC "));
			}
			
			if(searchParameters.getOrderParamDesc().get("weight") != null){
				orderList.add(" b.weight " + (searchParameters.getOrderParamDesc().get("weight") ? " DESC " : " ASC "));
			}
			
			
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	}; 
	
	public NoteType getNoteByName(String string) {
		NoteType noteType = null;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				
				Query query = entityManager.createQuery("from NoteType where name = :name");
				query.setParameter("name", string);
				noteType = (NoteType) query.getSingleResult();
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("get Note ByName error", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
			}
		}finally{entityManager.close();}
		
		return noteType;
	}
	public List<NoteType> getExamList() {
		List<NoteType> result = null;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		
		String searchString = " from NoteType b where b.name = 'last year' or b.name = 'entry exam' or b.name = 'GOS exam' ";

		
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);			

				@SuppressWarnings("unchecked")
				List<NoteType> countries = query.getResultList();
				result = countries;

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return result;
	}
}
