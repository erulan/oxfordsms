package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.Student;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class Student.
 * @see kg.enesaitech.entity.Student
 * @author Hibernate Tools
 */
public class StudentHome {

	private static final Logger log = Logger.getLogger(StudentHome.class);
	private static StudentHome studentHome = null;

	public static StudentHome getInstance(){
		if(studentHome == null){
			studentHome = new StudentHome();
		}
		return studentHome;
	}

	public Integer persist(Student transientInstance) {

		Integer studentId = null;
		log.debug("persisting Student instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				entityManager.flush();
				studentId = transientInstance.getId();
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		

		return studentId;
	}

	public void remove(Student persistentInstance) {
		log.debug("removing Student instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Student merge(Student detachedInstance) {
		log.debug("merging Student instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Student result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Student findById(Integer id) {
		log.debug("getting Student instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Student instance = entityManager.find(Student.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from Student where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;			
			}
		}finally{entityManager.close();}
		
		return true;
	}
	public SearchResult<Student> getList(SearchParameters searchParameters) {

		//Variable Decleration part
		SearchResult<Student> searchResult = new SearchResult<Student>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}

				
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<Student> students = query.getResultList();
				searchResult.setResultList(students);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return searchResult;
	}
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from Student b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){
			if(searchParameters.getSearchParameters().get("name") != null){
				searchStringList.add(" (upper(b.nameEn) like :name OR upper(b.nameRu) like :name ) ");
				queryParamIsString.put("name", true);
			}
			if(searchParameters.getSearchParameters().get("surname") != null){
				searchStringList.add(" (upper(b.surnameEn) like :surname OR upper(b.surnameRu) like :surname ) ");
				queryParamIsString.put("surname", true);
			}
			if (searchParameters.getSearchParameters().get("student") != null) {
				searchStringList
						.add(" (upper(b.nameEn) like :student OR upper(b.nameRu) like :student OR upper(b.nameRu) like :student OR upper(b.middleNameRu) like :student "
								+ " OR upper(b.surnameEn) like :student OR upper(b.surnameRu) like :student "
								+ " OR concat(upper(b.nameEn), ' ', upper(b.surnameEn)) like :student "
								+ " OR concat(upper(b.nameRu), ' ', upper(b.surnameRu)) like :student )  ");
				queryParamIsString.put("student", true);
			}
			if (searchParameters.getSearchParameters().get("class") != null) {
				searchStringList
						.add(" concat(b.CGroup.grade.grade, '', b.CGroup.classLetter.letter) like upper(:class) ");
				queryParamIsString.put("class", true);
			}
			if(searchParameters.getSearchParameters().get("studentNum") != null){
				searchStringList.add(" upper(b.studentNum) like :studentNum ");
				queryParamIsString.put("studentNum", true);
			}
			if(searchParameters.getSearchParameters().get("nationality") != null){
				searchStringList.add(" upper(b.nationality.nationality) like :nationality ");
				queryParamIsString.put("nationality", true);
			}
			if(searchParameters.getSearchParameters().get("cGroupId") != null){
				searchStringList.add(" cast(CGroup.id as string) = :cGroupId ");
				queryParamIsString.put("cGroupId", false);
			}
			if(searchParameters.getSearchParameters().get("schoolTypeId") != null){
				searchStringList.add(" cast(CGroup.grade.schoolType.id as string) = :schoolTypeId ");
				queryParamIsString.put("schoolTypeId", false);
			}
			if(searchParameters.getSearchParameters().get("courseId") != null){
				searchStringList.add(" cast(CGroup.id as string) = cast((select cg.id from Course c left join c.CGroup cg where cast(c.id as string) = :courseId ) as string)");
				queryParamIsString.put("courseId", false);
			}
			if(searchParameters.getSearchParameters().get("yearId") != null){
				searchStringList.add(" cast(year.id as string) = :yearId ");
				queryParamIsString.put("yearId", false);
			}
			if(searchParameters.getSearchParameters().get("nationalityId") != null){
				searchStringList.add(" cast(nationality.id as string) = :nationalityId ");
				queryParamIsString.put("nationalityId", false);
			}
			if(searchParameters.getSearchParameters().get("countryId") != null){
				searchStringList.add(" cast(country.id as string) = :countryId ");
				queryParamIsString.put("countryId", false);
			}
			if(searchParameters.getSearchParameters().get("oblastId") != null){
				searchStringList.add(" cast(oblast.id as string) = :oblastId ");
				queryParamIsString.put("oblastId", false);
			}
			if(searchParameters.getSearchParameters().get("regionId") != null){
				searchStringList.add(" cast(region.id as string) = :regionId ");
				queryParamIsString.put("regionId", false);
			}
			if(searchParameters.getSearchParameters().get("bloodId") != null){
				searchStringList.add(" cast(blood.id as string) = :bloodId ");
				queryParamIsString.put("bloodId", false);
			}
			if(searchParameters.getSearchParameters().get("status") != null){
				searchStringList.add(" cast(status.id as string) = :status ");
				queryParamIsString.put("status", false);
			}
			

			
			if(searchStringList.size() > 0){
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			

			
			if(searchParameters.getOrderParamDesc().get("name") != null){
				orderList.add(" b.nameEn " + (searchParameters.getOrderParamDesc().get("name") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("surname") != null){
				orderList.add(" b.surnameEn " + (searchParameters.getOrderParamDesc().get("surname") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("year") != null){
				orderList.add(" b.year " + (searchParameters.getOrderParamDesc().get("year") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("cGroup") != null){
				orderList.add(" b.CGroup.id " + (searchParameters.getOrderParamDesc().get("cGroup") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("country") != null){
				orderList.add(" b.country.id " + (searchParameters.getOrderParamDesc().get("country") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("region") != null){
				orderList.add(" b.region.id " + (searchParameters.getOrderParamDesc().get("region") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("oblast") != null){
				orderList.add(" b.oblast.id " + (searchParameters.getOrderParamDesc().get("oblast") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("nationality") != null){
				orderList.add(" b.nationality.id " + (searchParameters.getOrderParamDesc().get("nationality") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("class") != null) {
				orderList
				.add(" b.CGroup.grade.grade "
						+ (searchParameters.getOrderParamDesc().get(
								"class") ? " DESC " : " ASC ")
						+ " , b.CGroup.classLetter.letter "
						+ (searchParameters.getOrderParamDesc().get(
								"class") ? " DESC " : " ASC "));
	}
			
			
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	}

	public Set<String> studentNumList() {
		Set<String> studentNumbers = new HashSet<String>();
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				
				studentNumbers = new HashSet<String>(entityManager.createQuery(" select studentNum from Student s ").getResultList());
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		
		return studentNumbers;
	}

	public Student getByStudNum(String studNum) {
		Student student = null;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				List<Student> students = entityManager.createQuery("from Student s where s.studentNum = :studNum").setParameter("studNum", studNum).getResultList();
				if(!students.isEmpty()){
					student = (Student) students.get(0);
				}
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("get By StudNum error", re);
				entityManager.clear();
				if(entityManager.getTransaction().isActive()){
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		
		return student;
	};

}
