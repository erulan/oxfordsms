package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.Book;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class Book.
 * 
 * @see kg.enesaitech.entity.Book
 * @author Hibernate Tools
 */
public class BookHome {

	private static final Logger log = Logger.getLogger(BookHome.class);
	private static BookHome bookHome = null;

	public static BookHome getInstance() {
		if (bookHome == null) {
			bookHome = new BookHome();
		}
		return bookHome;
	}

	public void persist(Book transientInstance) {
		log.debug("persisting Book instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(Book persistentInstance) {
		log.debug("removing Book instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Book merge(Book detachedInstance) {
		log.debug("merging Book instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Book result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Book findById(Integer id) {
		log.debug("getting Book instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Book instance = entityManager.find(Book.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from Book where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
		
		return true;
	}
	
	public SearchResult<Book> getList(SearchParameters searchParameters) {
		//Variable Decleration part
		SearchResult<Book> searchResult = new SearchResult<Book>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}
				
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<Book> books = query.getResultList();
				searchResult.setResultList(books);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return searchResult;
	}
	
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from Book b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if (!searchParameters.getSearchParameters().isEmpty()) {
			if (searchParameters.getSearchParameters().get("name") != null) {
				searchStringList.add(" upper(b.bookType.name) like :name ");
				queryParamIsString.put("name", true);
			}
			if (searchParameters.getSearchParameters().get("barcode") != null) {
				searchStringList.add(" upper(b.barcode) like :barcode ");
				queryParamIsString.put("barcode", true);
			}
			if (searchParameters.getSearchParameters().get("invenNo") != null) {
				searchStringList.add(" cast(b.invenNo as string) like :invenNo ");
				queryParamIsString.put("invenNo", true);
			}
			
			if (searchParameters.getSearchParameters().get("author") != null) {
				searchStringList.add(" upper(b.bookType.author) like :author ");
				queryParamIsString.put("author", true);
			}
			
			if (searchParameters.getSearchParameters().get("categoryId") != null) {
				searchStringList.add(" cast(b.bookType.category.id as string) = :categoryId ");
				queryParamIsString.put("categoryId", false);
			}
			
			if (searchParameters.getSearchParameters().get("bookTypeId") != null) {
				searchStringList.add(" cast(b.bookType.id as string) = :bookTypeId ");
				queryParamIsString.put("bookTypeId", false);
			}
			
			if (searchParameters.getSearchParameters().get("bookStatusId") != null) {
				searchStringList.add(" cast(b.bookStatus.id as string) = :bookStatusId ");
				queryParamIsString.put("bookStatusId", false);
			}
			
			if (searchParameters.getSearchParameters().get("notBookStatusId") != null) {
				searchStringList.add(" cast(b.bookStatus.id as string) <> :notBookStatusId ");
				queryParamIsString.put("notBookStatusId", false);
			}
			
			if (searchParameters.getSearchParameters().get("categoryName") != null) {
				searchStringList.add(" upper(b.bookType.category.name) like :categoryName ");
				queryParamIsString.put("categoryName", true);
			}
			if(searchParameters.getSearchParameters().get("bookStatus") != null){
				searchStringList.add(" upper(b.bookStatus.name) like :bookStatus ");
				queryParamIsString.put("bookStatus", true);
			}
			if(searchParameters.getSearchParameters().get("publishYear") != null){
				searchStringList.add(" cast(b.bookType.publishYear as string) like :publishYear ");
				queryParamIsString.put("publishYear", true);
			}
			if(searchParameters.getSearchParameters().get("cost") != null){
				searchStringList.add(" cast(b.bookType.cost as string) like :cost ");
				queryParamIsString.put("cost", true);
			}
			
			if (searchStringList.size() > 0) {
				searchString += " where ";
			}
			
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if (!searchParameters.getOrderParamDesc().isEmpty()) {
			if (searchParameters.getOrderParamDesc().get("name") != null) {
				orderList.add(" bookType.name " + (searchParameters.getOrderParamDesc().get("name") ? " DESC " : " ASC "));
			}
			
			if (searchParameters.getOrderParamDesc().get("author") != null) {
				orderList.add(" bookType.author " + (searchParameters.getOrderParamDesc().get("author") ? " DESC " : " ASC "));
			}
			
			if (searchParameters.getOrderParamDesc().get("categoryId") != null) {
				orderList.add(" bookType.category.id " + (searchParameters.getOrderParamDesc().get("categoryId") ? " DESC " : " ASC "));
			}
			
			if (searchParameters.getOrderParamDesc().get("categoryName") != null) {
				orderList.add(" bookType.category.name " + (searchParameters.getOrderParamDesc().get("categoryName") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("publishYear") != null){
				orderList.add(" bookType.publishYear " + (searchParameters.getOrderParamDesc().get("publishYear") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("cost") != null){
				orderList.add(" bookType.cost " + (searchParameters.getOrderParamDesc().get("cost") ? " DESC " : " ASC "));
			}
			
			if (orderList.size() > 0) {
				searchString += " order by ";
			}
			
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	}; 
}
