package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.StudAccounting;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class StudAccounting.
 * @see kg.enesaitech.entity.StudAccounting
 * @author Hibernate Tools	
 */
public class StudAccountingHome {

	private static final Logger log = Logger.getLogger(StudAccountingHome.class);
	private static StudAccountingHome studAccountingHome = null;

	public static StudAccountingHome getInstance(){
		if(studAccountingHome == null){
			studAccountingHome = new StudAccountingHome();
		}
		return studAccountingHome;
	}

	public void persist(StudAccounting transientInstance) {
		log.debug("persisting StudAccounting instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(StudAccounting persistentInstance) {
		log.debug("removing StudAccounting instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public StudAccounting merge(StudAccounting detachedInstance) {
		log.debug("merging StudAccounting instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				StudAccounting result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public StudAccounting findById(Integer id) {
		log.debug("getting StudAccounting instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				StudAccounting instance = entityManager.find(StudAccounting.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	
	public StudAccounting findByStudentIdYear(Integer studId, Integer yearId) {
		log.debug(" from StudAccounting where student.id: " + studId);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager.createQuery("from StudAccounting where student.id = :studId and year.id = :yearId");
				query.setParameter("studId", studId);
				query.setParameter("yearId", yearId);
				List<StudAccounting> stList = query.getResultList();
				StudAccounting instance = stList.size() > 0 ? stList.get(0) : null;
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from StudAccounting where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;			
			}
		}finally{entityManager.close();}
		
		return true;
	}
	public SearchResult<StudAccounting> getList(SearchParameters searchParameters) {

		//Variable Decleration part
		SearchResult<StudAccounting> searchResult = new SearchResult<StudAccounting>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}

				
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<StudAccounting> studAccountings = 
						query.getResultList();
				searchResult.setResultList(studAccountings);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}

		}finally{entityManager.close();}
		
		return searchResult;
	}
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from StudAccounting b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){
			if(searchParameters.getSearchParameters().get("studentName") != null){
				searchStringList.add(" (upper(b.student.nameEn) like :studentName OR upper(b.student.nameRu) like :studentName ) ");
				queryParamIsString.put("studentName", true);
			}
			if(searchParameters.getSearchParameters().get("anualFee") != null){
				searchStringList.add(" (cast(b.anualFee as string) = :anualFee) ");
				queryParamIsString.put("anualFee", false);
			}
			if(searchParameters.getSearchParameters().get("payed") != null){
				searchStringList.add(" (cast(b.payed as string) = :payed) ");
				queryParamIsString.put("payed", false);
			}
			if(searchParameters.getSearchParameters().get("left") != null){
				searchStringList.add(" (cast((b.anualFee - b.payed) as string) = :left) ");
				queryParamIsString.put("left", false);
			}
			

			if (searchParameters.getSearchParameters().get("class") != null) {
				searchStringList
						.add(" concat(b.student.CGroup.grade.grade, '', b.student.CGroup.classLetter.letter) like upper(:class) ");
				queryParamIsString.put("class", true);
			}
			if (searchParameters.getSearchParameters().get("student") != null) {
				searchStringList
						.add(" (upper(b.student.nameEn) like :student OR upper(b.student.nameRu) like :student OR upper(b.student.nameRu) like :student OR upper(b.student.middleNameRu) like :student "
								+ " OR upper(b.student.surnameEn) like :student OR upper(b.student.surnameRu) like :student "
								+ " OR concat(upper(b.student.nameEn), ' ', upper(b.student.surnameEn)) like :student "
								+ " OR concat(upper(b.student.nameRu), ' ', upper(b.student.surnameRu)) like :student )  ");
				queryParamIsString.put("student", true);
			}
			if(searchParameters.getSearchParameters().get("studentId") != null){
				searchStringList.add(" cast(student.id as string) = :studentId ");
				queryParamIsString.put("studentId", false);
			}
			if(searchParameters.getSearchParameters().get("cGroupId") != null){
				searchStringList.add(" cast(student.CGroup.id as string) = :cGroupId ");
				queryParamIsString.put("cGroupId", false);
			}
			if(searchParameters.getSearchParameters().get("schoolTypeId") != null){
				searchStringList.add(" cast(student.CGroup.grade.schoolType.id as string) = :schoolTypeId ");
				queryParamIsString.put("schoolTypeId", false);
			}
			if(searchParameters.getSearchParameters().get("yearId") != null){
				searchStringList.add(" cast(year.id as string) = :yearId ");
				queryParamIsString.put("yearId", false);
			}
			if(searchParameters.getSearchParameters().get("status") != null){
				searchStringList.add(" cast(b.student.status.id as string) = :status ");
				queryParamIsString.put("status", false);
			}
			if(searchParameters.getSearchParameters().get("isPaid") != null){
				searchStringList.add(" b.anualFee-b.payed  " + (Boolean.parseBoolean(searchParameters.getSearchParameters().get("isPaid")) ? " <= 0 " : " > 0 "));
			}
			
			if(searchStringList.size() > 0){
				searchString += " where ";
			}	
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			

			
			if(searchParameters.getOrderParamDesc().get("studentName") != null){
				orderList.add(" b.student.nameEn " + (searchParameters.getOrderParamDesc().get("studentName") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("studentSurname") != null){
				orderList.add(" b.student.surnameEn " + (searchParameters.getOrderParamDesc().get("studentSurname") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("year") != null){
				orderList.add(" b.y " + (searchParameters.getOrderParamDesc().get("year") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("anualFee") != null){
				orderList.add(" b.anualFee " + (searchParameters.getOrderParamDesc().get("anualFee") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("payed") != null){
				orderList.add(" b.payed " + (searchParameters.getOrderParamDesc().get("payed") ? " DESC " : " ASC "));
			}
			
			
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	};
}
