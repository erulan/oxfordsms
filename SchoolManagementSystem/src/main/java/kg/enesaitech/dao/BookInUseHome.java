package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.BookInUse;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class BookInUse.
 * 
 * @see kg.enesaitech.entity.BookInUse
 * @author Hibernate Tools
 */
public class BookInUseHome {

	private static final Logger log = Logger.getLogger(BookInUseHome.class);
	private static BookInUseHome bookInUseHome = null;

	public static BookInUseHome getInstance() {
		if (bookInUseHome == null) {
			bookInUseHome = new BookInUseHome();
		}
		return bookInUseHome;
	}

	public void persist(BookInUse transientInstance) {
		log.debug("persisting BookInUse instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(BookInUse persistentInstance) {
		log.debug("removing BookInUse instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public BookInUse merge(BookInUse detachedInstance) {
		log.debug("merging BookInUse instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				BookInUse result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public BookInUse findById(Integer id) {
		log.debug("getting BookInUse instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				BookInUse instance = entityManager.find(BookInUse.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from BookInUse where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
		return true;
	}
	public SearchResult<BookInUse> getList(SearchParameters searchParameters) {
		//Variable Decleration part
		
		SearchResult<BookInUse> searchResult = new SearchResult<BookInUse>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<BookInUse> bookInUses = 
						query.getResultList();
				searchResult.setResultList(bookInUses);
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return searchResult;
	}
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from BookInUse b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){
			if(searchParameters.getSearchParameters().get("bookName") != null){
				searchStringList.add(" upper(b.book.bookType.name) like :bookName ");
				queryParamIsString.put("bookName", true);
			}
			if(searchParameters.getSearchParameters().get("bookAuthor") != null){
				searchStringList.add(" upper(b.book.bookType.author) like :bookAuthor ");
				queryParamIsString.put("bookAuthor", true);
			}
			if(searchParameters.getSearchParameters().get("studentName") != null){
				searchStringList.add(" (upper(b.student.nameEn) like :studentName OR upper(b.student.nameRu) like :studentName ) ");
				queryParamIsString.put("studentName", true);
			}
			if(searchParameters.getSearchParameters().get("studentSurname") != null){
				searchStringList.add(" (upper(b.student.surnameEn) like :studentSurname OR upper(b.student.surnameRu) like :studentSurname ) ");
				queryParamIsString.put("studentSurname", true);
			}
			if(searchParameters.getSearchParameters().get("teacherName") != null){
				searchStringList.add(" (upper(b.teacher.nameEn) like :teacherName OR upper(b.teacher.nameRu) like :teacherName ) ");
				queryParamIsString.put("teacherName", true);
			}
			if(searchParameters.getSearchParameters().get("teacherSurname") != null){
				searchStringList.add(" (upper(b.teacher.surnameEn) like :teacherSurname OR upper(b.teacher.surnameRu) like :teacherSurname ) ");
				queryParamIsString.put("teacherSurname", true);
			}
			if(searchParameters.getSearchParameters().get("stuffName") != null){
				searchStringList.add(" (upper(b.stuff.nameEn) like :stuffName OR upper(b.stuff.nameRu) like :stuffName ) ");
				queryParamIsString.put("stuffName", true);
			}
			if(searchParameters.getSearchParameters().get("stuffSurname") != null){
				searchStringList.add(" (upper(b.stuff.surnameEn) like :stuffSurname OR upper(b.stuff.surnameRu) like :stuffSurname ) ");
				queryParamIsString.put("stuffSurname", true);
			}
			if(searchParameters.getSearchParameters().get("stuffNum") != null){
				searchStringList.add(" upper(b.stuff.stuffNum) like :stuffNum ");
				queryParamIsString.put("stuffNum", true);
			}
			if(searchParameters.getSearchParameters().get("studentNum") != null){
				searchStringList.add(" upper(b.student.studentNum) like :studentNum ");
				queryParamIsString.put("studentNum", true);
			}
			if(searchParameters.getSearchParameters().get("teacherNum") != null){
				searchStringList.add(" upper(b.teacher.teacherNum) like :teacherNum ");
				queryParamIsString.put("teacherNum", true);
			}
			if(searchParameters.getSearchParameters().get("stuffId") != null){
				searchStringList.add(" cast(b.stuff.id as string) = :stuffId ");
				queryParamIsString.put("stuffId", false);
			}
			if(searchParameters.getSearchParameters().get("studentId") != null){
				searchStringList.add(" cast(b.student.id as string) = :studentId ");
				queryParamIsString.put("studentId", false);
			}
			if(searchParameters.getSearchParameters().get("teacherId") != null){
				searchStringList.add(" cast(b.teacher.id as string) = :teacherId ");
				queryParamIsString.put("teacherId", false);
			}
			

			
			if(searchStringList.size() > 0){
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			

			
			if(searchParameters.getOrderParamDesc().get("studentName") != null){
				orderList.add(" b.student.nameEn " + (searchParameters.getOrderParamDesc().get("studentName") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("studentSurname") != null){
				orderList.add(" b.student.surnameEn " + (searchParameters.getOrderParamDesc().get("studentSurname") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("bookName") != null){
				orderList.add(" b.book.bookType.name " + (searchParameters.getOrderParamDesc().get("bookName") ? " DESC " : " ASC "));
			}
			
			if(searchParameters.getOrderParamDesc().get("bookAuthor") != null){
				orderList.add(" b.book.bookType.author " + (searchParameters.getOrderParamDesc().get("bookAuthor") ? " DESC " : " ASC "));
			}
			
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	};
}
