package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.Users;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;


/**
 * Home object for domain model class Users.
 * 
 * @see kg.enesaitech.entity.Users
 * @author Hibernate Tools
 */
public class UsersHome {

	private static final Logger log = Logger.getLogger(UsersHome.class);
	private static UsersHome usersHome = null;

	public static UsersHome getInstance() {
		if (usersHome == null) {
			usersHome = new UsersHome();
		}
		return usersHome;
	}


	public Integer persist(Users transientInstance) throws IOException, NoSuchAlgorithmException {
		
		Integer usersId = null;
		log.debug("persisting Users instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				
				transientInstance.setUserPass(getMD5(transientInstance.getUserPass()));
				
				entityManager.persist(transientInstance);
				entityManager.flush();
				usersId = transientInstance.getId();
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
		
		return usersId;
	}

	public void remove(Users persistentInstance) {
		log.debug("removing Users instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Users merge(Users detachedInstance) throws NoSuchAlgorithmException {
		log.debug("merging Users instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				detachedInstance.setUserPass(getMD5(detachedInstance.getUserPass()));
				Users result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Users findById(int id) {
		log.debug("getting Users instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Users instance = entityManager.find(Users.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public boolean deleteById(int id) {
		boolean result = true;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("DELETE FROM Users WHERE id = :id");
				query.setParameter("id", id);

				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("Get failed", re);

				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}

				result = false;
				throw re;
			}
		}finally{entityManager.close();}
		

		return result;
	}

	public Users getByUserName(String string) {
		Users user = null;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				
				Query query = entityManager.createQuery("from Users where userName = :name");
				query.setParameter("name", string);
				user = (Users) query.getSingleResult();
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
			}
		}finally{entityManager.close();}
		
		return user;
	}
	
	public Users getByUsersRole(String string) {
		Users user = null;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				
				Query query = entityManager.createQuery("select u From Users u left join u.usersRoles ur left join ur.role r where r.name = :name");
				query.setParameter("name", string);
				user = (Users) query.getResultList().get(0);
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("get BY UsersRole Error", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
			}
		}finally{entityManager.close();}
		
		return user;
	}
	
	public List<Users> getList(SearchParameters searchParameters) {
		List<Users> result = null;
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}

				@SuppressWarnings("unchecked")
				List<Users> users = query.getResultList();
				result = users;
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}

		}finally{entityManager.close();}
		
		return result;
	}
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
//		String searchString = "from Users b Left Join on Users.userName=Stuff.stuffNum ";
		String searchString = "from Users b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){
			if(searchParameters.getSearchParameters().get("userName") != null){
				searchStringList.add(" upper(b.userName) like :userName ");
				queryParamIsString.put("userName", true);
			}
			if(searchParameters.getSearchParameters().get("status") != null){
				searchStringList.add(" upper(b.status) like :status ");
				queryParamIsString.put("status", true);
			}
						
			if(searchStringList.size() > 0){
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			
			
			if(searchParameters.getOrderParamDesc().get("userName") != null){
				orderList.add(" userName " + (searchParameters.getOrderParamDesc().get("userName") ? " DESC " : " ASC "));
			}
			
			
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	};
	
	
	public String getMD5(String password) throws NoSuchAlgorithmException{
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(password.getBytes());
		byte[] digest = md.digest();
		StringBuffer sb = new StringBuffer();
		for (byte b : digest) {
			sb.append(String.format("%02x", b & 0xff));
		}

		return sb.toString();
	}


}