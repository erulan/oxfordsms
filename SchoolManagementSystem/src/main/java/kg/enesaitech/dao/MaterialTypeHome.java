package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.MaterialType;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class Category.
 * 
 * @see kg.enesaitech.entity.Category
 * @author Hibernate Tools
 */
public class MaterialTypeHome {

	private static final Logger log = Logger.getLogger(MaterialTypeHome.class);
	private static MaterialTypeHome materialTypeHome = null;

	public static MaterialTypeHome getInstance() {
		if (materialTypeHome == null) {
			materialTypeHome = new MaterialTypeHome();
		}
		return materialTypeHome;
	}

	public void persist(MaterialType transientInstance) {
		log.debug("persisting MaterialType instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(MaterialType persistentInstance) {
		log.debug("removing MaterialType instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public MaterialType merge(MaterialType detachedInstance) {
		log.debug("merging MaterialType instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				MaterialType result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public MaterialType findById(Integer id) {
		log.debug("getting MaterialType instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				MaterialType instance = entityManager.find(MaterialType.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from MaterialType where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
		return true;
	}
	
	public SearchResult<MaterialType> getList(SearchParameters searchParameters) {
		//Variable Decleration part
		SearchResult<MaterialType> searchResult = new SearchResult<MaterialType>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}
				
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<MaterialType> materialCategories = query.getResultList();
				searchResult.setResultList(materialCategories);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return searchResult;
	}
	
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from MaterialType b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if (!searchParameters.getSearchParameters().isEmpty()) {
			if (searchParameters.getSearchParameters().get("name") != null) {
				searchStringList.add(" upper(b.name) like :name ");
				queryParamIsString.put("name", true);
			}
			if (searchParameters.getSearchParameters().get("description") != null) {
				searchStringList.add(" upper(b.description) like :description ");
				queryParamIsString.put("description", true);
			}
			if (searchParameters.getSearchParameters().get("materialCategory") != null) {
				searchStringList.add(" upper(b.materialCategory.name) like :materialCategory ");
				queryParamIsString.put("materialCategory", true);
			}
			if(searchParameters.getSearchParameters().get("categoryId") != null){
				searchStringList.add(" cast(b.materialCategory.id as string) = :categoryId ");
				queryParamIsString.put("categoryId", false);
			}
			
			if (searchStringList.size() > 0) {
				searchString += " where ";
			}
			
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if (!searchParameters.getOrderParamDesc().isEmpty()) {
			if (searchParameters.getOrderParamDesc().get("name") != null) {
				orderList.add(" b.name " + (searchParameters.getOrderParamDesc().get("name") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("materialCategory") != null) {
				orderList.add(" b.materialCategory.name " + (searchParameters.getOrderParamDesc().get("materialCategory") ? " DESC " : " ASC "));
			}			
			
			
			if (orderList.size() > 0) {
				searchString += " order by ";
			}
			
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	}; 
}