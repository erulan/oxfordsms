package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.ExTeacher;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class ExTeacher.
 * @see kg.enesaitech.entity.ExTeacher
 * @author Hibernate Tools
 */
public class ExTeacherHome {

	private static final Logger log = Logger.getLogger(ExTeacherHome.class);
	private static ExTeacherHome exTeacherHome = null;

	public static ExTeacherHome getInstance(){
		if(exTeacherHome == null){
			exTeacherHome = new ExTeacherHome();
		}
		return exTeacherHome;
	}

	public void persist(ExTeacher transientInstance) {
		log.debug("persisting ExTeacher instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(ExTeacher persistentInstance) {
		log.debug("removing ExTeacher instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public ExTeacher merge(ExTeacher detachedInstance) {
		log.debug("merging ExTeacher instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				ExTeacher result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public ExTeacher findById(Integer id) {
		log.debug("getting ExTeacher instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				ExTeacher instance = entityManager.find(ExTeacher.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from ExTeacher where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;			
			}
		}finally{entityManager.close();}
		
		return true;
	}
	public List<ExTeacher> getList(SearchParameters searchParameters) {
		List<ExTeacher> result = null;
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}

				@SuppressWarnings("unchecked")
				List<ExTeacher> exTeachers = query.getResultList();
				result = exTeachers;

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
			}
		}finally{entityManager.close();}
		

		return result;
	}
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from ExTeacher b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){
			if(searchParameters.getSearchParameters().get("courseLesson") != null){
				searchStringList.add(" upper(b.course.lesson.name) like :courseLesson ");
				queryParamIsString.put("courseLesson", true);
			}
			if(searchParameters.getSearchParameters().get("courseCode") != null){
				searchStringList.add(" upper(b.course.code) like :courseCode ");
				queryParamIsString.put("courseCode", true);
			}
			if(searchParameters.getSearchParameters().get("teacherName") != null){
				searchStringList.add(" (upper(b.teacher.nameEn) like :teacherName OR upper(b.teacher.nameRu) like :teacherName ) ");
				queryParamIsString.put("teacherName", true);
			}
			if(searchParameters.getSearchParameters().get("teacherSurname") != null){
				searchStringList.add(" (upper(b.teacher.surnameEn) like :teacherSurname OR upper(b.teacher.surnameRu) like :teacherSurname ) ");
				queryParamIsString.put("teacherSurname", true);
			}

			
			if(searchStringList.size() > 0){
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			

			if(searchParameters.getOrderParamDesc().get("courseLesson") != null){
				orderList.add(" b.course.lesson.name " + (searchParameters.getOrderParamDesc().get("courseLesson") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("courseCode") != null){
				orderList.add(" b.course.code " + (searchParameters.getOrderParamDesc().get("courseCode") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("teacherName") != null){
				orderList.add(" b.teacher.nameEn " + (searchParameters.getOrderParamDesc().get("teacherName") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("teacherSurname") != null){
				orderList.add(" b.teacher.surnameEn " + (searchParameters.getOrderParamDesc().get("teacherSurname") ? " DESC " : " ASC "));
			}
			
			
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	};
}
