package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.NoteAttendance;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class NoteAttendance.
 * 
 * @see kg.enesaitech.entity.NoteAttendance
 * @author Hibernate Tools
 */
public class NoteAttendanceHome {

	private static final Logger log = Logger.getLogger(NoteAttendanceHome.class);
	private static NoteAttendanceHome noteAttendanceHome = null;

	public static NoteAttendanceHome getInstance() {
		if (noteAttendanceHome == null) {
			noteAttendanceHome = new NoteAttendanceHome();
		}
		return noteAttendanceHome;
	}

	public void persist(NoteAttendance transientInstance) {
		log.debug("persisting NoteAttendance instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
	
	}

	public void remove(NoteAttendance persistentInstance) {
		log.debug("removing NoteAttendance instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public NoteAttendance merge(NoteAttendance detachedInstance) {
		log.debug("merging NoteAttendance instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				NoteAttendance result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public NoteAttendance findById(Integer id) {
		log.debug("getting NoteAttendance instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				NoteAttendance instance = entityManager.find(NoteAttendance.class,
						id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from NoteAttendance where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
		return true;
	}

	public SearchResult<NoteAttendance> getList(SearchParameters searchParameters) {

		//Variable Decleration part
		SearchResult<NoteAttendance> searchResult = new SearchResult<NoteAttendance>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		String ss = "from NoteAttendance b LEFT JOIN fetch b.student s ";
		String ssCount = "from NoteAttendance b Left join b.student s ";
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString, ss);
		String searchStringCount = prepareQueryString(searchParameters, queryParamIsString, ssCount);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchStringCount);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}

				
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<NoteAttendance> noteAttendances = 
						query.getResultList();
				searchResult.setResultList(noteAttendances);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return searchResult;
	}
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString, String searchString){
		
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){
			if(searchParameters.getSearchParameters().get("studentName") != null){
				searchStringList.add(" (upper(b.student.nameEn) like :studentName OR upper(b.student.nameRu) like :studentName ) ");
				queryParamIsString.put("studentName", true);
			}
			if(searchParameters.getSearchParameters().get("studentSurname") != null){
				searchStringList.add(" (upper(b.student.surnameEn) like :studentSurname OR upper(b.student.surnameRu) like :studentSurname ) ");
				queryParamIsString.put("studentSurname", true);
			}
			if(searchParameters.getSearchParameters().get("studentId") != null){
				searchStringList.add(" cast(b.student.id as string) = :studentId ");
				queryParamIsString.put("studentId", false);
			}
			if(searchParameters.getSearchParameters().get("cGroupId") != null){
				searchStringList.add(" cast(b.student.CGroup.id as string) = :cGroupId ");
				queryParamIsString.put("cGroupId", false);
			}
			if(searchParameters.getSearchParameters().get("syllabusId") != null){
				searchStringList.add(" cast(b.syllabus.syllabusCategory.id as string) = :syllabusId ");
				queryParamIsString.put("syllabusId", false);
			}
			if(searchParameters.getSearchParameters().get("courseId") != null){
				searchStringList.add(" cast(b.syllabus.syllabusCategory.course.id as string) = :courseId ");
				queryParamIsString.put("courseId", false);
			}
			if(searchParameters.getSearchParameters().get("yearId") != null){
				searchStringList.add(" cast(b.syllabus.syllabusCategory.course.year.id as string) = :yearId ");
				queryParamIsString.put("yearId", false);
			}
			if(searchParameters.getSearchParameters().get("quarterId") != null){
				searchStringList.add(" cast(b.syllabus.syllabusCategory.quarter.id as string) = :quarterId ");
				queryParamIsString.put("quarterId", false);
			}
			if(searchParameters.getSearchParameters().get("noteType") != null){
				searchStringList.add(" upper(b.noteType.name) like :noteType ");
				queryParamIsString.put("noteType", false);
			}
			

			
			if(searchStringList.size() > 0){
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			

			if(searchParameters.getOrderParamDesc().get("courseLesson") != null){
				orderList.add(" b.syllabus.syllabusCategory.course.lesson.name " + (searchParameters.getOrderParamDesc().get("courseLesson") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("courseCode") != null){
				orderList.add(" b.syllabus.syllabusCategory.course.code " + (searchParameters.getOrderParamDesc().get("courseCode") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("studentName") != null){
				orderList.add(" b.student.nameEn " + (searchParameters.getOrderParamDesc().get("studentName") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("studentSurname") != null){
				orderList.add(" b.student.surnameEn " + (searchParameters.getOrderParamDesc().get("studentSurname") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("date") != null){
				orderList.add(" b.syllabus.syllabusCategory.date " + (searchParameters.getOrderParamDesc().get("date") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("studentCGroup") != null){
				orderList.add(" b.student.CGroup " + (searchParameters.getOrderParamDesc().get("studentCGroup") ? " DESC " : " ASC "));
			}
			
			
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	};
}
