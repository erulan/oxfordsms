package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.Year;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;

import org.apache.log4j.Logger;

/**
 * Home object for domain model class Year.
 * @see kg.enesaitech.entity.Year
 * @author Hibernate Tools
 */

public class YearHome {

	private static final Logger log = Logger.getLogger(YearHome.class);
	private static YearHome yearHome = null;

	
	public static YearHome getInstance(){
		if(yearHome == null){
			yearHome = new YearHome();
		}
		return yearHome;
	}
	public void persist(Year transientInstance) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		log.debug("persisting Year instance");
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				entityManager.close();
				throw re;
			}
		}finally{entityManager.close();}
		
		
	}

	public void remove(Year persistentInstance) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		log.debug("removing Year instance");
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.close();
				throw re;
			}
		}finally{entityManager.close();}
	
		entityManager.close();
	}

	public Year merge(Year detachedInstance) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		log.debug("merging Year instance");
		try{
			try {
				entityManager.getTransaction().begin();
				Year result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				entityManager.close();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Year findById(Integer id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		log.debug("getting Year instance with id: " + id);
		try{
			try {
				entityManager.getTransaction().begin();
				Year instance = entityManager.find(Year.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				entityManager.close();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	
	public Year getCurrentYear() {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		log.debug("getting Year instance ");
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager.createQuery("from Year where isCurrent = 1 ");
				Year instance = (Year) query.getSingleResult();
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				entityManager.close();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	
	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		boolean result = true;
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager.createQuery("DELETE FROM Year WHERE id = :id");
				query.setParameter("id", id);
				
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("Get failed", re);
				
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				
				result = false;
				entityManager.close();
				throw re;
			}
		}finally{entityManager.close();}
		
		entityManager.close();
		return result;
	}
	public List<Year> getList(SearchParameters searchParameters) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		List<Year> result = null;
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery("FROM Year");

				@SuppressWarnings("unchecked")
				List<Year> years = query.getResultList();
				result = years;

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				entityManager.close();
			}
		}finally{entityManager.close();}		
		return result;
	}
}
