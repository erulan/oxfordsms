package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.Blood;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;

import org.apache.log4j.Logger;

/**
 * Home object for domain model class Blood.
 * 
 * @see kg.enesaitech.entity.Blood
 * @author Hibernate Tools
 */

public class BloodHome {

//	private static final Log log = LogFactory.getLog(BloodHome.class);
	private static final Logger log = Logger.getLogger(BloodHome.class);
	private static BloodHome bloodHome = null;

	public static BloodHome getInstance() {
		if (bloodHome == null) {
			bloodHome = new BloodHome();
		}
		return bloodHome;
	}

	public void persist(Blood transientInstance) {
		log.debug("persisting Blood instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				log.error("persist failed", re);
				throw re;
			}
		}finally{
			entityManager.close();
		}
		
	}

	public void remove(Blood persistentInstance) {
		log.debug("removing Blood instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Blood merge(Blood detachedInstance) {
		log.debug("merging Blood instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Blood result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Blood findById(Integer id) {
		log.debug("getting Blood instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Blood instance = entityManager.find(Blood.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public boolean deleteById(int id) {
		boolean result = true;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("DELETE FROM Blood WHERE id = :id");
				query.setParameter("id", id);

				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("Get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				result = false;
				throw re;
			}
		}finally{entityManager.close();}
		

		return result;
	}


	public List<Blood> getList(SearchParameters searchParameters) {
		List<Blood> result = null;

		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		String searchString = " from Blood b ";
		List<String> searchStringList = new ArrayList<String>();
		Map<String, String> queryParam = new HashMap<String, String>();

		if (searchParameters.getSearchParameters().get("code") != null) {
			searchStringList.add(" upper(b.code) like :code ");
			queryParam.put("code",
					searchParameters.getSearchParameters().get("code"));
		}

		if (searchStringList.size() > 0) {
			searchString += " where ";
		}

		for (int i = 0; i < searchStringList.size(); i++) {
			searchString += searchStringList.get(i);
			if (i + 1 != searchStringList.size()) {
				searchString += " and ";
			}
		}
		if (!searchParameters.getOrderParamDesc().isEmpty()) {
			searchString += " order by ";

			if (searchParameters.getOrderParamDesc().get("code") != null) {
				searchString += " code "
						+ (searchParameters.getOrderParamDesc().get("code") ? " DESC "
								: " ASC ");
			}

		}
		
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);

				for (Map.Entry<String, String> entry : queryParam.entrySet()) {
					query.setParameter(entry.getKey(), "%"
							+ entry.getValue().toUpperCase() + "%");
				}

				@SuppressWarnings("unchecked")
				List<Blood> bloods = query.getResultList();
				result = bloods;

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return result;
	}

}