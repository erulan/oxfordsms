package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.Region;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;

import org.apache.log4j.Logger;

/**
 * Home object for domain model class Region.
 * 
 * @see kg.enesaitech.entity.Region
 * @author Hibernate Tools
 */
public class RegionHome {

	private static final Logger log = Logger.getLogger(RegionHome.class);
	private static RegionHome regionHome = null;

	public static RegionHome getInstance() {
		if (regionHome == null) {
			regionHome = new RegionHome();
		}
		return regionHome;
	}

	public void persist(Region transientInstance) {
		log.debug("persisting Region instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(Region persistentInstance) {
		log.debug("removing Region instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Region merge(Region detachedInstance) {
		log.debug("merging Region instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Region result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Region findById(Integer id) {
		log.debug("getting Region instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Region instance = entityManager.find(Region.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public List<Region> getList(SearchParameters searchParameters) {
		List<Region> result = null;

		String searchString = " from Region b ";
		List<String> searchStringList = new ArrayList<String>();
		Map<String, String> queryParam = new HashMap<String, String>();
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		

		if (searchParameters.getSearchParameters().get("name") != null) {
			searchStringList.add(" upper(b.name) like :name ");
			queryParam.put("name",
					searchParameters.getSearchParameters().get("name"));
		}

		if (searchStringList.size() > 0) {
			searchString += " where ";
		}

		for (int i = 0; i < searchStringList.size(); i++) {
			searchString += searchStringList.get(i);
			if (i + 1 != searchStringList.size()) {
				searchString += " and ";
			}
		}
		if (!searchParameters.getOrderParamDesc().isEmpty()) {
			searchString += " order by ";

			if (searchParameters.getOrderParamDesc().get("name") != null) {
				searchString += " name "
						+ (searchParameters.getOrderParamDesc().get("name") ? " DESC "
								: " ASC ");
			}

		}
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);

				for (Map.Entry<String, String> entry : queryParam.entrySet()) {
					query.setParameter(entry.getKey(), "%"
							+ entry.getValue().toUpperCase() + "%");
				}

				@SuppressWarnings("unchecked")
				List<Region> regions = query.getResultList();
				result = regions;

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return result;
	}
}
