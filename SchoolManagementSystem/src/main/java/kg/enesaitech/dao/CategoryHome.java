package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.Category;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;

import org.apache.log4j.Logger;

/**
 * Home object for domain model class Category.
 * 
 * @see kg.enesaitech.entity.Category
 * @author Hibernate Tools
 */
public class CategoryHome {

	private static final Logger log = Logger.getLogger(CategoryHome.class);
	private static CategoryHome categoryHome = null;

	public static CategoryHome getInstance() {
		if (categoryHome == null) {
			categoryHome = new CategoryHome();
		}
		return categoryHome;
	}

	public void persist(Category transientInstance) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{
			entityManager.close();
		}
		
	}

	public void remove(Category persistentInstance) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("error", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{
			entityManager.close();
		}
	
	}

	public Category merge(Category detachedInstance) {
		log.debug("merging Category instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Category result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{
			entityManager.close();
		}
		
	}

	public Category findById(Integer id) {
		log.debug("getting Category instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Category instance = entityManager.find(Category.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{
			entityManager.close();
		}
		
	}

	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from Category where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{
			entityManager.close();
		}
		
		return true;
	}
	public List<Category> getList(SearchParameters searchParameters) {
		List<Category> result = null;
		String searchString = " from Category c ";
		List<String> searchStringList = new ArrayList<String>();
		Map<String, String> queryParam = new HashMap<String, String>();
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		
		
		if(searchParameters.getSearchParameters().get("name") != null){
			searchStringList.add(" upper(c.name) like :name ");
			queryParam.put("name", searchParameters.getSearchParameters().get("name"));
		}
	
		
		if(searchStringList.size() > 0){
			searchString += " where ";
		}
		
		for(int i = 0; i < searchStringList.size(); i++){
			searchString += searchStringList.get(i);
			if(i+1 != searchStringList.size()){
				searchString += " and ";
			}
		}
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			searchString += " order by ";
			
			if(searchParameters.getOrderParamDesc().get("name") != null){
				searchString += " name " + (searchParameters.getOrderParamDesc().get("name") ? " DESC " : " ASC ");
			}
			
		}

		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);

				for (Map.Entry<String, String> entry : queryParam.entrySet()) {
				    query.setParameter(entry.getKey(), "%" + entry.getValue().toUpperCase() + "%");
				}

				@SuppressWarnings("unchecked")
				List<Category> categories = query.getResultList();
				result = categories;

				
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{
			entityManager.close();
		}
		
		
		return result;
	}
}
