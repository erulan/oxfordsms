package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.Course;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class Course.
 * 
 * @see kg.enesaitech.entity.Course
 * @author Hibernate Tools
 */
public class CourseHome {

	private static final Logger log = Logger.getLogger(CourseHome.class);
	private static CourseHome courseHome = null;

	public static CourseHome getInstance() {
		if (courseHome == null) {
			courseHome = new CourseHome();
		}
		return courseHome;
	}

	public void persist(Course transientInstance) {
		log.debug("persisting Course instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(Course persistentInstance) {
		log.debug("removing Course instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Course merge(Course detachedInstance) {
		log.debug("merging Course instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Course result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Course findById(Integer id) {
		log.debug("getting Course instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Course instance = entityManager.find(Course.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from Course where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
		return true;
	}

	public SearchResult<Course> getList(SearchParameters searchParameters) {

		// Variable Decleration part
		
		SearchResult<Course> searchResult = new SearchResult<Course>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();

		String searchString = prepareQueryString(searchParameters,
				queryParamIsString);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		// Try part with query, Transaction start/close and catch part
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) "
						+ searchString);

				// Set Parameters after creating Query.
				// Parameters(queryParamIsString key->paramName, value->isString)
				// prepared in Prepare Parameters part of prepareQueryString
				// function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
					query.setParameter(
							varName.getKey(),
							varName.getValue() ? "%"
									+ searchParameters.getSearchParameters()
											.get(varName.getKey()).toUpperCase()
									+ "%" : searchParameters.getSearchParameters()
									.get(varName.getKey()));
					queryCount.setParameter(
							varName.getKey(),
							varName.getValue() ? "%"
									+ searchParameters.getSearchParameters()
											.get(varName.getKey()).toUpperCase()
									+ "%" : searchParameters.getSearchParameters()
									.get(varName.getKey()));
				}

				Integer totalData = ((Number) queryCount.getSingleResult())
						.intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<Course> courses = query.getResultList();
				searchResult.setResultList(courses);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}

		

		return searchResult;
	}

	private String prepareQueryString(SearchParameters searchParameters,
			Map<String, Boolean> queryParamIsString) {
		String searchString = " from Course c ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();

		// Prepare Parameters Part
		if (!searchParameters.getSearchParameters().isEmpty()) {
			if (searchParameters.getSearchParameters().get("cGroupId") != null) {
				searchStringList
						.add(" cast(c.CGroup.id as string) = :cGroupId ");
				queryParamIsString.put("cGroupId", false);
			}
			if (searchParameters.getSearchParameters().get("yearId") != null) {
				searchStringList.add(" cast(c.year.id as string) = :yearId ");
				queryParamIsString.put("yearId", false);
			}
			if (searchParameters.getSearchParameters().get("teacherId") != null) {
				searchStringList.add(" cast(c.teacher.id as string) = :teacherId ");
				queryParamIsString.put("teacherId", false);
			}
			if (searchParameters.getSearchParameters().get("lesson") != null) {
				searchStringList
						.add(" upper(c.lesson.name) like upper(:lesson) ");
				queryParamIsString.put("lesson", true);
			}
			if (searchParameters.getSearchParameters().get("class") != null) {
				searchStringList
						.add(" concat(c.CGroup.grade.grade, '', c.CGroup.classLetter.letter) like upper(:class) ");
				queryParamIsString.put("class", true);
			}

			if (searchParameters.getSearchParameters().get("teacherName") != null) {
				searchStringList
						.add(" (upper(c.teacher.nameEn) like :teacherName OR upper(c.teacher.nameRu) like :teacherName ) ");
				queryParamIsString.put("teacherName", true);
			}

			if (searchParameters.getSearchParameters().get("teacher") != null) {
				searchStringList
						.add(" (upper(c.teacher.nameEn) like :teacher OR upper(c.teacher.nameRu) like :teacher OR upper(c.teacher.nameRu) like :teacher "
								+ " OR upper(c.teacher.surnameEn) like :teacher OR upper(c.teacher.surnameRu) like :teacher "
								+ " OR concat(upper(c.teacher.nameEn), ' ', upper(c.teacher.surnameEn)) like :teacher )  ");

				queryParamIsString.put("teacher", true);
			}
			if (searchParameters.getSearchParameters().get("teacherSurname") != null) {
				searchStringList
						.add(" (upper(c.teacher.surnameEn) like :teacherSurname OR upper(c.teacher.surnameRu) like :teacherSurname ) ");

				queryParamIsString.put("teacherSurname", true);
			}
			if (searchParameters.getSearchParameters().get("code") != null) {
				searchStringList.add(" upper(c.code) like upper(:code) ");
				queryParamIsString.put("code", true);
			}
			if (searchParameters.getSearchParameters().get("bookName") != null) {
				searchStringList.add(" upper(c.book.name) like :bookName ");
				queryParamIsString.put("bookName", true);
			}
			if (searchParameters.getSearchParameters().get("bookAuthor") != null) {
				searchStringList.add(" upper(c.book.author) like :bookAuthor ");
				queryParamIsString.put("bookAuthor", true);
			}

			if (searchStringList.size() > 0) {
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}

		// Set order by part
		if (!searchParameters.getOrderParamDesc().isEmpty()) {

			if (searchParameters.getOrderParamDesc().get("cGroupId") != null) {
				orderList
						.add(" CGroup.id "
								+ (searchParameters.getOrderParamDesc().get(
										"cGroupId") ? " DESC " : " ASC "));
			}

			if (searchParameters.getOrderParamDesc().get("yearId") != null) {
				orderList
						.add(" year.id "
								+ (searchParameters.getOrderParamDesc().get(
										"yearId") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("lesson") != null) {
				orderList
						.add(" c.lesson.name "
								+ (searchParameters.getOrderParamDesc().get(
										"lesson") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("code") != null) {
				orderList
						.add(" c.code * 1 "
								+ (searchParameters.getOrderParamDesc().get(
										"code") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("teacherName") != null) {
				orderList.add(" c.teacher.nameEn "
						+ (searchParameters.getOrderParamDesc().get(
								"teacherName") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("teacherSurname") != null) {
				orderList.add(" c.teacher.surnameEn "
						+ (searchParameters.getOrderParamDesc().get(
								"teacherSurname") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("class") != null) {
				orderList
						.add(" c.CGroup.grade.grade "
								+ (searchParameters.getOrderParamDesc().get(
										"class") ? " DESC " : " ASC ")
								+ " , c.CGroup.classLetter.letter "
								+ (searchParameters.getOrderParamDesc().get(
										"class") ? " DESC " : " ASC "));
			}

			if (orderList.size() > 0) {
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}

		return searchString;
	};
}
