package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.Stuff;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class Stuff.
 * @see kg.enesaitech.entity.Stuff
 * @author Hibernate Tools
 */
public class StuffHome {

	private static final Logger log = Logger.getLogger(StuffHome.class);
	private static StuffHome stuffHome = null;

	public static StuffHome getInstance(){
		if(stuffHome == null){
			stuffHome = new StuffHome();
		}
		return stuffHome;
	}
	public void persist(Stuff transientInstance) {
		log.debug("persisting Stuff instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(Stuff persistentInstance) {
		log.debug("removing Stuff instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Stuff merge(Stuff detachedInstance) {
		log.debug("merging Stuff instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Stuff result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Stuff findById(Integer id) {
		log.debug("getting Stuff instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Stuff instance = entityManager.find(Stuff.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}
	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from Stuff where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;			
			}
		}finally{entityManager.close();}
		
		return true;
	}
	public SearchResult<Stuff> getList(SearchParameters searchParameters) {

		//Variable Decleration part
		SearchResult<Stuff> searchResult = new SearchResult<Stuff>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
		//Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}

				
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<Stuff> stuffs = 
						query.getResultList();
				searchResult.setResultList(stuffs);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return searchResult;
	}
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from Stuff b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){
			if(searchParameters.getSearchParameters().get("name") != null){
				searchStringList.add(" (upper(b.nameEn) like :name OR upper(b.nameRu) like :name ) ");
				queryParamIsString.put("name", true);
			}
			if (searchParameters.getSearchParameters().get("stuff") != null) {
				searchStringList
						.add(" (upper(b.nameEn) like :stuff OR upper(b.nameRu) like :stuff OR upper(b.nameRu) like :stuff "
								+ " OR upper(b.surnameEn) like :stuff OR upper(b.surnameRu) like :stuff "
								+ " OR concat(upper(b.nameEn), ' ', upper(b.surnameEn)) like :stuff "
								+ " OR concat(upper(b.nameRu), ' ', upper(b.surnameRu)) like :stuff )  ");
				queryParamIsString.put("stuff", true);
			}
			if(searchParameters.getSearchParameters().get("surname") != null){
				searchStringList.add(" (upper(b.surnameEn) like :surname OR upper(b.surnameRu) like :surname ) ");
				queryParamIsString.put("surname", true);
			}
			if(searchParameters.getSearchParameters().get("isMale") != null){
				searchStringList.add(" cast(isMale as string) = :isMale ");
				queryParamIsString.put("isMale", false);
			}
			if(searchParameters.getSearchParameters().get("stuffNum") != null){
				searchStringList.add(" cast(b.stuffNum as string) like :stuffNum ");
				queryParamIsString.put("stuffNum", true);
			}
			if(searchParameters.getSearchParameters().get("position") != null){
				searchStringList.add(" upper(b.position) like :position ");
				queryParamIsString.put("position", true);
			}
			if(searchParameters.getSearchParameters().get("status") != null){
				searchStringList.add(" cast(status.id as string) = :status ");
				queryParamIsString.put("status", false);
			}
			if(searchParameters.getSearchParameters().get("passportNo") != null){
				searchStringList.add(" upper(b.passportNo) like :passportNo ");
				queryParamIsString.put("passportNo", true);
			}
			if(searchParameters.getSearchParameters().get("education") != null){
				searchStringList.add(" upper(b.education) like :education ");
				queryParamIsString.put("education", true);
			}
			if(searchParameters.getSearchParameters().get("experience") != null){
				searchStringList.add(" upper(b.experience) like :experience");
				queryParamIsString.put("experience", true);
			}
			if(searchParameters.getSearchParameters().get("curradr") != null){
				searchStringList.add(" upper(b.curradr) like :curradr");
				queryParamIsString.put("curradr", true);
			}
			if(searchParameters.getSearchParameters().get("countryName") != null){
				searchStringList.add(" upper(b.country.name) like :countryName");
				queryParamIsString.put("countryName", true);
			}
				
				

			
			if(searchStringList.size() > 0){
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			
			
			
			if(searchParameters.getOrderParamDesc().get("name") != null){
				orderList.add(" b.nameEn " + (searchParameters.getOrderParamDesc().get("name") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("stuffNum") != null){
				orderList.add(" b.stuffNum " + (searchParameters.getOrderParamDesc().get("stuffNum") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("position") != null){
				orderList.add(" b.position " + (searchParameters.getOrderParamDesc().get("position") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("surname") != null){
				orderList.add(" b.surnameEn " + (searchParameters.getOrderParamDesc().get("surname") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("isMale") != null){
				orderList.add(" b.isMale " + (searchParameters.getOrderParamDesc().get("isMale") ? " DESC " : " ASC "));
			}
			if(searchParameters.getOrderParamDesc().get("countryName") != null){
				orderList.add(" b.country.name " + (searchParameters.getOrderParamDesc().get("countryName") ? " DESC " : " ASC "));
			}
			
			
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	}
	public Set<String> stuffNumList() {
		Set<String> stuffNumbers = new HashSet<String>();
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				
				stuffNumbers = new HashSet<String>(entityManager.createQuery(" select stuffNum from Stuff s ").getResultList());
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		
		return stuffNumbers;
	}
	
	public Stuff getByStuffNum(String stuffNum) {
		Stuff stuff = null;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				List<Stuff> stuffs = entityManager.createQuery("from Stuff s where s.stuffNum = :stuffNum").setParameter("stuffNum", stuffNum).getResultList();
				if(!stuffs.isEmpty()){
					stuff = (Stuff) stuffs.get(0);
				}
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("get by stuffNum error", re);
				entityManager.clear();
				if(entityManager.getTransaction().isActive()){
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		
		return stuff;
	};
}
