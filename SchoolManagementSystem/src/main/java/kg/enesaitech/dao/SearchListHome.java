
package kg.enesaitech.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.Blood;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;

import org.apache.log4j.Logger;



public class SearchListHome {
	private static final Logger log = Logger.getLogger(SearchListHome.class);
	public List<Blood> searchList(SearchParameters searchParameters) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		List<Blood> result = null;
		try{
			try {
				entityManager.getTransaction().begin();
				
//				Map<String,Object> params = new HashMap<String,Object>();
//				StringBuilder queryBuilder = new StringBuilder();
//				queryBuilder.append("SELECT b FROM Blood AS b");
				//queryBuilder.append("where myVar.number > :number and myVar.groupName = :name");
				//params.put("number",4);
				//params.put("name","supergroup");
				          
//				Query query = entityManager.createQuery(queryBuilder.toString());
				//for (Map.Entry<String,Object> param : params.entrySet()) {
				//   	query.setParameter(param.getKey(),param.getValue());
				//}
//				List<Blood> listOfBloods = (List<Blood>)query.getResultList();
//				result = listOfBloods;
				Query query = entityManager.createQuery("SELECT b FROM Blood AS b WHERE id >= :startFrom");
//				query.setParameter("objectType", "Blood");
				query.setParameter("startFrom", 1);
				
				@SuppressWarnings("unchecked")
				List<Blood> listOfBloods = query.getResultList();
				result = listOfBloods;
				
				entityManager.getTransaction().commit();
				entityManager.close();
			} catch (RuntimeException re) {
				log.error("list error", re);
				if(entityManager.getTransaction().isActive()){
					entityManager.getTransaction().rollback();
				}
				entityManager.close();
			}
			
		}finally{entityManager.close();}
		
		return result;
	}
}