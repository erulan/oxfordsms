package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.Material;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class Category.
 * 
 * @see kg.enesaitech.entity.Category
 * @author Hibernate Tools
 */
public class MaterialHome {

	private static final Logger log = Logger.getLogger(MaterialHome.class);
	private static MaterialHome materialHome = null;

	public static MaterialHome getInstance() {
		if (materialHome == null) {
			materialHome = new MaterialHome();
		}
		return materialHome;
	}

	public void persist(Material transientInstance) {
		log.debug("persisting Material instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(Material persistentInstance) {
		log.debug("removing Material instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Material merge(Material detachedInstance) {
		log.debug("merging MaterialType instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Material result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public Material findById(Integer id) {
		log.debug("getting Material instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Material instance = entityManager.find(Material.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public boolean deleteById(int id) {
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("delete from Material where id = :id");
				query.setParameter("id", id);
				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
		return true;
	}

	public SearchResult<Material> getList(SearchParameters searchParameters) {
		// Variable Decleration part
		SearchResult<Material> searchResult = new SearchResult<Material>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();

		String searchString = prepareQueryString(searchParameters,
				queryParamIsString);
		
		// Try part with query, Transaction start/close and catch part
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) "
						+ searchString);

				// Set Parameters after creating Query.
				// Parameters(queryParamIsString key->paramName, value->isString)
				// prepared in Prepare Parameters part of prepareQueryString
				// function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
					query.setParameter(
							varName.getKey(),
							varName.getValue() ? "%"
									+ searchParameters.getSearchParameters()
											.get(varName.getKey()).toUpperCase()
									+ "%" : searchParameters.getSearchParameters()
									.get(varName.getKey()));
					queryCount.setParameter(
							varName.getKey(),
							varName.getValue() ? "%"
									+ searchParameters.getSearchParameters()
											.get(varName.getKey()).toUpperCase()
									+ "%" : searchParameters.getSearchParameters()
									.get(varName.getKey()));
				}

				Integer totalData = ((Number) queryCount.getSingleResult())
						.intValue();
				searchResult.setTotalRecords(totalData);
				if (searchParameters.getStartIndex() != null) {
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if (searchParameters.getResultQuantity() != null) {
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				List<Material> materialCategories = query.getResultList();
				searchResult.setResultList(materialCategories);

				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();

				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		

		return searchResult;
	}

	private String prepareQueryString(SearchParameters searchParameters,
			Map<String, Boolean> queryParamIsString) {
		String searchString = " from Material b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();

		// Prepare Parameters Part
		if (!searchParameters.getSearchParameters().isEmpty()) {
//			if (searchParameters.getSearchParameters().get("owner") != null) {
//				String joiningTables = " inner join Teacher on b.ownerId = t.id and b.ownerType.id = 2 "
//						+ " inner join Stuff as s on b.ownerId = s.id and b.ownerType.id = 3 ";
//				searchStringList.add(joiningTables + " upper(Teacher.nameEn) like :owner  OR upper(s.nameEn) like :owner ");
//				queryParamIsString.put("owner", true);
//			}
			if (searchParameters.getSearchParameters().get("place") != null) {
				searchStringList.add(" upper(b.place.name) like :place ");
				queryParamIsString.put("place", true);
			}
			if (searchParameters.getSearchParameters().get("materialStatusId") != null) {
				searchStringList.add(" cast(b.materialStatus.id as string) = :materialStatusId ");
				queryParamIsString.put("materialStatusId", false);
			}
			if (searchParameters.getSearchParameters().get("materialType") != null) {
				searchStringList
						.add(" upper(b.materialType.name) like :materialType ");
				queryParamIsString.put("materialType", true);
			}
			if(searchParameters.getSearchParameters().get("materialTypeId") != null){
				searchStringList.add(" cast(b.materialType.id as string) = :materialTypeId ");
				queryParamIsString.put("materialTypeId", false);
			}
			if(searchParameters.getSearchParameters().get("registrationDate") != null){
				searchStringList.add(" cast(b.registrationDate.id as string) = :registrationDate ");
				queryParamIsString.put("registrationDate", false);
			}
			if (searchParameters.getSearchParameters().get("materialCategory") != null) {
				searchStringList
						.add(" upper(b.materialType.materialCategory.name) like :materialCategory ");
				queryParamIsString.put("materialCategory", true);
			}
			if (searchParameters.getSearchParameters().get("materialType") != null) {
				searchStringList
						.add(" upper(b.materialType.name) like :materialType");
				queryParamIsString.put("materialType", true);
			}
			if (searchParameters.getSearchParameters().get("ownerType") != null) {
				searchStringList
						.add(" upper(b.ownerType.name) like :ownerType");
				queryParamIsString.put("ownerType", true);
			}
			if (searchParameters.getSearchParameters().get("ownerName") != null) {
				searchStringList
						.add(" upper(b.fullname) like :ownerName");
				queryParamIsString.put("ownerName", true);
			}
			if (searchParameters.getSearchParameters().get("invenNo") != null) {
				searchStringList
						.add(" cast(b.invenNo as string) like :invenNo ");
				queryParamIsString.put("invenNo", true);
			}
			if(searchParameters.getSearchParameters().get("cost") != null){
				searchStringList.add(" cast(b.materialType.cost as string) like :cost ");
				queryParamIsString.put("cost", true);
			}
			

			if (searchStringList.size() > 0) {
				searchString += " where ";
			}

			searchString += StringUtils.join(searchStringList, " and ");
		}

		// Set order by part
		if (!searchParameters.getOrderParamDesc().isEmpty()) {
			if (searchParameters.getOrderParamDesc().get("place") != null) {
				orderList
						.add(" b.place.name "
								+ (searchParameters.getOrderParamDesc().get(
										"place") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("materialType") != null) {
				orderList
				.add(" b.materialType.name "
						+ (searchParameters.getOrderParamDesc().get(
								"materialType") ? " DESC " : " ASC "));
	}
			if (searchParameters.getOrderParamDesc().get("materialCategory") != null) {
				orderList.add(" b.materialType.materialCategory.name "
						+ (searchParameters.getOrderParamDesc().get(
								"materialCategory") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("teacher") != null) {
				orderList.add(" c.teacher.nameEn "
						+ (searchParameters.getOrderParamDesc().get(
								"teacher") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("stuff") != null) {
				orderList.add(" c.stuff.nameEn "
						+ (searchParameters.getOrderParamDesc().get(
								"stuff") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("registrationDate") != null) {
				orderList.add(" b.registrationDate "
						+ (searchParameters.getOrderParamDesc().get(
								"registrationDate") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("invenNo") != null) {
				orderList.add(" b.invenNo "
						+ (searchParameters.getOrderParamDesc().get(
								"invenNo") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("cost") != null) {
				orderList.add(" b.materialType.cost "
						+ (searchParameters.getOrderParamDesc().get(
								"cost") ? " DESC " : " ASC "));
			}

			if (orderList.size() > 0) {
				searchString += " order by ";
			}

			searchString += StringUtils.join(orderList, " , ");
		}

		return searchString;
	};
}
