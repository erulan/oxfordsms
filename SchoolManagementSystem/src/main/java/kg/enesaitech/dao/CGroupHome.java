package kg.enesaitech.dao;

// Generated Sep 9, 2014 9:24:48 PM by Hibernate Tools 4.3.1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import kg.enesaitech.entity.CGroup;
import kg.enesaitech.providers.EntityManagerProvider;
import kg.enesaitech.vo.SearchParameters;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Home object for domain model class CGroup.
 * 
 * @see kg.enesaitech.entity.CGroup
 * @author Hibernate Tools
 */
public class CGroupHome {

	private static final Logger log = Logger.getLogger(CGroupHome.class);
	private static CGroupHome cGroupHome = null;

	public static CGroupHome getInstance() {
		if (cGroupHome == null) {
			cGroupHome = new CGroupHome();
		}
		return cGroupHome;
	}

	public void persist(CGroup transientInstance) {
		log.debug("persisting CGroup instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(transientInstance);
				log.debug("persist successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(transientInstance)) {
					entityManager.detach(transientInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public void remove(CGroup persistentInstance) {
		log.debug("removing CGroup instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
				entityManager.getTransaction().commit();
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public CGroup merge(CGroup detachedInstance) {
		log.debug("merging CGroup instance");
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				CGroup result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				entityManager.getTransaction().commit();
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				if (entityManager.contains(detachedInstance)) {
					entityManager.detach(detachedInstance);
				}
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public CGroup findById(Integer id) {
		log.debug("getting CGroup instance with id: " + id);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				CGroup instance = entityManager.find(CGroup.class, id);
				log.debug("get successful");
				entityManager.getTransaction().commit();
				entityManager.clear();
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				throw re;
			}
		}finally{entityManager.close();}
		
	}

	public boolean deleteById(int id) {
		boolean result = true;
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();
				Query query = entityManager
						.createQuery("DELETE FROM CGroup WHERE id = :id");
				query.setParameter("id", id);

				int status = query.executeUpdate();
				entityManager.getTransaction().commit();
				System.out.println(status);
			} catch (RuntimeException re) {
				log.error("Get failed", re);
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				result = false;
				throw re;
			}
		}finally{entityManager.close();}
		

		return result;
	}
	public List<CGroup> getList(SearchParameters searchParameters) {
		List<CGroup> result = null;
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		try{
			try {
				entityManager.getTransaction().begin();

				Query query = entityManager.createQuery(searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}

				@SuppressWarnings("unchecked")
				List<CGroup> cGroups = query.getResultList();
				result = cGroups;
				entityManager.getTransaction().commit();
				entityManager.clear();
			} catch (RuntimeException re) {
				log.error("list error", re);
				entityManager.clear();
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}finally{entityManager.close();}
		
		//Try part with query, Transaction start/close and catch part
		

		return result;
	}
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " from CGroup b ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if(!searchParameters.getSearchParameters().isEmpty()){
			if(searchParameters.getSearchParameters().get("room") != null){
				searchStringList.add(" upper(b.room) like :room ");
				queryParamIsString.put("room", true);
			}
			if(searchParameters.getSearchParameters().get("gradeId") != null){
				searchStringList.add(" cast(grade.id as string) = :gradeId ");
				queryParamIsString.put("gradeId", false);
			}
			if(searchParameters.getSearchParameters().get("classLetterId") != null){
				searchStringList.add(" cast(classLetter.id as string) = :classLetterId ");
				queryParamIsString.put("classLetterId", false);
			}
			if(searchParameters.getSearchParameters().get("teacherName") != null){
				searchStringList.add(" (upper(b.teacher.nameEn) like :teacherName OR upper(b.teacher.nameRu) like :teacherName ) ");
				queryParamIsString.put("teacherName", true);
			}
			if(searchParameters.getSearchParameters().get("teacherSurname") != null){
				searchStringList.add(" (upper(b.teacher.surnameEn) like :teacherSurname OR upper(b.teacher.surnameRu) like :teacherSurname ) ");
				queryParamIsString.put("teacherSurname", true);
			}

			if(searchParameters.getSearchParameters().get("studentName") != null){
				searchStringList.add(" (upper(b.student.nameEn) like :studentName OR upper(b.student.nameRu) like :studentName ) ");
				queryParamIsString.put("studentName", true);
			}
			if(searchParameters.getSearchParameters().get("studentSurname") != null){
				searchStringList.add(" (upper(b.student.surnameEn) like :studentSurname OR upper(b.student.surnameRu) like :studentSurname ) ");
				queryParamIsString.put("studentSurname", true);
			}
			if(searchParameters.getSearchParameters().get("schoolTypeId") != null){
				searchStringList.add(" cast(grade.schoolType.id as string) = :schoolTypeId ");
				queryParamIsString.put("schoolTypeId", false);
			}
			
			if(searchStringList.size() > 0){
				searchString += " where ";
			}
			searchString += StringUtils.join(searchStringList, " and ");
		}
		
		//Set order by part
		if(!searchParameters.getOrderParamDesc().isEmpty()){
			
			if(searchParameters.getOrderParamDesc().get("gradeId") != null){
				orderList.add(" grade.id " + (searchParameters.getOrderParamDesc().get("gradeId") ? " DESC " : " ASC "));
			}
			
			if(searchParameters.getOrderParamDesc().get("classLetterId") != null){
				orderList.add(" classLetter.id " + (searchParameters.getOrderParamDesc().get("classLetterId") ? " DESC " : " ASC "));
			}
			
			if(searchParameters.getOrderParamDesc().get("grade") != null){
				orderList.add(" grade.grade " + (searchParameters.getOrderParamDesc().get("grade") ? " DESC " : " ASC "));
			}
			
			if(searchParameters.getOrderParamDesc().get("classLetter") != null){
				orderList.add(" classLetter.letter " + (searchParameters.getOrderParamDesc().get("classLetter") ? " DESC " : " ASC "));
			}
			
			if(orderList.size() > 0){
				searchString += " order by ";
			}
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	};
}
