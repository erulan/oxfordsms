package kg.enesaitech.entity;

// Generated Sep 9, 2014 9:24:47 PM by Hibernate Tools 4.3.1

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Lesson generated by hbm2java
 */
@Entity
@Table(name = "checkup")
public class Checkup implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "date", length = 60)
	private Year date;
	
	@Column(name = "weight", length = 100)
	private double weight;
	
	@Column(name = "height", length = 100)
	private double height;
	

	@Column(name = "weightFH", length = 100)
	private double weightFH;
	
	private String categoryH;
	private String categoryW;
	private String categoryWFH;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "student_id", nullable = false)
	private Student student;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "period_id", nullable = false)
	private CheckPeriod period;
	
	@Column(name = "weight_comment", length = 150)
	private String weightComment;
	
	@Column(name = "height_comment", length = 150)
	private String heightComment;
	
	@Column(name = "weightFH_comment", length = 150)
	private String weightFHComment;
	
	
	public Checkup() {
	}

	public Checkup(Year date, double height, double weight,double weightFH, Student student, String heightComment, String weightComment,String weightFHComment, CheckPeriod period) {
		this.date = date;
		this.height= height;
		this.weight = weight;
		this.weight = weightFH;
		this.student = student;
		this.weightComment = weightComment;
		this.heightComment = heightComment;
		this.weightComment = weightFHComment;
		this.period = period;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Year getDate() {
		return this.date;
	}

	public void setDate(Year date) {
		this.date = date;
	}
	
	public double getHeight(){
		return this.height;
	}
	
	public void setHeight(double height){
		this.height = height;
	}
	
	public double getWeight(){
		return this.weight;
	}
	
	public void setWeight(double weight){
		this.weight = weight;
	}
	
	public double getWeightFH(){
		return this.weightFH;
	}
	
	public void setWeightFH(double weightFH){
		this.weightFH = weightFH;
	}
	
	public Student getStudent(){
		return this.student;
	}
	
	public void setStudent(Student student){
		this.student = student;
	}
	
	public CheckPeriod getPeriod(){
		return this.period;
	}
	
	public void setPeriod(CheckPeriod period){
		this.period = period;
	}
	
	public String getCategoryH(){
		return this.categoryH;
	}
	
	public void setCategoryH(String categoryH){
		this.categoryH = categoryH;
	}
	
	public String getCategoryW(){
		return this.categoryW;
	}
	
	public void setCategoryW(String categoryW){
		this.categoryW = categoryW;
	}
	
	public String getCategoryWFH(){
		return this.categoryWFH;
	}
	
	public void setCategoryWFH(String categoryWFH){
		this.categoryWFH = categoryWFH;
	}
	
	public String getHeightComment(){
		return this.heightComment;
	}
	
	public void setHeightComment(String heightComment){
		this.heightComment = heightComment;
	}
	
	public String getWeightComment(){
		return this.weightComment;
	}
	
	public void setWeightComment(String weightComment){
		this.weightComment = weightComment;
	}
	
	public String getWeightFHComment(){
		return this.weightFHComment;
	}
	
	public void setWeightFHComment(String weightFHComment){
		this.weightFHComment = weightFHComment;
	}
}