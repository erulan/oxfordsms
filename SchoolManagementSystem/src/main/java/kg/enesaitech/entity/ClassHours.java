package kg.enesaitech.entity;

// Generated Sep 9, 2014 9:24:47 PM by Hibernate Tools 4.3.1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * ClassHours generated by hbm2java
 */
@Entity
@Table(name = "class_hours", uniqueConstraints = {
		@UniqueConstraint(columnNames = "order_num"),
		@UniqueConstraint(columnNames = "start_hour"),
		@UniqueConstraint(columnNames = "end_hour"),
		@UniqueConstraint(columnNames = "class_hour_type_id")  })
public class ClassHours implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "order_num", unique = true, nullable = false)
	private Integer orderNum;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "start_hour", unique = true, nullable = false, length = 0)
	private Date startHour;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "end_hour", unique = true, nullable = false, length = 0)
	private Date endHour;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "class_hour_type_id", nullable = false)
	private ClassHourType classHourType;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "classHours")
	private Set<Timetable> timetables = new HashSet<Timetable>(0);

	public ClassHours() {
	}

	public ClassHours(Integer id, Integer orderNum, Date startHour, Date endHour, ClassHourType classHourType) {
		this.id = id;
		this.orderNum = orderNum;
		this.startHour = startHour;
		this.endHour = endHour;
		this.classHourType = classHourType;
	}

	public ClassHours(Integer id, Integer orderNum, Date startHour, Date endHour,
			Set<Timetable> timetables, ClassHourType classHourType) {
		this.id = id;
		this.orderNum = orderNum;
		this.startHour = startHour;
		this.endHour = endHour;
		this.timetables = timetables;
		this.classHourType = classHourType;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public Integer getOrderNum() {
		return this.orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}


	public Date getStartHour() {
		return this.startHour;
	}

	public void setStartHour(Date startHour) {
		this.startHour = startHour;
	}


	public Date getEndHour() {
		return this.endHour;
	}

	public void setEndHour(Date endHour) {
		this.endHour = endHour;
	}


	public Set<Timetable> getTimetables() {
		return this.timetables;
	}

	public void setTimetables(Set<Timetable> timetables) {
		this.timetables = timetables;
	}

	public ClassHourType getClassHourType() {
		return classHourType;
	}

	public void setClassHourType(ClassHourType classHourType) {
		this.classHourType = classHourType;
	}

}
