package kg.enesaitech.providers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerProvider {
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("oxford");
	
	public static EntityManager getEntityManager(){
		return factory.createEntityManager();
	}
}
