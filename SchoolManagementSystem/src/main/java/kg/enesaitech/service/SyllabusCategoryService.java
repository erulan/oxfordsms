package kg.enesaitech.service;

import kg.enesaitech.dao.SyllabusCategoryHome;
import kg.enesaitech.entity.SyllabusCategory;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class SyllabusCategoryService {

	private static SyllabusCategoryService syllabusService = null;
	SyllabusCategoryHome syllabusCategoryHome = SyllabusCategoryHome.getInstance();

	public static SyllabusCategoryService getInstance() {
		if (syllabusService == null) {
			syllabusService = new SyllabusCategoryService();
		}
		return syllabusService;
	}

	public boolean create(SyllabusCategory syllabusCategory) {
		boolean result = true;

		try {
			syllabusCategoryHome.persist(syllabusCategory);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}
	public Integer createForExam(SyllabusCategory syllabusCategory) {
		Integer syllabusCategoryId = null;
		try {
			syllabusCategoryId = syllabusCategoryHome.persist(syllabusCategory);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return syllabusCategoryId;
	}

	public boolean update(SyllabusCategory syllabusCategory) {
		boolean result = true;

		try {

			syllabusCategoryHome.merge(syllabusCategory);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {

			syllabusCategoryHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public SyllabusCategory get(int id) {
		return syllabusCategoryHome.findById(id);
	}

	public SearchResult<SyllabusCategory> getList(SearchParameters searchParameters) {
		searchParameters.getSearchParameters().put("notQuarterId", 5+"");
		return syllabusCategoryHome.getList(searchParameters);
	}

	public SyllabusCategory findById(int id) {
		return syllabusCategoryHome.findById(id);
	}

}
