package kg.enesaitech.service;

import kg.enesaitech.dao.PlaceHome;
import kg.enesaitech.entity.Place;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

public class PlaceService {

	private static PlaceService placeService = null;
	Mapper mapper = DozerMapperProvider.getMapper();
	private PlaceHome placeHome = PlaceHome.getInstance();

	public static PlaceService getInstance() {
		if (placeService == null) {
			placeService = new PlaceService();
		}
		return placeService;
	}

	public boolean create(Place place) {
		boolean result = true;
		try {
			placeHome.persist(place);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(Place place) {
		boolean result = true;
		try {
			placeHome.merge(place);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		
		try {

			placeHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Place get(int id) {

		return placeHome.findById(id);
	}


	public SearchResult<Place> getList(SearchParameters searchParameters) {

		return placeHome.getList(searchParameters);
	}

	public Place findById(int id) {

		return placeHome.findById(id);
	}

}
