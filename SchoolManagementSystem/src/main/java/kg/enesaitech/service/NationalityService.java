package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.NationalityHome;
import kg.enesaitech.entity.Nationality;
import kg.enesaitech.vo.SearchParameters;

public class NationalityService {

	private static NationalityService nationalityService = null;
	private NationalityHome nationalityHome = NationalityHome.getInstance();

	public static NationalityService getInstance() {
		if (nationalityService == null) {
			nationalityService = new NationalityService();
		}
		return nationalityService;
	}

	public Nationality get(int id) {
		return nationalityHome.findById(id);
	}

	public List<Nationality> getList(SearchParameters searchParameters) {
		if (searchParameters.getOrderParamDesc().get("nationality") == null) {
			searchParameters.getOrderParamDesc().put("nationality", false);
		}
		return nationalityHome.getList(searchParameters);
	}

	public Nationality findById(int id) {
		return nationalityHome.findById(id);
	}

}
