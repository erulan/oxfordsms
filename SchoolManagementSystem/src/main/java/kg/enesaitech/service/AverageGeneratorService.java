package kg.enesaitech.service;

import kg.enesaitech.dao.NoteAttendanceHome;
import kg.enesaitech.entity.NoteAttendance;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

public class AverageGeneratorService {

	private static AverageGeneratorService averageGeneratorService = null;
	private NoteAttendanceHome noteAttendanceHome = NoteAttendanceHome
			.getInstance();
//	private NoteTypeHome noteTypeHome = NoteTypeHome.getInstance();
//	private SyllabusHome syllabusHome = SyllabusHome.getInstance();
//	private NoteSystemHome noteSystemHome = NoteSystemHome.getInstance();
//	private StudentHome studentHome = StudentHome.getInstance();
//	private CourseHome courseHome = CourseHome.getInstance();
//	private QuarterHome quarterHome = QuarterHome.getInstance();
//	private SyllabusCategoryHome syllabusCategoryHome = SyllabusCategoryHome.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	public static AverageGeneratorService getInstance() {
		if (averageGeneratorService == null) {
			averageGeneratorService = new AverageGeneratorService();
		}
		return averageGeneratorService;
	}

	public boolean create(NoteAttendance noteAttendance) {
		boolean result = true;
		try {
			noteAttendanceHome.persist(noteAttendance);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Double getAverageNotByStudent(Integer studentID, Integer quarterID, Integer yearID){
		return 0.0;
	}

	public Double getAverageNotByStudentByCourse(int studentID, int courseID, int quarterID, int yearID){		

		SearchParameters searchParameters = new SearchParameters();
		searchParameters.getSearchParameters().put("courseId",
				courseID + "");
		searchParameters.getSearchParameters().put("yearId",
				yearID + "");
		searchParameters.getSearchParameters().put("quarterId",
				quarterID + "");
		searchParameters.getSearchParameters().put("studentId",
				studentID + "");
		SearchResult<NoteAttendance> searchResult = noteAttendanceHome.getList(searchParameters);

		Double note = 0.0, totalWeight = 0.0;
		for (int n = 0; n < searchResult.getResultList().size(); n++) {
			if (searchResult.getResultList().get(n).getNote() != null) {
				System.out
						.println("note of the course: "
								+ searchResult.getResultList().get(n)
										.getNote());
				System.out.println("weight of the note: "
						+ searchResult.getResultList().get(n)
								.getNoteType().getWeight());
				int currNote;
				if (searchResult.getResultList().get(n).getNoteSystem()
						.getId() == 2) {
					currNote = to5System(searchResult.getResultList()
							.get(n).getNote());
					System.out.println("100duk note: "
							+ searchResult.getResultList().get(n)
									.getNote());
					System.out.println("converted to: " + currNote);

				} else {
					currNote = searchResult.getResultList().get(n)
							.getNote();
				}
				note = note
						+ (currNote * searchResult.getResultList()
								.get(n).getNoteType().getWeight());
				totalWeight = totalWeight
						+ searchResult.getResultList().get(n)
								.getNoteType().getWeight();
			}
			if (n == 0) {
				System.out.println("name of the course: "
						+ searchResult.getResultList().get(n)
								.getSyllabus().getSyllabusCategory()
								.getCourse().getLesson().getName());
			}
		}
		System.out.println("total note: " + note);
		System.out.println("total weight: " + totalWeight);
		if (totalWeight != 0) {
			note = note / totalWeight;
		} else {
			note = 0.0;
		}
		return note;	
	}
	

//	public SearchResult<QuarterNoteVO> getQuaterNote(
//			SearchParameters searchParameters) {
//
//		SearchResult<NoteAttendance> searchResult = noteAttendanceHome
//				.getList(searchParameters);
//		SearchResult<QuarterNoteVO> result = new SearchResult<QuarterNoteVO>();
//
//		ArrayList<Integer> courseIDs = new ArrayList<Integer>();
//		System.out.println("result list size: "
//				+ searchResult.getResultList().size());
//
//		for (int i = 0; i < searchResult.getResultList().size(); i++) {
//			int id = searchResult.getResultList().get(i).getSyllabus()
//					.getSyllabusCategory().getCourse().getId();
//			System.out.println("id: " + id);
//			if (!courseIDs.contains(id)) {
//				courseIDs.add(id);
//			}
//		}
//		for (int quarter = 1; quarter < 5; quarter++) {
//			System.out.println("** QUATER: " + quarter);
//
//			for (int i = 0; i < courseIDs.size(); i++) {
//				System.out.println("course ids: " + courseIDs.get(i));
//
//				searchParameters.getSearchParameters().put("courseId",
//						courseIDs.get(i) + "");
//				searchParameters.getSearchParameters().put("quarterId",
//						quarter + "");
//				searchResult = noteAttendanceHome.getList(searchParameters);
//
//				Double note = 0.0, totalWeight = 0.0;
//				String courseName = null;
//				for (int n = 0; n < searchResult.getResultList().size(); n++) {
//					if (searchResult.getResultList().get(n).getNote() != null) {
//						System.out
//								.println("note of the course: "
//										+ searchResult.getResultList().get(n)
//												.getNote());
//						System.out.println("weight of the note: "
//								+ searchResult.getResultList().get(n)
//										.getNoteType().getWeight());
//						int currNote;
//						if (searchResult.getResultList().get(n).getNoteSystem()
//								.getId() == 2) {
//							currNote = to5System(searchResult.getResultList()
//									.get(n).getNote());
//							System.out.println("100duk note: "
//									+ searchResult.getResultList().get(n)
//											.getNote());
//							System.out.println("converted to: " + currNote);
//
//						} else {
//							currNote = searchResult.getResultList().get(n)
//									.getNote();
//						}
//						note = note
//								+ (currNote * searchResult.getResultList()
//										.get(n).getNoteType().getWeight());
//						totalWeight = totalWeight
//								+ searchResult.getResultList().get(n)
//										.getNoteType().getWeight();
//					}
//					if (n == 0) {
//						System.out.println("name of the course: "
//								+ searchResult.getResultList().get(n)
//										.getSyllabus().getSyllabusCategory()
//										.getCourse().getLesson().getName());
//						courseName = searchResult.getResultList().get(n)
//								.getSyllabus().getSyllabusCategory()
//								.getCourse().getLesson().getName();
//					}
//				}
//				System.out.println("total note: " + note);
//				System.out.println("total weight: " + totalWeight);
//				if (totalWeight != 0) {
//					note = note / totalWeight;
//				} else {
//					note = 0.0;
//				}
//				System.out.println("result note: " + note);
//				if (quarter == 1) {
//					QuarterNoteVO quarterNoteVO = new QuarterNoteVO();
//					quarterNoteVO.setName(courseName);
//					quarterNoteVO.setQuarter1(note);
//					result.getResultList().add(quarterNoteVO);
//				} else if (quarter == 2) {
//					result.getResultList().get(i).setQuarter2(note);
//				} else if (quarter == 3) {
//					result.getResultList().get(i).setQuarter3(note);
//				} else if (quarter == 4) {
//					result.getResultList().get(i).setQuarter4(note);
//				}
//			}
//		}
//
//		return result;
//	}
//
//	public SearchResult<AverageNoteVO> getMonitoringByClass(
//			SearchParameters searchParameters) {
////		String quarterId = searchParameters.getSearchParameters().get("quarterId");
//		SearchResult<Course> searchCourseResult = courseHome
//				.getList(searchParameters);
//		searchParameters.getSearchParameters().put("cGroupId",
//				null);
//		SearchResult<AverageNoteVO> result = new SearchResult<AverageNoteVO>();
////		ArrayList<Integer> courseIDs = new ArrayList<Integer>();
//		System.out.println("course list size: "
//				+ searchCourseResult.getResultList().size());
//
////		for (int i = 0; i < searchResult.getResultList().size(); i++) {
////			int id = searchResult.getResultList().get(i).getSyllabus()
////					.getSyllabusCategory().getCourse().getId();
////			System.out.println("id: " + id);
////			if (!courseIDs.contains(id)) {
////				courseIDs.add(id);
////			}
////		}
//		for (int c = 0; c < searchCourseResult.getResultList().size(); c++) {
//			int id = searchCourseResult.getResultList().get(c).getId();
//			searchParameters.getSearchParameters().put("courseId", id + "");
//			SearchResult<Student> searchStudentResult = studentHome
//					.getList(searchParameters);
//			int allFives=0, allFours=0, allThrees= 0, allTwos=0;
//			for (int s = 0; s < searchStudentResult.getResultList().size(); s++) {
//				System.out.println("student ids: " + searchStudentResult.getResultList().get(s).getId());
//				int stud_id = searchStudentResult.getResultList().get(s).getId();
//				searchParameters.getSearchParameters().put("studentId", stud_id + "");
//				SearchResult<NoteAttendance> searchResult = noteAttendanceHome.getList(searchParameters);
//				Double note = 0.0, totalWeight = 0.0;
//				String courseName = null;
//				for (int n = 0; n < searchResult.getResultList().size(); n++) {
//					if (searchResult.getResultList().get(n).getNote() != null) {
//						System.out
//								.println("note of the course: "
//										+ searchResult.getResultList().get(n)
//												.getNote());
//						System.out.println("weight of the note: "
//								+ searchResult.getResultList().get(n)
//										.getNoteType().getWeight());
//						int currNote;
//						if (searchResult.getResultList().get(n).getNoteSystem()
//								.getId() == 2) {
//							currNote = to5System(searchResult.getResultList()
//									.get(n).getNote());
//							System.out.println("100duk note: "
//									+ searchResult.getResultList().get(n)
//											.getNote());
//							System.out.println("converted to: " + currNote);
//
//						} else {
//							currNote = searchResult.getResultList().get(n)
//									.getNote();
//						}
//						note = note
//								+ (currNote * searchResult.getResultList()
//										.get(n).getNoteType().getWeight());
//						totalWeight = totalWeight
//								+ searchResult.getResultList().get(n)
//										.getNoteType().getWeight();
//					}
//					if (n == 0) {
//						System.out.println("name of the course: "
//								+ searchResult.getResultList().get(n)
//										.getSyllabus().getSyllabusCategory()
//										.getCourse().getLesson().getName());
//						courseName = searchResult.getResultList().get(n)
//								.getSyllabus().getSyllabusCategory()
//								.getCourse().getLesson().getName();
//					}
//				}
//				System.out.println("total note: " + note);
//				System.out.println("total weight: " + totalWeight);
//				if (totalWeight != 0) {
//					note = note / totalWeight;
//				} else {
//					note = -1.0;
//				}
//				System.out.println("result note: " + note);
//				int roundedNote = note.intValue();
//				System.out.println("rounded esult note: " + roundedNote);
//				if(roundedNote!=-1){
//					if(roundedNote==5){
//						allFives++;
//					}else if(roundedNote==4){
//						allFours++;						
//					}else if(roundedNote==3){
//						allThrees++;						
//					}else if(roundedNote==2){
//						allTwos++;						
//					}
//				}
//			// note is average
//			}
//			int sumNumberOfStud = 5*allFives+4*allFours+3*allThrees+2*allTwos;
//			int sumNumberOfStud_4_5 = 5*allFives+4*allFours;
//			int total = allFives+allFours+allThrees+allTwos;
//			AverageNoteVO averageNoteVO = new AverageNoteVO();
//			CourseVO courseVO = new CourseVO();
//			LessonVO lessonVO = new LessonVO();
//			TeacherVO teacherVO = new TeacherVO();
//			teacherVO.setNameEn(searchCourseResult.getResultList().get(c).getTeacher().getNameEn());
//			teacherVO.setNameRu(searchCourseResult.getResultList().get(c).getTeacher().getNameRu());
//			teacherVO.setSurnameEn(searchCourseResult.getResultList().get(c).getTeacher().getSurnameEn());
//			teacherVO.setSurnameRu(searchCourseResult.getResultList().get(c).getTeacher().getSurnameRu());
//			lessonVO.setName(searchCourseResult.getResultList().get(c).getLesson().getName());
//			courseVO.setLesson(lessonVO);
//			courseVO.setTeacher(teacherVO);
//			averageNoteVO.setCourse(courseVO);
//			averageNoteVO.setFive(allFives);
//			averageNoteVO.setFour(allFours);
//			averageNoteVO.setThree(allThrees);
//			averageNoteVO.setTwo(allTwos);
//			if(total!=0){
//				averageNoteVO.setQuantity(((sumNumberOfStud/Double.parseDouble(total+""))*100/5));
//				averageNoteVO.setProgress(((sumNumberOfStud_4_5/Double.parseDouble(total+""))*100/5));
//			}else{
//				averageNoteVO.setQuantity(0.0);
//				averageNoteVO.setProgress(0.0);				
//			}
//			result.getResultList().add(averageNoteVO);
//		}
//		return result;
//	}
//
//	public SearchResult<QuarterNoteVO> getByCourses(
//			SearchParameters searchParameters) {
//
//		String courseId = searchParameters.getSearchParameters().get("courseId");
//		searchParameters.getSearchParameters().put("courseId",
//				null);
//		SearchResult<NoteAttendance> searchResult;
//		SearchResult<Student> searchStudentResult = studentHome
//				.getList(searchParameters);
//		SearchResult<QuarterNoteVO> result = new SearchResult<QuarterNoteVO>();
//		for (int i = 0; i < searchStudentResult.getResultList().size(); i++) {
//			int id = searchStudentResult.getResultList().get(i).getId();
//			searchParameters.getSearchParameters().put("studentId", id + "");
//			for (int quarter = 1; quarter < 5; quarter++) {
//				searchParameters.getSearchParameters().put("quarterId",
//						quarter + "");
//				searchParameters.getSearchParameters().put("courseId",
//						courseId);
//
//				searchResult = noteAttendanceHome.getList(searchParameters);
//
//				Double note = 0.0, totalWeight = 0.0;
//				for (int n = 0; n < searchResult.getResultList().size(); n++) {
//					if (searchResult.getResultList().get(n).getNote() != null) {
//						System.out
//								.println("note of the course: "
//										+ searchResult.getResultList().get(n)
//												.getNote());
//						System.out.println("weight of the note: "
//								+ searchResult.getResultList().get(n)
//										.getNoteType().getWeight());
//						int currNote;
//						if (searchResult.getResultList().get(n).getNoteSystem()
//								.getId() == 2) {
//							currNote = to5System(searchResult.getResultList()
//									.get(n).getNote());
//							System.out.println("100duk note: "
//									+ searchResult.getResultList().get(n)
//											.getNote());
//							System.out.println("converted to: " + currNote);
//
//						} else {
//							currNote = searchResult.getResultList().get(n)
//									.getNote();
//						}
//						note = note
//								+ (currNote * searchResult.getResultList()
//										.get(n).getNoteType().getWeight());
//						totalWeight = totalWeight
//								+ searchResult.getResultList().get(n)
//										.getNoteType().getWeight();
//					}
//					if (n == 0) {
//						System.out.println("name of the course: "
//								+ searchResult.getResultList().get(n)
//										.getSyllabus().getSyllabusCategory()
//										.getCourse().getLesson().getName());
//					}
//				}
//				System.out.println("total note: " + note);
//				System.out.println("total weight: " + totalWeight);
//				if (totalWeight != 0) {
//					note = note / totalWeight;
//				} else {
//					note = -1.0;
//				}
//				System.out.println("result note: " + note);
//				if (quarter == 1) {
//					QuarterNoteVO quarterNoteVO = new QuarterNoteVO();
//					quarterNoteVO.setName(searchStudentResult.getResultList()
//							.get(i).getNameEn()
//							+ " "
//							+ searchStudentResult.getResultList().get(i)
//									.getSurnameEn());
//					quarterNoteVO.setQuarter1(note);
//					quarterNoteVO.setCourse(false);
//					result.getResultList().add(quarterNoteVO);
//				} else if (quarter == 2) {
//					result.getResultList().get(i).setQuarter2(note);
//				} else if (quarter == 3) {
//					result.getResultList().get(i).setQuarter3(note);
//				} else if (quarter == 4) {
//					result.getResultList().get(i).setQuarter4(note);
//				}
//			}
//			System.out.println("**********working ************ ");			
//			searchParameters.getSearchParameters().put("quarterId",
//					5 + "");
//			searchParameters.getSearchParameters().put("courseId",
//					courseId);
//			searchResult = noteAttendanceHome.getList(searchParameters);			
//
//			for (int n = 0; n < searchResult.getResultList().size(); n++) {
//				if (searchResult.getResultList().get(n).getNote() != null) {
//					if(searchResult.getResultList().get(n).getNoteType().getName().equals("last year")){
//						result.getResultList().get(i).setLastYear(Double.parseDouble(""+searchResult.getResultList().get(n).getNote()));						
//					}
//					else if(searchResult.getResultList().get(n).getNoteType().getName().equals("entry exam")){
//						result.getResultList().get(i).setEntryExam(Double.parseDouble(""+searchResult.getResultList().get(n).getNote()));				
//					}
//					else if(searchResult.getResultList().get(n).getNoteType().getName().equals("GOS exam")){
//						result.getResultList().get(i).setGosExam(Double.parseDouble(""+searchResult.getResultList().get(n).getNote()));							
//					}
//				}
//			}
//			
//		}
//
//		return result;
//	}

	private int to5System(int note) {
		if (note >= 80)
			return 5;
		else if (note >= 60) {
			return 4;
		} else if (note >= 40) {
			return 3;
		} else {
			return 2;
		}
	}

}
