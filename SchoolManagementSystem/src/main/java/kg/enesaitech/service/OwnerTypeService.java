package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.OwnerTypeHome;
import kg.enesaitech.entity.OwnerType;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

public class OwnerTypeService {

	private static OwnerTypeService OwnerTypeService = null;

	Mapper mapper = DozerMapperProvider.getMapper();

	private OwnerTypeHome ownerTypeHome = OwnerTypeHome.getInstance();

	public static OwnerTypeService getInstance() {
		if (OwnerTypeService == null) {
			OwnerTypeService = new OwnerTypeService();
		}
		return OwnerTypeService;
	}

	public boolean create(OwnerType ownerType) {
		try {

			ownerTypeHome.persist(ownerType);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean update(OwnerType ownerType) {
		boolean result = true;
		try {
			ownerTypeHome.merge(ownerType);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		try {

			ownerTypeHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public OwnerType findById(int id) {
		return ownerTypeHome.findById(id);
	}

	public List<OwnerType> getList(SearchParameters searchParameters) {

		return ownerTypeHome.getList(searchParameters);
	}

	public OwnerType get(int id) {
		return ownerTypeHome.findById(id);
	}

}
