package kg.enesaitech.service;

import kg.enesaitech.dao.RoleHome;
import kg.enesaitech.dao.StuffHome;
import kg.enesaitech.dao.UsersHome;
import kg.enesaitech.dao.UsersRoleHome;
import kg.enesaitech.entity.Role;
import kg.enesaitech.entity.Stuff;
import kg.enesaitech.entity.Users;
import kg.enesaitech.entity.UsersRole;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.log4j.Logger;
import org.dozer.Mapper;

public class UsersRoleService {

	private static final Logger log = Logger.getLogger(UsersRoleService.class);
	private static UsersRoleService usersRoleService = null;
	private UsersRoleHome usersRoleHome = UsersRoleHome.getInstance();
	private UsersHome usersHome = UsersHome.getInstance();
	private StuffHome stuffHome = StuffHome.getInstance();
	private RoleHome roleHome = RoleHome.getInstance();

	Mapper mapper = DozerMapperProvider.getMapper();

	public static UsersRoleService getInstance() {
		if (usersRoleService == null) {
			usersRoleService = new UsersRoleService();
		}
		return usersRoleService;
	}

	public boolean create(UsersRole userRole) {
		boolean result = true;

		try {
			usersRoleHome.persist(userRole);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(UsersRole userRole) {
		boolean result = true;

		try {
			usersRoleHome.merge(userRole);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public UsersRole findById(int id) {
		return usersRoleHome.findById(id);
	}

	public boolean delete(int id) {
		boolean result = true;

		try {
			usersRoleHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean addStuffToUsers(int stuffId, int roleId) {
		boolean result = true;
		
		try {
			Stuff stuff=stuffHome.findById(stuffId);
			Role role = roleHome.findById(roleId);
			if (stuff==null){
				result=false;
			} else if(role==null||role.getName().toLowerCase().equals("admin")){
					result=false;				
			}
			else {
				UsersRole usersRole = new UsersRole();
				if(role.getName().toLowerCase().equals("accountant")
						|| role.getName().toLowerCase().equals("librarian")
						|| role.getName().toLowerCase().equals("secretary")){
			
					Users userToDel = usersHome.getByUsersRole(role.getName());
					usersRoleHome.deleteByRoleName(role.getName());
					if(userToDel != null ){
						usersHome.deleteById(userToDel.getId());
					}
				}
				
				Users user = usersHome.getByUserName(stuff.getStuffNum());
				if(user == null){
					user = new Users();
					
					user.setStatus("active");
					user.setUserName(stuff.getStuffNum());
					user.setUserPass(stuff.getStuffNum()+"12345");
					Integer userId= usersHome.persist(user);
					usersRole.setUsers(user);
					usersRole.getUsers().setId(userId);
					usersRole.setRole(role);

				}
				else{
					usersRole.setUsers(user);
					usersRole.setRole(role);
					
				}
				usersRoleHome.deleteByUserName(stuff.getStuffNum());
				usersRoleHome.persist(usersRole);
				
			} 
			
		} catch (Exception e) {
			log.error("error on stuff role set", e);
			result = false;
		}
		
		return result;
	}

	public SearchResult<UsersRole> getList(SearchParameters searchParameters) {

		return usersRoleHome.getList(searchParameters);
	}

	public UsersRole get(int id) {
		return usersRoleHome.findById(id);
	}
}