package kg.enesaitech.service;

import kg.enesaitech.dao.CourseHome;
import kg.enesaitech.entity.Course;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class CourseService {

	private static CourseService courseService = null;
	private CourseHome courseHome = CourseHome.getInstance();

	public static CourseService getInstance() {
		if (courseService == null) {
			courseService = new CourseService();
		}
		return courseService;
	}

	public boolean create(Course course) {
		boolean result = true;
		try {
			courseHome.persist(course);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(Course course) {
		boolean result = true;

		try {
			courseHome.merge(course);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {
			courseHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Course get(int id) {
		return courseHome.findById(id);
	}

	public SearchResult<Course> getList(SearchParameters searchParameters) {
		return courseHome.getList(searchParameters);
	}

	public Course findById(int id) {
		return courseHome.findById(id);
	}

}
