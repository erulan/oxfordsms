package kg.enesaitech.service;

import kg.enesaitech.dao.HeightStandardHome;
import kg.enesaitech.entity.HeightStandard;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

public class HeightStandardService {

	private static HeightStandardService heightStandardService = null;

	Mapper mapper = DozerMapperProvider.getMapper();

	private HeightStandardHome heightStandardHome = HeightStandardHome.getInstance();

	public static HeightStandardService getInstance() {
		if (heightStandardService == null) {
			heightStandardService = new HeightStandardService();
		}
		return heightStandardService;
	}

	public boolean create(HeightStandard heightStandard) {
		try {

			heightStandardHome.persist(heightStandard);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean update(HeightStandard heightStandard) {
		boolean result = true;
		try {

			heightStandardHome.merge(heightStandard);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		try {

			heightStandardHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public HeightStandard findById(int id) {
		return heightStandardHome.findById(id);
	}

	public SearchResult<HeightStandard> getList(SearchParameters searchParameters) {

		return heightStandardHome.getList(searchParameters);
	}

	public HeightStandard get(int id) {
		return heightStandardHome.findById(id);
	}

}
