
package kg.enesaitech.service;

import kg.enesaitech.dao.BookHome;
import kg.enesaitech.dao.BookInUseHome;
import kg.enesaitech.dao.BookStatusHome;
import kg.enesaitech.dao.StudentHome;
import kg.enesaitech.dao.StuffHome;
import kg.enesaitech.dao.TeacherHome;
import kg.enesaitech.entity.Book;
import kg.enesaitech.entity.BookInUse;
import kg.enesaitech.entity.BookStatus;
import kg.enesaitech.entity.Student;
import kg.enesaitech.entity.Stuff;
import kg.enesaitech.entity.Teacher;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class BookInUseService {

	private static BookInUseService bookInUseService = null;
	private BookInUseHome bookInUseHome = BookInUseHome.getInstance();
	private BookHome bookHome = BookHome.getInstance();
	private BookStatusHome bookStatusHome = BookStatusHome.getInstance();
	private TeacherHome teacherHome = TeacherHome.getInstance();
	private StuffHome stuffHome = StuffHome.getInstance();
	private StudentHome studentHome = StudentHome.getInstance();

	public static BookInUseService getInstance() {
		if (bookInUseService == null) {
			bookInUseService = new BookInUseService();
		}
		return bookInUseService;
	}

	public boolean create(BookInUse bookInUse) {
		boolean result = true;
		try {
			if ((bookInUse.getBook() != null)
					&& (bookInUse.getBook().getId() != null)) {
				Book book = bookHome.findById(bookInUse.getBook().getId());
				
				if (book == null) {
					result = false;

				} else {
					book.setId(bookInUse.getBook().getId());
					BookStatus bookStatus = bookStatusHome.getStatusByName("Колдонууда");
					book.setBookStatus(bookStatus);
					bookHome.merge(book);
					bookInUse.setBook(book);
				}
			} else {
				result = false;
			}
			if(result){
				bookInUseHome.persist(bookInUse);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(BookInUse bookInUse) {
		boolean result = true;

		try {
			if (bookInUse.getEndDate() != null){
				if ((bookInUse.getBook() != null)
						&& (bookInUse.getBook().getId() != null)) {
					Book book = bookHome.findById(bookInUse.getBook().getId());
					
					if (book == null) {
						result = false;	
					} else {
						book.setId(bookInUse.getBook().getId());
						BookStatus bookStatus = bookStatusHome.getStatusByName("Китепканада");
						book.setBookStatus(bookStatus);
						bookHome.merge(book);
						bookInUse.setBook(book);
					}
				} else {
					result = false;
				}
			}
			if(result){
				bookInUseHome.merge(bookInUse);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}
	
	public boolean returnBook(BookInUse bookInUse) {
		boolean result = true;

		try {
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}
	
	
	public boolean delete(int id) {
		boolean result = true;

		try {
			BookInUse bookInUse = bookInUseService.findById(id);
			if (bookInUse == null) {
				result = false;
			} else {
				if ((bookInUse.getBook() != null)
					&& (bookInUse.getBook().getId() != null)) {
					Book book = bookHome.findById(bookInUse.getBook().getId());
					
					if (book == null) {
						result = false;	
					} else {
						book.setId(bookInUse.getBook().getId());
						BookStatus bookStatus = bookStatusHome.getStatusByName("Китепканада");
						book.setBookStatus(bookStatus);
						bookHome.merge(book);
						bookInUse.setBook(book);
					}
				} else {
					result = false;
				}
			}
			if(result){
				bookInUseHome.deleteById(id);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public BookInUse get(int id) {
		
		return bookInUseHome.findById(id);
	}
	public SearchResult<BookInUse> getList(SearchParameters searchParameters) {
		SearchResult<BookInUse> searchResult = bookInUseHome.getList(searchParameters);
		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			if(searchResult.getResultList().get(i).getOwnerId()!=null){
				if(searchResult.getResultList().get(i).getOwnerType()!=null && searchResult.getResultList().get(i).getOwnerType().getName()!=null){	
					if(searchResult.getResultList().get(i).getOwnerType().getName().equals("teacher")){
						Teacher teacher = teacherHome.findById(searchResult.getResultList().get(i).getOwnerId());
						searchResult.getResultList().get(i).setFullname(teacher.getNameEn()+" "+teacher.getSurnameEn());
					} else if(searchResult.getResultList().get(i).getOwnerType().getName().equals("staff")){
						Stuff stuff = stuffHome.findById(searchResult.getResultList().get(i).getOwnerId());
						searchResult.getResultList().get(i).setFullname(stuff.getNameEn()+" "+stuff.getSurnameEn());
						
					} else if(searchResult.getResultList().get(i).getOwnerType().getName().equals("student")){
						Student student = studentHome.findById(searchResult.getResultList().get(i).getOwnerId());
						searchResult.getResultList().get(i).setFullname(student.getNameEn()+" "+student.getSurnameEn());
						
					}
				}
			}
		}
		return searchResult;
	}
	public BookInUse findById(int id) {
		return bookInUseHome.findById(id);
	}

}

