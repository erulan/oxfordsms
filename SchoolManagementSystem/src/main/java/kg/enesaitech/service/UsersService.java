package kg.enesaitech.service;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import kg.enesaitech.dao.UsersHome;
import kg.enesaitech.entity.Users;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

public class UsersService {

	private static UsersService usersService = null;
	private UsersHome usersHome = UsersHome.getInstance();

	Mapper mapper = DozerMapperProvider.getMapper();

	public static UsersService getInstance() {
		if (usersService == null) {
			usersService = new UsersService();
		}
		return usersService;
	}

	public boolean create(Users user) {
		boolean result = true;

		try {
			usersHome.persist(user);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(Users user) {
		boolean result = true;

		try {
			usersHome.merge(user);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Users findById(int id) {
		return usersHome.findById(id);
	}

	public boolean delete(int id) {
		boolean result = true;

		try {
			usersHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public List<Users> getList(SearchParameters searchParameters) {

		return usersHome.getList(searchParameters);
	}

	public Users get(int id) {
		return usersHome.findById(id);
	}
	
	public Users getByUsername(String username){
		return usersHome.getByUserName(username);
	}
	
	public String getMD5(String password){
		String md5Password = null;
		try {
			md5Password = usersHome.getMD5(password);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return md5Password;
	}
}