package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.QuarterHome;
import kg.enesaitech.entity.Quarter;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

public class QuarterService {

	private static QuarterService quarterService = null;
	private QuarterHome quarterHome = QuarterHome.getInstance();

	Mapper mapper = DozerMapperProvider.getMapper();

	public static QuarterService getInstance() {
		if (quarterService == null) {
			quarterService = new QuarterService();
		}
		return quarterService;
	}

	public boolean create(Quarter quarter) {
		boolean result = true;

		try {
			quarterHome.persist(quarter);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(Quarter quarter) {
		boolean result = true;

		try {
			quarterHome.merge(quarter);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Quarter findById(int id) {
		return quarterHome.findById(id);
	}

	public boolean delete(int id) {
		boolean result = true;

		try {
			quarterHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public List<Quarter> getList(SearchParameters searchParameters) {

		return quarterHome.getList(searchParameters);
	}

	public Quarter get(int id) {
		return quarterHome.findById(id);
	}
}