package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.YearHome;
import kg.enesaitech.entity.Year;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

public class YearService {

	private static YearService yearService = null;
	private YearHome yearHome = YearHome.getInstance();

	Mapper mapper = DozerMapperProvider.getMapper();

	public static YearService getInstance() {
		if (yearService == null) {
			yearService = new YearService();
		}
		return yearService;
	}

	public boolean create(Year year) {
		boolean result = true;

		try {
			yearHome.persist(year);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(Year year) {
		boolean result = true;

		try {
			yearHome.merge(year);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Year findById(int id) {
		return yearHome.findById(id);
	}

	public boolean delete(int id) {
		boolean result = true;

		try {
			yearHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public List<Year> getList(SearchParameters searchParameters) {

		return yearHome.getList(searchParameters);
	}

	public Year get(int id) {
		return yearHome.findById(id);
	}
}