package kg.enesaitech.service;

import java.util.Date;
import java.util.List;

import kg.enesaitech.dao.CGroupHome;
import kg.enesaitech.dao.CourseHome;
import kg.enesaitech.dao.NoteAttendanceHome;
import kg.enesaitech.dao.NoteSystemHome;
import kg.enesaitech.dao.NoteTypeHome;
import kg.enesaitech.dao.QuarterHome;
import kg.enesaitech.dao.StudentHome;
import kg.enesaitech.dao.SyllabusCategoryHome;
import kg.enesaitech.dao.SyllabusHome;
import kg.enesaitech.dao.YearHome;
import kg.enesaitech.entity.CGroup;
import kg.enesaitech.entity.Course;
import kg.enesaitech.entity.NoteAttendance;
import kg.enesaitech.entity.NoteSystem;
import kg.enesaitech.entity.NoteType;
import kg.enesaitech.entity.Quarter;
import kg.enesaitech.entity.Student;
import kg.enesaitech.entity.Syllabus;
import kg.enesaitech.entity.SyllabusCategory;
import kg.enesaitech.entity.Year;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.AverageNoteVO;
import kg.enesaitech.vo.ClassGroupVO;
import kg.enesaitech.vo.ClassLetterVO;
import kg.enesaitech.vo.CourseVO;
import kg.enesaitech.vo.GradeVO;
import kg.enesaitech.vo.LessonVO;
import kg.enesaitech.vo.MonitoringVO;
import kg.enesaitech.vo.NoteAttendanceVO;
import kg.enesaitech.vo.QuarterNoteVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;
import kg.enesaitech.vo.SyllNoteVO;
import kg.enesaitech.vo.TeacherVO;

import org.dozer.Mapper;

public class NoteAttendanceService {

	private static NoteAttendanceService noteAttendanceService = null;
	private NoteAttendanceHome noteAttendanceHome = NoteAttendanceHome
			.getInstance();
	private NoteTypeHome noteTypeHome = NoteTypeHome.getInstance();
	private SyllabusHome syllabusHome = SyllabusHome.getInstance();
	private NoteSystemHome noteSystemHome = NoteSystemHome.getInstance();
	private StudentHome studentHome = StudentHome.getInstance();
	private CourseHome courseHome = CourseHome.getInstance();
	private QuarterHome quarterHome = QuarterHome.getInstance();
	private SyllabusCategoryHome syllabusCategoryHome = SyllabusCategoryHome.getInstance();
	private YearHome yearHome = YearHome.getInstance();
	private CGroupHome cGroupHome = CGroupHome.getInstance();
	private AverageGeneratorService averageGeneratorService = AverageGeneratorService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	public static NoteAttendanceService getInstance() {
		if (noteAttendanceService == null) {
			noteAttendanceService = new NoteAttendanceService();
		}
		return noteAttendanceService;
	}

	public boolean create(NoteAttendance noteAttendance) {
		boolean result = true;
		try {
			noteAttendanceHome.persist(noteAttendance);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean createNoteAtt(SyllNoteVO syllNoteVO) {
		boolean result = true;
		try {

			Syllabus syllabus = syllabusHome.findById(syllNoteVO.getSyllabus()
					.getId());
			if (syllabus == null) {
				result = false;
			} else {
				if (syllNoteVO.getSyllabus().getCompletedDate() != null) {
					syllabus.setCompletedDate(syllNoteVO.getSyllabus()
							.getCompletedDate());
				} else {
					syllabus.setCompletedDate(new Date());
				}
				syllabusHome.merge(syllabus);
				for (NoteAttendanceVO n : syllNoteVO.getNoteAttendances()) {

					NoteType noteType;
					NoteSystem noteSystem;
					if (n.getNoteType() == null) {
						noteType = noteTypeHome.getNoteByName("homework");
					} else {
						noteType = noteTypeHome.findById(n.getNoteType()
								.getId());
					}
					if (n.getNoteSystem() == null) {
						noteSystem = noteSystemHome.findById(1);
					} else {
						noteSystem = noteSystemHome.findById(n.getNoteSystem()
								.getId());
					}
					NoteAttendance noteAttendance = mapper.map(n,
							NoteAttendance.class);
					noteAttendance.setNoteType(noteType);
					noteAttendance.setNoteSystem(noteSystem);
					noteAttendance.setSyllabus(syllabus);
					noteAttendanceHome.persist(noteAttendance);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean updateNoteAtt(SyllNoteVO syllNoteVO) {
		boolean result = true;
		try {

			Syllabus syllabus = syllabusHome.findById(syllNoteVO.getSyllabus()
					.getId());
			if (syllabus == null) {
				result = false;
			} else {
				NoteAttendance noteAttendance;
				for (NoteAttendanceVO n : syllNoteVO.getNoteAttendances()) {
					if (n != null && n.getId() != null) {
						noteAttendance = noteAttendanceHome.findById(n.getId());
						if (noteAttendance == null) {
							result = false;
							break;
						} else {
							if (noteAttendance.getSyllabus().getId() != syllabus
									.getId()) {
								result = false;
								break;
							}
						}
					}
				}
			}

			if (result) {
				for (NoteAttendanceVO n : syllNoteVO.getNoteAttendances()) {
					NoteType noteType;
					NoteSystem noteSystem;
					if (n.getNoteType() == null) {
						noteType = noteTypeHome.getNoteByName("homework");
					} else {
						noteType = noteTypeHome.findById(n.getNoteType()
								.getId());
					}
					if (n.getNoteSystem() == null) {
						noteSystem = noteSystemHome.findById(1);
					} else {
						noteSystem = noteSystemHome.findById(n.getNoteSystem()
								.getId());
					}
					NoteAttendance noteAttendance;
					if (n != null && n.getId() != null) {

						noteAttendance = noteAttendanceHome.findById(n.getId());

						noteAttendance.setNoteType(noteType);
						noteAttendance.setNoteSystem(noteSystem);
						noteAttendance.setSyllabus(syllabus);
						if (n.getNote() == null) {
							noteAttendance.setNote(null);
						}
						noteAttendanceHome.merge(noteAttendance);
					} else {

						noteAttendance = mapper.map(n, NoteAttendance.class);
						noteAttendance.setId(null);
						noteAttendance.setNoteType(noteType);
						noteAttendance.setNoteSystem(noteSystem);
						noteAttendance.setSyllabus(syllabus);
						noteAttendanceHome.persist(noteAttendance);
					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean createExamNote(SyllNoteVO syllNoteVO) {
		boolean result = true;
		try {
			Course course = null;
			Syllabus syllabus = null;
			if ((syllNoteVO.getSyllabus() != null)
					&& (syllNoteVO.getSyllabus().getSyllabusCategory() != null)
					&& (syllNoteVO.getSyllabus().getSyllabusCategory().getCourse() != null)
					&& (syllNoteVO.getSyllabus().getSyllabusCategory().getCourse().getId() != null)) {
					course = courseHome.findById(syllNoteVO.getSyllabus().getSyllabusCategory().getCourse().getId());
				if (course == null) {
					result = false;
				}
			} else if (syllNoteVO.getNoteAttendances().size() == 0) {

				result = false;

			}
			Quarter quarter = quarterHome.findById(5);
			if(quarter==null){
				result=false;
			}
			if(course==null){
				result = false;
			}
			NoteType noteType;
			NoteSystem noteSystem;
			List<NoteType> noteTypeList = noteTypeHome.getExamList();
			for (NoteAttendanceVO n : syllNoteVO.getNoteAttendances()) {

				if (n.getNoteType() != null && n.getNoteType().getId() != null) {
					noteType = noteTypeHome.findById(n.getNoteType()
							.getId());
					boolean isExists =false;
					for(NoteType nt : noteTypeList){
						if(nt.getName().equals(noteType.getName())){
							isExists = true;
						} 
					}
					if(!isExists){
						result = false;
						break;								
					}
				} 
				else{
					result = false;
					break;
				}
										
			}
			if (result) {
				SyllabusCategory syllabusCategory = new SyllabusCategory();
				syllabusCategory.setCourse(course);
				syllabusCategory.setQuarter(quarter);
				syllabusCategory.setLessonNo(1);
				syllabusCategory.setTopic("Entry, Gos, Last Year Exams");
				Integer syllabusCategoryId = syllabusCategoryHome.persist(syllabusCategory);
				if(syllabusCategoryId!=null){
					syllabusCategory.setId(syllabusCategoryId);
					syllabus = new Syllabus();
					syllabus.setCompleted(true);
					syllabus.setPlannedDate(new Date());
					syllabus.setTitle("Entry, Gos, Last Year Exams");
					syllabus.setSyllabusCategory(syllabusCategory);
					Integer syllabusId = syllabusHome.persist(syllabus);
					syllabus.setId(syllabusId);
				}
				if(syllabus!=null){
					
					if(result){
						for (NoteAttendanceVO n : syllNoteVO.getNoteAttendances()) {
		
							noteType = noteTypeHome.findById(n.getNoteType()
										.getId());
							if (n.getNoteSystem() == null) {
								noteSystem = noteSystemHome.findById(1);
							} else {
								noteSystem = noteSystemHome.findById(n.getNoteSystem()
										.getId());
							}
							NoteAttendance noteAttendance = mapper.map(n,
									NoteAttendance.class);
							noteAttendance.setNoteType(noteType);
							noteAttendance.setNoteSystem(noteSystem);
							noteAttendance.setSyllabus(syllabus);
							noteAttendanceHome.persist(noteAttendance);
						}
					}
				}
				else{
					result=false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}
	
	public boolean createQuarterExam(SyllNoteVO syllNoteVO, Quarter quarter) {
		boolean result = true;
		try {
			Course course = null;
			Syllabus syllabus = null;
			if ((syllNoteVO.getSyllabus() != null)
					&& (syllNoteVO.getSyllabus().getSyllabusCategory() != null)
					&& (syllNoteVO.getSyllabus().getSyllabusCategory().getCourse() != null)
					&& (syllNoteVO.getSyllabus().getSyllabusCategory().getCourse().getId() != null)) {
					course = courseHome.findById(syllNoteVO.getSyllabus().getSyllabusCategory().getCourse().getId());
				if (course == null) {
					result = false;
				}
			} else if (syllNoteVO.getNoteAttendances().size() == 0) {
				result = false;
			}
			if(course==null){
				result = false;
			}
			NoteType noteType = noteTypeHome.getNoteByName("quarter exam");
			if(noteType==null){
				result=false;
			}
			NoteSystem noteSystem = noteSystemHome.findById(2);
			if(noteSystem==null){
				result=false;
			}
			if (result) {
				SyllabusCategory syllabusCategory = new SyllabusCategory();
				syllabusCategory.setCourse(course);
				syllabusCategory.setQuarter(quarter);
				syllabusCategory.setLessonNo(1);
				syllabusCategory.setTopic(quarter.getName()+ " - quarter note");
				Integer syllabusCategoryId = syllabusCategoryHome.persist(syllabusCategory);
				if(syllabusCategoryId!=null){
					syllabusCategory.setId(syllabusCategoryId);
					syllabus = new Syllabus();
					syllabus.setCompleted(true);
					syllabus.setPlannedDate(new Date());
					syllabus.setTitle(quarter.getName()+ " - quarter note");
					syllabus.setSyllabusCategory(syllabusCategory);
					Integer syllabusId = syllabusHome.persist(syllabus);
					syllabus.setId(syllabusId);
				}
				if(syllabus!=null){					
					if(result){
						for (NoteAttendanceVO n : syllNoteVO.getNoteAttendances()) {
									
							NoteAttendance noteAttendance = mapper.map(n,
									NoteAttendance.class);
							noteAttendance.setNoteType(noteType);
							noteAttendance.setNoteSystem(noteSystem);
							noteAttendance.setSyllabus(syllabus);
							noteAttendanceHome.persist(noteAttendance);
						}
					}
				}
				else{
					result=false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}
	public boolean update(NoteAttendance noteAttendance) {
		boolean result = true;

		try {
			noteAttendanceHome.merge(noteAttendance);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {
			noteAttendanceHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public NoteAttendance get(int id) {
		return noteAttendanceHome.findById(id);
	}

	public SearchResult<NoteAttendance> getList(
			SearchParameters searchParameters) {

		return noteAttendanceHome.getList(searchParameters);
	}

	
	public SearchResult<QuarterNoteVO> getQuaterNoteByStudent(
			SearchParameters searchParameters) {
		
		SearchResult<QuarterNoteVO> result = new SearchResult<QuarterNoteVO>();
		
		String studentID = searchParameters.getSearchParameters().get("studentId");
		String yearID = searchParameters.getSearchParameters().get("yearId");
		if(yearID==null){
			Year currentYear = yearHome.getCurrentYear();
			yearID = currentYear.getId().toString();
		}
		if(studentID == null){
			
		} else{
			Student student = studentHome.findById(Integer.parseInt(studentID));
			if(student==null){
				
			}else{
				SearchParameters searchParameters2 = new SearchParameters();
				searchParameters2.getSearchParameters().put("yearId",
						yearID);
				searchParameters2.getSearchParameters().put("cGroupId",
						student.getCGroup().getId()+"");
				SearchResult<Course> searchCourseResult = courseHome
						.getList(searchParameters2);
				
				
				for (int quarter = 1; quarter < 5; quarter++) {
					System.out.println("** QUATER: " + quarter);
		
					for (int i = 0; i < searchCourseResult.getResultList().size(); i++) {
						System.out.println("course ids: " + searchCourseResult.getResultList().get(i).getId());
						Double note = averageGeneratorService.getAverageNotByStudentByCourse(Integer.parseInt(studentID), searchCourseResult.getResultList().get(i).getId(), quarter, Integer.parseInt(yearID));
						System.out.println("result note: " + note);
						if(note==0){
							note=null;
						}
						if (quarter == 1) {
							QuarterNoteVO quarterNoteVO = new QuarterNoteVO();
							quarterNoteVO.setName(searchCourseResult.getResultList().get(i).getLesson().getName());
							quarterNoteVO.setQuarter1(note);
							result.getResultList().add(quarterNoteVO);
						} else if (quarter == 2) {
							result.getResultList().get(i).setQuarter2(note);
						} else if (quarter == 3) {
							result.getResultList().get(i).setQuarter3(note);
						} else if (quarter == 4) {
							result.getResultList().get(i).setQuarter4(note);
						}
					}
				}
			}
		}
		return result;
	}
	public SearchResult<MonitoringVO> getQuaterNote(
			SearchParameters searchParameters) {		
		SearchResult<MonitoringVO> result = new SearchResult<MonitoringVO>();
		
		String cGroupId = searchParameters.getSearchParameters().get("cGroupId");
		String yearId = searchParameters.getSearchParameters().get("yearId");
		if(yearId==null){
			Year currentYear = yearHome.getCurrentYear();
			yearId = currentYear.getId().toString();
		}
		if(cGroupId == null){
			
		} else{
			SearchParameters searchParameters2 = new SearchParameters();
			searchParameters2.getSearchParameters().put("yearId",
					yearId);
			searchParameters2.getSearchParameters().put("cGroupId", cGroupId);
			SearchResult<Course> searchCourseResult = courseHome
					.getList(searchParameters2);
			
			for (int c = 0; c < searchCourseResult.getResultList().size(); c++) {
				System.out.println("course ids: " + searchCourseResult.getResultList().get(c).getId());
				int courseId = searchCourseResult.getResultList().get(c).getId();

				SearchParameters searchParameters3 = new SearchParameters();
				searchParameters3.getSearchParameters().put("yearId",
						yearId);
				searchParameters3.getSearchParameters().put("courseId", courseId+"");
				searchParameters3.getSearchParameters().put("quarter", 5+"");
				searchParameters3.getSearchParameters().put("noteType", "entry exam");
				SearchResult<NoteAttendance> searchNoteResult = noteAttendanceHome
						.getList(searchParameters3);
				int allFives=0, allFours=0, allThrees= 0, allTwos=0;
				for (int n = 0; n < searchNoteResult.getResultList().size(); n++) {
					if (searchNoteResult.getResultList().get(n).getNote() != null) {
						System.out
								.println("note of the course: "
										+ searchNoteResult.getResultList().get(n)
												.getNote());
						System.out.println("weight of the note: "
								+ searchNoteResult.getResultList().get(n)
										.getNoteType().getWeight());
						int currNote;
						if (searchNoteResult.getResultList().get(n).getNoteSystem()
								.getId() == 2) {
							currNote = to5System(searchNoteResult.getResultList()
									.get(n).getNote());
							System.out.println("100duk note: "
									+ searchNoteResult.getResultList().get(n)
											.getNote());
							System.out.println("converted to: " + currNote);

						} else {
							currNote = searchNoteResult.getResultList().get(n)
									.getNote();
						}
						if(currNote!=0){
							if(currNote==5){
								allFives++;
							}else if(currNote==4){
								allFours++;						
							}else if(currNote==3){
								allThrees++;						
							}else if(currNote==2){
								allTwos++;						
							}
						}
					}
				}
				int sumNumberOfStud = 5*allFives+4*allFours+3*allThrees+2*allTwos;
				int sumNumberOfStud_4_5 = 5*allFives+4*allFours;
				int total = allFives+allFours+allThrees+allTwos;
				MonitoringVO monitoringVO = new MonitoringVO();
				CourseVO courseVO = new CourseVO();
				LessonVO lessonVO = new LessonVO();
				TeacherVO teacherVO = new TeacherVO();
				teacherVO.setNameEn(searchCourseResult.getResultList().get(c).getTeacher().getNameEn());
				teacherVO.setNameRu(searchCourseResult.getResultList().get(c).getTeacher().getNameRu());
				teacherVO.setSurnameEn(searchCourseResult.getResultList().get(c).getTeacher().getSurnameEn());
				teacherVO.setSurnameRu(searchCourseResult.getResultList().get(c).getTeacher().getSurnameRu());
				lessonVO.setName(searchCourseResult.getResultList().get(c).getLesson().getName());
				courseVO.setLesson(lessonVO);
				courseVO.setTeacher(teacherVO);
				monitoringVO.setCourse(courseVO);
				if(total!=0){
					monitoringVO.setQualEE(((sumNumberOfStud/Double.parseDouble(total+""))*100/5));
					monitoringVO.setProgEE(((sumNumberOfStud_4_5/Double.parseDouble(total+""))*100/5));
				}else{
					monitoringVO.setQualEE(0.0);
					monitoringVO.setProgEE(0.0);				
				}
				result.getResultList().add(monitoringVO);
				for (int quarter = 0; quarter < 5; quarter++) {
					SearchParameters searchParameters4 = new SearchParameters();
					searchParameters4.getSearchParameters().put("yearId",
							yearId);
					searchParameters4.getSearchParameters().put("courseId", courseId+"");
					searchParameters4.getSearchParameters().put("quarter", quarter+"");
					searchParameters4.getSearchParameters().put("noteType", "quarter exam");
					SearchResult<NoteAttendance> searchNote2Result = noteAttendanceHome
							.getList(searchParameters4);
					int allFives2=0, allFours2=0, allThrees2= 0, allTwos2=0;
					for (int n = 0; n < searchNote2Result.getResultList().size(); n++) {
						if (searchNote2Result.getResultList().get(n).getNote() != null) {
							System.out
									.println("note of the course: "
											+ searchNote2Result.getResultList().get(n)
													.getNote());
							System.out.println("weight of the note: "
									+ searchNote2Result.getResultList().get(n)
											.getNoteType().getWeight());
							int currNote;
							if (searchNote2Result.getResultList().get(n).getNoteSystem()
									.getId() == 2) {
								currNote = to5System(searchNote2Result.getResultList()
										.get(n).getNote());
								System.out.println("100duk note: "
										+ searchNote2Result.getResultList().get(n)
												.getNote());
								System.out.println("converted to: " + currNote);
	
							} else {
								currNote = searchNote2Result.getResultList().get(n)
										.getNote();
							}
							if(currNote!=0){
								if(currNote==5){
									allFives2++;
								}else if(currNote==4){
									allFours2++;						
								}else if(currNote==3){
									allThrees2++;						
								}else if(currNote==2){
									allTwos2++;						
								}
							}
						}
					}
					int sumNumberOfStud2 = 5*allFives2+4*allFours2+3*allThrees2+2*allTwos2;
					int sumNumberOfStud_4_52 = 5*allFives2+4*allFours2;
					int total2 = allFives2+allFours2+allThrees2+allTwos2;
					if (quarter == 1) {
						if(total2!=0){
							result.getResultList().get(c).setQual1(((sumNumberOfStud2/Double.parseDouble(total2+""))*100/5));
							result.getResultList().get(c).setProg1(((sumNumberOfStud_4_52/Double.parseDouble(total2+""))*100/5));
						}else{
							result.getResultList().get(c).setQual1(0.0);
							result.getResultList().get(c).setProg1(0.0);				
						}					
					} else if (quarter == 2) {
						if(total2!=0){
							result.getResultList().get(c).setQual2(((sumNumberOfStud2/Double.parseDouble(total2+""))*100/5));
							result.getResultList().get(c).setProg2(((sumNumberOfStud_4_52/Double.parseDouble(total2+""))*100/5));
						}else{
							result.getResultList().get(c).setQual2(0.0);
							result.getResultList().get(c).setProg2(0.0);				
						}
					} else if (quarter == 3) {
						if(total2!=0){
							result.getResultList().get(c).setQual3(((sumNumberOfStud2/Double.parseDouble(total2+""))*100/5));
							result.getResultList().get(c).setProg3(((sumNumberOfStud_4_52/Double.parseDouble(total2+""))*100/5));
						}else{
							result.getResultList().get(c).setQual3(0.0);
							result.getResultList().get(c).setProg3(0.0);				
						}
					} else if (quarter == 4) {
						if(total2!=0){
							result.getResultList().get(c).setQual4(((sumNumberOfStud2/Double.parseDouble(total2+""))*100/5));
							result.getResultList().get(c).setProg4(((sumNumberOfStud_4_52/Double.parseDouble(total2+""))*100/5));
						}else{
							result.getResultList().get(c).setQual4(0.0);
							result.getResultList().get(c).setProg4(0.0);				
						}
					}
				}
			}
		}
		return result;
	}

	public SearchResult<AverageNoteVO> getMonitoringByClass(
			SearchParameters searchParameters) {
		SearchResult<AverageNoteVO> result = new SearchResult<AverageNoteVO>();
		String cGroupId = searchParameters.getSearchParameters().get("cGroupId");
		String quarterId = searchParameters.getSearchParameters().get("quarterId");
		String yearId = searchParameters.getSearchParameters().get("yearId");
		if(quarterId==null){
			quarterId = quarterHome.getCurrentQuarter().getId().toString();
		}
		if(yearId==null){
			yearId = yearHome.getCurrentYear().getId().toString();
		}
		if(cGroupId==null){
			
		}
		else{
			SearchParameters searchParameters2 = new SearchParameters();
			searchParameters2.getSearchParameters().put("cGroupId",
					cGroupId);
			searchParameters2.getSearchParameters().put("quarterId",
					quarterId);
			searchParameters2.getSearchParameters().put("yearId",
					yearId);
			SearchResult<Course> searchCourseResult = courseHome
					.getList(searchParameters2);
	//		ArrayList<Integer> courseIDs = new ArrayList<Integer>();
			System.out.println("course list size: "
					+ searchCourseResult.getResultList().size());
			for (int c = 0; c < searchCourseResult.getResultList().size(); c++) {
				int courseId = searchCourseResult.getResultList().get(c).getId();
				SearchParameters searchParameters3 = new SearchParameters();
				searchParameters3.getSearchParameters().put("courseId", courseId + "");
				SearchResult<Student> searchStudentResult = studentHome
						.getList(searchParameters3);
				int allFives=0, allFours=0, allThrees= 0, allTwos=0;
				for (int s = 0; s < searchStudentResult.getResultList().size(); s++) {
					System.out.println("student ids: " + searchStudentResult.getResultList().get(s).getId());
					int stud_id = searchStudentResult.getResultList().get(s).getId();
					
					System.out.println("course ids: " + courseId);
					Double note = averageGeneratorService.getAverageNotByStudentByCourse(stud_id, courseId, Integer.parseInt(quarterId), Integer.parseInt(yearId));
					System.out.println("result note: " + note);		
					int roundedNote = Integer.parseInt(Math.round(note)+"");
					System.out.println("rounded result note: " + roundedNote);
					if(roundedNote!=0){
						if(roundedNote==5){
							allFives++;
						}else if(roundedNote==4){
							allFours++;						
						}else if(roundedNote==3){
							allThrees++;						
						}else if(roundedNote==2){
							allTwos++;						
						}
					}
				// note is average
				}
				int sumNumberOfStud = 5*allFives+4*allFours+3*allThrees+2*allTwos;
				int sumNumberOfStud_4_5 = 5*allFives+4*allFours;
				int total = allFives+allFours+allThrees+allTwos;
				AverageNoteVO averageNoteVO = new AverageNoteVO();
				CourseVO courseVO = new CourseVO();
				LessonVO lessonVO = new LessonVO();
				TeacherVO teacherVO = new TeacherVO();
				teacherVO.setNameEn(searchCourseResult.getResultList().get(c).getTeacher().getNameEn());
				teacherVO.setNameRu(searchCourseResult.getResultList().get(c).getTeacher().getNameRu());
				teacherVO.setSurnameEn(searchCourseResult.getResultList().get(c).getTeacher().getSurnameEn());
				teacherVO.setSurnameRu(searchCourseResult.getResultList().get(c).getTeacher().getSurnameRu());
				lessonVO.setName(searchCourseResult.getResultList().get(c).getLesson().getName());
				courseVO.setLesson(lessonVO);
				courseVO.setTeacher(teacherVO);
				averageNoteVO.setCourse(courseVO);
				averageNoteVO.setFive(allFives);
				averageNoteVO.setFour(allFours);
				averageNoteVO.setThree(allThrees);
				averageNoteVO.setTwo(allTwos);
				if(total!=0){
					averageNoteVO.setQuantity(((sumNumberOfStud/Double.parseDouble(total+""))*100/5));
					averageNoteVO.setProgress(((sumNumberOfStud_4_5/Double.parseDouble(total+""))*100/5));
				}else{
					averageNoteVO.setQuantity(0.0);
					averageNoteVO.setProgress(0.0);				
				}
				result.getResultList().add(averageNoteVO);
			}
		}
		return result;
	}

	public SearchResult<QuarterNoteVO> getByCourses(
			SearchParameters searchParameters) {
		SearchResult<QuarterNoteVO> result = new SearchResult<QuarterNoteVO>();
		String courseId = searchParameters.getSearchParameters().get("courseId");
		String yearId = searchParameters.getSearchParameters().get("yearId");
		if(courseId == null){
			
		}else{
			if(yearId==null){
				yearId = yearHome.getCurrentYear().getId().toString();
			}
			SearchResult<NoteAttendance> searchResult;
			SearchParameters searchParameters2 = new SearchParameters();
			searchParameters2.getSearchParameters().put("courseId",
					courseId);
			SearchResult<Student> searchStudentResult = studentHome
					.getList(searchParameters2);
			for (int i = 0; i < searchStudentResult.getResultList().size(); i++) {
				int studentId = searchStudentResult.getResultList().get(i).getId();
				for (int quarter = 1; quarter < 5; quarter++) {
					Double note = averageGeneratorService.getAverageNotByStudentByCourse(studentId, Integer.parseInt(courseId), quarter, Integer.parseInt(yearId));
					System.out.println("result note: " + note);	
					if(note==0){
						note=null;
					}
					if (quarter == 1) {
						QuarterNoteVO quarterNoteVO = new QuarterNoteVO();
						quarterNoteVO.setName(searchStudentResult.getResultList()
								.get(i).getNameEn()
								+ " "
								+ searchStudentResult.getResultList().get(i)
										.getSurnameEn());
						quarterNoteVO.setQuarter1(note);
						quarterNoteVO.setCourse(false);
						result.getResultList().add(quarterNoteVO);
					} else if (quarter == 2) {
						result.getResultList().get(i).setQuarter2(note);
					} else if (quarter == 3) {
						result.getResultList().get(i).setQuarter3(note);
					} else if (quarter == 4) {
						result.getResultList().get(i).setQuarter4(note);
					}
				}
				System.out.println("**********working ************ ");			
				SearchParameters searchParameters3 = new SearchParameters();
				searchParameters3.getSearchParameters().put("quarterId",
						5 + "");
				searchParameters3.getSearchParameters().put("courseId",
						courseId);
				searchParameters3.getSearchParameters().put("studentId",
						studentId+"");
				searchResult = noteAttendanceHome.getList(searchParameters);			
	
				for (int n = 0; n < searchResult.getResultList().size(); n++) {
					if (searchResult.getResultList().get(n).getNote() != null) {
						if(searchResult.getResultList().get(n).getNoteType().getName().equals("last year")){
							result.getResultList().get(i).setLastYear(Double.parseDouble(""+searchResult.getResultList().get(n).getNote()));						
						}
						else if(searchResult.getResultList().get(n).getNoteType().getName().equals("entry exam")){
							result.getResultList().get(i).setEntryExam(Double.parseDouble(""+searchResult.getResultList().get(n).getNote()));				
						}
						else if(searchResult.getResultList().get(n).getNoteType().getName().equals("GOS exam")){
							result.getResultList().get(i).setGosExam(Double.parseDouble(""+searchResult.getResultList().get(n).getNote()));							
						}
					}
				}
				
			}
		}
		return result;
	}

	public SearchResult<AverageNoteVO> getMonitoringGeneral(
			SearchParameters searchParameters) {
		SearchResult<AverageNoteVO> result = new SearchResult<AverageNoteVO>();
		String quarterId = searchParameters.getSearchParameters().get("quarterId");
		String yearId = searchParameters.getSearchParameters().get("yearId");
		if(quarterId==null){
			quarterId = quarterHome.getCurrentQuarter().getId().toString();
		}
		if(yearId==null){
			yearId = yearHome.getCurrentYear().getId().toString();
		}
		SearchParameters searchParameters2 = new SearchParameters();
		List<CGroup> cGroupList = cGroupHome.getList(searchParameters2);
		for(int cgrp = 0; cgrp < cGroupList.size(); cgrp++){			
			int cGroupId = cGroupList.get(cgrp).getId();
			SearchParameters searchParameters3 = new SearchParameters();
			searchParameters3.getSearchParameters().put("cGroupId",
					cGroupId+"");
			SearchResult<Student> searchStudentResult = studentHome
					.getList(searchParameters3);
			System.out.println("student list size: "
					+ searchStudentResult.getResultList().size());
			int allFives=0, allFours=0, allThrees= 0, oneThrees= 0, twoThrees= 0, allTwos=0;
			for (int st = 0; st < searchStudentResult.getResultList().size(); st++) {
				int studentId = searchStudentResult.getResultList().get(st).getId();
				SearchParameters searchParameters4 = new SearchParameters();
				searchParameters4.getSearchParameters().put("cGroupId",
						cGroupId+"");
				searchParameters4.getSearchParameters().put("yearId",
						yearId);
				SearchResult<Course> searchCourseResult = courseHome
						.getList(searchParameters4);
				int is2345=6;
				int numOfthrees=0;
				for (int c = 0; c < searchCourseResult.getResultList().size(); c++) {
					int courseId = searchCourseResult.getResultList().get(c).getId();
					
					System.out.println("course ids: " + courseId);
					Double note = averageGeneratorService.getAverageNotByStudentByCourse(studentId, courseId, Integer.parseInt(quarterId), Integer.parseInt(yearId));
					System.out.println("result note: " + note);		
					int roundedNote = Integer.parseInt(Math.round(note)+"");
					System.out.println("rounded result note: " + roundedNote);
					if(roundedNote!=0){
						if(roundedNote==3){
							numOfthrees++;
						}
						if(is2345>roundedNote){
							is2345=roundedNote;
						}
					}
				// note is average
				}				
				if(is2345==6){
				}				
				else if(is2345==5){
					allFives++;
				}else if(is2345==4){
					allFours++;						
				}else if(is2345==3){
					if(numOfthrees==1){
						oneThrees++;
					}else if(numOfthrees==2){
						twoThrees++;
					}
					else{
						allThrees++;
					}						
				}else if(is2345==2){
					allTwos++;						
				}
			}
			if(searchStudentResult.getResultList().size()!=0){
				AverageNoteVO averageNoteVO = new AverageNoteVO();
				CourseVO courseVO = new CourseVO();
				ClassGroupVO classGroupVO = new ClassGroupVO();
				GradeVO gradeVO = new GradeVO();
				ClassLetterVO classLetterVO = new ClassLetterVO();
				gradeVO.setGrade(cGroupList.get(cgrp).getGrade().getGrade());
				classLetterVO.setLetter(cGroupList.get(cgrp).getClassLetter().getLetter());
				classGroupVO.setClassLetter(classLetterVO);
				classGroupVO.setGrade(gradeVO);
				courseVO.setClassGroup(classGroupVO);
				averageNoteVO.setCourse(courseVO);
				averageNoteVO.setFive(allFives);
				averageNoteVO.setFour(allFours);
				averageNoteVO.setThree(allThrees);
				averageNoteVO.setWithOneThree(oneThrees);
				averageNoteVO.setWithTwoThree(twoThrees);
				averageNoteVO.setTwo(allTwos);
				averageNoteVO.setTotalInClass(searchStudentResult.getResultList().size());
				result.getResultList().add(averageNoteVO);
			}
		}	
		return result;
	}
	private int to5System(int note) {
		if (note >= 80)
			return 5;
		else if (note >= 60) {
			return 4;
		} else if (note >= 40) {
			return 3;
		} else {
			return 2;
		}
	}
	public NoteAttendance findById(int id) {
		return noteAttendanceHome.findById(id);
	}

}
