
package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.ClassLetterHome;
import kg.enesaitech.entity.ClassLetter;
import kg.enesaitech.vo.SearchParameters;

public class ClassLetterService {
	
	private static ClassLetterService classLetterService = null;
	ClassLetterHome classLetterHome = ClassLetterHome.getInstance();
	
	public static ClassLetterService getInstance(){
		if(classLetterService == null){
			classLetterService = new ClassLetterService();
		}
		return classLetterService;
	}

	public ClassLetter findById(int id) {
		return classLetterHome.findById(id);
	}
	public List<ClassLetter> getList(SearchParameters searchParameters) {

		return classLetterHome.getList(searchParameters);
	}
	public ClassLetter get(int id) {
		return classLetterHome.findById(id);
	}

}

