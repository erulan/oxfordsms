package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.ClassHourTypeHome;
import kg.enesaitech.entity.ClassHourType;
import kg.enesaitech.vo.SearchParameters;

public class ClassHourTypeService {

	private static ClassHourTypeService classHourTypeService = null;
	private ClassHourTypeHome classHourTypeHome = ClassHourTypeHome.getInstance();

	public static ClassHourTypeService getInstance() {
		if (classHourTypeService == null) {
			classHourTypeService = new ClassHourTypeService();
		}
		return classHourTypeService;
	}

	public ClassHourType get(int id) {
		return classHourTypeHome.findById(id);
	}

	public List<ClassHourType> getList(SearchParameters searchParameters) {

		return classHourTypeHome.getList(searchParameters);
	}

	public ClassHourType findById(int id) {
		return classHourTypeHome.findById(id);
	}
}
