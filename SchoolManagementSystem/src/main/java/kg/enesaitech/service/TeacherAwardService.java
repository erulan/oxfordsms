package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.TeacherAwardHome;
import kg.enesaitech.entity.TeacherAward;
import kg.enesaitech.vo.SearchParameters;

public class TeacherAwardService {

	private static TeacherAwardService awardService = null;
	private TeacherAwardHome teacherAwardHome = TeacherAwardHome.getInstance();

	public static TeacherAwardService getInstance() {
		if (awardService == null) {
			awardService = new TeacherAwardService();
		}
		return awardService;
	}
	
	public boolean create(TeacherAward teacherAward) {
		boolean result = true;
		try {

			teacherAwardHome.persist(teacherAward);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(TeacherAward teacherAward) {
		boolean result = true;

		try {

			teacherAwardHome.merge(teacherAward);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {

			result = teacherAwardHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public TeacherAward get(int id) {
		return teacherAwardHome.findById(id);
	}

	public List<TeacherAward> getList(SearchParameters searchParameters) {

		return teacherAwardHome.getList(searchParameters);
	}

	public TeacherAward findById(int id) {
		return teacherAwardHome.findById(id);
	}
}
