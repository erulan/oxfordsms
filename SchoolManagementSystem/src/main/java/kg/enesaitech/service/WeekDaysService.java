package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.WeekDaysHome;
import kg.enesaitech.entity.WeekDays;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

public class WeekDaysService {

	private static WeekDaysService weekDaysService = null;
	private WeekDaysHome weekDaysHome = WeekDaysHome.getInstance();

	Mapper mapper = DozerMapperProvider.getMapper();

	public static WeekDaysService getInstance() {
		if (weekDaysService == null) {
			weekDaysService = new WeekDaysService();
		}
		return weekDaysService;
	}

	public boolean create(WeekDays weekDays) {
		boolean result = true;

		try {
			weekDaysHome.persist(weekDays);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(WeekDays weekDays) {
		boolean result = true;

		try {
			weekDaysHome.merge(weekDays);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public WeekDays findById(int id) {
		return weekDaysHome.findById(id);
	}

	public boolean delete(int id) {
		boolean result = true;

		try {
			weekDaysHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public List<WeekDays> getList(SearchParameters searchParameters) {

		return weekDaysHome.getList(searchParameters);
	}

	public WeekDays get(int id) {
		return weekDaysHome.findById(id);
	}
}