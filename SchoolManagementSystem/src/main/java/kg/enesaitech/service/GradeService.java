package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.GradeHome;
import kg.enesaitech.entity.Grade;
import kg.enesaitech.vo.SearchParameters;

public class GradeService {

	private static GradeService gradeService = null;
	GradeHome gradeHome = GradeHome.getInstance();

	public static GradeService getInstance() {
		if (gradeService == null) {
			gradeService = new GradeService();
		}
		return gradeService;
	}

	public Grade get(int id) {
		return gradeHome.findById(id);
	}

	public List<Grade> getList(SearchParameters searchParameters) {

		return gradeHome.getList(searchParameters);
	}

	public Grade findById(int id) {
		return gradeHome.findById(id);
	}

}
