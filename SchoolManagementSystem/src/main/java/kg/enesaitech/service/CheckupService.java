package kg.enesaitech.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import kg.enesaitech.dao.CheckupHome;
import kg.enesaitech.dao.HeightStandardHome;
import kg.enesaitech.dao.StudentHome;
import kg.enesaitech.dao.WeightForHeightHome;
import kg.enesaitech.dao.WeightStandardHome;
import kg.enesaitech.entity.Checkup;
import kg.enesaitech.entity.HeightStandard;
import kg.enesaitech.entity.Student;
import kg.enesaitech.entity.WeightForHeight;
import kg.enesaitech.entity.WeightStandard;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

public class CheckupService {

	private static CheckupService checkupService = null;

	Mapper mapper = DozerMapperProvider.getMapper();

	private CheckupHome checkupHome = CheckupHome.getInstance();
	private StudentHome studentHome = StudentHome.getInstance();
	private HeightStandardHome heightStandardHome = HeightStandardHome.getInstance();
	private WeightStandardHome weightStandardHome = WeightStandardHome.getInstance();
	private WeightForHeightHome weightForHeightHome = WeightForHeightHome.getInstance();

	public static CheckupService getInstance() {
		if (checkupService == null) {
			checkupService = new CheckupService();
		}
		return checkupService;
	}

	public boolean create(Checkup checkup) {
		try {

			checkupHome.persist(checkup);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean update(Checkup checkup) {
		boolean result = true;
		try {

			checkupHome.merge(checkup);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		try {

			checkupHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Checkup findById(int id) {
		return checkupHome.findById(id);
	}

	public SearchResult<Checkup> getList(SearchParameters searchParameters) {
		SearchResult<Checkup> checkups = checkupHome.getList(searchParameters);
		SearchParameters searchParameters2 = new SearchParameters();
		searchParameters2.getSearchParameters().put("cGroupId", searchParameters.getSearchParameters().get("CGroupId"));
		SearchResult<Student> students = studentHome.getList(searchParameters2);
		ArrayList<Integer> studIds = new ArrayList<Integer>();
		for(int i=0; i<checkups.getResultList().size();i++){
			studIds.add(checkups.getResultList().get(i).getStudent().getId());
			int month = differenceInMonths(checkups.getResultList().get(i).getStudent().getBirthDate(), new Date());
			String categoryH = getHeightCategory(checkups.getResultList().get(i).getHeight(), month, checkups.getResultList().get(i).getStudent().isIsMale() == true ? 1 : 0);
			String categoryW = getWeightCategory(checkups.getResultList().get(i).getWeight(), month, checkups.getResultList().get(i).getStudent().isIsMale() == true ? 1 : 0);
			String categoryWFH = getWeightForHeightCategory(checkups.getResultList().get(i).getWeight(), checkups.getResultList().get(i).getHeight(), checkups.getResultList().get(i).getStudent().isIsMale() == true ? 1 : 0);
			checkups.getResultList().get(i).setCategoryH(categoryH);
			checkups.getResultList().get(i).setCategoryW(categoryW);
			checkups.getResultList().get(i).setCategoryWFH(categoryWFH);
		}
		for(int i=0; i<students.getResultList().size();i++){
			if(!studIds.contains(students.getResultList().get(i).getId())){
				Checkup checkup = new Checkup();
				checkup.setStudent(students.getResultList().get(i));
				checkups.getResultList().add(checkup);
			}
		}

		return checkups;
	}

	public Checkup get(int id) {
		return checkupHome.findById(id);
	}

	 private Integer differenceInMonths(Date beginningDate, Date endingDate) {
       if (beginningDate == null || endingDate == null) {
           return 0;
       }
       Calendar cal1 = new GregorianCalendar();
       cal1.setTime(beginningDate);
       Calendar cal2 = new GregorianCalendar();
       cal2.setTime(endingDate);
       return differenceInMonths(cal1, cal2);
   }
   private Integer differenceInMonths(Calendar beginningDate, Calendar endingDate) {
       if (beginningDate == null || endingDate == null) {
           return 0;
       }
       int m1 = beginningDate.get(Calendar.YEAR) * 12 + beginningDate.get(Calendar.MONTH);
       int m2 = endingDate.get(Calendar.YEAR) * 12 + endingDate.get(Calendar.MONTH);
       return m2 - m1;
   }
   
   private String getHeightCategory(double height, int month, int isMale){
		HeightStandard heightStandard = heightStandardHome.getHeightStandartByMonth(month, isMale);
		if(heightStandard==null){
			return "NoStandard";
		}
		else if (height<heightStandard.getCat2())
			return "cat1";
		else if (height<heightStandard.getCat3())
			return "cat2";
		else if (height<heightStandard.getCat4())
			return "cat3";
		else if (height<heightStandard.getCat5())
			return "NORMA";
		else if (height<heightStandard.getCat6())
			return "1CO";
		else if (height<heightStandard.getCat7())
			return "2CO";
		else 
			return "3CO";	   	
   }
   
   private String getWeightCategory(double weight, int month, int isMale){
		WeightStandard weightStandard = weightStandardHome.getWeightStandartByMonth(month, isMale);
		if(weightStandard==null){
			return "NoStandard";
		}
		else if (weight<weightStandard.getCat2())
			return "cat1";
		else if (weight<weightStandard.getCat3())
			return "cat2";
		else if (weight<weightStandard.getCat4())
			return "cat3";
		else if (weight<weightStandard.getCat5())
			return "NORMA";
		else if (weight<weightStandard.getCat6())
			return "1CO";
		else if (weight<weightStandard.getCat7())
			return "2CO";
		else 
			return "3CO";	   	
  }
   private String getWeightForHeightCategory(double weight,double height, int isMale){
		WeightForHeight weightForHeight = weightForHeightHome.getWeightStandartByHeight(weight, height, isMale);
		if(weightForHeight ==null){
			return "NoStandard";
		}
		else if (weight<weightForHeight.getCat2())
			return "cat1";
		else if (weight<weightForHeight.getCat3())
			return "cat2";
		else if (weight<weightForHeight.getCat4())
			return "cat3";
		else if (weight<weightForHeight.getCat5())
			return "NORMA";
		else if (weight<weightForHeight.getCat6())
			return "1CO";
		else if (weight<weightForHeight.getCat7())
			return "2CO";
		else 
			return "3CO";	   	
 }

}
