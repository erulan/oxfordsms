package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.BloodHome;
import kg.enesaitech.entity.Blood;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

public class BloodService {

	private static BloodService bloodService = null;

	Mapper mapper = DozerMapperProvider.getMapper();

	private BloodHome bloodHome = BloodHome.getInstance();

	public static BloodService getInstance() {
		if (bloodService == null) {
			bloodService = new BloodService();
		}
		return bloodService;
	}

	public boolean create(Blood blood) {
		boolean result = true;

		try {
			bloodHome.persist(blood);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(Blood blood) {
		boolean result = true;

		try {

			bloodHome.merge(blood);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {
			Blood blood = bloodHome.findById(id);

			bloodHome.deleteById(blood.getId());

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Blood get(int id) {

		return bloodHome.findById(id);
	}

	public List<Blood> getList(SearchParameters searchParameters) {
		return bloodHome.getList(searchParameters);
	}

	public Blood findById(int id) {
		return bloodHome.findById(id);
	}
}