
package kg.enesaitech.service;

import kg.enesaitech.dao.MaritalStatusHome;
import kg.enesaitech.entity.MaritalStatus;
import kg.enesaitech.entity.SchoolType;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class MaritalStatusService {

	private static MaritalStatusService maritalStatusService = null;
	private MaritalStatusHome maritalStatusHome = MaritalStatusHome.getInstance();

	public static MaritalStatusService getInstance() {
		if (maritalStatusService == null) {
			maritalStatusService = new MaritalStatusService();
		}
		return maritalStatusService;
	}

	public boolean create(MaritalStatus maritalStatus) {
		boolean result = true;
		try {
			maritalStatusHome.persist(maritalStatus);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	

	public SearchResult<MaritalStatus> getList(SearchParameters searchParameters) {

		return maritalStatusHome.getList(searchParameters);
	}
	public MaritalStatus findById(int id) {
		return maritalStatusHome.findById(id);
	}
}
