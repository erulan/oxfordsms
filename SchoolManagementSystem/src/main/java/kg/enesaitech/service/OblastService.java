package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.OblastHome;
import kg.enesaitech.entity.Oblast;
import kg.enesaitech.vo.SearchParameters;

public class OblastService {

	private static OblastService oblastService = null;
	private OblastHome oblastHome = OblastHome.getInstance();

	public static OblastService getInstance() {
		if (oblastService == null) {
			oblastService = new OblastService();
		}
		return oblastService;
	}

	public Oblast get(int id) {
		return oblastHome.findById(id);
	}

	public List<Oblast> getList(SearchParameters searchParameters) {

		return oblastHome.getList(searchParameters);
	}

	public Oblast findById(int id) {
		return oblastHome.findById(id);
	}
}
