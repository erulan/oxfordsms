package kg.enesaitech.service;

import kg.enesaitech.dao.RoleHome;
import kg.enesaitech.entity.Role;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class RoleService {

	private static RoleService roleService = null;
	private RoleHome roleHome = RoleHome.getInstance();

	public static RoleService getInstance() {
		if (roleService == null) {
			roleService = new RoleService();
		}
		return roleService;
	}

	public Role get(int id) {
		return roleHome.findById(id);
	}

	public SearchResult<Role> getList(SearchParameters searchParameters) {

		return roleHome.getList(searchParameters);
	}

	public Role findById(Integer id) {
		return roleHome.findById(id);
	}

}
