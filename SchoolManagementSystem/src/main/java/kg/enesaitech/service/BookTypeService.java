
package kg.enesaitech.service;

import kg.enesaitech.dao.BookStatusHome;
import kg.enesaitech.dao.BookTypeHome;
import kg.enesaitech.entity.Book;
import kg.enesaitech.entity.BookStatus;
import kg.enesaitech.entity.BookType;
import kg.enesaitech.userException.BusinessException;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class BookTypeService {

	private static BookTypeService bookTypeService = null;
	private BookService bookService = BookService.getInstance();
	private BookTypeHome bookTypeHome = BookTypeHome.getInstance();
	private BookStatusHome bookStatusHome = BookStatusHome.getInstance();

	public static BookTypeService getInstance() {
		if (bookTypeService == null) {
			bookTypeService = new BookTypeService();
		}
		return bookTypeService;
	}

	public boolean create(BookType bookType) {
		boolean result = true;
		try {
			bookTypeHome.persist(bookType);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(BookType bookType) {
		boolean result = true;

		try {
			bookTypeHome.merge(bookType);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.addParameter("bookTypeId", String.valueOf(id));
		BookStatus bookStatus = bookStatusHome.getStatusByName("Deleted");
		searchParameters.getSearchParameters().put("notBookStatusId", bookStatus.getId().toString());
		
		SearchResult<Book> resut = bookService.getList(searchParameters);
		if(resut.getTotalRecords() > 0)
			throw new BusinessException("There is Books of this BookType. Delete that Books firstly");

		try {
			bookTypeHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public BookType get(int id) {
		
		return bookTypeHome.findById(id);
	}
	public SearchResult<BookType> getList(SearchParameters searchParameters) {

		return bookTypeHome.getList(searchParameters);
	}
	public BookType findById(int id) {
		return bookTypeHome.findById(id);
	}
}
