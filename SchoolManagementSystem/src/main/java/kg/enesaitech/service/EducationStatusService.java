package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.StatusHome;
import kg.enesaitech.entity.Status;
import kg.enesaitech.vo.SearchParameters;

public class EducationStatusService {
	
	private static EducationStatusService educationStatusService = null;
	StatusHome educationStatusHome=StatusHome.getInstance();
	
	public static EducationStatusService getInstance(){
		if(educationStatusService == null){
			educationStatusService = new EducationStatusService();
		}
		return educationStatusService;
	}

	public Status get(int id) {
		return educationStatusHome.findById(id);
	}
	public List<Status> getList(SearchParameters searchParameters) {

		return educationStatusHome.getList(searchParameters);
	}
	public Status findById(Integer id) {
		return educationStatusHome.findById(id);
	}

}
