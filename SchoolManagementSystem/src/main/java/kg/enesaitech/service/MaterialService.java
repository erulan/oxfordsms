package kg.enesaitech.service;

import kg.enesaitech.dao.MaterialHome;
import kg.enesaitech.dao.StuffHome;
import kg.enesaitech.dao.TeacherHome;
import kg.enesaitech.entity.Material;
import kg.enesaitech.entity.Stuff;
import kg.enesaitech.entity.Teacher;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

public class MaterialService {

	private static MaterialService materialService = null;
	Mapper mapper = DozerMapperProvider.getMapper();
	private MaterialHome materialHome = MaterialHome.getInstance();
	private TeacherHome teacherHome = TeacherHome.getInstance();
	private StuffHome stuffHome = StuffHome.getInstance();

	public static MaterialService getInstance() {
		if (materialService == null) {
			materialService = new MaterialService();
		}
		return materialService;
	}

	public boolean create(Material material) {
		boolean result = true;
		try {
			materialHome.persist(material);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(Material material) {
		boolean result = true;
		try {
			materialHome.merge(material);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		
		try {

			materialHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Material get(int id) {

		return materialHome.findById(id);
	}


	public SearchResult<Material> getList(SearchParameters searchParameters) {
		SearchResult<Material> searchResult = materialHome.getList(searchParameters);
		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			if(searchResult.getResultList().get(i).getOwnerId()!=null){
				if(searchResult.getResultList().get(i).getOwnerType()!=null && searchResult.getResultList().get(i).getOwnerType().getName()!=null){	
					if(searchResult.getResultList().get(i).getOwnerType().getName().equals("teacher")){
						Teacher teacher = teacherHome.findById(searchResult.getResultList().get(i).getOwnerId());
						searchResult.getResultList().get(i).setFullname(teacher.getNameEn()+" "+teacher.getSurnameEn());
					}else if(searchResult.getResultList().get(i).getOwnerType().getName().equals("staff")){
						Stuff stuff = stuffHome.findById(searchResult.getResultList().get(i).getOwnerId());
						searchResult.getResultList().get(i).setFullname(stuff.getNameEn()+" "+stuff.getSurnameEn());
						
					}else if(searchResult.getResultList().get(i).getOwnerType().getName().equals("no owner") || searchResult.getResultList().get(i).getOwnerId()==0){
						searchResult.getResultList().get(i).setFullname("no owner");
						
					}
				}
			}
		}
		return searchResult;
	}

	public Material findById(int id) {

		return materialHome.findById(id);
	}

}
