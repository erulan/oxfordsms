package kg.enesaitech.service;

import kg.enesaitech.dao.SyllabusHome;
import kg.enesaitech.entity.Syllabus;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class SyllabusService {

	private static SyllabusService syllabusService = null;
	SyllabusHome syllabusHome = SyllabusHome.getInstance();

	public static SyllabusService getInstance() {
		if (syllabusService == null) {
			syllabusService = new SyllabusService();
		}
		return syllabusService;
	}

	public boolean create(Syllabus syllabus) {
		boolean result = true;

		try {
			syllabusHome.persist(syllabus);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}
	
	public Integer createForExam(Syllabus syllabus) {
		Integer syllabusId = null;
		try {
			syllabusId = syllabusHome.persist(syllabus);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return syllabusId;
	}

	public boolean update(Syllabus syllabus) {
		boolean result = true;

		try {

			syllabusHome.merge(syllabus);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {

			syllabusHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Syllabus get(int id) {
		return syllabusHome.findById(id);
	}

	public SearchResult<Syllabus> getList(SearchParameters searchParameters) {
		searchParameters.getSearchParameters().put("notQuarterId", 5+"");
		return syllabusHome.getList(searchParameters);
	}

	public Syllabus findById(int id) {
		return syllabusHome.findById(id);
	}

}
