package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.CGroupHome;
import kg.enesaitech.entity.CGroup;
import kg.enesaitech.vo.SearchParameters;

public class CGroupService {

	private static CGroupService cGroupService = null;

	private CGroupHome cgroupHome = CGroupHome.getInstance();

	public static CGroupService getInstance() {
		if (cGroupService == null) {
			cGroupService = new CGroupService();
		}
		return cGroupService;
	}

	public boolean create(CGroup cGroup) {
		boolean result = true;

		try {
			cgroupHome.persist(cGroup);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}

	public boolean update(CGroup cGroup) {
		boolean result = true;

		try {

			cgroupHome.merge(cGroup);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {

			cgroupHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public CGroup get(int id) {
		return cgroupHome.findById(id);
	}
	public List<CGroup> getList(SearchParameters searchParameters) {

		if(searchParameters.getSearchParameters().get("schoolTypeId") == null){
			searchParameters.getSearchParameters().put("schoolTypeId", 1+"");
		}
		return cgroupHome.getList(searchParameters);
	}
	public CGroup findById(int id) {
		return cgroupHome.findById(id);
	}
}
