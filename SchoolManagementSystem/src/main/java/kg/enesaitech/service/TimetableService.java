package kg.enesaitech.service;

import kg.enesaitech.dao.CGroupHome;
import kg.enesaitech.dao.TimetableHome;
import kg.enesaitech.entity.CGroup;
import kg.enesaitech.entity.Timetable;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;
import kg.enesaitech.vo.TimetableVO;
import kg.enesaitech.vo.TimetablesVO;

import org.dozer.Mapper;

public class TimetableService {

	private static TimetableService timetableService = null;
	private TimetableHome timetableHome = TimetableHome.getInstance();
	private CGroupHome cGroupHome = CGroupHome.getInstance();

	Mapper mapper = DozerMapperProvider.getMapper();

	public static TimetableService getInstance() {
		if (timetableService == null) {
			timetableService = new TimetableService();
		}
		return timetableService;
	}

	public boolean create(Timetable timetable) {
		boolean result = true;

		try {
			timetableHome.persist(timetable);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean createTimetable(TimetablesVO timetablesVO) {
		boolean result = true;

		try {
			if (timetablesVO.getClassGroup() != null
					&& timetablesVO.getClassGroup().getId() != null) {
				CGroup cGroup = cGroupHome.findById(timetablesVO
						.getClassGroup().getId());
				if(cGroup== null){
					result=false;
				}
				else{
					result=timetableHome.deleteByCgroupId(cGroup.getId());
					if(result){
						for(TimetableVO timetableVO : timetablesVO.getTimetables()){
							Timetable timetable = mapper.map(timetableVO, Timetable.class);
							timetable.setId(null);
							timetable.setCGroup(cGroup);
							timetableHome.persist(timetable);
							
						}
					}
				}
			} else {
				result = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(Timetable timetable) {
		boolean result = true;

		try {
			timetableHome.merge(timetable);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Timetable findById(int id) {
		return timetableHome.findById(id);
	}

	public boolean delete(int id) {
		boolean result = true;

		try {
			timetableHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public SearchResult<Timetable> getList(SearchParameters searchParameters) {

		return timetableHome.getList(searchParameters);
	}

	public Timetable get(int id) {
		return timetableHome.findById(id);
	}
}