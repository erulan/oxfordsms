package kg.enesaitech.service;

import kg.enesaitech.dao.MaterialTypeHome;
import kg.enesaitech.entity.Material;
import kg.enesaitech.entity.MaterialType;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.userException.BusinessException;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

public class MaterialTypeService {

	private static MaterialTypeService materialTypeService = null;
	Mapper mapper = DozerMapperProvider.getMapper();
	private MaterialTypeHome materialTypeHome = MaterialTypeHome.getInstance();
	private MaterialService materialService = MaterialService.getInstance();

	public static MaterialTypeService getInstance() {
		if (materialTypeService == null) {
			materialTypeService = new MaterialTypeService();
		}
		return materialTypeService;
	}

	public boolean create(MaterialType materialType) {
		boolean result = true;
		try {
			materialTypeHome.persist(materialType);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(MaterialType materialType) {
		boolean result = true;
		try {
			materialTypeHome.merge(materialType);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.addParameter("materialTypeId", String.valueOf(id));
		
		SearchResult<Material> resut = materialService.getList(searchParameters);
		if(resut.getTotalRecords() > 0)
			throw new BusinessException("There is Inventory of this Type. Delete that Inventory firstly");
		
		try {

			materialTypeHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public MaterialType get(int id) {

		return materialTypeHome.findById(id);
	}


	public SearchResult<MaterialType> getList(SearchParameters searchParameters) {

		return materialTypeHome.getList(searchParameters);
	}

	public MaterialType findById(int id) {

		return materialTypeHome.findById(id);
	}

}
