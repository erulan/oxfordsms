
package kg.enesaitech.service;

import kg.enesaitech.dao.SchoolTypeHome;
import kg.enesaitech.entity.SchoolType;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class SchoolTypeService {

	private static SchoolTypeService schoolTypeService = null;
	private SchoolTypeHome schoolTypeHome = SchoolTypeHome.getInstance();

	public static SchoolTypeService getInstance() {
		if (schoolTypeService == null) {
			schoolTypeService = new SchoolTypeService();
		}
		return schoolTypeService;
	}

	public boolean create(SchoolType schoolType) {
		boolean result = true;
		try {
			schoolTypeHome.persist(schoolType);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(SchoolType schoolType) {
		boolean result = true;

		try {
			schoolTypeHome.merge(schoolType);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;		
		try {
			schoolTypeHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public SchoolType get(int id) {
		
		return schoolTypeHome.findById(id);
	}
	public SearchResult<SchoolType> getList(SearchParameters searchParameters) {

		return schoolTypeHome.getList(searchParameters);
	}
	public SchoolType findById(int id) {
		return schoolTypeHome.findById(id);
	}
}
