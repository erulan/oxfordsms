package kg.enesaitech.service;

import kg.enesaitech.dao.NoteSystemHome;
import kg.enesaitech.entity.NoteSystem;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class NoteSystemService {

	private static NoteSystemService noteSystemService = null;
	private NoteSystemHome noteSystemHome = NoteSystemHome.getInstance();

	public static NoteSystemService getInstance() {
		if (noteSystemService == null) {
			noteSystemService = new NoteSystemService();
		}
		return noteSystemService;
	}

	public boolean create(NoteSystem noteSystem) {
		boolean result = true;
		try {
			noteSystemHome.persist(noteSystem);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(NoteSystem noteSystem) {
		boolean result = true;

		try {
			noteSystemHome.merge(noteSystem);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {
			noteSystemHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public NoteSystem get(int id) {
		return noteSystemHome.findById(id);
	}

	public SearchResult<NoteSystem> getList(SearchParameters searchParameters) {

		return noteSystemHome.getList(searchParameters);
	}

	public NoteSystem findById(int id) {
		return noteSystemHome.findById(id);
	}

}
