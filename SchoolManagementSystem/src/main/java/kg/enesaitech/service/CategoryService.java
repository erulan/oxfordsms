package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.BookTypeHome;
import kg.enesaitech.dao.CategoryHome;
import kg.enesaitech.entity.BookType;
import kg.enesaitech.entity.Category;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.userException.BusinessException;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

public class CategoryService {

	private static CategoryService categoryService = null;
	Mapper mapper = DozerMapperProvider.getMapper();
	private CategoryHome categoryHome = CategoryHome.getInstance();
	private BookTypeHome bookTypeHome = BookTypeHome.getInstance();

	public static CategoryService getInstance() {
		if (categoryService == null) {
			categoryService = new CategoryService();
		}
		return categoryService;
	}

	public boolean create(Category category) {
		boolean result = true;
		try {
			categoryHome.persist(category);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(Category category) {
		boolean result = true;
		try {
			categoryHome.merge(category);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.addParameter("categoryId", Integer.toString(id));
		
		SearchResult<BookType> bookTypes = bookTypeHome.getList(searchParameters);
		if(bookTypes.getTotalRecords() > 0)
			throw new BusinessException("Book Category can't be deleted when there are BookTypes of that category. Delete that Book Categories Firstly");

		try {

			categoryHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Category get(int id) {

		return categoryHome.findById(id);
	}

	public List<Category> getList(SearchParameters searchParameters) {

		return categoryHome.getList(searchParameters);
	}

	public Category findById(int id) {

		return categoryHome.findById(id);
	}

}
