
package kg.enesaitech.service;

import kg.enesaitech.dao.MaterialCategoryHome;
import kg.enesaitech.entity.MaterialCategory;
import kg.enesaitech.entity.MaterialType;
import kg.enesaitech.userException.BusinessException;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class MaterialCategoryService {

	private static MaterialCategoryService materialCategoryService = null;
	private MaterialCategoryHome materialCategoryHome = MaterialCategoryHome.getInstance();
	private MaterialTypeService materialTypeService = MaterialTypeService.getInstance();

	public static MaterialCategoryService getInstance() {
		if (materialCategoryService == null) {
			materialCategoryService = new MaterialCategoryService();
		}
		return materialCategoryService;
	}

	public boolean create(MaterialCategory materialCategory) {
		boolean result = true;
		try {
			materialCategoryHome.persist(materialCategory);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(MaterialCategory materialCategory) {
		boolean result = true;

		try {
			materialCategoryHome.merge(materialCategory);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.addParameter("categoryId", String.valueOf(id));
		
		SearchResult<MaterialType> resut = materialTypeService.getList(searchParameters);
		if(resut.getTotalRecords() > 0)
			throw new BusinessException("There is Inventory Type of this Cateogry. Delete that Inventory Type firstly");

		try {
			materialCategoryHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public MaterialCategory get(int id) {
		
		return materialCategoryHome.findById(id);
	}
	public SearchResult<MaterialCategory> getList(SearchParameters searchParameters) {

		return materialCategoryHome.getList(searchParameters);
	}
	public MaterialCategory findById(int id) {
		return materialCategoryHome.findById(id);
	}
}
