package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.TeacherCategoryHome;
import kg.enesaitech.entity.TeacherCategory;
import kg.enesaitech.vo.SearchParameters;

public class TeacherCategoryService {

	private static TeacherCategoryService teacherCategoryService = null;
	private TeacherCategoryHome teacherCategoryHome = TeacherCategoryHome.getInstance();

	public static TeacherCategoryService getInstance() {
		if (teacherCategoryService == null) {
			teacherCategoryService = new TeacherCategoryService();
		}
		return teacherCategoryService;
	}

	public TeacherCategory get(int id) {
		return teacherCategoryHome.findById(id);
	}

	public List<TeacherCategory> getList(SearchParameters searchParameters) {

		return teacherCategoryHome.getList(searchParameters);
	}

	public TeacherCategory findById(int id) {
		return teacherCategoryHome.findById(id);
	}
}
