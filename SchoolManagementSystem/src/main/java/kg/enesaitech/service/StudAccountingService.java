package kg.enesaitech.service;

import kg.enesaitech.dao.StudAccountingHome;
import kg.enesaitech.dao.YearHome;
import kg.enesaitech.entity.StudAccounting;
import kg.enesaitech.entity.Year;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class StudAccountingService {

	private static StudAccountingService studAccountingService = null;
	private StudAccountingHome studAccountingHome = StudAccountingHome
			.getInstance();
	private YearHome yearHome = YearHome.getInstance();

	public static StudAccountingService getInstance() {
		if (studAccountingService == null) {
			studAccountingService = new StudAccountingService();
		}
		return studAccountingService;
	}

	public boolean create(StudAccounting studAccounting) {
		boolean result = true;

		try {
			studAccountingHome.persist(studAccounting);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}

	public boolean update(StudAccounting studAccounting) {
		boolean result = true;

		try {

			studAccountingHome.merge(studAccounting);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {

			studAccountingHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	

	public SearchResult<StudAccounting> getList(SearchParameters searchParameters) {
		searchParameters.getSearchParameters().put("status", "1");

		return studAccountingHome.getList(searchParameters);
	}

	public StudAccounting findById(int id) {

		return studAccountingHome.findById(id);
	}
	
	public StudAccounting findByStudentId(int stud_id) {
		Year year = yearHome.getCurrentYear();
		StudAccounting studAccounting= studAccountingHome.findByStudentIdYear(stud_id, year.getId());
		
		return studAccounting;
	}
	
	public boolean updateByStudentId(int id, double fee) {
		
		boolean result = true;

		try {
			StudAccounting studAccounting= studAccountingHome.findById(id);
			if (studAccounting==null){
			result=false;
			}
			else{
				double amount = studAccounting.getPayed();
				studAccounting.setPayed(amount+fee);
				studAccountingHome.merge(studAccounting);
				
			}

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}
	

}
