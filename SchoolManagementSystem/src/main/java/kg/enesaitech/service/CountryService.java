package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.CountryHome;
import kg.enesaitech.entity.Country;
import kg.enesaitech.vo.SearchParameters;

public class CountryService {

	private static CountryService countryService = null;
	CountryHome countryHome = CountryHome.getInstance();

	public static CountryService getInstance() {
		if (countryService == null) {
			countryService = new CountryService();
		}
		return countryService;
	}

	public Country get(int id) {
		return countryHome.findById(id);
	}

	public List<Country> getList(SearchParameters searchParameters) {

		if (searchParameters.getOrderParamDesc().get("name") == null) {
			searchParameters.getOrderParamDesc().put("name", false);
		}
		return countryHome.getList(searchParameters);
	}

	public Country findById(int id) {
		return countryHome.findById(id);
	}

}
