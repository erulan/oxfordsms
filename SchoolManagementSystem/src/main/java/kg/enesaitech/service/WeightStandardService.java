package kg.enesaitech.service;

import kg.enesaitech.dao.WeightStandardHome;
import kg.enesaitech.entity.WeightStandard;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

public class WeightStandardService {

	private static WeightStandardService weightStandardService = null;

	Mapper mapper = DozerMapperProvider.getMapper();

	private WeightStandardHome weightStandardHome = WeightStandardHome.getInstance();

	public static WeightStandardService getInstance() {
		if (weightStandardService == null) {
			weightStandardService = new WeightStandardService();
		}
		return weightStandardService;
	}

	public boolean create(WeightStandard weightStandard) {
		try {

			weightStandardHome.persist(weightStandard);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean update(WeightStandard weightStandard) {
		boolean result = true;
		try {

			weightStandardHome.merge(weightStandard);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		try {

			weightStandardHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public WeightStandard findById(int id) {
		return weightStandardHome.findById(id);
	}

	public SearchResult<WeightStandard> getList(SearchParameters searchParameters) {

		return weightStandardHome.getList(searchParameters);
	}

	public WeightStandard get(int id) {
		return weightStandardHome.findById(id);
	}

}
