
package kg.enesaitech.service;

import kg.enesaitech.dao.ClassHoursHome;
import kg.enesaitech.entity.ClassHours;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

public class ClassHourService {

	private static ClassHourService classHourService = null;
	Mapper mapper = DozerMapperProvider.getMapper();
	private ClassHoursHome classHoursHome = ClassHoursHome.getInstance();

	public static ClassHourService getInstance() {
		if (classHourService == null) {
			classHourService = new ClassHourService();
		}
		return classHourService;
	}

	public boolean create(ClassHours classHours) {
		boolean result = true;
		try {

			classHoursHome.persist(classHours);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(ClassHours classHours) {
		boolean result = true;

		try {

			classHoursHome.merge(classHours);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {

			classHoursHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public ClassHours get(int id) {
		return classHoursHome.findById(id);
	}
	public SearchResult<ClassHours> getList(SearchParameters searchParameters) {

		return classHoursHome.getList(searchParameters);
	}
	public ClassHours findById(int id) {
		return classHoursHome.findById(id);
	}

}

