package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.ExTeacherHome;
import kg.enesaitech.entity.ExTeacher;
import kg.enesaitech.vo.SearchParameters;

public class ExTeacherService {

	private static ExTeacherService exTeacherService = null;
	private ExTeacherHome exTeacherHome = ExTeacherHome.getInstance();

	public static ExTeacherService getInstance() {
		if (exTeacherService == null) {
			exTeacherService = new ExTeacherService();
		}
		return exTeacherService;
	}

	public boolean create(ExTeacher exTeacher) {
		boolean result = true;
		try {
			exTeacherHome.persist(exTeacher);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(ExTeacher exTeacher) {
		boolean result = true;

		try {
			exTeacherHome.merge(exTeacher);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {
			exTeacherHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public ExTeacher get(int id) {
		return exTeacherHome.findById(id);
	}

	public List<ExTeacher> getList(SearchParameters searchParameters) {

		return exTeacherHome.getList(searchParameters);
	}

	public ExTeacher findById(int id) {
		return exTeacherHome.findById(id);
	}

}
