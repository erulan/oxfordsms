package kg.enesaitech.service;

import java.util.Set;

import kg.enesaitech.dao.RoleHome;
import kg.enesaitech.dao.StatusHome;
import kg.enesaitech.dao.StuffHome;
import kg.enesaitech.dao.UsersHome;
import kg.enesaitech.dao.UsersRoleHome;
import kg.enesaitech.entity.Status;
import kg.enesaitech.entity.Stuff;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class StuffService {

	private static StuffService stuffService = null;
	StuffHome stuffHome = StuffHome.getInstance();
	UsersHome usersHome = new UsersHome();
	UsersRoleHome usersRoleHome = new UsersRoleHome();
	StatusHome statusHome = StatusHome.getInstance();
	RoleHome roleHome = RoleHome.getInstance();
	public static StuffService getInstance() {
		if (stuffService == null) {
			stuffService = new StuffService();
		}
		return stuffService;
	}

	public boolean create(Stuff stuff) {
		boolean result = true;
		String stuffNo = "4999";
		
		Set<String> stuffNumbers = stuffHome.stuffNumList();
		
		//Stuff number must be between 1000 and 5000;
		for(int number = 1000; number < 5000; number++){
			if(!stuffNumbers.contains(Integer.toString(number))){
				stuffNo = Integer.toString(number);
				break;
			}
		}

		stuff.setStuffNum(stuffNo);
		Status status =statusHome.getStatusByName("active");
		stuff.setStatus(status);
		try {
			stuffHome.persist(stuff);
			
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}

	public boolean update(Stuff stuff) {
		boolean result = true;

		try {

			Status status =statusHome.getStatusByName("active");
			stuff.setStatus(status);
			stuffHome.merge(stuff);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {

			Stuff stuff = stuffHome.findById(id);
			Status status =statusHome.getStatusByName("passive");
			stuff.setStatus(status);
			stuffHome.merge(stuff);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Stuff get(int id) {

		return stuffHome.findById(id);
	}

	public SearchResult<Stuff> getList(SearchParameters searchParameters) {
		searchParameters.getSearchParameters().put("status", "1");
		return stuffHome.getList(searchParameters);
	}

	public Stuff findById(Integer id) {
		return stuffHome.findById(id);
	}

	public Stuff getByStuffNum(String stuffNum) {
		return stuffHome.getByStuffNum(stuffNum);
	}

}
