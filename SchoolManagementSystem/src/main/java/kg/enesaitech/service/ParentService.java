package kg.enesaitech.service;

import kg.enesaitech.dao.ParentsHome;
import kg.enesaitech.entity.Parents;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class ParentService {

	private static ParentService parentService = null;
	private ParentsHome parentsHome = ParentsHome.getInstance();

	public static ParentService getInstance() {
		if (parentService == null) {
			parentService = new ParentService();
		}
		return parentService;
	}

	public boolean create(Parents parents) {
		boolean result = true;
		try {
			parentsHome.persist(parents);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(Parents parents) {
		boolean result = true;

		try {
			parentsHome.merge(parents);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {
			parentsHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Parents get(int id) {
		return parentsHome.findById(id);
	}

	public SearchResult<Parents> getList(SearchParameters searchParameters) {

		return parentsHome.getList(searchParameters);
	}

	public Parents findById(int id) {
		return parentsHome.findById(id);
	}

}
