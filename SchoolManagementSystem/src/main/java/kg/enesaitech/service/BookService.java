
package kg.enesaitech.service;

import kg.enesaitech.dao.BookHome;
import kg.enesaitech.dao.BookStatusHome;
import kg.enesaitech.entity.Book;
import kg.enesaitech.entity.BookStatus;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class BookService {

	private static BookService bookService = null;
	private BookHome bookHome = BookHome.getInstance();
	private BookStatusHome bookStatusHome = BookStatusHome.getInstance();

	public static BookService getInstance() {
		if (bookService == null) {
			bookService = new BookService();
		}
		return bookService;
	}

	public boolean create(Book book) {
		boolean result = true;
		try {
			bookHome.persist(book);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(Book book) {
		boolean result = true;

		try {
			bookHome.merge(book);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {			
			bookHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Book get(int id) {
		
		return bookHome.findById(id);
	}
	public SearchResult<Book> getList(SearchParameters searchParameters) {
		return bookHome.getList(searchParameters);
	}
	public Book findById(int id) {
		return bookHome.findById(id);
	}
}
