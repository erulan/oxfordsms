package kg.enesaitech.service;

import kg.enesaitech.dao.CheckPeriodHome;
import kg.enesaitech.entity.CheckPeriod;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

public class CheckPeriodService {

	private static CheckPeriodService checkPeriodService = null;

	Mapper mapper = DozerMapperProvider.getMapper();

	CheckPeriodHome checkPeriodHome = CheckPeriodHome.getInstance();
	
	public static CheckPeriodService getInstance() {
		if (checkPeriodService == null) {
			checkPeriodService = new CheckPeriodService();
		}
		return checkPeriodService;
	}

	public boolean create(CheckPeriod checkPeriod) {
		checkPeriodHome.persist(checkPeriod);
		return true;
	}

	public boolean update(CheckPeriod checkPeriod) {
		boolean result = true;
		try {

			checkPeriodHome.merge(checkPeriod);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		try {

			checkPeriodHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public CheckPeriod findById(int id) {
		return checkPeriodHome.findById(id);
	}

	public SearchResult<CheckPeriod> getList(SearchParameters searchParameters) {

		return checkPeriodHome.getList(searchParameters);
	}

	public CheckPeriod get(int id) {
		return checkPeriodHome.findById(id);
	}

}
