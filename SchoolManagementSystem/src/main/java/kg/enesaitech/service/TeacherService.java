package kg.enesaitech.service;

import java.util.Set;

import kg.enesaitech.dao.RoleHome;
import kg.enesaitech.dao.StatusHome;
import kg.enesaitech.dao.TeacherAwardHome;
import kg.enesaitech.dao.TeacherHome;
import kg.enesaitech.dao.UsersHome;
import kg.enesaitech.dao.UsersRoleHome;
import kg.enesaitech.entity.Role;
import kg.enesaitech.entity.Status;
import kg.enesaitech.entity.Teacher;
import kg.enesaitech.entity.TeacherAward;
import kg.enesaitech.entity.Users;
import kg.enesaitech.entity.UsersRole;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

public class TeacherService {

	private static TeacherService teacherService = null;
	private TeacherHome teacherHome = TeacherHome.getInstance();
	StatusHome statusHome = StatusHome.getInstance();
	UsersHome usersHome = UsersHome.getInstance();
	RoleHome roleHome = RoleHome.getInstance();
	TeacherAwardHome teacherAwardHome = TeacherAwardHome.getInstance();
	UsersRoleHome usersRoleHome = UsersRoleHome.getInstance();

	Mapper mapper = DozerMapperProvider.getMapper();

	public static TeacherService getInstance() {
		if (teacherService == null) {
			teacherService = new TeacherService();
		}
		return teacherService;
	}

	public boolean create(Teacher teacher) {
		boolean result = true;
		
		String teacherNo = "9999";
		
		Set<String> teacherNumbers = teacherHome.teacherNumList();
		
		//Student number must be between 10000 and 100000;
		for(int number = 5000; number < 10000; number++){
			if(!teacherNumbers.contains(Integer.toString(number))){
				teacherNo = Integer.toString(number);
				break;
			}
		}

		teacher.setTeacherNum(teacherNo);
		Users user = new Users();
		user.setUserName(teacherNo);
		user.setUserPass(teacherNo + "12345");
		user.setStatus("active");
		
		UsersRole usersRole = new UsersRole();

		if (teacher.getTeacherAward() != null) {
			TeacherAward teacherAward = new TeacherAward();
			if (teacher.getTeacherAward().getId() == null
					|| teacherAwardHome.findById(teacher.getTeacherAward().getId()) == null) {
				teacher.getTeacherAward().setId(null);
				teacherAward = teacher.getTeacherAward();
				Integer id = teacherAwardHome.persist(teacherAward);
				teacher.getTeacherAward().setId(id);
			}
		}
		try {
			Status status =statusHome.getStatusByName("active");
			teacher.setStatus(status);
			teacherHome.persist(teacher);
			
			Integer userId = usersHome.persist(user);
			
			usersRole.setUsers(user);
			usersRole.getUsers().setId(userId);
			Role role = roleHome.getRoleByName("teacher");
			usersRole.setRole(role);
			usersRoleHome.persist(usersRole);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(Teacher teacher) {
		boolean result = true;

		try {
			if (teacher.getTeacherAward() != null){
				if (teacher.getTeacherAward().getId() != null) {
					TeacherAward teacherAward = teacherAwardHome.findById(teacher.getTeacherAward()
							.getId());
					if (teacherAward != null) {
						teacherAwardHome.merge(teacher.getTeacherAward());
					} else {
						teacher.getTeacherAward().setId(null);
						teacherAwardHome.persist(teacher.getTeacherAward());
					}
				}else{
					teacherAwardHome.persist(teacher.getTeacherAward());					
				}
			}
			teacherHome.merge(teacher);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {

			Teacher teacher = teacherHome.findById(id);
			Status status =statusHome.getStatusByName("passive");
			teacher.setStatus(status);
			teacherHome.merge(teacher);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Teacher get(int id) {
		return teacherHome.findById(id);
	}

	public SearchResult<Teacher> getList(SearchParameters searchParameters) {
		searchParameters.getSearchParameters().put("status", "1");
		return teacherHome.getList(searchParameters);
	}

	public Teacher findById(Integer id) {
		return teacherHome.findById(id);
	}

	public Teacher getByTeacherNum(String teacherNum) {
		return teacherHome.getByTeacherNum(teacherNum);
	}
}