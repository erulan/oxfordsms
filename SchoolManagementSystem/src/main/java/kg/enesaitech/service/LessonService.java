package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.LessonHome;
import kg.enesaitech.entity.Lesson;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

public class LessonService {

	private static LessonService lessonService = null;

	Mapper mapper = DozerMapperProvider.getMapper();

	private LessonHome lessonHome = LessonHome.getInstance();

	public static LessonService getInstance() {
		if (lessonService == null) {
			lessonService = new LessonService();
		}
		return lessonService;
	}

	public boolean create(Lesson lesson) {
		try {

			lessonHome.persist(lesson);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean update(Lesson lesson) {
		boolean result = true;
		try {

			lessonHome.merge(lesson);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		try {

			lessonHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Lesson findById(int id) {
		return lessonHome.findById(id);
	}

	public List<Lesson> getList(SearchParameters searchParameters) {

		return lessonHome.getList(searchParameters);
	}

	public Lesson get(int id) {
		return lessonHome.findById(id);
	}

}
