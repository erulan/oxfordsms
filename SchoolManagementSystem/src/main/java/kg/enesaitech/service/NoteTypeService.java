package kg.enesaitech.service;

import kg.enesaitech.dao.NoteTypeHome;
import kg.enesaitech.dao.StatusHome;
import kg.enesaitech.entity.NoteType;
import kg.enesaitech.entity.Status;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class NoteTypeService {

	private static NoteTypeService noteTypeService = null;
	private NoteTypeHome noteTypeHome = NoteTypeHome.getInstance();
	private StatusHome statusHome = StatusHome.getInstance();

	public static NoteTypeService getInstance() {
		if (noteTypeService == null) {
			noteTypeService = new NoteTypeService();
		}
		return noteTypeService;
	}

	public boolean create(NoteType noteType) {
		boolean result = true;
		try {
			Status status=statusHome.getStatusByName("active");
			noteType.setStatus(status);
			noteTypeHome.persist(noteType);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean update(NoteType noteType) {
		boolean result = true;

		try {
			Status status=statusHome.getStatusByName("active");
			noteType.setStatus(status);
			noteTypeHome.merge(noteType);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {
			noteTypeHome.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public NoteType get(int id) {
		return noteTypeHome.findById(id);
	}

	public SearchResult<NoteType> getList(SearchParameters searchParameters) {

		return noteTypeHome.getList(searchParameters);
	}

	public NoteType findById(int id) {
		return noteTypeHome.findById(id);
	}

}
