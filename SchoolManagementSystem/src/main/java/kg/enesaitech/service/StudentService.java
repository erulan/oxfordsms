package kg.enesaitech.service;

import java.util.Set;

import kg.enesaitech.dao.ParentsHome;
import kg.enesaitech.dao.RoleHome;
import kg.enesaitech.dao.StatusHome;
import kg.enesaitech.dao.StudAccountingHome;
import kg.enesaitech.dao.StudentHome;
import kg.enesaitech.dao.UsersHome;
import kg.enesaitech.dao.UsersRoleHome;
import kg.enesaitech.dao.YearHome;
import kg.enesaitech.entity.Parents;
import kg.enesaitech.entity.Role;
import kg.enesaitech.entity.Status;
import kg.enesaitech.entity.StudAccounting;
import kg.enesaitech.entity.Student;
import kg.enesaitech.entity.Users;
import kg.enesaitech.entity.UsersRole;
import kg.enesaitech.entity.Year;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class StudentService {

	private static StudentService studentService = null;
	StudentHome studentHome = StudentHome.getInstance();
	ParentsHome parentsHome = ParentsHome.getInstance();
	UsersHome usersHome = UsersHome.getInstance();
	RoleHome roleHome = RoleHome.getInstance();
	UsersRoleHome usersRoleHome = UsersRoleHome.getInstance();
	StatusHome statusHome = StatusHome.getInstance();
	StudAccountingHome studAccountingHome = StudAccountingHome.getInstance();
	YearHome yearhome = YearHome.getInstance();

	public static StudentService getInstance() {
		if (studentService == null) {
			studentService = new StudentService();
		}
		return studentService;
	}

	public boolean create(Student student) {
		boolean result = true;

		String studentNo = "99999";

		Set<String> studentNumbers = studentHome.studentNumList();

		// Student number must be between 10000 and 100000;
		for (int number = 10000; number < 100000; number++) {
			if (!studentNumbers.contains(Integer.toString(number))) {
				studentNo = Integer.toString(number);
				break;
			}
		}

		student.setStudentNum(studentNo);
		Users user = new Users();
		user.setUserName(studentNo);
		user.setUserPass(studentNo + "12345");
		user.setStatus("active");

		UsersRole usersRole = new UsersRole();

		if (student.getParents() != null) {
			Parents parents = new Parents();
			if (student.getParents().getId() == null
					|| parentsHome.findById(student.getParents().getId()) == null) {
				student.getParents().setId(null);
				parents = student.getParents();
				Integer id = parentsHome.persist(parents);
				student.getParents().setId(id);
			}
		}
		Integer studId=0;
		try {
			studId = studentHome.persist(student);
			Integer userId = usersHome.persist(user);

			usersRole.setUsers(user);
			usersRole.getUsers().setId(userId);
			Role role = roleHome.getRoleByName("student");
			usersRole.setRole(role);
			usersRoleHome.persist(usersRole);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		try {
			StudAccounting studAccounting = new StudAccounting();
			studAccounting.setStudent(student);
			studAccounting.getStudent().setId(studId);
			studAccounting.setAnualFee(0);
			studAccounting.setPayed(0);

			studAccounting.setYear(yearhome.getCurrentYear());
			studAccountingHome.persist(studAccounting);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}

	public boolean update(Student student) {
		boolean result = true;

		try {
			if (student.getParents() != null){
				if(student.getParents().getId() != null) {
					Parents parent = parentsHome.findById(student.getParents()
							.getId());
					if (parent != null) {
						parentsHome.merge(student.getParents());
					} else {
						student.getParents().setId(null);
						parentsHome.persist(student.getParents());
					}
				} else{
					parentsHome.persist(student.getParents());
				}
			}
			studentHome.merge(student);
			Year currentYear = yearhome.getCurrentYear();
			StudAccounting studAccounting = studAccountingHome.findByStudentIdYear(student.getId(), currentYear.getId());
			if(studAccounting==null){
				studAccounting = new StudAccounting();
				studAccounting.setStudent(student);
				studAccounting.setAnualFee(0);
				studAccounting.setPayed(0);
				studAccounting.setYear(currentYear);
				studAccountingHome.persist(studAccounting);
			}					 
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;

		try {

			Student student = studentHome.findById(id);
			Status status = statusHome.getStatusByName("passive");
			student.setStatus(status);
			studentHome.merge(student);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public Student get(int id) {

		return studentHome.findById(id);
	}

	public SearchResult<Student> getList(SearchParameters searchParameters) {
		searchParameters.getSearchParameters().put("status", "1");
		if(searchParameters.getSearchParameters().get("schoolTypeId")==null){
			searchParameters.getSearchParameters().put("schoolTypeId", "1");
		}
		return studentHome.getList(searchParameters);
	}
	
	public Student findById(Integer id) {
		return studentHome.findById(id);
	}

	public Student getByStudNum(String studNum) {
		return studentHome.getByStudNum(studNum);
	}
}
