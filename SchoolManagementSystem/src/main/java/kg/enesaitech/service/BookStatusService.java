package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.BookStatusHome;
import kg.enesaitech.entity.Book;
import kg.enesaitech.entity.BookStatus;
import kg.enesaitech.userException.BusinessException;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

public class BookStatusService {
	
	private static BookStatusService bookStatusService = null;
	BookStatusHome bookStatusHome=BookStatusHome.getInstance();
	BookService bookService = BookService.getInstance();
	
	public static BookStatusService getInstance(){
		if(bookStatusService == null){
			bookStatusService = new BookStatusService();
		}
		return bookStatusService;
	}

	public BookStatus get(int id) {
		return bookStatusHome.findById(id);
	}
	public List<BookStatus> getList(SearchParameters searchParameters) {

		return bookStatusHome.getList(searchParameters);
	}
	public BookStatus findById(Integer id) {
		return bookStatusHome.findById(id);
	}

	public Boolean create(BookStatus bookStatus) {
		boolean result = true;
		try {
			bookStatusHome.persist(bookStatus);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		
		return result;
	}
	
	public boolean update(BookStatus bookStatus) {
		boolean result = true;
		try {
			bookStatusHome.merge(bookStatus);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.addParameter("bookStatusId", String.valueOf(id));
		
		SearchResult<Book> searchResult = bookService.getList(searchParameters);
		if(searchResult.getTotalRecords() > 0)
			throw new BusinessException("Delete books that has this Status firstly.");

		try {

			bookStatusHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

}
