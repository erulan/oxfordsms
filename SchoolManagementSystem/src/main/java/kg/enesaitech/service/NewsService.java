package kg.enesaitech.service;

import kg.enesaitech.dao.NewsHome;
import kg.enesaitech.dao.YearHome;
import kg.enesaitech.entity.News;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

public class NewsService {

	private static NewsService newsService = null;

	Mapper mapper = DozerMapperProvider.getMapper();

	NewsHome newsHome = NewsHome.getInstance();
	YearHome yearhome = YearHome.getInstance();
	
	public static NewsService getInstance() {
		if (newsService == null) {
			newsService = new NewsService();
		}
		return newsService;
	}

	public boolean create(News news) {
		newsHome.persist(news);
		return true;
	}

	public boolean update(News news) {
		boolean result = true;
		try {

			newsHome.merge(news);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		try {

			newsHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public News findById(int id) {
		return newsHome.findById(id);
	}

	public SearchResult<News> getList(SearchParameters searchParameters) {

		return newsHome.getList(searchParameters);
	}

	public News get(int id) {
		return newsHome.findById(id);
	}

}
