package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.RegionHome;
import kg.enesaitech.entity.Region;
import kg.enesaitech.vo.SearchParameters;

public class RegionService {

	private static RegionService regionService = null;
	private RegionHome regionHome = RegionHome.getInstance();

	public static RegionService getInstance() {
		if (regionService == null) {
			regionService = new RegionService();
		}
		return regionService;
	}

	public Region get(int id) {
		return regionHome.findById(id);
	}

	public List<Region> getList(SearchParameters searchParameters) {

		return regionHome.getList(searchParameters);
	}

	public Region findById(int id) {
		return regionHome.findById(id);
	}

}
