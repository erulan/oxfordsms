package kg.enesaitech.service;

import java.util.List;

import kg.enesaitech.dao.MaterialStatusHome;
import kg.enesaitech.entity.MaterialStatus;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

public class MaterialStatusService {

	private static MaterialStatusService materialStatusService = null;

	Mapper mapper = DozerMapperProvider.getMapper();

	private MaterialStatusHome materialStatusHome = MaterialStatusHome.getInstance();

	public static MaterialStatusService getInstance() {
		if (materialStatusService == null) {
			materialStatusService = new MaterialStatusService();
		}
		return materialStatusService;
	}

	public boolean create(MaterialStatus materialStatus) {
		try {

			materialStatusHome.persist(materialStatus);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean update(MaterialStatus materialStatus) {
		boolean result = true;
		try {

			materialStatusHome.merge(materialStatus);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		try {

			materialStatusHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public MaterialStatus findById(int id) {
		return materialStatusHome.findById(id);
	}

	public List<MaterialStatus> getList(SearchParameters searchParameters) {

		return materialStatusHome.getList(searchParameters);
	}

	public MaterialStatus get(int id) {
		return materialStatusHome.findById(id);
	}

}
