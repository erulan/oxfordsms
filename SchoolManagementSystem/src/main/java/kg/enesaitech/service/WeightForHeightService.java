package kg.enesaitech.service;

import kg.enesaitech.dao.WeightForHeightHome;
import kg.enesaitech.entity.WeightForHeight;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

public class WeightForHeightService {

	private static WeightForHeightService weightForHeightService = null;

	Mapper mapper = DozerMapperProvider.getMapper();

	private WeightForHeightHome weightForHeightHome = WeightForHeightHome.getInstance();

	public static WeightForHeightService getInstance() {
		if (weightForHeightService == null) {
			weightForHeightService = new WeightForHeightService();
		}
		return weightForHeightService;
	}

	public boolean create(WeightForHeight weightForHeight) {
		try {

			weightForHeightHome.persist(weightForHeight);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean update(WeightForHeight weightForHeight) {
		boolean result = true;
		try {

			weightForHeightHome.merge(weightForHeight);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean delete(int id) {
		boolean result = true;
		try {

			weightForHeightHome.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public WeightForHeight findById(int id) {
		return weightForHeightHome.findById(id);
	}

	public SearchResult<WeightForHeight> getList(SearchParameters searchParameters) {

		return weightForHeightHome.getList(searchParameters);
	}

	public WeightForHeight get(int id) {
		return weightForHeightHome.findById(id);
	}

}
