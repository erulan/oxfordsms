package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;

public class CountryVO {

	private Integer id;
	private String name;
	private String code;
	private String zip;
	private Set<TeacherVO> teachers = new HashSet<TeacherVO>(0);
	private Set<StudentVO> students = new HashSet<StudentVO>(0);

	public CountryVO() {
	}

	public CountryVO(String name, String code, String zip, Set<TeacherVO> teachers,
			Set<StudentVO> students) {
		this.name = name;
		this.code = code;
		this.zip = zip;
		this.teachers = teachers;
		this.students = students;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	
	public Set<TeacherVO> getTeachers() {
		return this.teachers;
	}

	public void setTeachers(Set<TeacherVO> teachers) {
		this.teachers = teachers;
	}

	
	public Set<StudentVO> getStudents() {
		return this.students;
	}

	public void setStudents(Set<StudentVO> students) {
		this.students = students;
	}

}
