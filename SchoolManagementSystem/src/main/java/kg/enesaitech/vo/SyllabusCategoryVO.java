package kg.enesaitech.vo;


public class SyllabusCategoryVO {

	private Integer id;
	private CourseVO course;
	private QuarterVO quarter;
	private String topic;
	private int lessonNo;
	
	public SyllabusCategoryVO() {
	}

	public SyllabusCategoryVO(CourseVO course, int lessonNo) {
		this.course = course;
		this.lessonNo = lessonNo;
	}

	public SyllabusCategoryVO(CourseVO course, QuarterVO quarter, String topic, int lessonNo) {
		this.course = course;
		this.quarter=quarter;
		this.topic = topic;
		this.lessonNo = lessonNo;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CourseVO getCourse() {
		return this.course;
	}

	public void setCourse(CourseVO course) {
		this.course = course;
	}
	
	public QuarterVO getQuarter() {
		return this.quarter;
	}

	public void setQuarter(QuarterVO quarter) {
		this.quarter = quarter;
	}

	public String getTopic() {
		return this.topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public int getLessonNo() {
		return this.lessonNo;
	}

	public void setLessonNo(int lessonNo) {
		this.lessonNo = lessonNo;
	}
	
}
