package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;

public class ClassLetterVO {

	private Integer id;
	private String letter;
	private Set<ClassGroupVO> classGroups = new HashSet<ClassGroupVO>(0);

	public ClassLetterVO() {
	}

	public ClassLetterVO(String letter, Set<ClassGroupVO> CGroups) {
		this.letter = letter;
		this.classGroups = CGroups;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getLetter() {
		return this.letter;
	}

	public void setLetter(String letter) {
		this.letter = letter;
	}


	public Set<ClassGroupVO> getCGroups() {
		return this.classGroups;
	}

	public void setCGroups(Set<ClassGroupVO> CGroups) {
		this.classGroups = CGroups;
	}

}
