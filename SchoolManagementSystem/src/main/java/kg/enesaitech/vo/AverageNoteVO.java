package kg.enesaitech.vo;


public class AverageNoteVO {

	private CourseVO course;
	private int totalInClass;
	private int five;
	private int four;
	private int three;
	private int withOneThree;
	private int withTwoThree;
	private int two;
	private Double quantity;
	private Double progress;
	
	public AverageNoteVO() {
	}

	

	public CourseVO getCourse() {
		return this.course;
	}

	public void setCourse(CourseVO course) {
		this.course = course;
	}



	public int getTotalInClass() {
		return totalInClass;
	}



	public void setTotalInClass(int totalInClass) {
		this.totalInClass = totalInClass;
	}



	public int getFive() {
		return five;
	}



	public void setFive(int five) {
		this.five = five;
	}



	public int getFour() {
		return four;
	}



	public void setFour(int four) {
		this.four = four;
	}



	public int getThree() {
		return three;
	}



	public void setThree(int three) {
		this.three = three;
	}



	public int getWithOneThree() {
		return withOneThree;
	}



	public void setWithOneThree(int withOneThree) {
		this.withOneThree = withOneThree;
	}



	public int getWithTwoThree() {
		return withTwoThree;
	}



	public void setWithTwoThree(int withTwoThree) {
		this.withTwoThree = withTwoThree;
	}



	public int getTwo() {
		return two;
	}



	public void setTwo(int two) {
		this.two = two;
	}



	public Double getQuantity() {
		return quantity;
	}



	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}



	public Double getProgress() {
		return progress;
	}



	public void setProgress(Double progress) {
		this.progress = progress;
	}


	
	
}
