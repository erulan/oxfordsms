package kg.enesaitech.vo;


public class UsersRoleVO {

	private Integer id;
	private RoleVO role;
	private UsersVO users;

	public UsersRoleVO() {
	}

	public UsersRoleVO(RoleVO role, UsersVO users) {
		this.role = role;
		this.users = users;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RoleVO getRole() {
		return this.role;
	}

	public void setRole(RoleVO role) {
		this.role = role;
	}

	public UsersVO getUsers() {
		return this.users;
	}

	public void setUsers(UsersVO users) {
		this.users = users;
	}

}