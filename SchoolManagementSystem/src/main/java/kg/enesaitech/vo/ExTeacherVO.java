package kg.enesaitech.vo;

import java.util.Date;

public class ExTeacherVO {

	private Integer id;
	private CourseVO course;
	private TeacherVO teacher;
	private Date beginDate;
	private Date endDate;

	public ExTeacherVO() {
	}

	public ExTeacherVO(CourseVO course, TeacherVO teacher, Date beginDate,
			Date endDate) {
		this.course = course;
		this.teacher = teacher;
		this.beginDate = beginDate;
		this.endDate = endDate;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public CourseVO getCourse() {
		return this.course;
	}

	public void setCourse(CourseVO course) {
		this.course = course;
	}


	public TeacherVO getTeacher() {
		return this.teacher;
	}

	public void setTeacher(TeacherVO teacher) {
		this.teacher = teacher;
	}


	public Date getBeginDate() {
		return this.beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
