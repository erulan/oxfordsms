package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;

public class LessonVO {

	private Integer id;
	private String name;
	private String description;
	private Set<CourseVO> courses = new HashSet<CourseVO>(0);

	public LessonVO() {
	}

	public LessonVO(String name, String description, Set<CourseVO> courses) {
		this.name = name;
		this.description = description;
		this.courses = courses;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Set<CourseVO> getCourses() {
		return this.courses;
	}

	public void setCourses(Set<CourseVO> courses) {
		this.courses = courses;
	}

}
