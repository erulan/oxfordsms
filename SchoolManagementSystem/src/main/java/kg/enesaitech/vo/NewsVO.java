package kg.enesaitech.vo;

import java.util.Date;



public class NewsVO {

	private Integer id;
	private String title;
	private String text;
	private boolean isExpired;
	private Date date;
	
	public NewsVO() {
	}

	public NewsVO(String title, String text, boolean isExpired, Date date) {
		this.title = title;
		this.text= text;
		this.isExpired = isExpired;
		this.date = date;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean getIsExpired() {
		return this.isExpired;
	}

	public void setIsExpired(boolean isExpired) {
		this.isExpired = isExpired;
	}
	
	public String getText(){
		return this.text;
	}
	public void setText(String text){
		this.text = text;
	}
	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date= date;
	}

}
