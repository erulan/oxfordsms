package kg.enesaitech.vo;

public class UserInfoVO {
	public Integer id;
	public String nameEn;
	public String surnameEn;
	public String nameRu;
	public String surnameRu;
	public String username;
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNameEn() {
		return nameEn;
	}
	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}
	public String getSurnameEn() {
		return surnameEn;
	}
	public void setSurnameEn(String surname) {
		this.surnameEn = surname;
	}
	public String getNameRu() {
		return nameRu;
	}
	public void setNameRu(String nameRu) {
		this.nameRu = nameRu;
	}
	public String getSurnameRu() {
		return surnameRu;
	}
	public void setSurnameRu(String surnameRu) {
		this.surnameRu = surnameRu;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
