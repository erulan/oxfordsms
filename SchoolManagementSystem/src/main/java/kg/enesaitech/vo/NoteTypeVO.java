package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;

/**
 * Region generated by hbm2java
 */
public class NoteTypeVO {
	/**
	 * 
	 */
	private Integer id;
	private String name;
	private Double weight;
	private StatusVO status;
	private Set<NoteAttendanceVO> noteAttendances = new HashSet<NoteAttendanceVO>(0);
	
	
	public NoteTypeVO() {
	}

	public NoteTypeVO(String name, Double weight, StatusVO status, Set<NoteAttendanceVO> noteAttendances) {
		this.name = name;
		this.weight = weight;
		this.status=status;
		this.noteAttendances = noteAttendances;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Double getWeight() {
		return this.weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public StatusVO getStatus() {
		return this.status;
	}

	public void setStatus(StatusVO status) {
		this.status = status;
	}


	public Set<NoteAttendanceVO> getNoteAttendances() {
		return this.noteAttendances;
	}

	public void setNoteAttendances(Set<NoteAttendanceVO> noteAttendances) {
		this.noteAttendances = noteAttendances;
	}



}