package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;

public class ClassGroupVO {
	private Integer id;
	private ClassLetterVO classLetter;
	private GradeVO grade;
	private TeacherVO teacher;
	private String room;
	private String description;
	private ClassHourTypeVO classHourType;
	private Set<StudentVO> students = new HashSet<StudentVO>(0);
	private Set<CourseVO> courses = new HashSet<CourseVO>(0);

	public ClassGroupVO() {
	}

	public ClassGroupVO(ClassLetterVO classLetter, GradeVO grade, TeacherVO teacher) {
		this.classLetter = classLetter;
		this.grade = grade;
		this.teacher = teacher;
	}

	public ClassGroupVO(ClassLetterVO classLetter, GradeVO grade,
			TeacherVO teacher, String room, String description, Set<StudentVO> students,
			Set<CourseVO> courses, ClassHourTypeVO classHourType) {
		this.classLetter = classLetter;
		this.grade = grade;
		this.teacher = teacher;
		this.room = room;
		this.description = description;
		this.students = students;
		this.courses = courses;
		this.classHourType = classHourType;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public ClassLetterVO getClassLetter() {
		return this.classLetter;
	}

	public void setClassLetter(ClassLetterVO classLetter) {
		this.classLetter = classLetter;
	}


	public GradeVO getGrade() {
		return this.grade;
	}

	public void setGrade(GradeVO grade) {
		this.grade = grade;
	}


	public TeacherVO getTeacher() {
		return this.teacher;
	}

	public void setTeacher(TeacherVO teacher) {
		this.teacher = teacher;
	}

	
	public String getRoom() {
		return this.room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Set<StudentVO> getStudents() {
		return this.students;
	}

	public void setStudents(Set<StudentVO> students) {
		this.students = students;
	}


	public Set<CourseVO> getCourses() {
		return this.courses;
	}

	public void setCourses(Set<CourseVO> courses) {
		this.courses = courses;
	}

	public ClassHourTypeVO getClassHourType() {
		return classHourType;
	}

	public void setClassHourType(ClassHourTypeVO classHourType) {
		this.classHourType = classHourType;
	}

}
