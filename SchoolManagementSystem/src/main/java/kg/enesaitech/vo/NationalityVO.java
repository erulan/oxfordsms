package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;

public class NationalityVO {

	private Integer id;
	private String country;
	private String nationality;
	private Set<StudentVO> students = new HashSet<StudentVO>(0);
	private Set<TeacherVO> teachers = new HashSet<TeacherVO>(0);

	public NationalityVO() {
	}

	public NationalityVO(String country, String nationality, Set<StudentVO> students,
			Set<TeacherVO> teachers) {
		this.country = country;
		this.nationality = nationality;
		this.students = students;
		this.teachers = teachers;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}


	public String getNationality() {
		return this.nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}


	public Set<StudentVO> getStudents() {
		return this.students;
	}

	public void setStudents(Set<StudentVO> students) {
		this.students = students;
	}


	public Set<TeacherVO> getTeachers() {
		return this.teachers;
	}

	public void setTeachers(Set<TeacherVO> teachers) {
		this.teachers = teachers;
	}

}
