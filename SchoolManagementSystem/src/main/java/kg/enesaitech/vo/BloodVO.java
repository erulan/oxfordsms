package kg.enesaitech.vo;

// Generated Sep 9, 2014 9:24:47 PM by Hibernate Tools 4.3.1



public class BloodVO {

	private Integer id;
	private String code;

	public BloodVO() {
	}

	public BloodVO(String code) {
		this.code = code;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}