package kg.enesaitech.vo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class SyllabusVO {

	private Integer id;
	private SyllabusCategoryVO syllabusCategory;
	private String title;
	private boolean completed;
	private String description;
	private Date plannedDate;
	private Date completedDate;
	private Set<NoteAttendanceVO> noteAttendances = new HashSet<NoteAttendanceVO>(0);

	public SyllabusVO() {
	}

	public SyllabusVO(boolean completed) {
		this.completed = completed;
	}

	public SyllabusVO(SyllabusCategoryVO syllabusCategoryVO, String title,
			boolean completed, String description, Date plannedDate, Date completedDate,
			Set<NoteAttendanceVO> noteAttendances) {
		this.syllabusCategory=syllabusCategoryVO;
		this.title = title;
		this.completed = completed;
		this.description = description;
		this.plannedDate = plannedDate;
		this.completedDate = completedDate;
		this.noteAttendances = noteAttendances;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public SyllabusCategoryVO getSyllabusCategory() {
		return this.syllabusCategory;
	}

	public void setSyllabusCategory(SyllabusCategoryVO syllabusCategory) {
		this.syllabusCategory = syllabusCategory;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isCompleted() {
		return this.completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getPlannedDate() {
		return this.plannedDate;
	}

	public void setPlannedDate(Date plannedDate) {
		this.plannedDate = plannedDate;
	}

	public Date getCompletedDate() {
		return this.completedDate;
	}

	public void setCompletedDate(Date completedDate) {
		this.completedDate = completedDate;
	}

	public Set<NoteAttendanceVO> getNoteAttendances() {
		return this.noteAttendances;
	}

	public void setNoteAttendances(Set<NoteAttendanceVO> noteAttendances) {
		this.noteAttendances = noteAttendances;
	}

}
