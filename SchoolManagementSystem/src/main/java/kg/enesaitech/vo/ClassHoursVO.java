package kg.enesaitech.vo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ClassHoursVO {

	private Integer id;
	private Integer orderNum;
	private Date startHour;
	private Date endHour;
	private ClassHourTypeVO classHourType;
	private Set<TimetableVO> timetables = new HashSet<TimetableVO>(0);

	public ClassHoursVO() {
	}

	public ClassHoursVO(Integer id, Integer orderNum, Date startHour, Date endHour, ClassHourTypeVO classHourType) {
		this.id = id;
		this.orderNum = orderNum;
		this.startHour = startHour;
		this.endHour = endHour;
	}

	public ClassHoursVO(Integer id, Integer orderNum, Date startHour, Date endHour,
			Set<TimetableVO> timetables, ClassHourTypeVO classHourType) {
		this.id = id;
		this.orderNum = orderNum;
		this.startHour = startHour;
		this.endHour = endHour;
		this.timetables = timetables;
		this.classHourType = classHourType;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public Integer getOrderNum() {
		return this.orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}


	public Date getStartHour() {
		return this.startHour;
	}

	public void setStartHour(Date startHour) {
		this.startHour = startHour;
	}


	public Date getEndHour() {
		return this.endHour;
	}

	public void setEndHour(Date endHour) {
		this.endHour = endHour;
	}


	public Set<TimetableVO> getTimetables() {
		return this.timetables;
	}

	public void setTimetables(Set<TimetableVO> timetables) {
		this.timetables = timetables;
	}

	public ClassHourTypeVO getClassHourType() {
		return classHourType;
	}

	public void setClassHourType(ClassHourTypeVO classHourType) {
		this.classHourType = classHourType;
	}

}
