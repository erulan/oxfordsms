package kg.enesaitech.vo;

// Generated Sep 9, 2014 9:24:47 PM by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;

/**
 * Book generated by hbm2java
 */

public class BookVO {
	
	/**
	 * 
	 */

	private Integer id;
	private BookStatusVO bookStatus;
	private BookTypeVO bookType;
	private Integer invenNo;
	private String barcode;
	private Integer createdActNo;
	private Integer deletedActNo;
	private Set<BookInUseVO> bookInUses = new HashSet<BookInUseVO>(0);

	public BookVO() {
	}

	
	public BookVO(BookStatusVO bookStatus, BookTypeVO bookType, Integer invenNo,
			String barcode, Integer createdActNo, Integer deletedActNo, 
			Set<BookInUseVO> bookInUses) {
		this.bookStatus = bookStatus;
		this.bookType = bookType;
		this.invenNo = invenNo;
		this.barcode = barcode;
		this.createdActNo = createdActNo;
		this.deletedActNo = deletedActNo;
		this.bookInUses = bookInUses;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public BookStatusVO getBookStatus() {
		return this.bookStatus;
	}

	public void setBookStatus(BookStatusVO bookStatus) {
		this.bookStatus = bookStatus;
	}

	
	public BookTypeVO getBookType() {
		return this.bookType;
	}

	public void setBookType(BookTypeVO bookType) {
		this.bookType = bookType;
	}



	public Integer getInvenNo() {
		return this.invenNo;
	}

	public void setInvenNo(Integer invenNo) {
		this.invenNo = invenNo;
	}


	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	
	public Integer getCreatedActNo() {
		return this.createdActNo;
	}

	public void setCreatedActNo(Integer createdActNo) {
		this.createdActNo = createdActNo;
	}
	public Integer getDeletedActNo() {
		return this.deletedActNo;
	}

	public void setDeletedActNo(Integer deletedActNo) {
		this.deletedActNo = deletedActNo;
	}
	


	public Set<BookInUseVO> getBookInUses() {
		return this.bookInUses;
	}

	public void setBookInUses(Set<BookInUseVO> bookInUses) {
		this.bookInUses = bookInUses;
	}

}
