package kg.enesaitech.vo;


public class StudAccountingVO {

	private Integer id;
	private StudentVO student;
	private YearVO year;
	private double anualFee;
	private double payed;

	public StudAccountingVO() {
	}

	public StudAccountingVO(StudentVO student, YearVO year, double anualFee,
			double payed) {
		this.student = student;
		this.year = year;
		this.anualFee = anualFee;
		this.payed = payed;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public StudentVO getStudent() {
		return this.student;
	}

	public void setStudent(StudentVO student) {
		this.student = student;
	}


	public YearVO getYear() {
		return this.year;
	}

	public void setYear(YearVO year) {
		this.year = year;
	}


	public double getAnualFee() {
		return this.anualFee;
	}

	public void setAnualFee(double anualFee) {
		this.anualFee = anualFee;
	}


	public double getPayed() {
		return this.payed;
	}

	public void setPayed(double payed) {
		this.payed = payed;
	}

}
