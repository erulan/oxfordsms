package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;

public class CategoryVO {

	private Integer id;
	private String name;
	private Integer amount;
	private Double totalFee;
	private Set<BookTypeVO> bookTypes = new HashSet<BookTypeVO>(0);

	public CategoryVO() {
	}

	public CategoryVO(String name) {
		this.name = name;
	}

	public CategoryVO(String name, Set<BookTypeVO> books) {
		this.name = name;
		this.bookTypes = books;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Set<BookTypeVO> getBooks() {
		return this.bookTypes;
	}

	public void setBooks(Set<BookTypeVO> books) {
		this.bookTypes = books;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Double getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Double totalFee) {
		this.totalFee = totalFee;
	}

}
