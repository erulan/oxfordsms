package kg.enesaitech.vo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class YearVO {
	
	private Integer id;
	private Date year;
	private Boolean isCurrent;
	private Set<CourseVO> courses = new HashSet<CourseVO>(0);
	private Set<StudAccountingVO> studAccountings = new HashSet<StudAccountingVO>(0);
	private Set<StudentVO> students = new HashSet<StudentVO>(0);

	public YearVO() {
	}

	public YearVO(Date year, Boolean isCurrent, Set<CourseVO> courses, Set<StudAccountingVO> studAccountings,
			Set<StudentVO> students) {
		this.year = year;
		this.isCurrent = isCurrent;
		this.courses = courses;
		this.studAccountings = studAccountings;
		this.students = students;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getYear() {
		return this.year;
	}

	public void setYear(Date year) {
		this.year = year;
	}

	public Boolean getIsCurrent() {
		return this.isCurrent;
	}

	public void setIsCurrent(Boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public Set<CourseVO> getCourses() {
		return this.courses;
	}

	public void setCourses(Set<CourseVO> courses) {
		this.courses = courses;
	}

	public Set<StudAccountingVO> getStudAccountings() {
		return this.studAccountings;
	}

	public void setStudAccountings(Set<StudAccountingVO> studAccountings) {
		this.studAccountings = studAccountings;
	}

	public Set<StudentVO> getStudents() {
		return this.students;
	}

	public void setStudents(Set<StudentVO> students) {
		this.students = students;
	}

}