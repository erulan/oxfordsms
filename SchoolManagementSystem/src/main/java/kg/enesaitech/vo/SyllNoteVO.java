package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;



public class SyllNoteVO {
	private SyllabusVO syllabus;
	private Set<NoteAttendanceVO> noteAttendances = new HashSet<NoteAttendanceVO>(0);
	
	public SyllNoteVO(){
		
	}
	
	public SyllNoteVO(SyllabusVO syllabus, Set<NoteAttendanceVO> noteAttendances) {
		this.syllabus = syllabus;
		this.noteAttendances = noteAttendances;
	}



	public SyllabusVO getSyllabus() {
		return this.syllabus;
	}

	public void setSyllabus(SyllabusVO syllabus) {
		this.syllabus = syllabus;
	}



	public Set<NoteAttendanceVO> getNoteAttendances() {
		return this.noteAttendances;
	}

	public void setNoteAttendances(Set<NoteAttendanceVO> courses) {
		this.noteAttendances = courses;
	}


}
