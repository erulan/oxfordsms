package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;

public class ParentsVO {
	private Integer id;
	private String parent1;
	private String parent2;
	private String parent1Number;
	private String parent2Number;
	private Set<StudentVO> students = new HashSet<StudentVO>(0);

	public ParentsVO() {
	}

	public ParentsVO(String parent1, String parent2, String parent1Number,
			String parent2Number, Set<StudentVO> students) {
		this.parent1 = parent1;
		this.parent2 = parent2;
		this.parent1Number = parent1Number;
		this.parent2Number = parent2Number;
		this.students = students;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getParent1() {
		return this.parent1;
	}

	public void setParent1(String parent1) {
		this.parent1 = parent1;
	}


	public String getParent2() {
		return this.parent2;
	}

	public void setParent2(String parent2) {
		this.parent2 = parent2;
	}


	public String getParent1Number() {
		return this.parent1Number;
	}

	public void setParent1Number(String parent1Number) {
		this.parent1Number = parent1Number;
	}


	public String getParent2Number() {
		return this.parent2Number;
	}

	public void setParent2Number(String parent2Number) {
		this.parent2Number = parent2Number;
	}

	
	public Set<StudentVO> getStudents() {
		return this.students;
	}

	public void setStudents(Set<StudentVO> students) {
		this.students = students;
	}

}
