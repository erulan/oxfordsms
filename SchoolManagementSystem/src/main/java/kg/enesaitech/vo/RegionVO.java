package kg.enesaitech.vo;


public class RegionVO {

	private Integer id;
	private String name;
	private String zip;
	public RegionVO() {
	}

	public RegionVO(String name, String zip) {
		this.name = name;
		this.zip = zip;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

}
