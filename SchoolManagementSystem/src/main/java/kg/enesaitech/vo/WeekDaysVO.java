package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;

public class WeekDaysVO {
	private Integer id;
	private String name;
	private Set<TimetableVO> timetables = new HashSet<TimetableVO>(0);

	public WeekDaysVO() {
	}

	public WeekDaysVO(String name) {
		this.name = name;
	}

	public WeekDaysVO(String name, Set<TimetableVO> timetables) {
		this.name = name;
		this.timetables = timetables;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Set<TimetableVO> getTimetables() {
		return this.timetables;
	}

	public void setTimetables(Set<TimetableVO> timetables) {
		this.timetables = timetables;
	}

}
