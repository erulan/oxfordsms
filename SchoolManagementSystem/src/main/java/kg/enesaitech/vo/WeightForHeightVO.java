package kg.enesaitech.vo;


public class WeightForHeightVO {

	private Integer id;
	private Double height;
	private boolean isMale;
	private double cat1;
	private double cat2;
	private double cat3;
	private double cat4;
	private double cat5;
	private double cat6;
	private double cat7;

	public WeightForHeightVO() {
	}

	public WeightForHeightVO(Double height, boolean isMale, Double cat1, Double cat2,
			Double cat3, Double cat4, Double cat5, Double cat6, Double cat7) {
		this.height = height;
		this.isMale = isMale;
		this.cat1 = cat1;
		this.cat2 = cat2;
		this.cat3 = cat3;
		this.cat4 = cat4;
		this.cat5 = cat5;
		this.cat6 = cat6;
		this.cat7 = cat7;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Double getHeight() {
		return this.height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}


	public boolean getIsMale() {
		return this.isMale;
	}

	public void setIsMale(boolean isMale) {
		this.isMale = isMale;
	}

	public double getCat1() {
		return this.cat1;
	}

	public void setCat1(double cat1) {
		this.cat1 = cat1;
	}

	public double getCat2() {
		return this.cat2;
	}

	public void setCat2(double cat2) {
		this.cat2 = cat2;
	}
	public double getCat3() {
		return this.cat3;
	}

	public void setCat3(double cat3) {
		this.cat3 = cat3;
	}
	public double getCat4() {
		return this.cat4;
	}

	public void setCat4(double cat4) {
		this.cat4 = cat4;
	}
	public double getCat5() {
		return this.cat5;
	}

	public void setCat5(double cat5) {
		this.cat5 = cat5;
	}
	public double getCat6() {
		return this.cat6;
	}

	public void setCat6(double cat6) {
		this.cat6 = cat6;
	}
	public double getCat7() {
		return this.cat7;
	}

	public void setCat7(double cat7) {
		this.cat7 = cat7;
	}

}