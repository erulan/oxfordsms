package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;

import org.dozer.Mapping;

public class CourseVO {

	private Integer id;
	private BookTypeVO bookType;
	@Mapping("CGroup")	
	private ClassGroupVO classGroup;
	private LessonVO lesson;
	private TeacherVO teacher;
	private YearVO year;
	private String code;
	private boolean active;
	private String description;
	private boolean isMain;
	private Set<TimetableVO> timetables = new HashSet<TimetableVO>(0);
//	private Set<SyllabusVO> syllabuses = new HashSet<SyllabusVO>(0);
	private Set<ExTeacherVO> exTeachers = new HashSet<ExTeacherVO>(0);

	public CourseVO() {
	}

	public CourseVO(ClassGroupVO CGroup, LessonVO lesson, TeacherVO teacher, YearVO year,
			boolean active) {
		this.classGroup = CGroup;
		this.lesson = lesson;
		this.teacher = teacher;
		this.year = year;
		this.active = active;
	}

	public CourseVO(BookTypeVO bookTypeVO, ClassGroupVO CGroup, LessonVO lesson, TeacherVO teacher,
			YearVO year, String code, boolean active, String description, boolean isMain,
			Set<TimetableVO> timetables,/* Set<SyllabusVO> syllabuses,*/ Set<ExTeacherVO> exTeachers) {
		this.bookType = bookTypeVO;
		this.classGroup = CGroup;
		this.lesson = lesson;
		this.teacher = teacher;
		this.year = year;
		this.code = code;
		this.active = active;
		this.description = description;
		this.isMain = isMain;
		this.timetables = timetables;
//		this.syllabuses = syllabuses;
		this.exTeachers = exTeachers;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public BookTypeVO getBookType() {
		return this.bookType;
	}

	public void setBookType(BookTypeVO bookType) {
		this.bookType = bookType;
	}


	public boolean isIsMain() {
		return this.isMain;
	}

	public void setIsMain(boolean isMain) {
		this.isMain = isMain;
	}
	

	public ClassGroupVO getClassGroup() {
		return this.classGroup;
	}

	public void setClassGroup(ClassGroupVO classGroup) {
		this.classGroup = classGroup;
	}


	public LessonVO getLesson() {
		return this.lesson;
	}

	public void setLesson(LessonVO lesson) {
		this.lesson = lesson;
	}


	public TeacherVO getTeacher() {
		return this.teacher;
	}

	public void setTeacher(TeacherVO teacher) {
		this.teacher = teacher;
	}


	public YearVO getYear() {
		return this.year;
	}

	public void setYear(YearVO year) {
		this.year = year;
	}


	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}


	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Set<TimetableVO> getTimetables() {
		return this.timetables;
	}

	public void setTimetables(Set<TimetableVO> timetables) {
		this.timetables = timetables;
	}


//	public Set<SyllabusVO> getSyllabuses() {
//		return this.syllabuses;
//	}
//
//	public void setSyllabuses(Set<SyllabusVO> syllabuses) {
//		this.syllabuses = syllabuses;
//	}
//

	public Set<ExTeacherVO> getExTeachers() {
		return this.exTeachers;
	}

	public void setExTeachers(Set<ExTeacherVO> exTeachers) {
		this.exTeachers = exTeachers;
	}

}
