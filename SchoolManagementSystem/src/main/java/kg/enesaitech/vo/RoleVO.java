package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.entity.UsersRole;

public class RoleVO  {

	private Integer id;
	private String name;
	private Set<UsersRole> usersRoles = new HashSet<UsersRole>(0);

	public RoleVO() {
	}

	public RoleVO(String name) {
		this.name = name;
	}

	public RoleVO(String name, Set<UsersRole> usersRoles) {
		this.name = name;
		this.usersRoles = usersRoles;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Set<UsersRole> getUsersRoles() {
		return this.usersRoles;
	}

	public void setUsersRoles(Set<UsersRole> usersRoles) {
		this.usersRoles = usersRoles;
	}

}
