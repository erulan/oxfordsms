package kg.enesaitech.vo;

import kg.enesaitech.entity.Year;

public class CheckupVO {

	private Integer id;
	private Year date;
	private double weight;
	private double height;
	private double weightFH;
	private StudentVO student;
	private String categoryH;
	private String categoryW;
	private String categoryWFH;
	private String weightComment;
	private String heightComment;
	private String weightFHComment;
	private CheckPeriodVO period;
	
	public CheckupVO() {
	}

	public CheckupVO(Year date, double height, double weight, double weightFH, StudentVO student, CheckPeriodVO period, String heightComment, String weightComment,String weightFHComment) {
		this.date = date;
		this.height= height;
		this.weight = weight;
		this.student = student;
		this.period = period; 
		this.weightComment = weightComment;
		this.heightComment = heightComment;
		this.weightComment = weightFHComment;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Year getDate() {
		return this.date;
	}

	public void setDate(Year date) {
		this.date = date;
	}
	
	public double getHeight(){
		return this.height;
	}
	
	public void setHeight(double height){
		this.height = height;
	}
	
	public double getWeight(){
		return this.weight;
	}
	
	public void setWeight(double weight){
		this.weight = weight;
	}
	
	public double getWeightFH(){
		return this.weightFH;
	}
	
	public void setWeightFH(double weightFH){
		this.weightFH = weightFH;
	}
	
	public StudentVO getStudent(){
		return this.student;
	}
	
	public void setStudent(StudentVO student){
		this.student = student;
	}
	
	public String getCategoryH(){
		return this.categoryH;
	}
	
	public void setCategoryH(String categoryH){
		this.categoryH = categoryH;
	}
	
	public String getCategoryW(){
		return this.categoryW;
	}
	
	public void setCategoryW(String categoryW){
		this.categoryW = categoryW;
	}
	
	public String getCategoryWFH(){
		return this.categoryWFH;
	}
	
	public void setCategoryWFH(String categoryWFH){
		this.categoryWFH = categoryWFH;
	}
	
	public String getHeightComment(){
		return this.heightComment;
	}
	
	public void setHeightComment(String heightComment){
		this.heightComment = heightComment;
	}
	
	public String getWeightComment(){
		return this.weightComment;
	}
	
	public void setWeightComment(String weightComment){
		this.weightComment = weightComment;
	}
	public String getWeightFHComment(){
		return this.weightFHComment;
	}
	
	public void setWeightFHComment(String weightFHComment){
		this.weightFHComment = weightFHComment;
	}
	
	public CheckPeriodVO getPeriod(){
		return this.period;
	}
	
	public void setPeriod(CheckPeriodVO period){
		this.period = period;
	}
}