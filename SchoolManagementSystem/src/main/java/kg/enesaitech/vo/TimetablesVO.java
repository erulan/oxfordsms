package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;

public class TimetablesVO {

	private ClassGroupVO classGroup;
	private Set<TimetableVO> timetables = new HashSet<TimetableVO>(0);

	public TimetablesVO() {
	}

	

	public TimetablesVO(ClassGroupVO CGroup, Set<TimetableVO> timetables) {
		this.classGroup = CGroup;
		this.timetables = timetables;
	}



	public ClassGroupVO getClassGroup() {
		return this.classGroup;
	}

	public void setClassGroup(ClassGroupVO classGroup) {
		this.classGroup = classGroup;
	}



	public Set<TimetableVO> getTimetables() {
		return this.timetables;
	}

	public void setTimetables(Set<TimetableVO> timetables) {
		this.timetables = timetables;
	}
	
}
