package kg.enesaitech.vo;

import java.util.Date;



public class CheckPeriodVO {

	private Integer id;
	private Date dateFrom;
	private Date dateTo;
	private String description;
	private String name;
	
	public CheckPeriodVO() {
	}

	public CheckPeriodVO(String description,String name, Date dateFrom, Date dateTo) {
		this.description = description;
		this.dateFrom = dateFrom;
		this.dateTo= dateTo;
		this.name = name;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDescription(){
		return this.description;
	}
	public void setDescription(String description){
		this.description = description;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setName(String name){
		this.name = name;
	}

	public Date getDateFrom() {
		return this.dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Date getDateTo() {
		return this.dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}
}
