package kg.enesaitech.vo;






public class NoteAttendanceVO {
	private Integer id;
	private StudentVO student;
	private NoteTypeVO noteType;
	private NoteSystemVO noteSystem;
	private SyllabusVO syllabus;
	private Integer note;
	private boolean attended;
	private String description;

	public NoteAttendanceVO() {
	}

	public NoteAttendanceVO(StudentVO student, NoteTypeVO noteType, NoteSystemVO noteSystem, SyllabusVO syllabus, boolean attended) {
		this.student = student;
		this.noteType = noteType;
		this.noteSystem = noteSystem;
		this.syllabus = syllabus;
		this.attended = attended;
	}

	public NoteAttendanceVO(StudentVO student, NoteTypeVO noteType, NoteSystemVO noteSystem, SyllabusVO syllabus, Integer note,
			boolean attended, String description) {
		this.student = student;
		this.noteType = noteType;
		this.noteSystem = noteSystem;
		this.syllabus = syllabus;
		this.note = note;
		this.attended = attended;
		this.description = description;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public StudentVO getStudent() {
		return this.student;
	}

	public void setStudent(StudentVO student) {
		this.student = student;
	}

	public NoteTypeVO getNoteType() {
		return this.noteType;
	}

	public void setNoteType(NoteTypeVO noteType) {
		this.noteType = noteType;
	}


	public NoteSystemVO getNoteSystem() {
		return this.noteSystem;
	}

	public void setNoteSystem(NoteSystemVO noteSystem) {
		this.noteSystem = noteSystem;
	}


	public SyllabusVO getSyllabus() {
		return this.syllabus;
	}

	public void setSyllabus(SyllabusVO syllabus) {
		this.syllabus = syllabus;
	}


	public Integer getNote() {
		return this.note;
	}

	public void setNote(Integer note) {
		this.note = note;
	}


	public boolean isAttended() {
		return this.attended;
	}

	public void setAttended(boolean attended) {
		this.attended = attended;
	}


	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
