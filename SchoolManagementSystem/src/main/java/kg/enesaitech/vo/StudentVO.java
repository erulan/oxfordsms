package kg.enesaitech.vo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.dozer.Mapping;

public class StudentVO {
	
	private Integer id;
	@Mapping("CGroup")
	private ClassGroupVO classGroup;
	private CountryVO country;
	private StatusVO status;
	private NationalityVO nationality;
	private ParentsVO parents;
	private YearVO year;
	private String nameEn;
	private String surnameEn;
	private String nameRu;
	private String surnameRu;
	private String middleNameRu;
	private String studentNum;
	private String phonenum;
	private Date birthDate;
	private String email;
	private String photo;
	private String description;
	private String permAdress;
	private String currAdress;
	private boolean isMale;
	private Set<ClassGroupVO> classGroups = new HashSet<ClassGroupVO>(0);
	private Set<StudAccountingVO> studAccountings = new HashSet<StudAccountingVO>(0);
	private Set<NoteAttendanceVO> noteAttendances = new HashSet<NoteAttendanceVO>(0);

	public StudentVO() {
	}

	public StudentVO(ClassGroupVO classGroup, CountryVO country,
			StatusVO status, NationalityVO nationality,
			String nameRu, String surnameRu,
			String studentNum, boolean isMale) {
		this.classGroup = classGroup;
		this.country = country;
		this.status = status;
		this.nationality = nationality;
		this.nameRu = nameRu;
		this.surnameRu = surnameRu;
		this.studentNum = studentNum;
		this.isMale = isMale;
	}

	public StudentVO(ClassGroupVO classGroup, CountryVO country,
			StatusVO status, NationalityVO nationality,
			ParentsVO parents, YearVO year,
			String nameEn, String surnameEn, String nameRu, String surnameRu,
			String middleNameRu, String studentNum, String phonenum,
			Date birthDate, String email, String photo, String description,
			String permAdress, String currAdress, boolean isMale,
			Set<ClassGroupVO> classGroups, Set<StudAccountingVO> studAccountings,
			Set<NoteAttendanceVO> noteAttendances) {
		this.classGroup = classGroup;
		this.country = country;
		this.status = status;
		this.nationality = nationality;
		this.parents = parents;
		this.year = year;
		this.nameEn = nameEn;
		this.surnameEn = surnameEn;
		this.nameRu = nameRu;
		this.surnameRu = surnameRu;
		this.middleNameRu = middleNameRu;
		this.studentNum = studentNum;
		this.phonenum = phonenum;
		this.birthDate = birthDate;
		this.email = email;
		this.photo = photo;
		this.description = description;
		this.permAdress = permAdress;
		this.currAdress = currAdress;
		this.isMale = isMale;
		this.classGroups = classGroups;
		this.studAccountings = studAccountings;
		this.noteAttendances = noteAttendances;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}



	public ClassGroupVO getClassGroup() {
		return this.classGroup;
	}

	public void setClassGroup(ClassGroupVO classGroup) {
		this.classGroup = classGroup;
	}


	public CountryVO getCountry() {
		return this.country;
	}

	public void setCountry(CountryVO country) {
		this.country = country;
	}


	public StatusVO getStatus() {
		return this.status;
	}

	public void setStatus(StatusVO status) {
		this.status = status;
	}

	public NationalityVO getNationality() {
		return this.nationality;
	}

	public void setNationality(NationalityVO nationality) {
		this.nationality = nationality;
	}



	public ParentsVO getParents() {
		return this.parents;
	}

	public void setParents(ParentsVO parents) {
		this.parents = parents;
	}



	public YearVO getYear() {
		return this.year;
	}

	public void setYear(YearVO year) {
		this.year = year;
	}


	public String getNameEn() {
		return this.nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}


	public String getSurnameEn() {
		return this.surnameEn;
	}

	public void setSurnameEn(String surnameEn) {
		this.surnameEn = surnameEn;
	}


	public String getNameRu() {
		return this.nameRu;
	}

	public void setNameRu(String nameRu) {
		this.nameRu = nameRu;
	}


	public String getSurnameRu() {
		return this.surnameRu;
	}

	public void setSurnameRu(String surnameRu) {
		this.surnameRu = surnameRu;
	}


	public String getMiddleNameRu() {
		return this.middleNameRu;
	}

	public void setMiddleNameRu(String middleNameRu) {
		this.middleNameRu = middleNameRu;
	}


	public String getStudentNum() {
		return this.studentNum;
	}

	public void setStudentNum(String studentNum) {
		this.studentNum = studentNum;
	}


	public String getPhonenum() {
		return this.phonenum;
	}

	public void setPhonenum(String phonenum) {
		this.phonenum = phonenum;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}


	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhoto() {
		return this.photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getPermAdress() {
		return this.permAdress;
	}

	public void setPermAdress(String permAdress) {
		this.permAdress = permAdress;
	}


	public String getCurrAdress() {
		return this.currAdress;
	}

	public void setCurrAdress(String currAdress) {
		this.currAdress = currAdress;
	}


	public boolean isIsMale() {
		return this.isMale;
	}

	public void setIsMale(boolean isMale) {
		this.isMale = isMale;
	}

	public Set<ClassGroupVO> getClassGroups() {
		return this.classGroups;
	}

	public void setClassGroups(Set<ClassGroupVO> classGroups) {
		this.classGroups = classGroups;
	}

	public Set<StudAccountingVO> getStudAccountings() {
		return this.studAccountings;
	}

	public void setStudAccountings(Set<StudAccountingVO> studAccountings) {
		this.studAccountings = studAccountings;
	}


	public Set<NoteAttendanceVO> getNoteAttendances() {
		return this.noteAttendances;
	}

	public void setNoteAttendances(Set<NoteAttendanceVO> noteAttendances) {
		this.noteAttendances = noteAttendances;
	}

}
