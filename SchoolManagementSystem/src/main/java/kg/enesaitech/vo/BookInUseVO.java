package kg.enesaitech.vo;

import java.util.Date;


public class BookInUseVO {
	
	private Integer id;
	private BookVO book;
	private OwnerTypeVO ownerType;	
	private Integer ownerId;
	private Date beginDate;
	private Date endDate;
	private Date returnDate;
	private String fullname;

	public BookInUseVO() {
	}

	public BookInUseVO(BookVO book) {
		this.book = book;
	}

	public BookInUseVO(BookVO book, OwnerTypeVO ownerType, Integer ownerId, Date beginDate,
			Date endDate, String fullname, Date returnDate) {
		this.book = book;
		this.ownerType = ownerType;
		this.ownerId = ownerId;
		this.beginDate = beginDate;
		this.endDate = endDate;
		this.fullname = fullname;
		this.returnDate = returnDate;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public BookVO getBook() {
		return this.book;
	}

	public void setBook(BookVO book) {
		this.book = book;
	}


	public OwnerTypeVO getOwnerType() {
		return this.ownerType;
	}

	public void setOwnerType(OwnerTypeVO ownerType) {
		this.ownerType = ownerType;
	}

	public Integer getOwnerId() {
		return this.ownerId;
	}

	public void setownerId(Integer ownerId) {
		this.ownerId = ownerId;
	}
	
	public Date getBeginDate() {
		return this.beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

}
