package kg.enesaitech.vo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class TeacherVO {

	private Integer id;
	private CountryVO country;
	private StatusVO status;
	private NationalityVO nationality;
	private TeacherCategoryVO teacherCategory;
	private TeacherAwardVO teacherAward;
	private String nameRu;
	private String surnameRu;
	private String nameEn;
	private String surnameEn;
	private String teacherNum;
	private String phone;
	private String permadr;
	private String curradr;
	private Date birthDate;
	private String passportNo;
	private String email;
	private String photo;
	private Boolean isMale;
	private Date accreditation;
	private String education;
	private Set<ExTeacherVO> exTeachers = new HashSet<ExTeacherVO>(0);
	private Set<CourseVO> courses = new HashSet<CourseVO>(0);
	private Set<ClassGroupVO> CGroups = new HashSet<ClassGroupVO>(0);

	public TeacherVO() {
	}

	public TeacherVO(CountryVO country, StatusVO status, NationalityVO nationality,
			String nameRu, String nameEn,
			String permadr) {
		this.country = country;
		this.status = status;
		this.nationality = nationality;
		this.nameRu = nameRu;
		this.nameEn = nameEn;
		this.permadr = permadr;
	}

	public TeacherVO(CountryVO country, StatusVO status, NationalityVO nationality,
			TeacherCategoryVO teacherCategory, TeacherAwardVO teacherAward, 
			String nameRu, String surnameRu,
			String nameEn, String surnameEn, String teacherNum, String phone,
			String permadr, String curradr, Date birthDate, String passportNo,
			String email, String photo, Boolean isMale, Date accreditation, String education, Set<ExTeacherVO> exTeachers,
			Set<CourseVO> courses, Set<ClassGroupVO> CGroups) {
		this.country = country;
		this.status = status;
		this.nationality = nationality;
		this.teacherCategory = teacherCategory;
		this.teacherAward = teacherAward;
		this.nameRu = nameRu;
		this.surnameRu = surnameRu;
		this.nameEn = nameEn;
		this.surnameEn = surnameEn;
		this.accreditation = accreditation;
		this.teacherNum = teacherNum;
		this.phone = phone;
		this.permadr = permadr;
		this.curradr = curradr;
		this.birthDate = birthDate;
		this.passportNo = passportNo;
		this.email = email;
		this.photo = photo;
		this.isMale = isMale;
		this.exTeachers = exTeachers;
		this.courses = courses;
		this.CGroups = CGroups;
		this.education = education;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public CountryVO getCountry() {
		return this.country;
	}

	public void setCountry(CountryVO country) {
		this.country = country;
	}


	public StatusVO getStatus() {
		return this.status;
	}

	public void setStatus(StatusVO status) {
		this.status = status;
	}

	public NationalityVO getNationality() {
		return this.nationality;
	}

	public void setNationality(NationalityVO nationality) {
		this.nationality = nationality;
	}


	public String getNameRu() {
		return this.nameRu;
	}

	public void setNameRu(String nameRu) {
		this.nameRu = nameRu;
	}


	public String getSurnameRu() {
		return this.surnameRu;
	}

	public void setSurnameRu(String surnameRu) {
		this.surnameRu = surnameRu;
	}


	public String getNameEn() {
		return this.nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}


	public String getSurnameEn() {
		return this.surnameEn;
	}

	public void setSurnameEn(String surnameEn) {
		this.surnameEn = surnameEn;
	}


	public String getTeacherNum() {
		return this.teacherNum;
	}

	public void setTeacherNum(String teacherNum) {
		this.teacherNum = teacherNum;
	}


	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getPermadr() {
		return this.permadr;
	}

	public void setPermadr(String permadr) {
		this.permadr = permadr;
	}


	public String getCurradr() {
		return this.curradr;
	}

	public void setCurradr(String curradr) {
		this.curradr = curradr;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}


	public String getPassportNo() {
		return this.passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}


	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhoto() {
		return this.photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public Boolean getIsMale() {
		return this.isMale;
	}

	public void setIsMale(Boolean isMale) {
		this.isMale = isMale;
	}


	public Set<ExTeacherVO> getExTeachers() {
		return this.exTeachers;
	}

	public void setExTeachers(Set<ExTeacherVO> exTeachers) {
		this.exTeachers = exTeachers;
	}


	public Set<CourseVO> getCourses() {
		return this.courses;
	}

	public void setCourses(Set<CourseVO> courses) {
		this.courses = courses;
	}



	public Set<ClassGroupVO> getCGroups() {
		return this.CGroups;
	}

	public void setCGroups(Set<ClassGroupVO> CGroups) {
		this.CGroups = CGroups;
	}

	public TeacherCategoryVO getTeacherCategory() {
		return teacherCategory;
	}

	public void setTeacherCategory(TeacherCategoryVO teacherCategory) {
		this.teacherCategory = teacherCategory;
	}

	public TeacherAwardVO getTeacherAward() {
		return teacherAward;
	}

	public void setTeacherAward(TeacherAwardVO teacherAward) {
		this.teacherAward = teacherAward;
	}

	public Date getAccreditation() {
		return accreditation;
	}

	public void setAccreditation(Date accreditation) {
		this.accreditation = accreditation;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

}
