package kg.enesaitech.vo;

import java.util.Date;

import org.dozer.Mapping;

public class StuffVO  {
	
	private Integer id;
	private String nameRu;
	private String surnameRu;
	private String nameEn;
	private String surnameEn;
	private String stuffNum;
	private String position;
	private String phone;
	private String permadr;
	private String curradr;
	private Date birthDate;
	private String passportNo;
	private String email;
	private StatusVO status;
	private String experience;
	private NationalityVO nationality;
	private String education;
	private String languages;
	private String middleNameRu;
	private CountryVO country;
	private String photo;
	@Mapping("isMale")
	private Boolean isMale;
	private MaritalStatusVO maritalStatus;

	public StuffVO() {
	}

	public StuffVO(String nameRu, String surnameRu, String nameEn, String photo,
			String surnameEn, String stuffNum, String position, String phone, String permadr,
			String curradr, Date birthDate, String passportNo, String email, StatusVO status,
			String experience, NationalityVO nationality, String education,
			String languages, String middleNameRu, Boolean isMale, CountryVO country, MaritalStatusVO maritalStatus) {
		this.nameRu = nameRu;
		this.surnameRu = surnameRu;
		this.nameEn = nameEn;
		this.surnameEn = surnameEn;
		this.stuffNum = stuffNum;
		this.setPosition(position);
		this.phone = phone;
		this.permadr = permadr;
		this.curradr = curradr;
		this.birthDate = birthDate;
		this.passportNo = passportNo;
		this.email = email;
		this.status = status;
		this.setExperience(experience);
		this.setNationality(nationality);
		this.setCountry(country);
		this.setEducation(education);
		this.setLanguages(languages);
		this.setMiddleNameRu(middleNameRu);
		this.isMale = isMale;
		this.maritalStatus = maritalStatus;
		this.photo=photo;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getNameRu() {
		return this.nameRu;
	}

	public void setNameRu(String nameRu) {
		this.nameRu = nameRu;
	}


	public String getSurnameRu() {
		return this.surnameRu;
	}

	public void setSurnameRu(String surnameRu) {
		this.surnameRu = surnameRu;
	}


	public String getNameEn() {
		return this.nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}


	public String getSurnameEn() {
		return this.surnameEn;
	}

	public void setSurnameEn(String surnameEn) {
		this.surnameEn = surnameEn;
	}


	public String getStuffNum() {
		return this.stuffNum;
	}

	public void setStuffNum(String stuffNum) {
		this.stuffNum = stuffNum;
	}


	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getPermadr() {
		return this.permadr;
	}

	public void setPermadr(String permadr) {
		this.permadr = permadr;
	}


	public String getCurradr() {
		return this.curradr;
	}

	public void setCurradr(String curradr) {
		this.curradr = curradr;
	}


	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}


	public String getPassportNo() {
		return this.passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}


	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public StatusVO getStatus() {
		return this.status;
	}

	public void setStatus(StatusVO status) {
		this.status = status;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public NationalityVO getNationality() {
		return nationality;
	}

	public void setNationality(NationalityVO nationality) {
		this.nationality = nationality;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getLanguages() {
		return languages;
	}

	public void setLanguages(String languages) {
		this.languages = languages;
	}

	public String getMiddleNameRu() {
		return middleNameRu;
	}

	public void setMiddleNameRu(String middleNameRu) {
		this.middleNameRu = middleNameRu;
	}

	public Boolean isIsMale() {
		return isMale;
	}

	public void setIsMale(Boolean isMale) {
		this.isMale = isMale;
	}

	public CountryVO getCountry() {
		return country;
	}

	public void setCountry(CountryVO country) {
		this.country = country;
	}

	public MaritalStatusVO getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(MaritalStatusVO maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
}
