package kg.enesaitech.vo;


public class TeacherAwardVO {

	private Integer id;
	private String gramota;
	private boolean otlichnik;

	public TeacherAwardVO() {
	}

	public TeacherAwardVO(String gramota) {
		this.setGramota(gramota);
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGramota() {
		return gramota;
	}

	public void setGramota(String gramota) {
		this.gramota = gramota;
	}

	public boolean isOtlichnik() {
		return otlichnik;
	}

	public void setOtlichnik(boolean otlichnik) {
		this.otlichnik = otlichnik;
	}


}
