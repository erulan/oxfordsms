package kg.enesaitech.vo;


public class MaritalStatusVO {

	private Integer id;
	private String name;

	public MaritalStatusVO() {
	}

	public MaritalStatusVO(String name) {
		this.name = name;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
