package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;

public class GradeVO {

	private Integer id;
	private int grade;
	private SchoolTypeVO schoolTypeVO;
	private Set<ClassGroupVO> classGroups = new HashSet<ClassGroupVO>(0);

	public GradeVO() {
	}

	public GradeVO(int grade) {
		this.grade = grade;
	}

	public GradeVO(int grade, SchoolTypeVO schoolTypeVO, Set<ClassGroupVO> CGroups) {
		this.grade = grade;
		this.classGroups = CGroups;
		this.schoolTypeVO = schoolTypeVO;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public int getGrade() {
		return this.grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}


	public Set<ClassGroupVO> getCGroups() {
		return this.classGroups;
	}

	public void setCGroups(Set<ClassGroupVO> CGroups) {
		this.classGroups = CGroups;
	}

	public SchoolTypeVO getSchoolTypeVO() {
		return schoolTypeVO;
	}

	public void setSchoolTypeVO(SchoolTypeVO schoolTypeVO) {
		this.schoolTypeVO = schoolTypeVO;
	}

}
