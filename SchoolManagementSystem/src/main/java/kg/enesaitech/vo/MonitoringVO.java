package kg.enesaitech.vo;


public class MonitoringVO {

	private CourseVO course;
	private Double qualEE;
	private Double progEE;
	private Double qual1;
	private Double prog1;
	private Double qual2;
	private Double prog2;
	private Double qual3;
	private Double prog3;
	private Double qual4;
	private Double prog4;
	
	public MonitoringVO() {
	}

	

	public CourseVO getCourse() {
		return this.course;
	}

	public void setCourse(CourseVO course) {
		this.course = course;
	}



	public Double getProg4() {
		return prog4;
	}



	public void setProg4(Double prog4) {
		this.prog4 = prog4;
	}



	public Double getQual4() {
		return qual4;
	}



	public void setQual4(Double qual4) {
		this.qual4 = qual4;
	}



	public Double getProg3() {
		return prog3;
	}



	public void setProg3(Double prog3) {
		this.prog3 = prog3;
	}



	public Double getQual3() {
		return qual3;
	}



	public void setQual3(Double qual3) {
		this.qual3 = qual3;
	}



	public Double getProg2() {
		return prog2;
	}



	public void setProg2(Double prog2) {
		this.prog2 = prog2;
	}



	public Double getQualEE() {
		return qualEE;
	}



	public void setQualEE(Double qualEE) {
		this.qualEE = qualEE;
	}



	public Double getProgEE() {
		return progEE;
	}



	public void setProgEE(Double progEE) {
		this.progEE = progEE;
	}



	public Double getQual1() {
		return qual1;
	}



	public void setQual1(Double qual1) {
		this.qual1 = qual1;
	}



	public Double getProg1() {
		return prog1;
	}



	public void setProg1(Double prog1) {
		this.prog1 = prog1;
	}



	public Double getQual2() {
		return qual2;
	}



	public void setQual2(Double qual2) {
		this.qual2 = qual2;
	}
	
	
	
}
