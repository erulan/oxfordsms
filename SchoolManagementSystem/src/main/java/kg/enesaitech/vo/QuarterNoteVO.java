package kg.enesaitech.vo;


public class QuarterNoteVO {

	private String name;
	private Double quarter1;
	private Double quarter2;
	private Double quarter3;
	private Double quarter4;	
	private boolean isCourse;	
	private Double lastYear;	
	private Double entryExam;	
	private Double gosExam;	

	
	public Double getQuarter4() {
		return quarter4;
	}



	public void setQuarter4(Double quarter4) {
		this.quarter4 = quarter4;
	}



	public Double getQuarter3() {
		return quarter3;
	}



	public void setQuarter3(Double quarter3) {
		this.quarter3 = quarter3;
	}



	public Double getQuarter2() {
		return quarter2;
	}



	public void setQuarter2(Double quarter2) {
		this.quarter2 = quarter2;
	}



	public Double getQuarter1() {
		return quarter1;
	}



	public void setQuarter1(Double quarter1) {
		this.quarter1 = quarter1;
	}



	public String getName() {
		return name;
	}



	public void setName(String courseName) {
		this.name = courseName;
	}



	public Double getGosExam() {
		return gosExam;
	}



	public void setGosExam(Double gosExam) {
		this.gosExam = gosExam;
	}



	public Double getEntryExam() {
		return entryExam;
	}



	public void setEntryExam(Double entryExam) {
		this.entryExam = entryExam;
	}



	public Double getLastYear() {
		return lastYear;
	}



	public void setLastYear(Double lastYear) {
		this.lastYear = lastYear;
	}



	public boolean isCourse() {
		return isCourse;
	}



	public void setCourse(boolean isCourse) {
		this.isCourse = isCourse;
	}



}
