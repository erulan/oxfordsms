package kg.enesaitech.vo;

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.entity.UsersRole;

public class UsersVO {
	private Integer id;
	private String userName;
	private String userPass;
	private String status;
	private Set<UsersRole> usersRoles = new HashSet<UsersRole>(0);

	public UsersVO() {
	}

	public UsersVO(Integer id, String userName, String status) {
		this.id = id;
		this.userName = userName;
		this.status = status;
	}

	public UsersVO(Integer id, String userName, String userPass, String status,
			Set<UsersRole> usersRoles) {
		this.id = id;
		this.userName = userName;
		this.userPass = userPass;
		this.status = status;
		this.usersRoles = usersRoles;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getUserPass() {
		return this.userPass;
	}

	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}


	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public Set<UsersRole> getUsersRoles() {
		return this.usersRoles;
	}

	public void setUsersRoles(Set<UsersRole> usersRoles) {
		this.usersRoles = usersRoles;
	}

}