package kg.enesaitech.vo;

import org.dozer.Mapping;


public class TimetableVO {
	
	private Integer id;
	private ClassHoursVO classHours;
	private CourseVO course;
	private WeekDaysVO weekDays;
	@Mapping("CGroup")
	private ClassGroupVO classGroup;

	public TimetableVO() {
	}

	public TimetableVO(ClassHoursVO classHours, CourseVO course, WeekDaysVO weekDays, ClassGroupVO classGroup) {
		this.classHours = classHours;
		this.course = course;
		this.weekDays = weekDays;
		this.classGroup = classGroup;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public ClassHoursVO getClassHours() {
		return this.classHours;
	}

	public void setClassHours(ClassHoursVO classHours) {
		this.classHours = classHours;
	}


	public CourseVO getCourse() {
		return this.course;
	}

	public void setCourse(CourseVO course) {
		this.course = course;
	}


	public WeekDaysVO getWeekDays() {
		return this.weekDays;
	}

	public void setWeekDays(WeekDaysVO weekDays) {
		this.weekDays = weekDays;
	}


	public ClassGroupVO getClassGroup() {
		return this.classGroup;
	}

	public void setClassGroup(ClassGroupVO classGroup) {
		this.classGroup = classGroup;
	}

}
