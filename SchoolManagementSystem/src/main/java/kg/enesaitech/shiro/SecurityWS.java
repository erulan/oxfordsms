package kg.enesaitech.shiro;

import java.net.URI;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import kg.enesaitech.dao.RoleHome;
import kg.enesaitech.entity.Student;
import kg.enesaitech.entity.Stuff;
import kg.enesaitech.entity.Teacher;
import kg.enesaitech.entity.Users;
import kg.enesaitech.service.RoleService;
import kg.enesaitech.service.StudentService;
import kg.enesaitech.service.StuffService;
import kg.enesaitech.service.TeacherService;
import kg.enesaitech.service.UsersService;
import kg.enesaitech.userException.BusinessException;
import kg.enesaitech.vo.UserInfoVO;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

@Path("/security")
public class SecurityWS {
	RoleService roleService = RoleService.getInstance();
	RoleHome roleHome = RoleHome.getInstance();
	UsersService usersService = UsersService.getInstance();
	Logger log = Logger.getLogger(SecurityWS.class);

	@GET
	@Path("/redirect")
	public Response authenticate() {
		Response response = Response.ok().build();
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated()) {
			
			String username = currentUser.getPrincipal().toString();
			Set<String> rolesAsString = roleHome.getNameSetByUserName(username);
			log.warn("************************************************************");
			log.warn("Roles for User in SecurityWS: ["+currentUser.getPrincipal().toString()+"] : " + rolesAsString.toString());
			log.warn("************************************************************");

			if (rolesAsString.contains("admin")) {
				response = Response
						.seeOther(
								URI.create("/SchoolManagementSystem/apps/admin/students"))
						.build();
			} else if (rolesAsString.contains("secretary")) {
				response = Response
						.seeOther(
								URI.create("/SchoolManagementSystem/apps/secretary/students"))
						.build();
			} else if (rolesAsString.contains("accountant")) {
				response = Response
						.seeOther(
								URI.create("/SchoolManagementSystem/apps/accountant/accounting"))
						.build();
			} else if (rolesAsString.contains("librarian")) {
				response = Response
						.seeOther(
								URI.create("/SchoolManagementSystem/apps/library/books"))
						.build();
			} else if (rolesAsString.contains("teacher")) {
				response = Response
						.seeOther(
								URI.create("/SchoolManagementSystem/apps/teacher/course"))
						.build();
			} else if (rolesAsString.contains("student")) {
				response = Response
						.seeOther(
								URI.create("/SchoolManagementSystem/apps/student/timetable"))
						.build();
			} else if (rolesAsString.contains("deputy_director")) {
				response = Response
						.seeOther(
								URI.create("/SchoolManagementSystem/apps/deputy_director/general"))
								.build();
			}else if (rolesAsString.contains("hr_manager")) {
				response = Response
						.seeOther(
								URI.create("/SchoolManagementSystem/apps/hrmanager/stuff"))
								.build();
			}else if (rolesAsString.contains("doctor")) {
				response = Response
						.seeOther(
								URI.create("/SchoolManagementSystem/apps/doctor/news"))
								.build();
			}else {
				response = Response.seeOther(URI.create("/unauthorized.html"))
						.build();
			}
		} else {
//			throw new UnauthorizedException();
			response = Response.seeOther(
					URI.create("/SchoolManagementSystem/login.html")).build();
		}
		return response;
	}

	@GET
	@Path("/current_user")
	@Produces("application/json")
	public UserInfoVO getCurrentUser() {
		UserInfoVO userInfoVO = new UserInfoVO();
		Subject currentUser = SecurityUtils.getSubject();

		StudentService studentService = StudentService.getInstance();
		TeacherService teacherService = TeacherService.getInstance();
		StuffService stuffService = StuffService.getInstance();

		Student student = studentService.getByStudNum(currentUser
				.getPrincipal().toString());
		if (student != null) {
			userInfoVO.id = student.getId();
			userInfoVO.nameEn = student.getNameEn();
			userInfoVO.nameRu = student.getNameRu();
			userInfoVO.surnameEn = student.getSurnameEn();
			userInfoVO.surnameRu = student.getSurnameRu();
			userInfoVO.username = student.getStudentNum();
		}
		Teacher teacher = teacherService.getByTeacherNum(currentUser
				.getPrincipal().toString());
		if (teacher != null) {
			userInfoVO.id = teacher.getId();
			userInfoVO.nameEn = teacher.getNameEn();
			userInfoVO.nameRu = teacher.getNameRu();
			userInfoVO.surnameEn = teacher.getSurnameEn();
			userInfoVO.surnameRu = teacher.getSurnameRu();
			userInfoVO.username = teacher.getTeacherNum();
		}
		Stuff stuff = stuffService.getByStuffNum(currentUser.getPrincipal()
				.toString());
		if (stuff != null) {
			userInfoVO.id = stuff.getId();
			userInfoVO.nameEn = stuff.getNameEn();
			userInfoVO.nameRu = stuff.getNameRu();
			userInfoVO.surnameEn = stuff.getSurnameEn();
			userInfoVO.surnameRu = stuff.getSurnameRu();
			userInfoVO.username = stuff.getStuffNum();
		}

		return userInfoVO;
	}
	
	@Path("change_password/{username}/{current_p}/{new_p}")
	public Response changePassword(@PathParam("username") String username, @PathParam("current_p") String currentPass, @PathParam("new_p") String newPass ){
		Response response = Response.status(Status.OK).build();
		
		Users user = usersService.getByUsername(username);
		Subject currentUser = SecurityUtils.getSubject();
		
		if(!currentUser.isAuthenticated() || !user.getUserName().equals(currentUser.getPrincipal())){
			return Response.status(Status.UNAUTHORIZED).build();
		}

		if(usersService.getMD5(currentPass).equals(user.getUserPass())){
			user.setUserPass(newPass);
			usersService.update(user);
		}else{
			throw new BusinessException("Current password doesn't match ");
		}
			
		return response;
	}
}
