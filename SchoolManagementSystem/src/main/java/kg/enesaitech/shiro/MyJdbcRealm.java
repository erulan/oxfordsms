package kg.enesaitech.shiro;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import kg.enesaitech.dao.CategoryHome;
import kg.enesaitech.dao.RoleHome;
import kg.enesaitech.dao.UsersHome;
import kg.enesaitech.entity.Role;
import kg.enesaitech.entity.Users;

import org.apache.log4j.Logger;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
//import org.apache.shiro.authz.Authorizer;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.subject.PrincipalCollection;
//import org.hibernate.mapping.Set;
//import javax.naming.AuthenticationException;
//import javax.naming.InitialContext;
//import javax.naming.NamingException;
//import javax.sql.DataSource;

public class MyJdbcRealm extends JdbcRealm {
	private static final Logger log = Logger.getLogger(CategoryHome.class);
	UsersHome usersHome = UsersHome.getInstance();
	RoleHome roleHome = RoleHome.getInstance();
	protected boolean permissionsLookupEnabled = false;
	
	public MyJdbcRealm() {
		super();
	}
	
	
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token)
				throws org.apache.shiro.authc.AuthenticationException {
		
		UsernamePasswordToken upToken = (UsernamePasswordToken) token;
		String username = upToken.getUsername();
		
		log.info("username : " + username + " | " + "upToken : " + upToken.toString());
		
		AuthenticationInfo info = null;
		Users user = usersHome.getByUserName(username);
		
		if(user == null || user.getUserPass() == null){
			log.error("No account found for user [" + username + "]");
			throw new UnknownAccountException("No account found for user [" + username + "]");
		}
		info = new SimpleAuthenticationInfo(username, user.getUserPass().toCharArray(), getName());
		log.info("info : " + info.toString());
		
		return info;
	}
	
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		if (principals == null) {
			log.error("PrincipalCollection method argument cannot be null.");
			throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
		}
		
		String username = (String) getAvailablePrincipal(principals);
		System.out.println("Auth | username : " + username);

		
		Set<String> roleNames = roleHome.getNameSetByUserName(username);

		
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roleNames);
		return info;
	}
	
}