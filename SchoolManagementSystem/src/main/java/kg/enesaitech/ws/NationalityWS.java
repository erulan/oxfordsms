package kg.enesaitech.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import kg.enesaitech.entity.Nationality;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.NationalityService;
import kg.enesaitech.vo.NationalityVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

@Path("/nationality")
public class NationalityWS {

	Mapper mapper = DozerMapperProvider.getMapper();
	NationalityService nationalityService = NationalityService.getInstance();

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public NationalityVO get(@PathParam("id") int id) {
		Nationality nationality = nationalityService.get(id);
		nationality.setStudents(null);
		nationality.setTeachers(null);
		NationalityVO nationalityVO = mapper.map(nationality,
				NationalityVO.class);
		return nationalityVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public List<NationalityVO> search(SearchParameters searchParameters) {
		List<Nationality> nationalities = nationalityService.getList(searchParameters);

		List<NationalityVO> nationalityVOs = new ArrayList<NationalityVO>();

		for (int i = 0; i < nationalities.size(); i++) {
			nationalities.get(i).setStudents(null);
			nationalities.get(i).setTeachers(null);
			nationalityVOs.add(mapper.map(nationalities.get(i), NationalityVO.class));
		}

		return nationalityVOs;
	}
}
