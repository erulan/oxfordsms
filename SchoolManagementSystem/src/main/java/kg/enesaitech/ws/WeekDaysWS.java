package kg.enesaitech.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.WeekDays;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.WeekDaysService;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.WeekDaysVO;

import org.dozer.Mapper;

@Path("/week_days")
public class WeekDaysWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	WeekDaysService weekDaysService = WeekDaysService.getInstance();

	@POST
	@Path("/create")
	@Consumes("application/json")
	public Response create(WeekDaysVO weekDaysVO) {
		boolean result = true;

		WeekDays weekDays = mapper.map(weekDaysVO, WeekDays.class);
		result = weekDaysService.create(weekDays);

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update")
	public Response update(WeekDaysVO weekDaysVO) {
		boolean result = true;

		WeekDays weekDays = mapper.map(weekDaysVO, WeekDays.class);
		result = weekDaysService.update(weekDays);

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;

		WeekDays weekDays = weekDaysService.findById(id);

		if (weekDays == null) {
			result = false;
		} else {
			result = weekDaysService.delete(id);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public WeekDaysVO get(@PathParam("id") int id) {
		WeekDays weekDays = weekDaysService.get(id);
		weekDays.setTimetables(null);
		WeekDaysVO weekDaysVO = mapper.map(weekDays, WeekDaysVO.class);
		return weekDaysVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public List<WeekDaysVO> search(SearchParameters searchParameters) {
		List<WeekDays> weekDays = weekDaysService.getList(searchParameters);

		List<WeekDaysVO> weekDaysVOs = new ArrayList<WeekDaysVO>();

		for (int i = 0; i < weekDays.size(); i++) {
			weekDays.get(i).setTimetables(null);

			weekDaysVOs.add(mapper.map(weekDays.get(i), WeekDaysVO.class));
		}

		return weekDaysVOs;
	}
}