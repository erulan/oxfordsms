package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Checkup;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.CheckupService;
import kg.enesaitech.vo.CheckupVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/checkup")
public class CheckupWS {
	CheckupService checkupService = CheckupService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	public Response create(CheckupVO checkupVO) {

		Checkup checkup = mapper.map(checkupVO, Checkup.class);
		Boolean created = checkupService.create(checkup);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(CheckupVO checkupVO, @PathParam("id") int id) {
		boolean result = true;
		Checkup checkup = checkupService.findById(id);
		if (checkup == null) {
			result = false;
		} else {
			checkup = mapper.map(checkupVO, Checkup.class);
			result = checkupService.update(checkup);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		Checkup checkup= checkupService.findById(id);
		if (checkup == null) {
			result = false;
		} else {
			result = checkupService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public CheckupVO get(@PathParam("id") int id) {
 		Checkup checkup = checkupService.get(id);
 		
			if (checkup.getStudent().getCGroup()!= null) {

				checkup.getStudent().getCGroup().setCourses(null);
				checkup.getStudent().getCGroup().setStudents(null);
				if (checkup.getStudent().getCGroup().getTeacher() != null) {
					checkup.getStudent().getCGroup().setTeacher(null);
				}
				if (checkup.getStudent().getCGroup().getGrade() != null) {
					checkup.getStudent().getCGroup().getGrade().setCGroups(null);
				}
				if (checkup.getStudent().getCGroup().getClassLetter() != null) {
					checkup.getStudent().getCGroup().getClassLetter().setCGroups(null);
				}
				checkup.getStudent().setNationality(null);
				checkup.getStudent().setCountry(null);
				checkup.getStudent().setStatus(null);
				checkup.getStudent().setNoteAttendances(null);
				checkup.getStudent().setParents(null);
				checkup.getStudent().setStudAccountings(null);
				checkup.getStudent().setYear(null);
			}

		CheckupVO checkupVO = mapper.map(checkup, CheckupVO.class);
		return checkupVO;
	} 

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<CheckupVO> search(SearchParameters searchParameters) {
		SearchResult<Checkup> checkups = checkupService.getList(searchParameters);

		SearchResult<CheckupVO> checkupVOs = new SearchResult<CheckupVO>();

		for (int i = 0; i < checkups.getResultList().size(); i++) {
			if (checkups.getResultList().get(i).getStudent()
					.getCGroup() != null) {

				checkups.getResultList().get(i).getStudent()
						.getCGroup().setCourses(null);
				checkups.getResultList().get(i).getStudent()
						.getCGroup().setStudents(null);
				if (checkups.getResultList().get(i).getStudent()
						.getCGroup().getTeacher() != null) {
					checkups.getResultList().get(i).getStudent()
							.getCGroup().setTeacher(null);
				}
				if (checkups.getResultList().get(i).getStudent()
						.getCGroup().getGrade() != null) {
					checkups.getResultList().get(i).getStudent()
							.getCGroup().getGrade().setCGroups(null);
				}
				if (checkups.getResultList().get(i).getStudent()
						.getCGroup().getClassLetter() != null) {
					checkups.getResultList().get(i).getStudent()
							.getCGroup().getClassLetter().setCGroups(null);
				}
			checkups.getResultList().get(i).getStudent()
					.setNationality(null);
			checkups.getResultList().get(i).getStudent()
					.setCountry(null);
			checkups.getResultList().get(i).getStudent()
					.setStatus(null);
			checkups.getResultList().get(i).getStudent()
					.setNoteAttendances(null);
			checkups.getResultList().get(i).getStudent()
					.setParents(null);
			checkups.getResultList().get(i).getStudent()
					.setStudAccountings(null);
			checkups.getResultList().get(i).getStudent().setYear(null);
		}
			checkupVOs.getResultList().add(mapper.map(checkups.getResultList().get(i), CheckupVO.class));
		}

		return checkupVOs;
	}
}
