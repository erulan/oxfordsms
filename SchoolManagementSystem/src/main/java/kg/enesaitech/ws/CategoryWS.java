package kg.enesaitech.ws;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.BookType;
import kg.enesaitech.entity.Category;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.BookTypeService;
import kg.enesaitech.service.CategoryService;
import kg.enesaitech.vo.CategoryVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.dozer.Mapper;

@Path("/category")
public class CategoryWS {
	CategoryService categoryService = CategoryService.getInstance();
	BookTypeService bookTypeService = BookTypeService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();
	

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(CategoryVO categoryVO) {
		Category category = mapper.map(categoryVO, Category.class);
		Boolean created = categoryService.create(category);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(CategoryVO categoryVO, @PathParam("id") int id) {
		boolean result = true;
		Category category = categoryService.findById(id);
		if (category == null) {
			result = false;
		} else {
			category = mapper.map(categoryVO, Category.class);
			result = categoryService.update(category);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		Category category = categoryService.findById(id);
		if (category == null) {
			result = false;
		} else {
			result = categoryService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public CategoryVO get(@PathParam("id") int id) {
		Category category = categoryService.get(id);
		category.setBookTypes(null);
		CategoryVO categoryVO = mapper.map(category, CategoryVO.class);
		return categoryVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public List<CategoryVO> search(SearchParameters searchParameters) {
		List<Category> categories = categoryService.getList(searchParameters);

		List<CategoryVO> categoryVOs = new ArrayList<CategoryVO>();

		for (int i = 0; i < categories.size(); i++) {
			categories.get(i).setBookTypes(null);
			CategoryVO categoryVO = mapper.map(categories.get(i), CategoryVO.class);
			categoryVO.setTotalFee(Double.valueOf(0));
			categoryVO.setAmount(0);
			
			SearchParameters searchParameters2 = new SearchParameters();
			searchParameters2.addParameter("categoryId", categories.get(i).getId().toString());
			SearchResult<BookType> result = bookTypeService.getList(searchParameters2);
			
			for(BookType bookType:  result.getResultList()){
				categoryVO.setAmount(categoryVO.getAmount() + bookType.getBooks().size());
				categoryVO.setTotalFee(categoryVO.getTotalFee() + bookType.getBooks().size()  * (bookType.getCost() != null ? bookType.getCost() : 0 ));
			}
			categoryVOs.add(categoryVO);
		}

		return categoryVOs;
	}

	@POST
	@Path("/excel")
	@Produces("application/json")
	public String toExcel(SearchParameters searchParameters) {
		List<Category> categories = categoryService.getList(searchParameters);

		// 1. Create a new Workbook
		Workbook wb = new HSSFWorkbook();

		// 2. Create a new sheet
		Sheet sheet = wb.createSheet("sheet 1");

		// 3. Create a row

		// 4. Create cells
		Row row = sheet.createRow(0);
		row.createCell(0).setCellValue("ID");
		row.createCell(1).setCellValue("NAME");
		for (int i = 0; i < categories.size(); i++) {
			categories.get(i).setBookTypes(null);
			row = sheet.createRow((short) i + 1);
			// 4.1 number cell
			row.createCell(0).setCellValue(categories.get(i).getId());
			// 4.2 text
			row.createCell(1).setCellValue(categories.get(i).getName());
			// 4.3 date cell
		}

		// 5. create excel file
		FileOutputStream fileOut;
		
		System.getProperty("user.dir");
		
		String filepath = "./categories.xlsx";
		try {

			fileOut = new FileOutputStream(filepath);
			wb.write(fileOut);
			fileOut.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("File created!");

		return filepath;
	}
}