package kg.enesaitech.ws;
 
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import kg.enesaitech.entity.TeacherCategory;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.TeacherCategoryService;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.TeacherCategoryVO;

import org.dozer.Mapper;
 
@Path("/teacher_category")
public class TeacherCategoryWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	TeacherCategoryService teacherCategoryService = TeacherCategoryService
			.getInstance();
 
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public TeacherCategoryVO get(@PathParam("id") int id) {
		TeacherCategory teacherCategory = teacherCategoryService.get(id);
		TeacherCategoryVO teacherCategoryVO = mapper.map(teacherCategory, TeacherCategoryVO.class);
		return teacherCategoryVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public List<TeacherCategoryVO> search(SearchParameters searchParameters) {
		List<TeacherCategory> teacherCategories = teacherCategoryService.getList(searchParameters);

		List<TeacherCategoryVO> teacherCategoryVOs = new ArrayList<TeacherCategoryVO>();

		for (int i = 0; i < teacherCategories.size(); i++) {

			teacherCategoryVOs.add(mapper.map(teacherCategories.get(i), TeacherCategoryVO.class));
		}

		return teacherCategoryVOs;
	}
}
