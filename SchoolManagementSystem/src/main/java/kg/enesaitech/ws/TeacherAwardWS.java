package kg.enesaitech.ws;
 
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.TeacherAward;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.TeacherAwardService;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.TeacherAwardVO;

import org.dozer.Mapper;
 
@Path("/teacher_award")
public class TeacherAwardWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	TeacherAwardService teacherAwardService = TeacherAwardService
			.getInstance();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(TeacherAwardVO teacherAwardVO) {
		TeacherAward teacherAward = mapper.map(teacherAwardVO, TeacherAward.class);
		Boolean created = teacherAwardService.create(teacherAward);

		return created ? Response.ok().build() : Response.serverError().build();
	}
 
	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(TeacherAwardVO teacherAwardVO, @PathParam("id") int id) {
		boolean result = true;
		TeacherAward teacherAward = teacherAwardService
				.findById(id);
		if (teacherAward == null) {
			result = false;
		} else {
			teacherAward = mapper.map(teacherAwardVO, TeacherAward.class);
			teacherAward.setId(id);
			result = teacherAwardService.update(teacherAward);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public TeacherAwardVO get(@PathParam("id") int id) {
		TeacherAward teacherAward = teacherAwardService.get(id);
		TeacherAwardVO awardVO = mapper.map(teacherAward, TeacherAwardVO.class);
		return awardVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public List<TeacherAwardVO> search(SearchParameters searchParameters) {
		List<TeacherAward> teacherAwards = teacherAwardService.getList(searchParameters);

		List<TeacherAwardVO> awardVOs = new ArrayList<TeacherAwardVO>();

		for (int i = 0; i < teacherAwards.size(); i++) {

			awardVOs.add(mapper.map(teacherAwards.get(i), TeacherAwardVO.class));
		}

		return awardVOs;
	}
}
