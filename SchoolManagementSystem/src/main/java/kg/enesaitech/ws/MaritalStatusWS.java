package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import kg.enesaitech.entity.MaritalStatus;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.MaritalStatusService;
import kg.enesaitech.vo.MaritalStatusVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/marital_status")
public class MaritalStatusWS {
	MaritalStatusService maritalStatusService = MaritalStatusService
			.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public MaritalStatusVO get(@PathParam("id") int id) {
		MaritalStatus maritalStatus = maritalStatusService.findById(id);
		MaritalStatusVO schoolTypeVO = mapper.map(maritalStatus,
				MaritalStatusVO.class);
		return schoolTypeVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<MaritalStatusVO> search(SearchParameters searchParameters) {
		SearchResult<MaritalStatus> searchResult = maritalStatusService
				.getList(searchParameters);

		SearchResult<MaritalStatusVO> searchResultVO = new SearchResult<MaritalStatusVO>();

		for (int i = 0; i < searchResult.getResultList().size(); i++) {
//			Set<MaterialCategory> newBooks = new HashSet<MaterialCategory>();
//			for (MaterialCategory b : searchResult.getResultList().get(i).getBooks()) {
//				if (b.getBookStatus().getId() != 4) {
//					b.setBookInUses(null);
//					b.setBookType(null);
//					b.setBookStatus(null);
//					newBooks.add(b);
//				}
//			}

			searchResultVO.getResultList().add(
					mapper.map(searchResult.getResultList().get(i),
							MaritalStatusVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;

	}

}