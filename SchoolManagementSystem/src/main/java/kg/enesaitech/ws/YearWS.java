package kg.enesaitech.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Year;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.YearService;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.YearVO;

import org.dozer.Mapper;

@Path("/year")
public class YearWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	YearService yearService = YearService.getInstance();

	@POST
	@Path("/create")
	@Consumes("application/json")
	public Response create(YearVO yearVO) {
		boolean result = true;

		Year year = mapper.map(yearVO, Year.class);
		result = yearService.create(year);

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update")
	public Response update(YearVO yearVO) {
		boolean result = true;

		Year year = mapper.map(yearVO, Year.class);
		result = yearService.update(year);

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;

		Year year = yearService.findById(id);

		if (year == null) {
			result = false;
		} else {
			result = yearService.delete(id);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public YearVO get(@PathParam("id") int id) {
		Year year = yearService.get(id);
		year.setCourses(null);
		year.setStudAccountings(null);
		year.setStudents(null);
		YearVO yearVO = mapper.map(year, YearVO.class);
		return yearVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public List<YearVO> search(SearchParameters searchParameters) {
		List<Year> years = yearService.getList(searchParameters);

		List<YearVO> yearVOs = new ArrayList<YearVO>();

		for (int i = 0; i < years.size(); i++) {
			years.get(i).setCourses(null);
			years.get(i).setStudAccountings(null);
			years.get(i).setStudents(null);
			yearVOs.add(mapper.map(years.get(i), YearVO.class));
		}

		return yearVOs;
	}
}