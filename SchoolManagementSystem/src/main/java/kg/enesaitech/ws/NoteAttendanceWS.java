package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.NoteAttendance;
import kg.enesaitech.entity.Quarter;
import kg.enesaitech.entity.Student;
import kg.enesaitech.entity.Syllabus;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.CourseService;
import kg.enesaitech.service.NoteAttendanceService;
import kg.enesaitech.service.QuarterService;
import kg.enesaitech.service.StudentService;
import kg.enesaitech.service.SyllabusCategoryService;
import kg.enesaitech.service.SyllabusService;
import kg.enesaitech.vo.NoteAttendanceVO;
import kg.enesaitech.vo.QuarterNoteVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;
import kg.enesaitech.vo.SyllNoteVO;

import org.dozer.Mapper;

@Path("/note_attendance")
public class NoteAttendanceWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	NoteAttendanceService noteAttendanceService = NoteAttendanceService
			.getInstance();
	StudentService studentService = StudentService.getInstance();
	SyllabusService syllabusService = SyllabusService.getInstance();
	QuarterService quarterService = QuarterService.getInstance();
	CourseService courseService = CourseService.getInstance();
	SyllabusCategoryService syllabusCategoryService = SyllabusCategoryService.getInstance();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(NoteAttendanceVO noteAttendanceVO) {
		boolean result = true;
		NoteAttendance noteAttendance = mapper.map(noteAttendanceVO,
				NoteAttendance.class);
		if ((noteAttendanceVO.getStudent() != null)
				&& (noteAttendanceVO.getStudent().getId() != null)) {
			Student student = studentService.findById(noteAttendanceVO
					.getStudent().getId());
			if (student == null) {
				result = false;

			} else {
				noteAttendance.setStudent(student);
			}
		} else {
			noteAttendance.setStudent(null);
		}
		if ((noteAttendanceVO.getSyllabus() != null)
				&& (noteAttendanceVO.getSyllabus().getId() != null)) {
			Syllabus syllabus = syllabusService.findById(noteAttendanceVO
					.getSyllabus().getId());
			if (syllabus == null) {
				result = false;

			} else {
				noteAttendance.setSyllabus(syllabus);
			}
		} else {
			noteAttendance.setSyllabus(null);
		}

		if (result) {
			result = noteAttendanceService.create(noteAttendance);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/create_note_att")
	@Produces("text/html")
	public Response createNoteAtt(SyllNoteVO syllNoteVO) {
		boolean result = true;

		if ((syllNoteVO.getSyllabus() != null)
				&& (syllNoteVO.getSyllabus().getId() != null)) {
			Syllabus syllabus = syllabusService.findById(syllNoteVO
					.getSyllabus().getId());
			if (syllabus == null) {
				result = false;
			}
		} else if (syllNoteVO.getNoteAttendances().size() == 0) {

			result = false;

		}
		if (result) {
			result = noteAttendanceService.createNoteAtt(syllNoteVO);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}
	
	@POST
	@Path("/create_exam_note")
	@Produces("text/html")
	public Response createExamNote(SyllNoteVO syllNoteVO) {
		boolean result = true;

		if (syllNoteVO.getNoteAttendances().size() == 0) {
			result = false;
		}
		if (result) {
			result = noteAttendanceService.createExamNote(syllNoteVO);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}
	
	@POST
	@Path("/create_quarter_exam/{quarter_id}")
	@Produces("text/html")
	public Response createQuarterExam(SyllNoteVO syllNoteVO, @PathParam("quarter_id") int quarterId) {
		boolean result = true;

		if (syllNoteVO.getNoteAttendances().size() == 0) {
			result = false;
		}
		Quarter quarter = quarterService.findById(quarterId);
		if(quarter==null){
			result = false;
		}
		if (result) {
			result = noteAttendanceService.createQuarterExam(syllNoteVO, quarter);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update_note_att")
	@Produces("text/html")
	public Response updateNoteAtt(SyllNoteVO syllNoteVO) {
		boolean result = true;

		if ((syllNoteVO.getSyllabus() != null)
				&& (syllNoteVO.getSyllabus().getId() != null)) {
			Syllabus syllabus = syllabusService.findById(syllNoteVO
					.getSyllabus().getId());
			if (syllabus == null) {
				result = false;
			}
		} else if (syllNoteVO.getNoteAttendances().size() == 0) {

			result = false;

		}
		if (result) {
			result = noteAttendanceService.updateNoteAtt(syllNoteVO);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}
	
	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(NoteAttendanceVO noteAttendanceVO,
			@PathParam("id") int id) {
		boolean result = true;
		NoteAttendance noteAttendance = noteAttendanceService.findById(id);
		if (noteAttendance == null) {
			result = false;
		} else {
			noteAttendance = mapper.map(noteAttendanceVO, NoteAttendance.class);
			if ((noteAttendanceVO.getStudent() != null)
					&& (noteAttendanceVO.getStudent().getId() != null)) {
				Student student = studentService.findById(noteAttendanceVO
						.getStudent().getId());
				if (student == null) {
					result = false;

				} else {
					noteAttendance.setStudent(student);
				}
			} else {
				noteAttendance.setStudent(null);
			}
			if ((noteAttendanceVO.getSyllabus() != null)
					&& (noteAttendanceVO.getSyllabus().getId() != null)) {
				Syllabus syllabus = syllabusService.findById(noteAttendanceVO
						.getSyllabus().getId());
				if (syllabus == null) {
					result = false;

				} else {
					noteAttendance.setSyllabus(syllabus);
				}
			} else {
				noteAttendance.setSyllabus(null);
			}

			if (result) {
				result = noteAttendanceService.update(noteAttendance);
			}
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		NoteAttendance noteAttendance = noteAttendanceService.findById(id);
		if (noteAttendance == null) {
			result = false;
		} else {
			result = noteAttendanceService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public NoteAttendanceVO get(@PathParam("id") int id) {
		NoteAttendance noteAttendance = noteAttendanceService.get(id);
		if (noteAttendance.getStudent() != null) {

			if (noteAttendance.getStudent().getCGroup() != null) {

				noteAttendance.getStudent().getCGroup().setCourses(null);
				noteAttendance.getStudent().getCGroup().setStudents(null);
				if (noteAttendance.getStudent().getCGroup().getTeacher() != null) {
					noteAttendance.getStudent().getCGroup().setTeacher(null);
				}
				if (noteAttendance.getStudent().getCGroup().getGrade() != null) {
					noteAttendance.getStudent().getCGroup().getGrade()
							.setCGroups(null);
				}
				if (noteAttendance.getStudent().getCGroup().getClassLetter() != null) {
					noteAttendance.getStudent().getCGroup().getClassLetter()
							.setCGroups(null);
				}
			}
			noteAttendance.getStudent().setNationality(null);
			noteAttendance.getStudent().setCountry(null);
			noteAttendance.getStudent().setStatus(null);
			noteAttendance.getStudent().setNoteAttendances(null);
			noteAttendance.getStudent().setParents(null);
			noteAttendance.getStudent().setStudAccountings(null);
			noteAttendance.getStudent().setYear(null);
		}
		if (noteAttendance.getSyllabus() != null) {
			noteAttendance.getSyllabus().setNoteAttendances(null);
			if (noteAttendance.getSyllabus().getSyllabusCategory().getCourse() != null) {
				noteAttendance.getSyllabus().getSyllabusCategory().setCourse(null);
			}
			if (noteAttendance.getSyllabus().getSyllabusCategory().getQuarter() != null) {
				noteAttendance.getSyllabus().getSyllabusCategory().setQuarter(null);
			}
		}
		if (noteAttendance.getNoteType() != null) {
			noteAttendance.getNoteType().setNoteAttendances(null);
			noteAttendance.getNoteType().setStatus(null);
		}
		if (noteAttendance.getNoteSystem() != null) {
			noteAttendance.getNoteSystem().setNoteAttendances(null);
		}
		NoteAttendanceVO noteAttendanceVO = mapper.map(noteAttendance,
				NoteAttendanceVO.class);
		return noteAttendanceVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<NoteAttendanceVO> search(
			SearchParameters searchParameters) {
		SearchResult<NoteAttendance> searchResult = noteAttendanceService
				.getList(searchParameters);

		SearchResult<NoteAttendanceVO> searchResultVO = new SearchResult<NoteAttendanceVO>();
		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			if (searchResult.getResultList().get(i).getStudent() != null) {

				if (searchResult.getResultList().get(i).getStudent()
						.getCGroup() != null) {

					searchResult.getResultList().get(i).getStudent()
							.getCGroup().setCourses(null);
					searchResult.getResultList().get(i).getStudent()
							.getCGroup().setStudents(null);
					if (searchResult.getResultList().get(i).getStudent()
							.getCGroup().getTeacher() != null) {
						searchResult.getResultList().get(i).getStudent()
								.getCGroup().setTeacher(null);
					}
					if (searchResult.getResultList().get(i).getStudent()
							.getCGroup().getGrade() != null) {
						searchResult.getResultList().get(i).getStudent()
								.getCGroup().getGrade().setCGroups(null);
					}
					if (searchResult.getResultList().get(i).getStudent()
							.getCGroup().getClassLetter() != null) {
						searchResult.getResultList().get(i).getStudent()
								.getCGroup().getClassLetter().setCGroups(null);
					}
				}
				searchResult.getResultList().get(i).getStudent()
						.setNationality(null);
				searchResult.getResultList().get(i).getStudent()
						.setCountry(null);
				searchResult.getResultList().get(i).getStudent()
						.setStatus(null);
				searchResult.getResultList().get(i).getStudent()
						.setNoteAttendances(null);
				searchResult.getResultList().get(i).getStudent()
						.setParents(null);
				searchResult.getResultList().get(i).getStudent()
						.setStudAccountings(null);
				searchResult.getResultList().get(i).getStudent().setYear(null);
			}
			if (searchResult.getResultList().get(i).getSyllabus() != null) {
				searchResult.getResultList().get(i).getSyllabus()
						.setNoteAttendances(null);
				if (searchResult.getResultList().get(i).getSyllabus()
						.getSyllabusCategory().getCourse() != null) {
					searchResult.getResultList().get(i).getSyllabus()
							.getSyllabusCategory().setCourse(null);
				}
				if (searchResult.getResultList().get(i).getSyllabus()
						.getSyllabusCategory().getQuarter() != null) {
					searchResult.getResultList().get(i).getSyllabus()
							.getSyllabusCategory().setQuarter(null);
				}
			}

			if (searchResult.getResultList().get(i).getNoteType() != null) {
				searchResult.getResultList().get(i).getNoteType()
						.setNoteAttendances(null);
				searchResult.getResultList().get(i).getNoteType()
						.setStatus(null);
			}

			if (searchResult.getResultList().get(i).getNoteSystem() != null) {
				searchResult.getResultList().get(i).getNoteSystem()
						.setNoteAttendances(null);
			}
			searchResultVO.getResultList().add(
					mapper.map(searchResult.getResultList().get(i),
							NoteAttendanceVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}

	@POST
	@Path("/notes")
	@Produces("application/json")
	public SearchResult<QuarterNoteVO> notes(SearchParameters searchParameters) {
		SearchResult<QuarterNoteVO> searchResultVO = noteAttendanceService
				.getQuaterNoteByStudent(searchParameters);

		return searchResultVO;
	}
}
