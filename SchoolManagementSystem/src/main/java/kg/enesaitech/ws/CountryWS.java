
package kg.enesaitech.ws;
 
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import kg.enesaitech.entity.Country;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.CountryService;
import kg.enesaitech.vo.CountryVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;
 
@Path("/country")
public class CountryWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	CountryService countryService = CountryService.getInstance();
 
   
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public CountryVO get(@PathParam("id") int id) {
		Country country = countryService.get(id);
		country.setStudents(null);
		country.setTeachers(null);
		CountryVO countryVO = mapper.map(country, CountryVO.class);
		return countryVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public List<CountryVO> search(SearchParameters searchParameters) {
		List<Country> countries = countryService.getList(searchParameters);

		List<CountryVO> countryVOs = new ArrayList<CountryVO>();

		for (int i = 0; i < countries.size(); i++) {
			countries.get(i).setTeachers(null);
			countries.get(i).setStudents(null);
			countryVOs.add(mapper.map(countries.get(i), CountryVO.class));
		}

		return countryVOs;
	}
}