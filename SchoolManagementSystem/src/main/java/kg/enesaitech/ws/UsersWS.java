package kg.enesaitech.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Users;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.UsersService;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.UsersVO;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.dozer.Mapper;

@Path("/users")
public class UsersWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	UsersService usersService = UsersService.getInstance();

	@POST
	@Path("/create")
	@Consumes("application/json")
	public Response create(UsersVO userVO) {
		boolean result = true;

		Users user = mapper.map(userVO, Users.class);
		result = usersService.create(user);

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	public Response update(UsersVO userVO, @PathParam("id") int id) {
		boolean result = true;
		Users user = usersService.findById(id);
		if (user == null) {
			result = false;
		} else {
			user = mapper.map(userVO, Users.class);
			result = usersService.update(user);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;

		Users user = usersService.findById(id);

		if (user == null) {
			result = false;
		} else {
			result = usersService.delete(id);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	@RequiresAuthentication
	@RequiresRoles("admin")
	public UsersVO get(@PathParam("id") int id) {
		Users user = usersService.get(id);
		user.setUsersRoles(null);
		UsersVO userVO = mapper.map(user, UsersVO.class);

		return userVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public List<UsersVO> search(SearchParameters searchParameters) {
		List<Users> users = usersService.getList(searchParameters);

		List<UsersVO> usersVOs = new ArrayList<UsersVO>();

		for (int i = 0; i < users.size(); i++) {
			users.get(i).setUsersRoles(null);
			usersVOs.add(mapper.map(users.get(i), UsersVO.class));
		}

		return usersVOs;
	}
}