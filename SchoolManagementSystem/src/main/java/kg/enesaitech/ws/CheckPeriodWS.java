package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.CheckPeriod;
import kg.enesaitech.entity.News;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.CheckPeriodService;
import kg.enesaitech.vo.CheckPeriodVO;
import kg.enesaitech.vo.NewsVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/check_period")
public class CheckPeriodWS {
	CheckPeriodService checkPeriodService = CheckPeriodService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	public Response create(CheckPeriodVO checkPeriodVO) {

		CheckPeriod checkPeriod = mapper.map(checkPeriodVO, CheckPeriod.class);
		Boolean created = checkPeriodService.create(checkPeriod);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(CheckPeriodVO checkPeriodVO, @PathParam("id") int id) {
		boolean result = true;
		CheckPeriod checkPeriod = checkPeriodService.findById(id);
		if (checkPeriod == null) {
			result = false;
		} else {
			checkPeriod = mapper.map(checkPeriodVO, CheckPeriod.class);
			result = checkPeriodService.update(checkPeriod);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		CheckPeriod checkPeriod = checkPeriodService.findById(id);
		if (checkPeriod == null) {
			result = false;
		} else {
			result = checkPeriodService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public CheckPeriodVO get(@PathParam("id") int id) {
		CheckPeriod checkPeriod= checkPeriodService.get(id);
		CheckPeriodVO checkPeriodVO = mapper.map(checkPeriod, CheckPeriodVO.class);
		return checkPeriodVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<CheckPeriodVO> search(SearchParameters searchParameters) {
		SearchResult<CheckPeriod> searchResult = checkPeriodService.getList(searchParameters);

		SearchResult<CheckPeriodVO> searchResultVO = new SearchResult<CheckPeriodVO>();

		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i),CheckPeriodVO.class));
		}

		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
}
