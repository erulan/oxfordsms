package kg.enesaitech.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Lesson;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.LessonService;
import kg.enesaitech.vo.LessonVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

@Path("/lesson")
public class LessonWS {
	LessonService lessonService = LessonService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	public Response create(LessonVO lessonVO) {

		Lesson lesson = mapper.map(lessonVO, Lesson.class);
		Boolean created = lessonService.create(lesson);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(LessonVO lessonVO, @PathParam("id") int id) {
		boolean result = true;
		Lesson lesson = lessonService.findById(id);
		if (lesson == null) {
			result = false;
		} else {
			lesson = mapper.map(lessonVO, Lesson.class);
			result = lessonService.update(lesson);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		Lesson lesson = lessonService.findById(id);
		if (lesson == null) {
			result = false;
		} else {
			result = lessonService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public LessonVO get(@PathParam("id") int id) {
		Lesson lesson = lessonService.get(id);
		lesson.setCourses(null);
		LessonVO lessonVO = mapper.map(lesson, LessonVO.class);
		return lessonVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public List<LessonVO> search(SearchParameters searchParameters) {
		List<Lesson> lessons = lessonService.getList(searchParameters);

		List<LessonVO> lessonVOs = new ArrayList<LessonVO>();

		for (int i = 0; i < lessons.size(); i++) {
			lessons.get(i).setCourses(null);
			lessonVOs.add(mapper.map(lessons.get(i), LessonVO.class));
		}

		return lessonVOs;
	}
}
