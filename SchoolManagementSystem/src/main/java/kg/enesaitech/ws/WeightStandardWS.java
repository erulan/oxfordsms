package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.WeightStandard;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.WeightStandardService;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;
import kg.enesaitech.vo.WeightStandardVO;

import org.dozer.Mapper;

@Path("/weight_standard")
public class WeightStandardWS {
	WeightStandardService weightStandardService = WeightStandardService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	public Response create(WeightStandardVO weightStandardVO) {

		WeightStandard weightStandard = mapper.map(weightStandardVO, WeightStandard.class);
		Boolean created = weightStandardService.create(weightStandard);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(WeightStandardVO weightStandardVO, @PathParam("id") int id) {
		boolean result = true;
		WeightStandard weightStandard = weightStandardService.findById(id);
		if (weightStandard == null) {
			result = false;
		} else {
			weightStandard = mapper.map(weightStandardVO, WeightStandard.class);
			result = weightStandardService.update(weightStandard);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		WeightStandard weightStandard = weightStandardService.findById(id);
		if (weightStandard == null) {
			result = false;
		} else {
			result = weightStandardService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public WeightStandardVO get(@PathParam("id") int id) {
		WeightStandard weightStandard = weightStandardService.get(id);
		WeightStandardVO weightStandardVO = mapper.map(weightStandard, WeightStandardVO.class);
		return weightStandardVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<WeightStandardVO> search(SearchParameters searchParameters) {
		SearchResult<WeightStandard> weightStandards = weightStandardService.getList(searchParameters);

		SearchResult<WeightStandardVO> weightStandardVOs = new SearchResult<WeightStandardVO>();

		for (int i = 0; i < weightStandards.getResultList().size(); i++) {
			weightStandardVOs.getResultList().add(mapper.map(weightStandards.getResultList().get(i), WeightStandardVO.class));
		}

		return weightStandardVOs;
	}
}
