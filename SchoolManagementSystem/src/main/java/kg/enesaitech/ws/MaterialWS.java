package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Material;
import kg.enesaitech.entity.OwnerType;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.MaterialService;
import kg.enesaitech.service.OwnerTypeService;
import kg.enesaitech.vo.MaterialVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/material")
public class MaterialWS {
	MaterialService materialService = MaterialService.getInstance();
	OwnerTypeService ownerTypeService = OwnerTypeService.getInstance();

	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	public Response create(MaterialVO materialVO) {

		boolean result = true;

		Material material = mapper.map(materialVO, Material.class);
		if ((materialVO.getOwnerType() != null)
				&& (materialVO.getOwnerType().getId() != null)) {
			OwnerType ownerType = ownerTypeService.findById(materialVO.getOwnerType().getId());
			if (ownerType == null) {
				result = false;

			} else {
				material.setOwnerType(ownerType);
			}
		} else {
			material.setOwnerType(null);
		}

		if (result) {
			result = materialService.create(material);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}
	
	

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(MaterialVO materialVO, @PathParam("id") int id) {

		boolean result = true;
		Material material = materialService.findById(id);
		if (material == null) {
			result = false;
		} else {
			materialVO.setId(id);
			material = mapper.map(materialVO, Material.class);
			if ((materialVO.getOwnerType() != null)
					&& (materialVO.getOwnerType().getId() != null)) {
				OwnerType ownerType = ownerTypeService.findById(materialVO.getOwnerType().getId());
				if (ownerType == null) {
					result = false;

				} else {
					material.setOwnerType(ownerType);
				}
			} else {
				material.setOwnerType(null);
			}
			if (result) {
				
				result = materialService.update(material);
			}

		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {

		Boolean result = true;
		Material material = materialService.findById(id);
		if (material == null) {
			result = false;
		} else {
			result = materialService.delete(id);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}
	

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public MaterialVO get(@PathParam("id") int id) {
		Material material = materialService.get(id);
		MaterialVO materialVO = mapper.map(material, MaterialVO.class);
		return materialVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<MaterialVO> search(SearchParameters searchParameters) {
		SearchResult<Material> searchResult = materialService.getList(searchParameters);

		SearchResult<MaterialVO> searchResultVO = new SearchResult<MaterialVO>();

		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), MaterialVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
}