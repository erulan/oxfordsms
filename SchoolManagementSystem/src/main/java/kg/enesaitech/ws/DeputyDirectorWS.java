package kg.enesaitech.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.NoteAttendanceService;
import kg.enesaitech.vo.AverageNoteVO;
import kg.enesaitech.vo.MonitoringVO;
import kg.enesaitech.vo.QuarterNoteVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/deputy_director")
public class DeputyDirectorWS {

	Mapper mapper = DozerMapperProvider.getMapper();
	NoteAttendanceService noteAttendanceService = NoteAttendanceService.getInstance();

//	@GET
//	@Path("/{id}")
//	@Produces("application/json")
//	public NationalityVO get(@PathParam("id") int id) {
//		Nationality nationality = nationalityService.get(id);
//		nationality.setStudents(null);
//		nationality.setTeachers(null);
//		NationalityVO nationalityVO = mapper.map(nationality,
//				NationalityVO.class);
//		return nationalityVO;
//	}
	@POST
	@Path("/by_course")
	@Produces("application/json")
	public SearchResult<QuarterNoteVO> search(SearchParameters searchParameters) {
		SearchResult<QuarterNoteVO> searchResult = noteAttendanceService
				.getByCourses(searchParameters);
		return searchResult;
	}
	@POST
	@Path("/by_c_group")
	@Produces("application/json")
	public SearchResult<AverageNoteVO> searchByClass(SearchParameters searchParameters) {
		SearchResult<AverageNoteVO> searchResult = noteAttendanceService
				.getMonitoringByClass(searchParameters);
		return searchResult;
	}
	@POST
	@Path("/general")
	@Produces("application/json")
	public SearchResult<AverageNoteVO> searchGeneral(SearchParameters searchParameters) {
		SearchResult<AverageNoteVO> searchResult = noteAttendanceService
				.getMonitoringGeneral(searchParameters);
		return searchResult;
	}
	@POST
	@Path("/by_exam")
	@Produces("application/json")
	public SearchResult<MonitoringVO> searchByExam(SearchParameters searchParameters) {
		SearchResult<MonitoringVO> searchResult = noteAttendanceService
				.getQuaterNote(searchParameters);
		return searchResult;
	}
}
