package kg.enesaitech.ws;
 
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import kg.enesaitech.entity.Status;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.EducationStatusService;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.StatusVO;

import org.dozer.Mapper;
 
@Path("/status")
public class EducationStatusWS {

	Mapper mapper = DozerMapperProvider.getMapper();
	EducationStatusService educationStatusService = EducationStatusService.getInstance();
   
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public StatusVO get(@PathParam("id") int id) {
		Status educationStatus = educationStatusService.get(id);
		StatusVO educationStatusVO = mapper.map(educationStatus, StatusVO.class);
		return educationStatusVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public List<StatusVO> search(SearchParameters searchParameters) {
		List<Status> educationStatus = educationStatusService.getList(searchParameters);

		List<StatusVO> educationStatusVOs = new ArrayList<StatusVO>();

		for (int i = 0; i < educationStatus.size(); i++) {
			educationStatusVOs.add(mapper.map(educationStatus.get(i), StatusVO.class));
		}

		return educationStatusVOs;
	}
}
