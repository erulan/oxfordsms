package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Parents;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.ParentService;
import kg.enesaitech.vo.ParentsVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/parents")
public class ParentWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	ParentService parentService = ParentService.getInstance();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(ParentsVO parentsVO) {
		Parents parents = mapper.map(parentsVO, Parents.class);
		Boolean created = parentService.create(parents);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(ParentsVO parentsVO, @PathParam("id") int id) {
		boolean result = true;
		Parents parents = parentService.findById(id);
		if (parents == null) {
			result = false;
		} else {
			parents = mapper.map(parentsVO, Parents.class);
			result = parentService.update(parents);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		Parents parents = parentService.findById(id);
		if (parents == null) {
			result = false;
		} else {
			result = parentService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public ParentsVO get(@PathParam("id") int id) {
		Parents parents = parentService.get(id);
		parents.setStudents(null);
		ParentsVO parentsVO = mapper.map(parents, ParentsVO.class);
		return parentsVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<ParentsVO> search(SearchParameters searchParameters) {
		SearchResult<Parents> searchResult = parentService.getList(searchParameters);

		SearchResult<ParentsVO> searchResultVO = new SearchResult<ParentsVO>();

		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			searchResult.getResultList().get(i).setStudents(null);
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), ParentsVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
}
