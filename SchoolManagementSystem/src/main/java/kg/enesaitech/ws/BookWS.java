package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Book;
import kg.enesaitech.entity.BookStatus;
import kg.enesaitech.entity.BookType;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.BookService;
import kg.enesaitech.service.BookStatusService;
import kg.enesaitech.service.BookTypeService;
import kg.enesaitech.vo.BookVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.dozer.Mapper;

@RequiresAuthentication
@RequiresRoles( "admin" )
@Path("/book")
public class BookWS {
	BookService bookService = BookService.getInstance();
	BookTypeService bookTypeService = BookTypeService.getInstance();
	BookStatusService bookStatusService = BookStatusService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(BookVO bookVO) {
		boolean result = true;
		Book book = mapper.map(bookVO, Book.class);
		if ((bookVO.getBookType() != null)
				&& (bookVO.getBookType().getId() != null)) {
			BookType bookType = bookTypeService.findById(bookVO.getBookType()
					.getId());
			if (bookType == null) {
				result = false;

			} else {
				book.setBookType(bookType);
			}
		} else {
			result = false;
		}
		if ((bookVO.getBookStatus() != null)
				&& (bookVO.getBookStatus().getId() != null)) {
			BookStatus bookStatus = bookStatusService.findById(bookVO
					.getBookStatus().getId());
			if (bookStatus == null) {
				result = false;

			} else {
				book.setBookStatus(bookStatus);
			}
		} else {
			book.setBookStatus(null);
		}
		if (result) {
			bookService.create(book);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(BookVO bookVO, @PathParam("id") int id) {
		boolean result = true;
		Book book = bookService.findById(id);
		if (book == null) {
			result = false;
		} else {
			book = mapper.map(bookVO, Book.class);
			if ((bookVO.getBookType() != null)
					&& (bookVO.getBookType().getId() != null)) {
				BookType bookType = bookTypeService.findById(bookVO
						.getBookType().getId());
				if (bookType == null) {
					result = false;

				} else {
					book.setBookType(bookType);
				}
			} else {
				result = false;
			}
			if ((bookVO.getBookStatus() != null)
					&& (bookVO.getBookStatus().getId() != null)) {
				BookStatus bookStatus = bookStatusService.findById(bookVO
						.getBookStatus().getId());
				if (bookStatus == null) {
					result = false;

				} else {
					book.setBookStatus(bookStatus);
				}
			} else {
				book.setBookStatus(null);
			}
			if (result) {
				bookService.update(book);
			}
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		Book book = bookService.findById(id);
		if (book == null) {
			result = false;
		} else {
			result = bookService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public BookVO get(@PathParam("id") int id) {
		Book book = bookService.get(id);
		book.setBookInUses(null);
		book.getBookType().setCourses(null);
		book.getBookType().getCategory().setBookTypes(null);
		book.getBookType().setBooks(null);
		book.getBookStatus().setBooks(null);
		BookVO bookVO = mapper.map(book, BookVO.class);
		return bookVO;
	}

	@RequiresAuthentication
	@RequiresRoles( "admin" )
	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<BookVO> search(SearchParameters searchParameters) {
		SearchResult<Book> searchResult = bookService.getList(searchParameters);

		SearchResult<BookVO> searchResultVO = new SearchResult<BookVO>();

		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			searchResult.getResultList().get(i).setBookInUses(null);
			searchResult.getResultList().get(i).getBookType().setCourses(null);
			searchResult.getResultList().get(i).getBookType().getCategory().setBookTypes(null);
			searchResult.getResultList().get(i).getBookType().setBooks(null);
			searchResult.getResultList().get(i).getBookStatus().setBooks(null);

			searchResultVO.getResultList().add(
					mapper.map(searchResult.getResultList().get(i),
							BookVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;

	}
}