package kg.enesaitech.ws;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Country;
import kg.enesaitech.entity.Nationality;
import kg.enesaitech.entity.Teacher;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.CGroupService;
import kg.enesaitech.service.CountryService;
import kg.enesaitech.service.NationalityService;
import kg.enesaitech.service.OblastService;
import kg.enesaitech.service.RegionService;
import kg.enesaitech.service.TeacherService;
import kg.enesaitech.userException.BusinessException;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;
import kg.enesaitech.vo.TeacherVO;

import org.dozer.Mapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

@Path("/teacher")
public class TeacherWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	TeacherService teacherService = TeacherService.getInstance();
	RegionService regionService = RegionService.getInstance();
	CGroupService cGroupService = CGroupService.getInstance();
	OblastService oblastService = OblastService.getInstance();
	CountryService countryService = CountryService.getInstance();
	NationalityService nationalityService = NationalityService.getInstance();

	@POST
	@Path("/create")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(
			@FormDataParam("photo") InputStream fileInputStream,
            @FormDataParam("photo") FormDataContentDisposition contentDispositionHeader,
            @FormDataParam("teacher") TeacherVO teacherVO) {
		boolean result = true;
		
		new File(System.getProperty("user.home") + "/files/").mkdirs();
		
		String filePath = contentDispositionHeader.getFileName();

		if(filePath!=null){
			File f = new File(System.getProperty("user.home") + "/files/" + filePath);
			if(f.exists() && !f.isDirectory()) {
				result=false;
				throw new BusinessException("Photo with this name is exists !! Change name of photo");
			}
			// save the file to the server
			saveFile(fileInputStream, System.getProperty("user.home") + "/files/" + filePath);
			teacherVO.setPhoto(filePath);
		}
		

		Teacher teacher = mapper.map(teacherVO, Teacher.class);
		if ((teacherVO.getCountry() != null)
				&& (teacherVO.getCountry().getId() != null)) {
			Country country = countryService.findById(teacherVO.getCountry()
					.getId());
			if (country == null) {
				result = false;

			} else {
				teacher.setCountry(country);
			}
		} else {
			teacher.setCountry(null);
		}
		if ((teacherVO.getNationality() != null)
				&& (teacherVO.getNationality().getId() != null)) {
			Nationality nationality = nationalityService.findById(teacherVO
					.getNationality().getId());
			if (nationality == null) {
				result = false;

			} else {
				teacher.setNationality(nationality);
			}
		} else {
			teacher.setNationality(null);
		}
		
		if (result) {
			result = teacherService.create(teacher);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(
			@FormDataParam("photo") InputStream fileInputStream,
			@FormDataParam("photo") FormDataContentDisposition contentDispositionHeader,
            @FormDataParam("teacher") TeacherVO teacherVO,
			@PathParam("id") int id) {
		boolean result = true;
		Teacher teacher = teacherService.findById(id);
		if (teacher == null) {
			result = false;
		} else {
			new File(System.getProperty("user.home") + "/files/").mkdirs();
			
			String filePath = contentDispositionHeader.getFileName();
			if(filePath!=null){
				File f = new File(System.getProperty("user.home") + "/files/" + filePath);
				if(f.exists() && !f.isDirectory()) {
					result=false;
					throw new BusinessException("Photo with this name is exists !! Change name of photo");
				}
				// save the file to the server
				saveFile(fileInputStream, System.getProperty("user.home") + "/files/" + filePath);
				teacherVO.setPhoto(filePath);
			}
			teacher = mapper.map(teacherVO, Teacher.class);
			
			if ((teacherVO.getCountry() != null)
					&& (teacherVO.getCountry().getId() != null)) {
				Country country = countryService.findById(teacherVO
						.getCountry().getId());
				if (country == null) {
					result = false;

				} else {
					teacher.setCountry(country);
				}
			} else {
				teacher.setCountry(null);
			}
			if ((teacherVO.getNationality() != null)
					&& (teacherVO.getNationality().getId() != null)) {
				Nationality nationality = nationalityService.findById(teacherVO
						.getNationality().getId());
				if (nationality == null) {
					result = false;

				} else {
					teacher.setNationality(nationality);
				}
			} else {
				teacher.setNationality(null);
			}
			
			if (result) {
				result = teacherService.update(teacher);
			}
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;

		Teacher teacher = teacherService.findById(id);

		if (teacher == null) {
			result = false;
		} else {
			result = teacherService.delete(id);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public TeacherVO get(@PathParam("id") int id) {
		Teacher teacher = teacherService.get(id);
		teacher.setCGroups(null);
		teacher.setCourses(null);
		teacher.setExTeachers(null);
		if (teacher.getNationality() != null) {
			teacher.getNationality().setStudents(null);
			teacher.getNationality().setTeachers(null);
		}
		if (teacher.getCountry() != null) {
			teacher.getCountry().setStudents(null);
			teacher.getCountry().setTeachers(null);
		}
		TeacherVO teacherVO = mapper.map(teacher, TeacherVO.class);
		return teacherVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<TeacherVO> search(SearchParameters searchParameters) {
		SearchResult<Teacher> searchResult = teacherService.getList(searchParameters);
//		log.log(Level.DEBUG, "This is debug from Level.DEBUG");
//		log.error("asdfas");
		
		SearchResult<TeacherVO> searchResultVO = new SearchResult<TeacherVO>();
		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			searchResult.getResultList().get(i).setCGroups(null);
			searchResult.getResultList().get(i).setCourses(null);
			searchResult.getResultList().get(i).setExTeachers(null);
			if (searchResult.getResultList().get(i).getNationality() != null) {
				searchResult.getResultList().get(i).getNationality().setStudents(null);
				searchResult.getResultList().get(i).getNationality().setTeachers(null);
			}
			if (searchResult.getResultList().get(i).getCountry() != null) {
				searchResult.getResultList().get(i).getCountry().setStudents(null);
				searchResult.getResultList().get(i).getCountry().setTeachers(null);
			}
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), TeacherVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
	
	// save uploaded file to a defined location on the server
    private void saveFile(InputStream uploadedInputStream,
            String serverLocation) {
 
        try {
            OutputStream outpuStream = new FileOutputStream(new File(serverLocation));
            int read = 0;
            byte[] bytes = new byte[1024];
 
            outpuStream = new FileOutputStream(new File(serverLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                outpuStream.write(bytes, 0, read);
            }
            outpuStream.flush();
            outpuStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
    }
}