package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.MaterialCategory;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.MaterialCategoryService;
import kg.enesaitech.vo.MaterialCategoryVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/material_category")
public class MaterialCategoryWS {
	MaterialCategoryService materialCategoryService = MaterialCategoryService
			.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(MaterialCategoryVO materialCategoryVO) {
		MaterialCategory materialCategory = mapper.map(materialCategoryVO,
				MaterialCategory.class);
		Boolean created = materialCategoryService.create(materialCategory);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(MaterialCategoryVO materialCategoryVO, @PathParam("id") Integer id) {
		MaterialCategory materialCategory = null;
		boolean result = true;
		if(id != null){
			materialCategory = materialCategoryService.findById(id);
			
		}
		if (materialCategory == null) {
			result = false;
		} else {
			materialCategoryVO.setId(id);
			materialCategory = mapper.map(materialCategoryVO, MaterialCategory.class);
			result = materialCategoryService.update(materialCategory);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		MaterialCategory materialCategory = materialCategoryService
				.findById(id);
		if (materialCategory == null) {
			result = false;
		} else {
			result = materialCategoryService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public MaterialCategoryVO get(@PathParam("id") int id) {
		MaterialCategory materialCategory = materialCategoryService.get(id);
		MaterialCategoryVO categoryVO = mapper.map(materialCategory,
				MaterialCategoryVO.class);
		return categoryVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<MaterialCategoryVO> search(SearchParameters searchParameters) {
		SearchResult<MaterialCategory> searchResult = materialCategoryService
				.getList(searchParameters);

		SearchResult<MaterialCategoryVO> searchResultVO = new SearchResult<MaterialCategoryVO>();

		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			searchResultVO.getResultList().add(
					mapper.map(searchResult.getResultList().get(i),
							MaterialCategoryVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;

	}

}