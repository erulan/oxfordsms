package kg.enesaitech.ws;
 
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import kg.enesaitech.entity.Role;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.RoleService;
import kg.enesaitech.vo.RoleVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;
 
@Path("/role")
public class RoleWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	RoleService roleService = RoleService.getInstance();
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public RoleVO get(@PathParam("id") int id) {
		Role role = roleService.get(id);
		role.setUsersRoles(null);
		RoleVO roleVO = mapper.map(role, RoleVO.class);
		return roleVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public List<RoleVO> search(SearchParameters searchParameters) {
		List<Role> roles = roleService.getList(searchParameters).getResultList();

		List<RoleVO> roleVOs = new ArrayList<RoleVO>();

		for (int i = 0; i < roles.size(); i++) {
			roles.get(i).setUsersRoles(null);
			roleVOs.add(mapper.map(roles.get(i), RoleVO.class));
		}

		return roleVOs;
	}
}
