package kg.enesaitech.ws;
 
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.MaterialType;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.MaterialTypeService;
import kg.enesaitech.vo.MaterialTypeVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;
 
@Path("/material_type")
public class MaterialTypeWS {
 
	MaterialTypeService materialTypeService = MaterialTypeService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(MaterialTypeVO materialTypeVO) {
		MaterialType materialType = mapper.map(materialTypeVO, MaterialType.class);
		Boolean created = materialTypeService.create(materialType);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(MaterialTypeVO materialTypeVO, @PathParam("id") Integer id) {
		MaterialType materialType = null;
		boolean result = true;
		if(id != null){
			materialType = materialTypeService.findById(id);
			
		}
		if (materialType == null) {
			result = false;
		} else {
			materialTypeVO.setId(id);
			materialType = mapper.map(materialTypeVO, MaterialType.class);
			result = materialTypeService.update(materialType);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		MaterialType materialType = materialTypeService.findById(id);
		if (materialType == null) {
			result = false;
		} else {
			result = materialTypeService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public MaterialTypeVO get(@PathParam("id") int id) {
		MaterialType materialType = materialTypeService.get(id);
		MaterialTypeVO materialTypeVO = mapper.map(materialType, MaterialTypeVO.class);
		return materialTypeVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<MaterialTypeVO> search(SearchParameters searchParameters) {
		SearchResult<MaterialType> searchResult = materialTypeService.getList(searchParameters);

		SearchResult<MaterialTypeVO> searchResultVO = new SearchResult<MaterialTypeVO>();

		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), MaterialTypeVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
}
