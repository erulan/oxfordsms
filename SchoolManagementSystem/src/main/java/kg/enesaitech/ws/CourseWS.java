package kg.enesaitech.ws;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.BookType;
import kg.enesaitech.entity.CGroup;
import kg.enesaitech.entity.Course;
import kg.enesaitech.entity.Lesson;
import kg.enesaitech.entity.Student;
import kg.enesaitech.entity.Teacher;
import kg.enesaitech.entity.Year;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.BookTypeService;
import kg.enesaitech.service.CGroupService;
import kg.enesaitech.service.CourseService;
import kg.enesaitech.service.LessonService;
import kg.enesaitech.service.TeacherService;
import kg.enesaitech.service.YearService;
import kg.enesaitech.vo.CourseVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/course")
public class CourseWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	CourseService courseService = CourseService.getInstance();
	CGroupService cGroupService = CGroupService.getInstance();
	LessonService lessonService = LessonService.getInstance();
	TeacherService teacherService = TeacherService.getInstance();
	YearService yearService = YearService.getInstance();
	BookTypeService bookTypeService = BookTypeService.getInstance();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(CourseVO courseVO) {
		boolean result = true;
		Course course = mapper.map(courseVO, Course.class);
		if ((courseVO.getClassGroup() != null)
				&& (courseVO.getClassGroup().getId() != null)) {
			CGroup cGroup = cGroupService
					.findById(courseVO.getClassGroup().getId());
			if (cGroup == null) {
				result = false;

			} else {
				course.setCGroup(cGroup);
			}
		} else {
			course.setCGroup(null);
		}
		if ((courseVO.getLesson() != null)
				&& (courseVO.getLesson().getId() != null)) {
			Lesson lesson = lessonService
					.findById(courseVO.getLesson().getId());
			if (lesson == null) {
				result = false;

			} else {
				course.setLesson(lesson);
			}
		} else {
			course.setLesson(null);
		}

		if ((courseVO.getTeacher() != null)
				&& (courseVO.getTeacher().getId() != null)) {
			Teacher teacher = teacherService.findById(courseVO.getTeacher()
					.getId());
			if (teacher == null) {
				result = false;

			} else {
				course.setTeacher(teacher);
			}
		} else {
			course.setTeacher(null);
		}

		if ((courseVO.getYear() != null)
				&& (courseVO.getYear().getId() != null)) {
			Year year = yearService.findById(courseVO.getYear().getId());
			if (year == null) {
				result = false;

			} else {
				course.setYear(year);
			}
		} else {
			course.setYear(null);
		}

		if ((courseVO.getBookType() != null)
				&& (courseVO.getBookType().getId() != null)) {
			BookType bookType = bookTypeService.findById(courseVO.getBookType()
					.getId());
			if (bookType == null) {
				result = false;

			} else {
				course.setBookType(bookType);
			}
		} else {
			course.setBookType(null);
		}
		if (result) {
			result = courseService.create(course);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(CourseVO courseVO, @PathParam("id") int id) {
		boolean result = true;
		Course course = courseService.findById(id);
		if (course == null) {
			result = false;
		} else {
			course = mapper.map(courseVO, Course.class);
			if ((courseVO.getClassGroup() != null)
					&& (courseVO.getClassGroup().getId() != null)) {
				CGroup cGroup = cGroupService.findById(courseVO.getClassGroup()
						.getId());
				if (cGroup == null) {
					result = false;

				} else {
					course.setCGroup(cGroup);
				}
			} else {
				course.setCGroup(null);
			}
			if ((courseVO.getLesson() != null)
					&& (courseVO.getLesson().getId() != null)) {
				Lesson lesson = lessonService.findById(courseVO.getLesson()
						.getId());
				if (lesson == null) {
					result = false;

				} else {
					course.setLesson(lesson);
				}
			} else {
				course.setLesson(null);
			}

			if ((courseVO.getTeacher() != null)
					&& (courseVO.getTeacher().getId() != null)) {
				Teacher teacher = teacherService.findById(courseVO.getTeacher()
						.getId());
				if (teacher == null) {
					result = false;

				} else {
					course.setTeacher(teacher);
				}
			} else {
				course.setTeacher(null);
			}

			if ((courseVO.getYear() != null)
					&& (courseVO.getYear().getId() != null)) {
				Year year = yearService.findById(courseVO.getYear().getId());
				if (year == null) {
					result = false;

				} else {
					course.setYear(year);
				}
			} else {
				course.setYear(null);
			}

			if ((courseVO.getBookType() != null)
					&& (courseVO.getBookType().getId() != null)) {
				BookType bookType = bookTypeService.findById(courseVO
						.getBookType().getId());
				if (bookType == null) {
					result = false;

				} else {
					course.setBookType(bookType);
				}
			} else {
				course.setBookType(null);
			}

			result = courseService.update(course);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		Course course = courseService.findById(id);
		if (course == null) {
			result = false;
		} else {
			result = courseService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public CourseVO get(@PathParam("id") int id) {
		Course course = courseService.get(id);
		course.setExTeachers(null);
//		course.setSyllabuses(null);
		course.setTimetables(null);
		if (course.getTeacher() != null) {
			course.getTeacher().setCGroups(null);
			course.getTeacher().setNationality(null);
			course.getTeacher().setCountry(null);
			course.getTeacher().setCourses(null);
			course.getTeacher().setExTeachers(null);
			course.getTeacher().setStatus(null);
		}
		if (course.getBookType() != null) {
			course.getBookType().setCourses(null);
			course.getBookType().getCategory().setBookTypes(null);
		}
		if (course.getLesson() != null) {
			course.getLesson().setCourses(null);
		}
		if (course.getCGroup() != null) {
			course.getCGroup().setCourses(null);
			course.getCGroup().setStudents(null);
			if (course.getCGroup().getTeacher() != null) {
				course.getCGroup().getTeacher().setCGroups(null);
				course.getCGroup().getTeacher().setNationality(null);
				course.getCGroup().getTeacher().setCountry(null);
				course.getCGroup().getTeacher().setCourses(null);
				course.getCGroup().getTeacher().setExTeachers(null);
			}
			if (course.getCGroup().getGrade() != null) {
				course.getCGroup().getGrade().setCGroups(null);
			}
			if (course.getCGroup().getClassLetter() != null) {
				course.getCGroup().getClassLetter().setCGroups(null);
			}
		}
		if (course.getYear() != null) {
			course.getYear().setCourses(null);
			course.getYear().setStudAccountings(null);
			course.getYear().setStudents(null);
		}
		CourseVO courseVO = mapper.map(course, CourseVO.class);
		return courseVO;
	}
	
	@GET
	@Path("/students/{id}")
	@Produces("application/json")
	public CourseVO getStudent(@PathParam("id") int id) {
		Course course = courseService.get(id);
		course.setExTeachers(null);
//		course.setSyllabuses(null);
		course.setTimetables(null);
		if (course.getTeacher() != null) {
			course.getTeacher().setCGroups(null);
			course.getTeacher().setNationality(null);
			course.getTeacher().setCountry(null);
			course.getTeacher().setCourses(null);
			course.getTeacher().setExTeachers(null);
			course.getTeacher().setStatus(null);
		}
		if (course.getBookType() != null) {
			course.getBookType().setCourses(null);
			course.getBookType().getCategory().setBookTypes(null);
		}
		if (course.getLesson() != null) {
			course.getLesson().setCourses(null);
		}
		if (course.getCGroup() != null) {
			course.getCGroup().setCourses(null);
			if(course.getCGroup().getStudents()!=null){

				Set<Student> newStudents = new HashSet<Student>();
				for(Student s:course.getCGroup().getStudents()){
					
					s.setNationality(null);
					s.setCountry(null);
					s.setStatus(null);
					s.setCGroup(null);
					s.setNoteAttendances(null);
					s.setParents(null);
					s.setStudAccountings(null);
					s.setYear(null);
					newStudents.add(s);
				}
				course.getCGroup().setStudents(newStudents);
			}
			if (course.getCGroup().getTeacher() != null) {
				course.getCGroup().getTeacher().setCGroups(null);
				course.getCGroup().getTeacher().setNationality(null);
				course.getCGroup().getTeacher().setCountry(null);
				course.getCGroup().getTeacher().setCourses(null);
				course.getCGroup().getTeacher().setExTeachers(null);
			}
			if (course.getCGroup().getGrade() != null) {
				course.getCGroup().getGrade().setCGroups(null);
			}
			if (course.getCGroup().getClassLetter() != null) {
				course.getCGroup().getClassLetter().setCGroups(null);
			}
		}
		if (course.getYear() != null) {
			course.getYear().setCourses(null);
			course.getYear().setStudAccountings(null);
			course.getYear().setStudents(null);
		}
		CourseVO courseVO = mapper.map(course, CourseVO.class);
		return courseVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<CourseVO> search(SearchParameters searchParameters) {
		SearchResult<Course> searchResult = courseService
				.getList(searchParameters);

		SearchResult<CourseVO> searchResultVO = new SearchResult<CourseVO>();
		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			searchResult.getResultList().get(i).setExTeachers(null);
//			searchResult.getResultList().get(i).setSyllabuses(null);
			searchResult.getResultList().get(i).setTimetables(null);
			if (searchResult.getResultList().get(i).getTeacher() != null) {
				searchResult.getResultList().get(i).getTeacher()
						.setCGroups(null);
				searchResult.getResultList().get(i).getTeacher()
						.setNationality(null);
				searchResult.getResultList().get(i).getTeacher()
						.setCountry(null);
				searchResult.getResultList().get(i).getTeacher()
				.setStatus(null);
				searchResult.getResultList().get(i).getTeacher()
						.setCourses(null);
				searchResult.getResultList().get(i).getTeacher()
						.setExTeachers(null);
			}
			if (searchResult.getResultList().get(i).getBookType() != null) {
				searchResult.getResultList().get(i).getBookType()
						.setCourses(null);
				searchResult.getResultList().get(i).getBookType().getCategory()
						.setBookTypes(null);
			}
			if (searchResult.getResultList().get(i).getLesson() != null) {
				searchResult.getResultList().get(i).getLesson()
						.setCourses(null);
			}
			if (searchResult.getResultList().get(i).getCGroup() != null) {
				searchResult.getResultList().get(i).getCGroup()
						.setCourses(null);
				searchResult.getResultList().get(i).getCGroup()
						.setStudents(null);
				if (searchResult.getResultList().get(i).getCGroup()
						.getTeacher() != null) {
					searchResult.getResultList().get(i).getCGroup()
							.getTeacher().setCGroups(null);
					searchResult.getResultList().get(i).getCGroup()
							.getTeacher().setNationality(null);
					searchResult.getResultList().get(i).getCGroup()
							.getTeacher().setCountry(null);
					searchResult.getResultList().get(i).getCGroup()
							.getTeacher().setCourses(null);
					searchResult.getResultList().get(i).getCGroup()
							.getTeacher().setExTeachers(null);
				}
				if (searchResult.getResultList().get(i).getCGroup().getGrade() != null) {
					searchResult.getResultList().get(i).getCGroup().getGrade()
							.setCGroups(null);
				}
				if (searchResult.getResultList().get(i).getCGroup()
						.getClassLetter() != null) {
					searchResult.getResultList().get(i).getCGroup()
							.getClassLetter().setCGroups(null);
				}
			}
			if (searchResult.getResultList().get(i).getYear() != null) {
				searchResult.getResultList().get(i).getYear().setCourses(null);
				searchResult.getResultList().get(i).getYear()
						.setStudAccountings(null);
				searchResult.getResultList().get(i).getYear().setStudents(null);
			}
			searchResultVO.getResultList().add(
					mapper.map(searchResult.getResultList().get(i),
							CourseVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}

}
