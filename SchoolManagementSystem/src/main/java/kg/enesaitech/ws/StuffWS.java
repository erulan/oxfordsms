package kg.enesaitech.ws;
 
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Country;
import kg.enesaitech.entity.MaritalStatus;
import kg.enesaitech.entity.Nationality;
import kg.enesaitech.entity.Stuff;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.CountryService;
import kg.enesaitech.service.MaritalStatusService;
import kg.enesaitech.service.NationalityService;
import kg.enesaitech.service.StuffService;
import kg.enesaitech.userException.BusinessException;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;
import kg.enesaitech.vo.StuffVO;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.dozer.Mapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
 
@Path("/stuff")
public class StuffWS {
 
	StuffService stuffService = StuffService.getInstance();
	NationalityService nationalityService = NationalityService.getInstance();
	CountryService countryService = CountryService.getInstance();
	MaritalStatusService maritalStatusService = MaritalStatusService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(
			@FormDataParam("photo") InputStream fileInputStream,
            @FormDataParam("photo") FormDataContentDisposition contentDispositionHeader,
            @FormDataParam("stuff") StuffVO stuffVO) {

		boolean result = true;
		new File(System.getProperty("user.home") + "/files/").mkdirs();
		
		String filePath = contentDispositionHeader.getFileName();

		if(filePath!=null){
			File f = new File(System.getProperty("user.home") + "/files/" + filePath);
			if(f.exists() && !f.isDirectory()) {
				result=false;
				throw new BusinessException("Photo with this name is exists !! Change name of photo");
			}
			// save the file to the server
			saveFile(fileInputStream, System.getProperty("user.home") + "/files/" + filePath);
			stuffVO.setPhoto(filePath);
		}		
		
		Stuff stuff = mapper.map(stuffVO, Stuff.class);
		if ((stuffVO.getNationality() != null)
				&& (stuffVO.getNationality().getId() != null)) {
			Nationality nationality = nationalityService.findById(stuffVO
					.getNationality().getId());
			if (nationality == null) {
				result = false;

			} else {
				stuff.setNationality(nationality);
			}
		} else {
			stuff.setNationality(null);
		}
		
		if ((stuffVO.getCountry() != null)
				&& (stuffVO.getCountry().getId() != null)) {
			Country country = countryService.findById(stuffVO
					.getCountry().getId());
			if (country == null) {
				result = false;

			} else {
				stuff.setCountry(country);
			}
		} else {
			stuff.setCountry(null);
		}
		result = stuffService.create(stuff);

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(
			@FormDataParam("photo") InputStream fileInputStream,
			@FormDataParam("photo") FormDataContentDisposition contentDispositionHeader,
            @FormDataParam("stuff") StuffVO stuffVO,
			@PathParam("id") int id) {
		Stuff stuff = null;
		boolean result = true;
		stuff = stuffService.findById(id);
		if (stuff == null) {
			result = false;
		} else {
			new File(System.getProperty("user.home") + "/files/").mkdirs();
			
			String filePath = contentDispositionHeader.getFileName();
			if(filePath!=null){
				File f = new File(System.getProperty("user.home") + "/files/" + filePath);
				if(f.exists() && !f.isDirectory()) {
					result=false;
					throw new BusinessException("Photo with this name is exists !! Change name of photo");
				}
				// save the file to the server
				saveFile(fileInputStream, System.getProperty("user.home") + "/files/" + filePath);
				stuffVO.setPhoto(filePath);
			}
			stuff = mapper.map(stuffVO, Stuff.class);
			if ((stuffVO.getNationality() != null)
					&& (stuffVO.getNationality().getId() != null)) {
				Nationality nationality = nationalityService
						.findById(stuffVO.getNationality().getId());
				if (nationality == null) {
					result = false;

				} else {
					stuff.setNationality(nationality);
				}
			} else {
				stuff.setNationality(null);
			}

			if ((stuffVO.getCountry() != null)
					&& (stuffVO.getCountry().getId() != null)) {
				Country country = countryService.findById(stuffVO
						.getCountry().getId());
				if (country == null) {
					result = false;

				} else {
					stuff.setCountry(country);
				}
			} else {
				stuff.setCountry(null);
			}
			if ((stuffVO.getMaritalStatus() != null)
					&& (stuffVO.getMaritalStatus().getId() != null)) {
				MaritalStatus maritalStatus = maritalStatusService.findById(stuffVO
						.getCountry().getId());
				if (maritalStatus == null) {
					result = false;

				} else {
					stuff.setMaritalStatus(maritalStatus);
				}
			} else {
				stuff.setMaritalStatus(null);
			}
			stuffVO.setId(id);
			result = stuffService.update(stuff);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		Stuff stuff = stuffService.findById(id);
		if (stuff == null) {
			result = false;
		} else {
			
			result = stuffService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public StuffVO get(@PathParam("id") int id) {
		Stuff stuff = stuffService.get(id);
		if (stuff.getNationality() != null) {
			stuff.getNationality().setStudents(null);
			stuff.getNationality().setTeachers(null);
		}
		if (stuff.getCountry() != null) {
			stuff.getCountry().setStudents(null);
			stuff.getCountry().setTeachers(null);
		}
		StuffVO stuffVO = mapper.map(stuff, StuffVO.class);
		return stuffVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<StuffVO> search(SearchParameters searchParameters) {
		SearchResult<Stuff> searchResult = stuffService.getList(searchParameters);

		SearchResult<StuffVO> searchResultVO = new SearchResult<StuffVO>();

		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			if (searchResult.getResultList().get(i).getNationality() != null) {
				searchResult.getResultList().get(i).getNationality()
						.setStudents(null);
				searchResult.getResultList().get(i).getNationality()
						.setTeachers(null);
			}

			if (searchResult.getResultList().get(i).getCountry() != null) {
				searchResult.getResultList().get(i).getCountry().setStudents(null);
				searchResult.getResultList().get(i).getCountry().setTeachers(null);
			}
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), StuffVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
	// save uploaded file to a defined location on the server
    private void saveFile(InputStream uploadedInputStream,
            String serverLocation) {
 
        try {
            OutputStream outpuStream = new FileOutputStream(new File(serverLocation));
            int read = 0;
            byte[] bytes = new byte[1024];
 
            outpuStream = new FileOutputStream(new File(serverLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                outpuStream.write(bytes, 0, read);
            }
            outpuStream.flush();
            outpuStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
    }
    @GET
	@Path("/excel")
	@Produces("application/json")
	public String toExcel() {
    	SearchParameters searchParameters = new SearchParameters();
		SearchResult<Stuff> staffs = stuffService.getList(searchParameters);

		// 1. Create a new Workbook
		Workbook wb = new HSSFWorkbook();

		// 2. Create a new sheet
		Sheet sheet = wb.createSheet("sheet 1");

		// 3. Create a row

		// 4. Create cells
		Row row = sheet.createRow(0);
		row.createCell(0).setCellValue("No");
		row.createCell(1).setCellValue("Name");
		row.createCell(2).setCellValue("Surname");
		row.createCell(3).setCellValue("Имя");
		row.createCell(4).setCellValue("Фамилия");
		row.createCell(5).setCellValue("Телефон");
		row.createCell(6).setCellValue("Должность");
		for (int i = 0; i < staffs.getResultList().size(); i++) {
			row = sheet.createRow((short) i + 1);
			// 4.2 text
			row.createCell(0).setCellValue(i + 1);
			row.createCell(1).setCellValue(staffs.getResultList().get(i).getNameEn());
			row.createCell(2).setCellValue(staffs.getResultList().get(i).getSurnameEn());
			row.createCell(3).setCellValue(staffs.getResultList().get(i).getNameRu());
			row.createCell(4).setCellValue(staffs.getResultList().get(i).getSurnameRu());
			row.createCell(5).setCellValue(staffs.getResultList().get(i).getPhone());
			row.createCell(6).setCellValue(staffs.getResultList().get(i).getPosition());
			// 4.3 date cell
		}

		// 5. create excel file
		FileOutputStream fileOut;
		
		System.getProperty("user.dir");
		
		String filepath = "./categories.xlsx";
		try {

			fileOut = new FileOutputStream(filepath);
			wb.write(fileOut);
			fileOut.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("File created!");

		return filepath;
	}
}
