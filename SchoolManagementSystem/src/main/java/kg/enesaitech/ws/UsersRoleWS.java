package kg.enesaitech.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Role;
import kg.enesaitech.entity.Users;
import kg.enesaitech.entity.UsersRole;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.RoleService;
import kg.enesaitech.service.UsersRoleService;
import kg.enesaitech.service.UsersService;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;
import kg.enesaitech.vo.UsersRoleVO;

import org.dozer.Mapper;

@Path("/users_role")
public class UsersRoleWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	UsersRoleService usersRoleService = UsersRoleService.getInstance();
	RoleService roleService = RoleService.getInstance();
	UsersService usersService = UsersService.getInstance();

	@POST
	@Path("/create")
	@Consumes("application/json")
	public Response create(UsersRoleVO userRoleVO) {
		boolean result = true;

		UsersRole userRole = mapper.map(userRoleVO, UsersRole.class);
		if ((userRoleVO.getRole() != null)
				&& (userRoleVO.getRole().getId() != null)) {
			Role role = roleService.findById(userRoleVO.getRole().getId());
			if (role == null) {
				result = false;

			} else {
				userRole.setRole(role);
			}
		} else {
			userRole.setRole(null);
		}
		if ((userRoleVO.getUsers() != null)
				&& (userRoleVO.getUsers().getId() != null)) {
			Users user = usersService.findById(userRoleVO.getUsers().getId());
			if (user == null) {
				result = false;

			} else {
				userRole.setUsers(user);
			}
		} else {
			userRole.setUsers(null);
		}
		if (result) {
			result = usersRoleService.create(userRole);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	public Response update(UsersRoleVO userRoleVO, @PathParam("id") int id) {
		boolean result = true;
		UsersRole userRole = usersRoleService.findById(id);
		if (userRole == null) {
			result = false;
		} else {
			userRole = mapper.map(userRoleVO, UsersRole.class);
			if ((userRoleVO.getRole() != null)
					&& (userRoleVO.getRole().getId() != null)) {
				Role role = roleService.findById(userRoleVO.getRole().getId());
				if (role == null) {
					result = false;

				} else {
					userRole.setRole(role);
				}
			} else {
				userRole.setRole(null);
			}
			if ((userRoleVO.getUsers() != null)
					&& (userRoleVO.getUsers().getId() != null)) {
				Users user = usersService.findById(userRoleVO.getUsers()
						.getId());
				if (user == null) {
					result = false;

				} else {
					userRole.setUsers(user);
				}
			} else {
				userRole.setUsers(null);
			}
			if (result) {
				result = usersRoleService.update(userRole);
			}
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;

		UsersRole userRole = usersRoleService.findById(id);

		if (userRole == null) {
			result = false;
		} else {
			result = usersRoleService.delete(id);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public UsersRoleVO get(@PathParam("id") int id) {
		UsersRole userRole = usersRoleService.get(id);
		if (userRole.getUsers() != null) {
			userRole.getUsers().setUsersRoles(null);
		}
		if (userRole.getRole() != null) {
			userRole.getRole().setUsersRoles(null);
		}
		UsersRoleVO userVO = mapper.map(userRole, UsersRoleVO.class);

		return userVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<UsersRoleVO> search(SearchParameters searchParameters) {
		SearchResult<UsersRole> searchResult = usersRoleService.getList(searchParameters);

		SearchResult<UsersRoleVO> searchResultVO = new SearchResult<UsersRoleVO>();
		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			if (searchResult.getResultList().get(i).getUsers() != null) {
				searchResult.getResultList().get(i).getUsers().setUsersRoles(null);
			}
			if (searchResult.getResultList().get(i).getRole() != null) {
				searchResult.getResultList().get(i).getRole().setUsersRoles(null);
			}
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), UsersRoleVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
	
	@GET
	@Path("/stuff/{stuffId}/{roleId}")
	public Response addStuffUser(@PathParam("stuffId") int stuffId, @PathParam("roleId") int roleId) {
		boolean result = true;
		result = usersRoleService.addStuffToUsers(stuffId, roleId);

		return result ? Response.ok().build() : Response.serverError().build();
	}
}