package kg.enesaitech.ws;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Book;
import kg.enesaitech.entity.BookType;
import kg.enesaitech.entity.Category;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.BookTypeService;
import kg.enesaitech.service.CategoryService;
import kg.enesaitech.vo.BookTypeVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/book_type")
public class BookTypeWS {
	BookTypeService bookTypeService = BookTypeService.getInstance();
	CategoryService categoryService = CategoryService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(BookTypeVO bookTypeVO) {
		boolean result = true;
		BookType bookType = mapper.map(bookTypeVO, BookType.class);
		if ((bookTypeVO.getCategory() != null)
				&& (bookTypeVO.getCategory().getId() != null)) {
			Category category = categoryService.findById(bookTypeVO
					.getCategory().getId());
			if (category == null) {
				result = false;

			} else {
				bookType.setCategory(category);
				result = bookTypeService.create(bookType);
			}
		} else {
			result = false;
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(BookTypeVO bookTypeVO, @PathParam("id") int id) {
		boolean result = true;
		BookType bookType = bookTypeService.findById(id);
		if (bookType == null) {
			result = false;
		} else {
			bookType = mapper.map(bookTypeVO, BookType.class);
			if ((bookTypeVO.getCategory() != null)
					&& (bookTypeVO.getCategory().getId() != null)) {

				Category category = categoryService.findById(bookTypeVO
						.getCategory().getId());
				if (category == null) {
					result = false;
				} else {
					bookType.setCategory(category);
					result = bookTypeService.update(bookType);
				}
			} else {
				result = false;
			}
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		BookType bookType = bookTypeService.findById(id);
		if (bookType == null) {
			result = false;
		} else {
			result = bookTypeService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public BookTypeVO get(@PathParam("id") int id) {
		BookType bookType = bookTypeService.get(id);
		bookType.setCourses(null);
		bookType.getCategory().setBookTypes(null);
		for (Book b : bookType.getBooks()) {
			b.setBookInUses(null);
			b.setBookType(null);
			b.setBookStatus(null);
		}
		BookTypeVO bookTypeVO = mapper.map(bookType, BookTypeVO.class);
		return bookTypeVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<BookTypeVO> search(SearchParameters searchParameters) {
		SearchResult<BookType> searchResult = bookTypeService
				.getList(searchParameters);

		SearchResult<BookTypeVO> searchResultVO = new SearchResult<BookTypeVO>();

		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			searchResult.getResultList().get(i).setCourses(null);
			searchResult.getResultList().get(i).getCategory()
					.setBookTypes(null);
			Set<Book> newBooks = new HashSet<Book>();
			for (Book b : searchResult.getResultList().get(i).getBooks()) {
				if (b.getBookStatus().getId() != 4) {
					b.setBookInUses(null);
					b.setBookType(null);
					b.setBookStatus(null);
					newBooks.add(b);
				}
			}
			searchResult.getResultList().get(i).setBooks(newBooks);

			searchResultVO.getResultList().add(
					mapper.map(searchResult.getResultList().get(i),
							BookTypeVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;

	}
}