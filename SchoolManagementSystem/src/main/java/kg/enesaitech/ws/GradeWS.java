package kg.enesaitech.ws;
 
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import kg.enesaitech.entity.Grade;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.GradeService;
import kg.enesaitech.vo.GradeVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;
 
@Path("/grade")
public class GradeWS {

	Mapper mapper = DozerMapperProvider.getMapper();
	GradeService gradeService = GradeService.getInstance();
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public GradeVO get(@PathParam("id") int id) {
		Grade grade = gradeService.get(id);
		grade.setCGroups(null);
		GradeVO gradeVO = mapper.map(grade, GradeVO.class);
		return gradeVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public List<GradeVO> search(SearchParameters searchParameters) {
		List<Grade> grades = gradeService.getList(searchParameters);

		List<GradeVO> gradeVOs = new ArrayList<GradeVO>();

		for (int i = 0; i < grades.size(); i++) {
			grades.get(i).setCGroups(null);
			gradeVOs.add(mapper.map(grades.get(i), GradeVO.class));
		}

		return gradeVOs;
	}
}
