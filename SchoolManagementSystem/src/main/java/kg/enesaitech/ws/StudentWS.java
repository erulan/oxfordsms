package kg.enesaitech.ws;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.CGroup;
import kg.enesaitech.entity.Country;
import kg.enesaitech.entity.Nationality;
import kg.enesaitech.entity.Parents;
import kg.enesaitech.entity.Status;
import kg.enesaitech.entity.Student;
import kg.enesaitech.entity.Year;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.CGroupService;
import kg.enesaitech.service.CountryService;
import kg.enesaitech.service.EducationStatusService;
import kg.enesaitech.service.NationalityService;
import kg.enesaitech.service.OblastService;
import kg.enesaitech.service.ParentService;
import kg.enesaitech.service.RegionService;
import kg.enesaitech.service.StudentService;
import kg.enesaitech.service.YearService;
import kg.enesaitech.userException.BusinessException;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;
import kg.enesaitech.vo.StudentVO;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.dozer.Mapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

@Path("/student")
public class StudentWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	StudentService studentService = StudentService.getInstance();
	YearService yearService = YearService.getInstance();
	RegionService regionService = RegionService.getInstance();
	CGroupService cGroupService = CGroupService.getInstance();
	OblastService oblastService = OblastService.getInstance();
	CountryService countryService = CountryService.getInstance();
	NationalityService nationalityService = NationalityService.getInstance();
	ParentService parentService = ParentService.getInstance();
	EducationStatusService educationStatusService = EducationStatusService
			.getInstance();

	@POST
	@Path("/create")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create( 
			@FormDataParam("photo") InputStream fileInputStream,
            @FormDataParam("photo") FormDataContentDisposition contentDispositionHeader,
            @FormDataParam("student") StudentVO studentVO){
		
		boolean result = true;

		new File(System.getProperty("user.home") + "/files/").mkdirs();
		
		String filePath = contentDispositionHeader.getFileName();
		if(filePath!=null){
			File f = new File(System.getProperty("user.home") + "/files/" + filePath);
			if(f.exists() && !f.isDirectory()) {
				result=false;
				throw new BusinessException("Photo with this name is exists !! Change name of photo");
			}
			// save the file to the server
			saveFile(fileInputStream, System.getProperty("user.home") + "/files/" + filePath);
			studentVO.setPhoto(filePath);
		}
		

		Student student = mapper.map(studentVO, Student.class);
		if ((studentVO.getClassGroup() != null)
				&& (studentVO.getClassGroup().getId() != null)) {
			CGroup cGroup = cGroupService.findById(studentVO.getClassGroup()
					.getId());
			if (cGroup == null) {
				result = false;

			} else {
				student.setCGroup(cGroup);
			}
		} else {
			student.setCGroup(null);
		}
		if ((studentVO.getStatus() != null)
				&& (studentVO.getStatus().getId() != null)) {
			Status educationStatus = educationStatusService.findById(studentVO
					.getStatus().getId());
			if (educationStatus == null) {
				result = false;

			} else {
				student.setStatus(educationStatus);
			}
		} else {
			student.setStatus(null);
		}
		if ((studentVO.getYear() != null)
				&& (studentVO.getYear().getId() != null)) {
			Year year = yearService.findById(studentVO.getYear().getId());
			if (year == null) {
				result = false;

			} else {
				student.setYear(year);
			}
		} else {
			student.setYear(null);
		}
		
		if ((studentVO.getCountry() != null)
				&& (studentVO.getCountry().getId() != null)) {
			Country country = countryService.findById(studentVO.getCountry()
					.getId());
			if (country == null) {
				result = false;

			} else {
				student.setCountry(country);
			}
		} else {
			student.setCountry(null);
		}
		if ((studentVO.getNationality() != null)
				&& (studentVO.getNationality().getId() != null)) {
			Nationality nationality = nationalityService.findById(studentVO
					.getNationality().getId());
			if (nationality == null) {
				result = false;

			} else {
				student.setNationality(nationality);
			}
		} else {
			student.setNationality(null);
		}
		
		if ((studentVO.getParents() != null)) {
			if (studentVO.getParents().getId() != null) {
				Parents parents = parentService.findById(studentVO.getParents()
						.getId());
				if (parents == null) {
					result = false;

				} else {
					student.setParents(parents);
				}
			} else {
				student.setParents(mapper.map(studentVO.getParents(),
						Parents.class));
			}
		} else {
			student.setParents(null);
		}
		if (result) {
			result = studentService.create(student);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

		
	@POST
	@Path("/update/{id}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(
			@FormDataParam("photo") InputStream fileInputStream,
			@FormDataParam("photo") FormDataContentDisposition contentDispositionHeader,
            @FormDataParam("student") StudentVO studentVO,
			@PathParam("id") int id) {

		boolean result = true;	
		
		
		Student student = studentService.findById(id);
		if (student == null) {
			result = false;
		} else {
			
			//photo upload code
			new File(System.getProperty("user.home") + "/files/").mkdirs();
			
			String filePath = contentDispositionHeader.getFileName();
			if(filePath!=null){
				File f = new File(System.getProperty("user.home") + "/files/" + filePath);
				if(f.exists() && !f.isDirectory()) {
					result=false;
					throw new BusinessException("Photo with this name is exists !! Change name of photo");
				}
				// save the file to the server
				saveFile(fileInputStream, System.getProperty("user.home") + "/files/" + filePath);
				studentVO.setPhoto(filePath);
			} 
			// end of upload code
			
			if (student.getParents() != null && studentVO.getParents() !=null && !student.getParents().getId().equals(studentVO.getParents().getId())) {
				result = false;
			} else {
				student = mapper.map(studentVO, Student.class);
				if ((studentVO.getClassGroup() != null)
						&& (studentVO.getClassGroup().getId() != null)) {
					CGroup cGroup = cGroupService.findById(studentVO
							.getClassGroup().getId());
					if (cGroup == null) {
						result = false;

					} else {
						student.setCGroup(cGroup);
					}
				} else {
					student.setCGroup(null);
				}
				if ((studentVO.getStatus() != null)
						&& (studentVO.getStatus().getId() != null)) {
					Status educationStatus = educationStatusService
							.findById(studentVO.getStatus().getId());
					if (educationStatus == null) {
						result = false;

					} else {
						student.setStatus(educationStatus);
					}
				} else {
					student.setStatus(null);
				}
				if ((studentVO.getYear() != null)
						&& (studentVO.getYear().getId() != null)) {
					Year year = yearService.findById(studentVO.getYear()
							.getId());
					if (year == null) {
						result = false;

					} else {
						student.setYear(year);
					}
				} else {
					student.setYear(null);
				}
				
				if ((studentVO.getCountry() != null)
						&& (studentVO.getCountry().getId() != null)) {
					Country country = countryService.findById(studentVO
							.getCountry().getId());
					if (country == null) {
						result = false;

					} else {
						student.setCountry(country);
					}
				} else {
					student.setCountry(null);
				}
				if ((studentVO.getNationality() != null)
						&& (studentVO.getNationality().getId() != null)) {
					Nationality nationality = nationalityService
							.findById(studentVO.getNationality().getId());
					if (nationality == null) {
						result = false;

					} else {
						student.setNationality(nationality);
					}
				} else {
					student.setNationality(null);
				}
								
				if (result) {
					result = studentService.update(student);
				}
			}
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;

		Student student = studentService.findById(id);

		if (student == null) {
			result = false;
		} else {
			result = studentService.delete(id);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public StudentVO get(@PathParam("id") int id) {
		Student student = studentService.get(id);

		student.setNoteAttendances(null);
		student.setStudAccountings(null);
		if (student.getCGroup() != null) {

			student.getCGroup().setCourses(null);
			student.getCGroup().setStudents(null);
			if (student.getCGroup().getTeacher() != null) {
				student.getCGroup().setTeacher(null);
			}
			if (student.getCGroup().getGrade() != null) {
				student.getCGroup().getGrade().setCGroups(null);
			}
			if (student.getCGroup().getClassLetter() != null) {
				student.getCGroup().getClassLetter().setCGroups(null);
			}
		}
		if (student.getNationality() != null) {
			student.getNationality().setStudents(null);
			student.getNationality().setTeachers(null);
		}
		if (student.getCountry() != null) {
			student.getCountry().setStudents(null);
			student.getCountry().setTeachers(null);
		}
		if (student.getParents() != null) {
			student.getParents().setStudents(null);
		}
		if (student.getYear() != null) {
			student.getYear().setStudents(null);
			student.getYear().setStudAccountings(null);
			student.getYear().setCourses(null);
		}
		StudentVO studentVO = mapper.map(student, StudentVO.class);

		return studentVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<StudentVO> search(SearchParameters searchParameters) {
		SearchResult<Student> searchResult = studentService
				.getList(searchParameters);

		SearchResult<StudentVO> searchResultVO = new SearchResult<StudentVO>();
		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			searchResult.getResultList().get(i).setNoteAttendances(null);
			searchResult.getResultList().get(i).setStudAccountings(null);
			if (searchResult.getResultList().get(i).getCGroup() != null) {

				searchResult.getResultList().get(i).getCGroup()
						.setCourses(null);
				searchResult.getResultList().get(i).getCGroup()
						.setStudents(null);
				if (searchResult.getResultList().get(i).getCGroup()
						.getTeacher() != null) {
					searchResult.getResultList().get(i).getCGroup()
							.setTeacher(null);
				}
				if (searchResult.getResultList().get(i).getCGroup().getGrade() != null) {
					searchResult.getResultList().get(i).getCGroup().getGrade()
							.setCGroups(null);
					if (searchResult.getResultList().get(i).getCGroup().getGrade().getGrade()>=20){
						searchResult.getResultList().get(i).getCGroup().setGrade(null);
					}
				}
				if (searchResult.getResultList().get(i).getCGroup()
						.getClassLetter() != null) {
					searchResult.getResultList().get(i).getCGroup()
							.getClassLetter().setCGroups(null);
				}
			}
			if (searchResult.getResultList().get(i).getNationality() != null) {
				searchResult.getResultList().get(i).getNationality()
						.setStudents(null);
				searchResult.getResultList().get(i).getNationality()
						.setTeachers(null);
			}
			if (searchResult.getResultList().get(i).getCountry() != null) {
				searchResult.getResultList().get(i).getCountry()
						.setStudents(null);
				searchResult.getResultList().get(i).getCountry()
						.setTeachers(null);
			}
			if (searchResult.getResultList().get(i).getParents() != null) {
				searchResult.getResultList().get(i).getParents()
						.setStudents(null);
			}
			if (searchResult.getResultList().get(i).getYear() != null) {
				searchResult.getResultList().get(i).getYear().setStudents(null);
				searchResult.getResultList().get(i).getYear()
						.setStudAccountings(null);
				searchResult.getResultList().get(i).getYear().setCourses(null);
			}
			searchResultVO.getResultList().add(
					mapper.map(searchResult.getResultList().get(i),
							StudentVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
	
	
	// save uploaded file to a defined location on the server
	    private void saveFile(InputStream uploadedInputStream,
	            String serverLocation) {
	 
	        try {
	            OutputStream outpuStream = new FileOutputStream(new File(serverLocation));
	            int read = 0;
	            byte[] bytes = new byte[1024];
	 
	            outpuStream = new FileOutputStream(new File(serverLocation));
	            while ((read = uploadedInputStream.read(bytes)) != -1) {
	                outpuStream.write(bytes, 0, read);
	            }
	            outpuStream.flush();
	            outpuStream.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	 
	    }


	    @GET
		@Path("/excel")
		@Produces("application/json")
		public String toExcel() {
	    	SearchParameters searchParameters = new SearchParameters();
			SearchResult<Student> staffs = studentService.getList(searchParameters);
			searchParameters.getSearchParameters().put("schoolTypeId", "2");
			SearchResult<Student> staffs2 = studentService.getList(searchParameters);
			staffs.getResultList().addAll(staffs2.getResultList());
			// 1. Create a new Workbook
			Workbook wb = new HSSFWorkbook(); 

			// 2. Create a new sheet
			Sheet sheet = wb.createSheet("sheet 1");

			// 3. Create a row

			// 4. Create cells
			Row row = sheet.createRow(0);
			row.createCell(0).setCellValue("No");
			row.createCell(1).setCellValue("Name");
			row.createCell(2).setCellValue("Surname");
			row.createCell(3).setCellValue("Имя");
			row.createCell(4).setCellValue("Фамилия");
			row.createCell(5).setCellValue("Student NO");
			row.createCell(6).setCellValue("Class or Group");
			row.createCell(7).setCellValue("Phone");
			for (int i = 0; i < staffs.getResultList().size(); i++) {
				row = sheet.createRow((short) i + 1);
				// 4.2 text
				row.createCell(0).setCellValue(i + 1);
				row.createCell(1).setCellValue(staffs.getResultList().get(i).getNameEn());
				row.createCell(2).setCellValue(staffs.getResultList().get(i).getSurnameEn());
				row.createCell(3).setCellValue(staffs.getResultList().get(i).getNameRu());
				row.createCell(4).setCellValue(staffs.getResultList().get(i).getSurnameRu());
				row.createCell(5).setCellValue(staffs.getResultList().get(i).getStudentNum());
				if(staffs.getResultList().get(i).getCGroup().getGrade().getSchoolType().getId()==1){

					row.createCell(6).setCellValue(staffs.getResultList().get(i).getCGroup().getGrade().getGrade()+" "
							+staffs.getResultList().get(i).getCGroup().getClassLetter().getLetter());
				}else{
					row.createCell(6).setCellValue(staffs.getResultList().get(i).getCGroup().getClassLetter().getLetter());
					
				}
				row.createCell(7).setCellValue(staffs.getResultList().get(i).getPhonenum());
				// 4.3 date cell
			}

			// 5. create excel file
			FileOutputStream fileOut;
			
			System.getProperty("user.dir");
			
			String filepath = "./allStudents.xlsx";
			try {

				fileOut = new FileOutputStream(filepath);
				wb.write(fileOut);
				fileOut.close();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			System.out.println("File created!");

			return filepath;
		}
}