package kg.enesaitech.ws;
 
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import kg.enesaitech.entity.ClassHourType;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.ClassHourTypeService;
import kg.enesaitech.vo.ClassHourTypeVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;
 
@Path("/class_hour_type")
public class ClassHourTypeWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	ClassHourTypeService classHourTypeService = ClassHourTypeService
			.getInstance();
 
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public ClassHourTypeVO get(@PathParam("id") int id) {
		ClassHourType classHourType = classHourTypeService.get(id);
		ClassHourTypeVO classHourTypeVO = mapper.map(classHourType, ClassHourTypeVO.class);
		return classHourTypeVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public List<ClassHourTypeVO> search(SearchParameters searchParameters) {
		List<ClassHourType> classHourTypes = classHourTypeService.getList(searchParameters);

		List<ClassHourTypeVO> classHourTypeVOs = new ArrayList<ClassHourTypeVO>();

		for (int i = 0; i < classHourTypes.size(); i++) {

			classHourTypeVOs.add(mapper.map(classHourTypes.get(i), ClassHourTypeVO.class));
		}

		return classHourTypeVOs;
	}
}
