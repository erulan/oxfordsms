package kg.enesaitech.ws;
 
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Place;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.PlaceService;
import kg.enesaitech.vo.PlaceVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;
 
@Path("/place")
public class PlaceWS {
 
	PlaceService placeService = PlaceService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(PlaceVO placeVO) {
		Place place = mapper.map(placeVO, Place.class);
		Boolean created = placeService.create(place);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(PlaceVO placeVO, @PathParam("id") Integer id) {
		Place place = null;
		boolean result = true;
		if(id != null){
			place = placeService.findById(id);
			
		}
		if (place == null) {
			result = false;
		} else {
			placeVO.setId(id);
			place = mapper.map(placeVO, Place.class);
			result = placeService.update(place);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		Place place = placeService.findById(id);
		if (place == null) {
			result = false;
		} else {
			result = placeService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public PlaceVO get(@PathParam("id") int id) {
		Place place = placeService.get(id);
		PlaceVO placeVO = mapper.map(place, PlaceVO.class);
		return placeVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<PlaceVO> search(SearchParameters searchParameters) {
		SearchResult<Place> searchResult = placeService.getList(searchParameters);

		SearchResult<PlaceVO> searchResultVO = new SearchResult<PlaceVO>();

		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), PlaceVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
}
