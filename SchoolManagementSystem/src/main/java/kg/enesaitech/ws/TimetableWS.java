package kg.enesaitech.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.ClassHours;
import kg.enesaitech.entity.Course;
import kg.enesaitech.entity.Timetable;
import kg.enesaitech.entity.WeekDays;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.ClassHourService;
import kg.enesaitech.service.CourseService;
import kg.enesaitech.service.TimetableService;
import kg.enesaitech.service.WeekDaysService;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;
import kg.enesaitech.vo.TimetableVO;
import kg.enesaitech.vo.TimetablesVO;

import org.dozer.Mapper;

@Path("/timetable")
public class TimetableWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	TimetableService timetableService = TimetableService.getInstance();
	WeekDaysService weekDaysService = WeekDaysService.getInstance();
	ClassHourService classHourService = ClassHourService.getInstance();
	CourseService courseService = CourseService.getInstance();

	@POST
	@Path("/create")
	@Consumes("application/json")
	public Response create(TimetableVO timetableVO) {
		boolean result = true;

		Timetable timetable = mapper.map(timetableVO, Timetable.class);
		if ((timetableVO.getWeekDays() != null)
				&& (timetableVO.getWeekDays().getId() != null)) {
			WeekDays weekDays = weekDaysService.findById(timetableVO
					.getWeekDays().getId());
			if (weekDays == null) {
				result = false;

			} else {
				timetable.setWeekDays(weekDays);
			}
		} else {
			timetable.setWeekDays(null);
		}
		if ((timetableVO.getClassHours() != null)
				&& (timetableVO.getClassHours().getId() != null)) {
			ClassHours classHours = classHourService.findById(timetableVO
					.getClassHours().getId());
			if (classHours == null) {
				result = false;

			} else {
				timetable.setClassHours(classHours);
			}
		} else {
			timetable.setClassHours(null);
		}
		if ((timetableVO.getCourse() != null)
				&& (timetableVO.getCourse().getId() != null)) {
			Course course = courseService.findById(timetableVO.getCourse()
					.getId());
			if (course == null) {
				result = false;

			} else {
				timetable.setCourse(course);
			}
		} else {
			timetable.setCourse(null);
		}
		if (result) {
			result = timetableService.create(timetable);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}
	@POST
	@Path("/create_timetable")
	@Consumes("application/json")
	public Response createTimetables(TimetablesVO timetablesVO) {
		boolean result = true;

		
			result = timetableService.createTimetable(timetablesVO);
		
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	public Response update(TimetableVO timetableVO, @PathParam("id") int id) {
		boolean result = true;
		Timetable timetable = timetableService.findById(id);
		if (timetable == null) {
			result = false;
		} else {
			timetable = mapper.map(timetableVO, Timetable.class);
			if ((timetableVO.getWeekDays() != null)
					&& (timetableVO.getWeekDays().getId() != null)) {
				WeekDays weekDays = weekDaysService.findById(timetableVO
						.getWeekDays().getId());
				if (weekDays == null) {
					result = false;

				} else {
					timetable.setWeekDays(weekDays);
				}
			} else {
				timetable.setWeekDays(null);
			}
			if ((timetableVO.getClassHours() != null)
					&& (timetableVO.getClassHours().getId() != null)) {
				ClassHours classHours = classHourService.findById(timetableVO
						.getClassHours().getId());
				if (classHours == null) {
					result = false;

				} else {
					timetable.setClassHours(classHours);
				}
			} else {
				timetable.setClassHours(null);
			}
			if ((timetableVO.getCourse() != null)
					&& (timetableVO.getCourse().getId() != null)) {
				Course course = courseService.findById(timetableVO.getCourse()
						.getId());
				if (course == null) {
					result = false;

				} else {
					timetable.setCourse(course);
				}
			} else {
				timetable.setCourse(null);
			}
			if (result) {
				result = timetableService.update(timetable);
			}
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;

		Timetable timetable = timetableService.findById(id);

		if (timetable == null) {
			result = false;
		} else {
			result = timetableService.delete(id);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public TimetableVO get(@PathParam("id") int id) {
		Timetable timetable = timetableService.get(id);
		if (timetable.getWeekDays() != null) {
			timetable.getWeekDays().setTimetables(null);
		}
		if (timetable.getClassHours() != null) {
			timetable.getClassHours().setTimetables(null);
		}
		if(timetable.getCGroup()!=null){
			timetable.getCGroup().setCourses(null);
			timetable.getCGroup().setStudents(null);
			if (timetable.getCGroup().getTeacher() != null) {
				timetable.getCGroup().setTeacher(null);
			}
			if (timetable.getCGroup().getGrade() != null) {
				timetable.getCGroup().getGrade().setCGroups(null);
			}
			if (timetable.getCGroup().getClassLetter() != null) {
				timetable.getCGroup().getClassLetter().setCGroups(null);
			}
		}
		if (timetable.getCourse() != null) {
			timetable.getCourse().setExTeachers(null);
//			timetable.getCourse().setSyllabuses(null);
			timetable.getCourse().setTimetables(null);
			if (timetable.getCourse().getTeacher() != null) {
				timetable.getCourse().setTeacher(null);
			}
			if (timetable.getCourse().getBookType() != null) {
				timetable.getCourse().setBookType(null);
			}
			if (timetable.getCourse().getLesson() != null) {
				timetable.getCourse().setLesson(null);
				;
			}
			if (timetable.getCourse().getCGroup() != null) {

				timetable.getCourse().getCGroup().setCourses(null);
				timetable.getCourse().getCGroup().setStudents(null);
				if (timetable.getCourse().getCGroup().getTeacher() != null) {
					timetable.getCourse().getCGroup().setTeacher(null);
				}
				if (timetable.getCourse().getCGroup().getGrade() != null) {
					timetable.getCourse().getCGroup().getGrade()
							.setCGroups(null);
				}
				if (timetable.getCourse().getCGroup().getClassLetter() != null) {
					timetable.getCourse().getCGroup().getClassLetter()
							.setCGroups(null);
				}
			}
			if (timetable.getCourse().getYear() != null) {
				timetable.getCourse().getYear().setCourses(null);
				timetable.getCourse().getYear().setStudAccountings(null);
				timetable.getCourse().getYear().setStudents(null);
			}
		}
		TimetableVO timetableVO = mapper.map(timetable, TimetableVO.class);
		return timetableVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<TimetableVO> search(SearchParameters searchParameters) {
		SearchResult<Timetable> searchResult = timetableService.getList(searchParameters);

		SearchResult<TimetableVO> searchResultVO = new SearchResult<TimetableVO>();
		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			if (searchResult.getResultList().get(i).getWeekDays() != null) {
				searchResult.getResultList().get(i).getWeekDays().setTimetables(null);
			}
			if (searchResult.getResultList().get(i).getCGroup() != null) {

				searchResult.getResultList().get(i).getCGroup().setCourses(null);
				searchResult.getResultList().get(i).getCGroup().setStudents(null);
				if (searchResult.getResultList().get(i).getCGroup().getTeacher() != null) {
					searchResult.getResultList().get(i).getCGroup().setTeacher(null);
				}
				if (searchResult.getResultList().get(i).getCGroup().getGrade() != null) {
					searchResult.getResultList().get(i).getCGroup().getGrade().setCGroups(null);
				}
				if (searchResult.getResultList().get(i).getCGroup().getClassLetter() != null) {
					searchResult.getResultList().get(i).getCGroup().getClassLetter()
							.setCGroups(null);
				}
			}
			if (searchResult.getResultList().get(i).getClassHours() != null) {
				searchResult.getResultList().get(i).getClassHours().setTimetables(null);
			}
			if (searchResult.getResultList().get(i).getCourse() != null) {
				searchResult.getResultList().get(i).getCourse().setExTeachers(null);
//				searchResult.getResultList().get(i).getCourse().setSyllabuses(null);
				searchResult.getResultList().get(i).getCourse().setTimetables(null);
				if (searchResult.getResultList().get(i).getCourse().getTeacher() != null) {
					searchResult.getResultList().get(i).getCourse().setTeacher(null);
				}
				if (searchResult.getResultList().get(i).getCourse().getBookType() != null) {
					searchResult.getResultList().get(i).getCourse().setBookType(null);
				}
				if (searchResult.getResultList().get(i).getCourse().getLesson() != null) {
					searchResult.getResultList().get(i).getCourse().getLesson().setCourses(null);
				}
				if (searchResult.getResultList().get(i).getCourse().getCGroup() != null) {

					searchResult.getResultList().get(i).getCourse().getCGroup().setCourses(null);
					searchResult.getResultList().get(i).getCourse().getCGroup().setStudents(null);
					if (searchResult.getResultList().get(i).getCourse().getCGroup().getTeacher() != null) {
						searchResult.getResultList().get(i).getCourse().getCGroup()
								.setTeacher(null);
					}
					if (searchResult.getResultList().get(i).getCourse().getCGroup().getGrade() != null) {
						searchResult.getResultList().get(i).getCourse().getCGroup().getGrade()
								.setCGroups(null);
					}
					if (searchResult.getResultList().get(i).getCourse().getCGroup()
							.getClassLetter() != null) {
						searchResult.getResultList().get(i).getCourse().getCGroup()
								.getClassLetter().setCGroups(null);
					}
				}
				if (searchResult.getResultList().get(i).getCourse().getYear() != null) {
					searchResult.getResultList().get(i).getCourse().getYear().setCourses(null);
					searchResult.getResultList().get(i).getCourse().getYear()
							.setStudAccountings(null);
					searchResult.getResultList().get(i).getCourse().getYear().setStudents(null);
				}
			}
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), TimetableVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
}