package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.HeightStandard;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.HeightStandardService;
import kg.enesaitech.vo.HeightStandardVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/height_standard")
public class HeightStandardWS {
	HeightStandardService heightStandardService = HeightStandardService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	public Response create(HeightStandardVO heightStandardVO) {

		HeightStandard heightStandard = mapper.map(heightStandardVO, HeightStandard.class);
		Boolean created = heightStandardService.create(heightStandard);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(HeightStandardVO heightStandardVO, @PathParam("id") int id) {
		boolean result = true;
		HeightStandard heightStandard = heightStandardService.findById(id);
		if (heightStandard == null) {
			result = false;
		} else {
			heightStandard = mapper.map(heightStandardVO, HeightStandard.class);
			result = heightStandardService.update(heightStandard);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		HeightStandard heightStandard = heightStandardService.findById(id);
		if (heightStandard == null) {
			result = false;
		} else {
			result = heightStandardService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public HeightStandardVO get(@PathParam("id") int id) {
		HeightStandard heightStandard = heightStandardService.get(id);
		HeightStandardVO heightStandardVO = mapper.map(heightStandard, HeightStandardVO.class);
		return heightStandardVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<HeightStandardVO> search(SearchParameters searchParameters) {
		SearchResult<HeightStandard> heightStandards = heightStandardService.getList(searchParameters);												

		SearchResult<HeightStandardVO> heightStandardVOs = new SearchResult<HeightStandardVO>();

		for (int i = 0; i < heightStandards.getResultList().size(); i++) {
			heightStandardVOs.getResultList().add(mapper.map(heightStandards.getResultList().get(i), HeightStandardVO.class));
		}

		return heightStandardVOs;
	}
}
