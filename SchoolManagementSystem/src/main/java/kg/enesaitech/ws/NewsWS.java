package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.News;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.NewsService;
import kg.enesaitech.vo.NewsVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/news")
public class NewsWS {
	NewsService newsService = NewsService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	public Response create(NewsVO newsVO) {

		News news = mapper.map(newsVO, News.class);
		Boolean created = newsService.create(news);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(NewsVO newsVO, @PathParam("id") int id) {
		boolean result = true;
		News news = newsService.findById(id);
		if (news == null) {
			result = false;
		} else {
			news = mapper.map(newsVO, News.class);
			result = newsService.update(news);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		News news = newsService.findById(id);
		if (news == null) {
			result = false;
		} else {
			result = newsService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public NewsVO get(@PathParam("id") int id) {
		News news= newsService.get(id);
		NewsVO newsVO = mapper.map(news, NewsVO.class);
		return newsVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<NewsVO> search(SearchParameters searchParameters) {
		SearchResult<News> searchResult = newsService.getList(searchParameters);

		SearchResult<NewsVO> searchResultVO = new SearchResult<NewsVO>();

		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i),NewsVO.class));
		}

		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
}
