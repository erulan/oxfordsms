package kg.enesaitech.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.MaterialStatus;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.MaterialStatusService;
import kg.enesaitech.vo.MaterialStatusVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

@Path("/material_status")
public class MaterialStatusWS {
	MaterialStatusService materialStatusService = MaterialStatusService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

//	@POST
//	@Path("/create")
//	public Response create(MaterialStatusVO materialStatusVO) {
//
//		MaterialStatus materialStatus = (MaterialStatus) mapper.map(materialStatusVO, materialStatus.class);
//		Boolean created = materialStatusService.create(materialStatus);
//
//		return created ? Response.ok().build() : Response.serverError().build();
//	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(MaterialStatusVO materialStatusVO, @PathParam("id") int id) {
		boolean result = true;
		MaterialStatus materialStatus = materialStatusService.findById(id);
		if (materialStatus == null) {
			result = false;
		} else {
			materialStatus = mapper.map(materialStatusVO, MaterialStatus.class);
			result = materialStatusService.update(materialStatus);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		MaterialStatus materialStatus = materialStatusService.findById(id);
		if (materialStatus == null) {
			result = false;
		} else {
			result = materialStatusService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

//	@GET
//	@Path("/{id}")
//	@Produces("application/json")
//	public MaterialStatusVO get(@PathParam("id") int id) {
//		MaterialStatus materialStatus = materialStatusService.get(id);
//		materialStatus.setCourses(null);
//		MaterialStatusVO materialStatusVO = mapper.map(materialStatus, MaterialStatusVO.class);
//		return materialStatusVO;
//	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public List<MaterialStatusVO> search(SearchParameters searchParameters) {
		List<MaterialStatus> materialStatuses = materialStatusService.getList(searchParameters);

		List<MaterialStatusVO> materialStatusVOs = new ArrayList<MaterialStatusVO>();

		for (int i = 0; i < materialStatuses.size(); i++) {
//			materialStatuses.get(i).setCourses(null);
			materialStatusVOs.add(mapper.map(materialStatuses.get(i), MaterialStatusVO.class));
		}

		return materialStatusVOs;
	}
}
