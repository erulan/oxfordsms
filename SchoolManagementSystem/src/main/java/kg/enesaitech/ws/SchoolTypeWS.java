package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.SchoolType;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.SchoolTypeService;
import kg.enesaitech.vo.SchoolTypeVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/school_type")
public class SchoolTypeWS {
	SchoolTypeService schoolTypeService = SchoolTypeService
			.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(SchoolTypeVO schoolTypeVO) {
		SchoolType schoolType = mapper.map(schoolTypeVO,
				SchoolType.class);
		Boolean created = schoolTypeService.create(schoolType);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(SchoolTypeVO schoolTypeVO, @PathParam("id") Integer id) {
		SchoolType schoolType = null;
		boolean result = true;
		if(id != null){
			schoolType = schoolTypeService.findById(id);
			
		}
		if (schoolType == null) {
			result = false;
		} else {
			schoolTypeVO.setId(id);
			schoolType = mapper.map(schoolTypeVO, SchoolType.class);
			result = schoolTypeService.update(schoolType);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}
//
//	@GET
//	@Path("/delete/{id}")
//	@Produces("text/html")
//	public Response delete(@PathParam("id") int id) {
//		boolean result = true;
//		MaterialCategory materialCategory = schoolTypeService
//				.findById(id);
//		if (materialCategory == null) {
//			result = false;
//		} else {
//			result = schoolTypeService.delete(id);
//		}
//		return result ? Response.ok().build() : Response.serverError().build();
//	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public SchoolTypeVO get(@PathParam("id") int id) {
		SchoolType schoolType = schoolTypeService.get(id);
		SchoolTypeVO schoolTypeVO = mapper.map(schoolType,
				SchoolTypeVO.class);
		return schoolTypeVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<SchoolTypeVO> search(SearchParameters searchParameters) {
		SearchResult<SchoolType> searchResult = schoolTypeService
				.getList(searchParameters);

		SearchResult<SchoolTypeVO> searchResultVO = new SearchResult<SchoolTypeVO>();

		for (int i = 0; i < searchResult.getResultList().size(); i++) {
//			Set<MaterialCategory> newBooks = new HashSet<MaterialCategory>();
//			for (MaterialCategory b : searchResult.getResultList().get(i).getBooks()) {
//				if (b.getBookStatus().getId() != 4) {
//					b.setBookInUses(null);
//					b.setBookType(null);
//					b.setBookStatus(null);
//					newBooks.add(b);
//				}
//			}

			searchResultVO.getResultList().add(
					mapper.map(searchResult.getResultList().get(i),
							SchoolTypeVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;

	}

}