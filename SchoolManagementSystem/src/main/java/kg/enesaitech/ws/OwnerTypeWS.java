package kg.enesaitech.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import kg.enesaitech.entity.OwnerType;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.OwnerTypeService;
import kg.enesaitech.vo.OwnerTypeVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

@Path("/owner_type")
public class OwnerTypeWS {
	OwnerTypeService ownerTypeService = OwnerTypeService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public OwnerTypeVO get(@PathParam("id") int id) {
		OwnerType ownerType = ownerTypeService.get(id);
		OwnerTypeVO ownerTypeVO = mapper.map(ownerType, OwnerTypeVO.class);
		return ownerTypeVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public List<OwnerTypeVO> search(SearchParameters searchParameters) {
		List<OwnerType> ownerType = ownerTypeService.getList(searchParameters);

		List<OwnerTypeVO> ownerTypeVOs = new ArrayList<OwnerTypeVO>();

		for (int i = 0; i < ownerType.size(); i++) {
			ownerTypeVOs.add(mapper.map(ownerType.get(i), OwnerTypeVO.class));
		}

		return ownerTypeVOs;
	}
}
