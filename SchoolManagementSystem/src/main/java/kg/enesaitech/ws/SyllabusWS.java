package kg.enesaitech.ws;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.NoteAttendance;
import kg.enesaitech.entity.Syllabus;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.SyllabusService;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;
import kg.enesaitech.vo.SyllabusVO;

import org.dozer.Mapper;

@Path("/syllabus")
public class SyllabusWS {

	private SyllabusService syllabusService = SyllabusService.getInstance();
	//private CourseService courseService = CourseService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	public Response create(SyllabusVO syllabusVO) {

		boolean result = true;

		Syllabus syllabus = mapper.map(syllabusVO, Syllabus.class);
		
		if (result) {
			result = syllabusService.create(syllabus);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(SyllabusVO syllabusVO, @PathParam("id") int id) {

		boolean result = true;
		Syllabus syllabus = syllabusService.findById(id);
		if (syllabus == null) {
			result = false;
		} else {
			syllabus = mapper.map(syllabusVO, Syllabus.class);
			
			if (result) {
				result = syllabusService.update(syllabus);
			}
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {

		Boolean result = true;
		Syllabus syllabus = syllabusService.findById(id);
		if (syllabus == null) {
			result = false;
		} else {
			result = syllabusService.delete(id);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public SyllabusVO get(@PathParam("id") int id) {
		Syllabus syllabus = syllabusService.get(id);
		syllabus.setNoteAttendances(null);
		if (syllabus.getSyllabusCategory().getCourse() != null) {
			syllabus.getSyllabusCategory().getCourse().setExTeachers(null);
//			syllabus.getSyllabusCategory().getCourse().setSyllabuses(null);
			syllabus.getSyllabusCategory().getCourse().setTimetables(null);
			if (syllabus.getSyllabusCategory().getCourse().getTeacher() != null) {
				syllabus.getSyllabusCategory().getCourse().setTeacher(null);
			}
			if (syllabus.getSyllabusCategory().getCourse().getBookType() != null) {
				syllabus.getSyllabusCategory().getCourse().setBookType(null);
			}
			if (syllabus.getSyllabusCategory().getCourse().getLesson() != null) {
				syllabus.getSyllabusCategory().getCourse().getLesson().setCourses(null);
				;
			}
			if (syllabus.getSyllabusCategory().getCourse().getCGroup() != null) {

				syllabus.getSyllabusCategory().getCourse().getCGroup().setCourses(null);
				syllabus.getSyllabusCategory().getCourse().getCGroup().setStudents(null);
				if (syllabus.getSyllabusCategory().getCourse().getCGroup().getTeacher() != null) {
					syllabus.getSyllabusCategory().getCourse().getCGroup().setTeacher(null);
				}
				if (syllabus.getSyllabusCategory().getCourse().getCGroup().getGrade() != null) {
					syllabus.getSyllabusCategory().getCourse().getCGroup().getGrade()
							.setCGroups(null);
				}
				if (syllabus.getSyllabusCategory().getCourse().getCGroup().getClassLetter() != null) {
					syllabus.getSyllabusCategory().getCourse().getCGroup().getClassLetter()
							.setCGroups(null);
				}
			}
			if (syllabus.getSyllabusCategory().getCourse().getYear() != null) {
				syllabus.getSyllabusCategory().getCourse().getYear().setCourses(null);
				syllabus.getSyllabusCategory().getCourse().getYear().setStudAccountings(null);
				syllabus.getSyllabusCategory().getCourse().getYear().setStudents(null);
			}
		}
		if (syllabus.getSyllabusCategory().getQuarter() != null) {
//			syllabus.getSyllabusCategory().getQuarter().setSyllabus(null);
		}
		SyllabusVO syllabusVO = mapper.map(syllabus, SyllabusVO.class);
		return syllabusVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<SyllabusVO> search(SearchParameters searchParameters) {
		SearchResult<Syllabus> searchResult = syllabusService.getList(searchParameters);

		SearchResult<SyllabusVO> searchResultVO = new SearchResult<SyllabusVO>();
		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			searchResult.getResultList().get(i).setNoteAttendances(null);
			if (searchResult.getResultList().get(i).getSyllabusCategory().getCourse() != null) {
				searchResult.getResultList().get(i).getSyllabusCategory().getCourse().setExTeachers(null);
//				searchResult.getResultList().get(i).getSyllabusCategory().getCourse().setSyllabuses(null);
				searchResult.getResultList().get(i).getSyllabusCategory().getCourse().setTimetables(null);
				if (searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getTeacher() != null) {
					searchResult.getResultList().get(i).getSyllabusCategory().getCourse().setTeacher(null);
				}
				if (searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getBookType() != null) {
					searchResult.getResultList().get(i).getSyllabusCategory().getCourse().setBookType(null);
				}
				if (searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getLesson() != null) {
					searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getLesson().setCourses(null);;
					;
				}
				if (searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getCGroup() != null) {

					searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getCGroup().setCourses(null);
					searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getCGroup().setStudents(null);
					if (searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getCGroup().getTeacher() != null) {
						searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getCGroup()
								.setTeacher(null);
					}
					if (searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getCGroup().getGrade() != null) {
						searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getCGroup().getGrade()
								.setCGroups(null);
					}
					if (searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getCGroup()
							.getClassLetter() != null) {
						searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getCGroup()
								.getClassLetter().setCGroups(null);
					}
				}
				if (searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getYear() != null) {
					searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getYear().setCourses(null);
					searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getYear()
							.setStudAccountings(null);
					searchResult.getResultList().get(i).getSyllabusCategory().getCourse().getYear().setStudents(null);
				}
			}

			if (searchResult.getResultList().get(i).getSyllabusCategory().getQuarter() != null) {
//				searchResult.getResultList().get(i).getSyllabusCategory().getQuarter().setSyllabus(null);
			}
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), SyllabusVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
	
	@POST
	@Path("/notes")
	@Produces("application/json")
	public SearchResult<SyllabusVO> notes(SearchParameters searchParameters) {
		SearchResult<Syllabus> searchResult = syllabusService.getList(searchParameters);

		SearchResult<SyllabusVO> searchResultVO = new SearchResult<SyllabusVO>();
		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			searchResult.getResultList().get(i).getSyllabusCategory().setCourse(null);
			if (searchResult.getResultList().get(i).getNoteAttendances() != null) {
				Set<NoteAttendance> newStudents = new HashSet<NoteAttendance>();
				for (NoteAttendance note : searchResult.getResultList().get(i).getNoteAttendances()) {
					note.setSyllabus(null);
					if(note.getStudent()!=null){
						note.getStudent().setNationality(null);
						note.getStudent().setCountry(null);
						note.getStudent().setStatus(null);
						note.getStudent().setCGroup(null);
						note.getStudent().setNoteAttendances(null);
						note.getStudent().setParents(null);
						note.getStudent().setStudAccountings(null);
						note.getStudent().setYear(null);
						note.setNoteType(null);
						note.setNoteSystem(null);
					}
						newStudents.add(note);
					
				}
				searchResult.getResultList().get(i).setNoteAttendances(newStudents);
			}
			if (searchResult.getResultList().get(i).getSyllabusCategory().getQuarter() != null) {
//				searchResult.getResultList().get(i).getSyllabusCategory().getQuarter().setSyllabus(null);
			}
			
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), SyllabusVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
}
