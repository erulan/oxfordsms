package kg.enesaitech.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Quarter;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.QuarterService;
import kg.enesaitech.vo.QuarterVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

@Path("/quarter")
public class QuarterWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	QuarterService quarterService = QuarterService.getInstance();

	@POST
	@Path("/create")
	@Consumes("application/json")
	public Response create(QuarterVO quarterVO) {
		boolean result = true;

		Quarter quarter = mapper.map(quarterVO, Quarter.class);
		result = quarterService.create(quarter);

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	public Response update(QuarterVO quarterVO, @PathParam("id") int id) {
		boolean result = true;

		Quarter quarter = quarterService.findById(id);
		if (quarter == null) {
			result = false;
		} else {
			quarter = mapper.map(quarterVO, Quarter.class);
			result = quarterService.update(quarter);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;

		Quarter quarter = quarterService.findById(id);

		if (quarter == null) {
			result = false;
		} else {
			result = quarterService.delete(id);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public QuarterVO get(@PathParam("id") int id) {
		Quarter quarter = quarterService.get(id);
//		quarter.setSyllabus(null);
		QuarterVO quarterVO = mapper.map(quarter, QuarterVO.class);
		return quarterVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public List<QuarterVO> search(SearchParameters searchParameters) {
		List<Quarter> quarters = quarterService.getList(searchParameters);

		List<QuarterVO> quarterVOs = new ArrayList<QuarterVO>();

		for (int i = 0; i < 4; i++) {
//			quarters.get(i).setSyllabus(null);
			quarterVOs.add(mapper.map(quarters.get(i), QuarterVO.class));
		}

		return quarterVOs;
	}
}