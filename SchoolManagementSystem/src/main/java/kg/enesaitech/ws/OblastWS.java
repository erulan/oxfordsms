package kg.enesaitech.ws;
 
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import kg.enesaitech.entity.Oblast;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.OblastService;
import kg.enesaitech.vo.OblastVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;
 
@Path("/oblast")
public class OblastWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	OblastService oblastService = OblastService
			.getInstance();
 
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public OblastVO get(@PathParam("id") int id) {
		Oblast oblast = oblastService.get(id);
		OblastVO oblastVO = mapper.map(oblast, OblastVO.class);
		return oblastVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public List<OblastVO> search(SearchParameters searchParameters) {
		List<Oblast> oblast = oblastService.getList(searchParameters);

		List<OblastVO> oblastVOs = new ArrayList<OblastVO>();

		for (int i = 0; i < oblast.size(); i++) {

			oblastVOs.add(mapper.map(oblast.get(i), OblastVO.class));
		}

		return oblastVOs;
	}
}
