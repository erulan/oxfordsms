package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.NoteSystem;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.NoteSystemService;
import kg.enesaitech.vo.NoteSystemVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/note_system")
public class NoteSystemWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	NoteSystemService noteSystemService = NoteSystemService.getInstance();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(NoteSystemVO noteSystemVO) {
		NoteSystem noteSystem = mapper.map(noteSystemVO, NoteSystem.class);
		Boolean created = noteSystemService.create(noteSystem);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(NoteSystemVO noteSystemVO, @PathParam("id") int id) {
		boolean result = true;
		NoteSystem noteSystem = noteSystemService.findById(id);
		if (noteSystem == null) {
			result = false;
		} else {
			noteSystem = mapper.map(noteSystemVO, NoteSystem.class);
			result = noteSystemService.update(noteSystem);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		NoteSystem noteSystem = noteSystemService.findById(id);
		if (noteSystem == null) {
			result = false;
		} else {
			result = noteSystemService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public NoteSystemVO get(@PathParam("id") int id) {
		NoteSystem noteSystem = noteSystemService.get(id);
		noteSystem.setNoteAttendances(null);
		NoteSystemVO noteSystemVO = mapper.map(noteSystem, NoteSystemVO.class);
		return noteSystemVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<NoteSystemVO> search(SearchParameters searchParameters) {
		SearchResult<NoteSystem> searchResult = noteSystemService.getList(searchParameters);

		SearchResult<NoteSystemVO> searchResultVO = new SearchResult<NoteSystemVO>();

		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			searchResult.getResultList().get(i).setNoteAttendances(null);
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), NoteSystemVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
}
