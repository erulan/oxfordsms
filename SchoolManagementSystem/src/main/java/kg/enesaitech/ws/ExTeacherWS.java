package kg.enesaitech.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Course;
import kg.enesaitech.entity.ExTeacher;
import kg.enesaitech.entity.Teacher;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.CourseService;
import kg.enesaitech.service.ExTeacherService;
import kg.enesaitech.service.TeacherService;
import kg.enesaitech.vo.ExTeacherVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

@Path("/ex_teacher")
public class ExTeacherWS {

	Mapper mapper = DozerMapperProvider.getMapper();
	ExTeacherService exTeacherService = ExTeacherService.getInstance();
	TeacherService teacherService = TeacherService.getInstance();
	CourseService courseService = CourseService.getInstance();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(ExTeacherVO exTeacherVO) {
		boolean result = true;
		ExTeacher exTeacher = mapper.map(exTeacherVO, ExTeacher.class);
		if ((exTeacherVO.getTeacher() != null)
				&& (exTeacherVO.getTeacher().getId() != null)) {
			Teacher teacher = teacherService.findById(exTeacherVO.getTeacher()
					.getId());
			if (teacher == null) {
				result = false;

			} else {
				exTeacher.setTeacher(teacher);
			}
		} else {
			exTeacher.setTeacher(null);
		}
		if ((exTeacherVO.getCourse() != null)
				&& (exTeacherVO.getCourse().getId() != null)) {
			Course course = courseService.findById(exTeacherVO.getCourse()
					.getId());
			if (course == null) {
				result = false;

			} else {
				exTeacher.setCourse(course);
			}
		} else {
			exTeacher.setCourse(null);
		}

		if (result) {
			result = exTeacherService.create(exTeacher);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(ExTeacherVO exTeacherVO, @PathParam("id") int id) {
		boolean result = true;
		ExTeacher exTeacher = exTeacherService.findById(id);
		if (exTeacher == null) {
			result = false;
		} else {
			exTeacher = mapper.map(exTeacherVO, ExTeacher.class);
			if ((exTeacherVO.getTeacher() != null)
					&& (exTeacherVO.getTeacher().getId() != null)) {
				Teacher teacher = teacherService.findById(exTeacherVO
						.getTeacher().getId());
				if (teacher == null) {
					result = false;

				} else {
					exTeacher.setTeacher(teacher);
				}
			} else {
				exTeacher.setTeacher(null);
			}
			if ((exTeacherVO.getCourse() != null)
					&& (exTeacherVO.getCourse().getId() != null)) {
				Course course = courseService.findById(exTeacherVO.getCourse()
						.getId());
				if (course == null) {
					result = false;

				} else {
					exTeacher.setCourse(course);
				}
			} else {
				exTeacher.setCourse(null);
			}

			if (result) {
				result = exTeacherService.update(exTeacher);
			}
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		ExTeacher exTeacher = exTeacherService.findById(id);
		if (exTeacher == null) {
			result = false;
		} else {
			result = exTeacherService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public ExTeacherVO get(@PathParam("id") int id) {
		ExTeacher exTeacher = exTeacherService.get(id);
		if (exTeacher.getTeacher() != null) {
			exTeacher.getTeacher().setCGroups(null);
			exTeacher.getTeacher().setNationality(null);
			exTeacher.getTeacher().setCountry(null);
			exTeacher.getTeacher().setCourses(null);
			exTeacher.getTeacher().setExTeachers(null);
		}
		if (exTeacher.getCourse() != null) {
			exTeacher.getCourse().setExTeachers(null);
//			exTeacher.getCourse().setSyllabuses(null);
			exTeacher.getCourse().setTimetables(null);
			if (exTeacher.getCourse().getTeacher() != null) {
				exTeacher.getCourse().setTeacher(null);
			}
			if (exTeacher.getCourse().getBookType() != null) {
				exTeacher.getCourse().setBookType(null);
			}
			if (exTeacher.getCourse().getLesson() != null) {
				exTeacher.getCourse().setLesson(null);
				;
			}
			if (exTeacher.getCourse().getCGroup() != null) {
				exTeacher.getCourse().getCGroup().setCourses(null);
				exTeacher.getCourse().getCGroup().setStudents(null);
				if (exTeacher.getCourse().getCGroup().getTeacher() != null) {
					exTeacher.getCourse().getCGroup().setTeacher(null);
				}
				if (exTeacher.getCourse().getCGroup().getGrade() != null) {
					exTeacher.getCourse().getCGroup().getGrade()
							.setCGroups(null);
				}
				if (exTeacher.getCourse().getCGroup().getClassLetter() != null) {
					exTeacher.getCourse().getCGroup().getClassLetter()
							.setCGroups(null);
				}
			}
			if (exTeacher.getCourse().getYear() != null) {
				exTeacher.getCourse().getYear().setCourses(null);
				exTeacher.getCourse().getYear().setStudAccountings(null);
				exTeacher.getCourse().getYear().setStudents(null);
			}
		}
		ExTeacherVO exTeacherVO = mapper.map(exTeacher, ExTeacherVO.class);
		return exTeacherVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public List<ExTeacherVO> search(SearchParameters searchParameters) {
		List<ExTeacher> exTeachers = exTeacherService.getList(searchParameters);

		List<ExTeacherVO> exTeacherVOs = new ArrayList<ExTeacherVO>();

		for (int i = 0; i < exTeachers.size(); i++) {
			if (exTeachers.get(i).getTeacher() != null) {
				exTeachers.get(i).getTeacher().setCGroups(null);
				exTeachers.get(i).getTeacher().setNationality(null);
				exTeachers.get(i).getTeacher().setCountry(null);
				exTeachers.get(i).getTeacher().setCourses(null);
				exTeachers.get(i).getTeacher().setExTeachers(null);
			}
			if (exTeachers.get(i).getCourse() != null) {
				exTeachers.get(i).getCourse().setExTeachers(null);
//				exTeachers.get(i).getCourse().setSyllabuses(null);
				exTeachers.get(i).getCourse().setTimetables(null);
				if (exTeachers.get(i).getCourse().getTeacher() != null) {
					exTeachers.get(i).getCourse().setTeacher(null);
				}
				if (exTeachers.get(i).getCourse().getBookType() != null) {
					exTeachers.get(i).getCourse().setBookType(null);
				}
				if (exTeachers.get(i).getCourse().getLesson() != null) {
					exTeachers.get(i).getCourse().setLesson(null);
					;
				}
				if (exTeachers.get(i).getCourse().getCGroup() != null) {
					exTeachers.get(i).getCourse().getCGroup().setCourses(null);
					exTeachers.get(i).getCourse().getCGroup().setStudents(null);
					if (exTeachers.get(i).getCourse().getCGroup().getTeacher() != null) {
						exTeachers.get(i).getCourse().getCGroup()
								.setTeacher(null);
					}
					if (exTeachers.get(i).getCourse().getCGroup().getGrade() != null) {
						exTeachers.get(i).getCourse().getCGroup().getGrade()
								.setCGroups(null);
					}
					if (exTeachers.get(i).getCourse().getCGroup()
							.getClassLetter() != null) {
						exTeachers.get(i).getCourse().getCGroup()
								.getClassLetter().setCGroups(null);
					}
				}
				if (exTeachers.get(i).getCourse().getYear() != null) {
					exTeachers.get(i).getCourse().getYear().setCourses(null);
					exTeachers.get(i).getCourse().getYear()
							.setStudAccountings(null);
					exTeachers.get(i).getCourse().getYear().setStudents(null);
				}
			}
			exTeacherVOs.add(mapper.map(exTeachers.get(i), ExTeacherVO.class));
		}

		return exTeacherVOs;
	}
}
