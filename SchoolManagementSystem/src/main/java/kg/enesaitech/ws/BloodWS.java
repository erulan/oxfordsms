package kg.enesaitech.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Blood;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.BloodService;
import kg.enesaitech.vo.BloodVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

@Path("/blood")
public class BloodWS {
	BloodService bloodService = BloodService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(BloodVO bloodVO) {
		Blood blood = mapper.map(bloodVO, Blood.class);
		Boolean created = bloodService.create(blood);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(BloodVO bloodVO, @PathParam("id") int id) {
		boolean result = true;

		Blood blood = bloodService.findById(id);
		if (blood == null) {
			result = false;
		} else {
			blood = mapper.map(bloodVO, Blood.class);
			result = bloodService.update(blood);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		Blood blood = bloodService.findById(id);
		if (blood == null) {
			result = false;
		} else {
			result = bloodService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public BloodVO get(@PathParam("id") int id) {
		Blood blood = bloodService.get(id);


		BloodVO bloodVO = mapper.map(blood, BloodVO.class);

		return bloodVO;
	}


	@POST
	@Path("/list")
	@Produces("application/json")
	public List<BloodVO> search(SearchParameters searchParameters) {
		List<Blood> bloods = bloodService.getList(searchParameters);

		List<BloodVO> bloodVOs = new ArrayList<BloodVO>();

		for (int i = 0; i < bloods.size(); i++) {
			bloodVOs.add(mapper.map(bloods.get(i), BloodVO.class));
		}

		return bloodVOs;
	}
}