package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.StudAccounting;
import kg.enesaitech.entity.Student;
import kg.enesaitech.entity.Year;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.StudAccountingService;
import kg.enesaitech.service.StudentService;
import kg.enesaitech.service.YearService;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;
import kg.enesaitech.vo.StudAccountingVO;

import org.dozer.Mapper;

@Path("/stud_accounting")
public class StudAccountingWS {

	Mapper mapper = DozerMapperProvider.getMapper();
	StudAccountingService studAccountingService = StudAccountingService
			.getInstance();
	StudentService studentService = StudentService.getInstance();
	YearService yearService = YearService.getInstance();

	@POST
	@Path("/create")
	public Response create(StudAccountingVO studAccountingVO) {

		boolean result = true;

		StudAccounting studAccounting = mapper.map(studAccountingVO,
				StudAccounting.class);
		if ((studAccountingVO.getStudent() != null)
				&& (studAccountingVO.getStudent().getId() != null)) {
			Student student = studentService.findById(studAccountingVO
					.getStudent().getId());
			if (student == null) {
				result = false;

			} else {
				studAccounting.setStudent(student);
			}
		} else {
			studAccounting.setStudent(null);
		}
		if ((studAccountingVO.getYear() != null)
				&& (studAccountingVO.getYear().getId() != null)) {
			Year year = yearService
					.findById(studAccountingVO.getYear().getId());
			if (year == null) {
				result = false;

			} else {
				studAccounting.setYear(year);
			}
		} else {
			studAccounting.setYear(null);
		}

		if (result) {
			result = studAccountingService.create(studAccounting);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(StudAccountingVO studAccountingVO, @PathParam("id") int id) {

		boolean result = true;
		StudAccounting studAccounting = studAccountingService
				.findById(id);
		if (studAccounting == null) {
			result = false;
		} else {

			studAccounting = mapper.map(studAccountingVO, StudAccounting.class);
			if ((studAccountingVO.getStudent() != null)
					&& (studAccountingVO.getStudent().getId() != null)) {
				Student student = studentService.findById(studAccountingVO
						.getStudent().getId());
				if (student == null) {
					result = false;

				} else {
					studAccounting.setStudent(student);
				}
			} else {
				studAccounting.setStudent(null);
			}
			if ((studAccountingVO.getYear() != null)
					&& (studAccountingVO.getYear().getId() != null)) {
				Year year = yearService.findById(studAccountingVO.getYear()
						.getId());
				if (year == null) {
					result = false;

				} else {
					studAccounting.setYear(year);
				}
			} else {
				studAccounting.setYear(null);
			}

			if (result) {
				result = studAccountingService.update(studAccounting);
			}
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {

		boolean result = true;
		StudAccounting studAccounting = studAccountingService.findById(id);
		if (studAccounting == null) {
			result = false;
		} else {
			result = studAccountingService.delete(id);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/pay/{st_acc_id}/{fee}")
	@Produces("text/html")
	public Response updateByStudent(@PathParam("st_acc_id") int stAccountingId, @PathParam("fee") double fee) {

		boolean result = true;
			result = studAccountingService.updateByStudentId(stAccountingId, fee);

		return result ? Response.ok().build() : Response.serverError().build();
	}



	@GET
	@Path("/{id}")
	@Produces("application/json")
	public StudAccountingVO get(@PathParam("id") int id) {
		StudAccounting studAccounting = studAccountingService.findById(id);
		if (studAccounting.getYear() != null) {
			studAccounting.getYear().setCourses(null);
			studAccounting.getYear().setStudAccountings(null);
			studAccounting.getYear().setStudents(null);
		}
		if (studAccounting.getStudent() != null) {

			if (studAccounting.getStudent().getCGroup() != null) {

				studAccounting.getStudent().getCGroup().setCourses(null);
				studAccounting.getStudent().getCGroup().setStudents(null);
				if (studAccounting.getStudent().getCGroup().getTeacher() != null) {
					studAccounting.getStudent().getCGroup().setTeacher(null);
				}
				if (studAccounting.getStudent().getCGroup().getGrade() != null) {
					studAccounting.getStudent().getCGroup().getGrade()
							.setCGroups(null);
				}
				if (studAccounting.getStudent().getCGroup().getClassLetter() != null) {
					studAccounting.getStudent().getCGroup().getClassLetter()
							.setCGroups(null);
				}
			}
			studAccounting.getStudent().setNationality(null);
			studAccounting.getStudent().setCountry(null);
			studAccounting.getStudent().setStatus(null);
			studAccounting.getStudent().setNoteAttendances(null);
			studAccounting.getStudent().setParents(null);
			studAccounting.getStudent().setStudAccountings(null);
			studAccounting.getStudent().setYear(null);
		}
		StudAccountingVO studAccountingVO = mapper.map(studAccounting,
				StudAccountingVO.class);
		return studAccountingVO;
	}
	
	@GET
	@Path("/get_by_st/{id}")
	@Produces("application/json")
	public StudAccountingVO getByStudentId(@PathParam("id") int stud_id) {
		StudAccounting studAccounting = studAccountingService.findByStudentId(stud_id);
		if (studAccounting.getYear() != null) {
			studAccounting.getYear().setCourses(null);
			studAccounting.getYear().setStudAccountings(null);
			studAccounting.getYear().setStudents(null);
		}
		studAccounting.setStudent(null);
		StudAccountingVO studAccountingVO = mapper.map(studAccounting,
				StudAccountingVO.class);
		return studAccountingVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<StudAccountingVO> search(SearchParameters searchParameters) {
		SearchResult<StudAccounting> searchResult = studAccountingService.getList(searchParameters);

		SearchResult<StudAccountingVO> searchResultVO = new SearchResult<StudAccountingVO>();
		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			if (searchResult.getResultList().get(i).getYear() != null) {
				searchResult.getResultList().get(i).getYear().setCourses(null);
				searchResult.getResultList().get(i).getYear().setStudAccountings(null);
				searchResult.getResultList().get(i).getYear().setStudents(null);
			}
			if (searchResult.getResultList().get(i).getStudent() != null) {

				if (searchResult.getResultList().get(i).getStudent().getCGroup() != null) {

					searchResult.getResultList().get(i).getStudent().getCGroup().setCourses(null);
					searchResult.getResultList().get(i).getStudent().getCGroup().setStudents(null);
					if (searchResult.getResultList().get(i).getStudent().getCGroup().getTeacher() != null) {
						searchResult.getResultList().get(i).getStudent().getCGroup().setTeacher(null);
					}
					if (searchResult.getResultList().get(i).getStudent().getCGroup().getGrade() != null) {
						searchResult.getResultList().get(i).getStudent().getCGroup().getGrade()
								.setCGroups(null);
						if (searchResult.getResultList().get(i).getStudent().getCGroup().getGrade().getGrade()>=20){
							searchResult.getResultList().get(i).getStudent().getCGroup().setGrade(null);
						}
					}
					if (searchResult.getResultList().get(i).getStudent().getCGroup().getClassLetter() != null) {
						searchResult.getResultList().get(i).getStudent().getCGroup().getClassLetter()
								.setCGroups(null);
					}
				}
				searchResult.getResultList().get(i).getStudent().setNationality(null);
				searchResult.getResultList().get(i).getStudent().setCountry(null);
				searchResult.getResultList().get(i).getStudent().setStatus(null);
				searchResult.getResultList().get(i).getStudent().setNoteAttendances(null);
				searchResult.getResultList().get(i).getStudent().setParents(null);
				searchResult.getResultList().get(i).getStudent().setStudAccountings(null);
				searchResult.getResultList().get(i).getStudent().setYear(null);
			}
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), StudAccountingVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}

}
