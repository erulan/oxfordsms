package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.SyllabusCategory;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.SyllabusCategoryService;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;
import kg.enesaitech.vo.SyllabusCategoryVO;

import org.dozer.Mapper;

@Path("/syllabus_category")
public class SyllabusCategoryWS {

	private SyllabusCategoryService syllabusCategoryService = SyllabusCategoryService.getInstance();
	//private CourseService courseService = CourseService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	public Response create(SyllabusCategoryVO syllabusCategoryVO) {

		boolean result = true;

		SyllabusCategory syllabusCategory = mapper.map(syllabusCategoryVO, SyllabusCategory.class);
		
		if (result) {
			result = syllabusCategoryService.create(syllabusCategory);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(SyllabusCategoryVO syllabusCategoryVO, @PathParam("id") int id) {

		boolean result = true;
		SyllabusCategory syllabusCategory = syllabusCategoryService.findById(id);
		if (syllabusCategory == null) {
			result = false;
		} else {
			syllabusCategory = mapper.map(syllabusCategoryVO, SyllabusCategory.class);
			
			if (result) {
				result = syllabusCategoryService.update(syllabusCategory);
			}
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {

		Boolean result = true;
		SyllabusCategory syllabusCategory = syllabusCategoryService.findById(id);
		if (syllabusCategory == null) {
			result = false;
		} else {
			result = syllabusCategoryService.delete(id);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public SyllabusCategoryVO get(@PathParam("id") int id) {
		SyllabusCategory syllabusCategory = syllabusCategoryService.get(id);
//		syllabusCategory.set(null);
		if (syllabusCategory.getCourse() != null) {
			syllabusCategory.getCourse().setExTeachers(null);
//			syllabus.getSyllabusCategory().getCourse().setSyllabuses(null);
			syllabusCategory.getCourse().setTimetables(null);
			if (syllabusCategory.getCourse().getTeacher() != null) {
				syllabusCategory.getCourse().setTeacher(null);
			}
			if (syllabusCategory.getCourse().getBookType() != null) {
				syllabusCategory.getCourse().setBookType(null);
			}
			if (syllabusCategory.getCourse().getLesson() != null) {
				syllabusCategory.getCourse().getLesson().setCourses(null);
				;
			}
			if (syllabusCategory.getCourse().getCGroup() != null) {

				syllabusCategory.getCourse().getCGroup().setCourses(null);
				syllabusCategory.getCourse().getCGroup().setStudents(null);

				if (syllabusCategory.getCourse().getCGroup().getTeacher() != null) {
					syllabusCategory.getCourse().getCGroup().setTeacher(null);
				}
				if (syllabusCategory.getCourse().getCGroup().getGrade() != null) {
					syllabusCategory.getCourse().getCGroup().getGrade()
							.setCGroups(null);
				}
				if (syllabusCategory.getCourse().getCGroup().getClassLetter() != null) {
					syllabusCategory.getCourse().getCGroup().getClassLetter()
							.setCGroups(null);
				}
			}
			if (syllabusCategory.getCourse().getYear() != null) {
				syllabusCategory.getCourse().getYear().setCourses(null);
				syllabusCategory.getCourse().getYear().setStudAccountings(null);
				syllabusCategory.getCourse().getYear().setStudents(null);
			}
		}
		if (syllabusCategory.getQuarter() != null) {
//			syllabus.getSyllabusCategory().getQuarter().setSyllabus(null);
		}
		SyllabusCategoryVO syllabusCategoryVO = mapper.map(syllabusCategory, SyllabusCategoryVO.class);
		return syllabusCategoryVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<SyllabusCategoryVO> search(SearchParameters searchParameters) {
		SearchResult<SyllabusCategory> searchResult = syllabusCategoryService.getList(searchParameters);

		SearchResult<SyllabusCategoryVO> searchResultVO = new SearchResult<SyllabusCategoryVO>();
		for (int i = 0; i < searchResult.getResultList().size(); i++) {
//			searchResult.getResultList().get(i).setNoteAttendances(null);
			if (searchResult.getResultList().get(i).getCourse() != null) {
				searchResult.getResultList().get(i).getCourse().setExTeachers(null);
//				searchResult.getResultList().get(i).getSyllabusCategory().getCourse().setSyllabuses(null);
				searchResult.getResultList().get(i).getCourse().setTimetables(null);
				if (searchResult.getResultList().get(i).getCourse().getTeacher() != null) {
					searchResult.getResultList().get(i).getCourse().setTeacher(null);
				}
				if (searchResult.getResultList().get(i).getCourse().getBookType() != null) {
					searchResult.getResultList().get(i).getCourse().setBookType(null);
				}
				if (searchResult.getResultList().get(i).getCourse().getLesson() != null) {
					searchResult.getResultList().get(i).getCourse().getLesson().setCourses(null);;
					;
				}
				if (searchResult.getResultList().get(i).getCourse().getCGroup() != null) {

					searchResult.getResultList().get(i).getCourse().getCGroup().setCourses(null);
					searchResult.getResultList().get(i).getCourse().getCGroup().setStudents(null);
					if (searchResult.getResultList().get(i).getCourse().getCGroup().getTeacher() != null) {
						searchResult.getResultList().get(i).getCourse().getCGroup()
								.setTeacher(null);
					}
					if (searchResult.getResultList().get(i).getCourse().getCGroup().getGrade() != null) {
						searchResult.getResultList().get(i).getCourse().getCGroup().getGrade()
								.setCGroups(null);
					}
					if (searchResult.getResultList().get(i).getCourse().getCGroup()
							.getClassLetter() != null) {
						searchResult.getResultList().get(i).getCourse().getCGroup()
								.getClassLetter().setCGroups(null);
					}
				}
				if (searchResult.getResultList().get(i).getCourse().getYear() != null) {
					searchResult.getResultList().get(i).getCourse().getYear().setCourses(null);
					searchResult.getResultList().get(i).getCourse().getYear()
							.setStudAccountings(null);
					searchResult.getResultList().get(i).getCourse().getYear().setStudents(null);
				}
			}

			if (searchResult.getResultList().get(i).getQuarter() != null) {
//				searchResult.getResultList().get(i).getSyllabusCategory().getQuarter().setSyllabus(null);
			}
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), SyllabusCategoryVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
	
	
}
