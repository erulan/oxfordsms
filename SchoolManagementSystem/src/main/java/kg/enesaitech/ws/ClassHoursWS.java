
package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.ClassHours;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.ClassHourService;
import kg.enesaitech.vo.ClassHoursVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/class_hours")
public class ClassHoursWS {
	ClassHourService classHoursService = ClassHourService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(ClassHoursVO classHoursVO) {
		ClassHours classHours = mapper.map(classHoursVO, ClassHours.class);
		Boolean created = classHoursService.create(classHours);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(ClassHoursVO classHoursVO, @PathParam("id") int id) {
		boolean result = true;
		ClassHours classHours = classHoursService
				.findById(id);
		if (classHours == null) {
			result = false;
		} else {
			classHours = mapper.map(classHoursVO, ClassHours.class);
			result = classHoursService.update(classHours);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {

		boolean result = true;
		ClassHours classHours = classHoursService.findById(id);
		if (classHours == null) {
			result = false;
		} else {
			result = classHoursService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public ClassHoursVO get(@PathParam("id") int id) {
		ClassHours classHours = classHoursService.get(id);
		classHours.setTimetables(null);
		ClassHoursVO classHoursVO = mapper.map(classHours, ClassHoursVO.class);
		return classHoursVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<ClassHoursVO> search(SearchParameters searchParameters) {
		SearchResult<ClassHours> classHours = classHoursService.getList(searchParameters);

		SearchResult<ClassHoursVO> classHoursVOs = new SearchResult<ClassHoursVO>();

		for (int i = 0; i < classHours.getResultList().size(); i++) {
			classHours.getResultList().get(i).setTimetables(null);
			classHoursVOs.getResultList().add(mapper.map(classHours.getResultList().get(i), ClassHoursVO.class));
		}
		classHoursVOs.setTotalRecords(classHours.getTotalRecords());

		return classHoursVOs;
	}
}
