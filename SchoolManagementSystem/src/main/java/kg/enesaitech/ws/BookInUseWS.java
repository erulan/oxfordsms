package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.Book;
import kg.enesaitech.entity.BookInUse;
import kg.enesaitech.entity.OwnerType;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.BookInUseService;
import kg.enesaitech.service.BookService;
import kg.enesaitech.service.OwnerTypeService;
import kg.enesaitech.vo.BookInUseVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/book_in_use")
public class BookInUseWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	BookInUseService bookInUseService = BookInUseService.getInstance();
	BookService bookService = BookService.getInstance();
	OwnerTypeService ownerTypeService = OwnerTypeService.getInstance(); 

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(BookInUseVO bookInUseVO) {
		boolean result = true;
		BookInUse bookInUse = mapper.map(bookInUseVO, BookInUse.class);
		
		if ((bookInUseVO.getOwnerType() != null)
				&& (bookInUseVO.getOwnerType().getId() != null)) {
			OwnerType ownerType = ownerTypeService.findById(bookInUseVO.getOwnerType()
					.getId());
			if (ownerType == null) {
				result = false;

			} else {
				bookInUse.setOwnerType(ownerType);
			}
		} else {
			bookInUse.setOwnerType(null);
		}
		if (result) {
			result = bookInUseService.create(bookInUse);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(BookInUseVO bookInUseVO, @PathParam("id") int id) {
		boolean result = true;
		BookInUse bookInUse = bookInUseService.findById(id);
		if (bookInUse == null) {
			result = false;
		} else {
			bookInUse = mapper.map(bookInUseVO, BookInUse.class);
			if ((bookInUseVO.getBook() != null)
					&& (bookInUseVO.getBook().getId() != null)) {
				Book book = bookService.findById(bookInUseVO.getBook().getId());
				if (book == null) {
					result = false;

				} else {
					bookInUse.setBook(book);
				}
			} else {
				bookInUse.setBook(null);
			}
			if ((bookInUseVO.getOwnerType() != null)
					&& (bookInUseVO.getOwnerType().getId() != null)) {
				OwnerType ownerType = ownerTypeService.findById(bookInUseVO.getOwnerType()
						.getId());
				if (ownerType == null) {
					result = false;

				} else {
					bookInUse.setOwnerType(ownerType);
				}
			} else {
				bookInUse.setOwnerType(null);
			}
			if (result) {
				result = bookInUseService.update(bookInUse);
			}
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}
	
	@POST
	@Path("/return/{id}")
	@Produces("text/html")
	public Response returnBook(@PathParam("id") int id) {
		boolean result = true;
		BookInUse bookInUse = bookInUseService.findById(id);
		if (bookInUse==null)
			result = false;
		else
			result = bookInUseService.returnBook(bookInUse);
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		
		result = bookInUseService.delete(id);
		
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public BookInUseVO get(@PathParam("id") int id) {
		BookInUse bookInUse = bookInUseService.get(id);
		if (bookInUse.getBook() != null) {
			bookInUse.getBook().setBookInUses(null);
			bookInUse.getBook().getBookStatus().setBooks(null);
			bookInUse.getBook().getBookType().setCourses(null);
		}
		BookInUseVO bookInUseVO = mapper.map(bookInUse, BookInUseVO.class);
		return bookInUseVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<BookInUseVO> search(SearchParameters searchParameters) {
		SearchResult<BookInUse> searchResult = bookInUseService.getList(searchParameters);

		SearchResult<BookInUseVO> searchResultVO = new SearchResult<BookInUseVO>();
		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			if (searchResult.getResultList().get(i).getBook() != null) {
				searchResult.getResultList().get(i).getBook().setBookInUses(null);
				searchResult.getResultList().get(i).getBook().getBookType().setCourses(null);
				searchResult.getResultList().get(i).getBook().getBookType().setBooks(null);
				searchResult.getResultList().get(i).getBook().getBookType().setCategory(null);
				searchResult.getResultList().get(i).getBook().setBookStatus(null);;
			}
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), BookInUseVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;

	}

}
