package kg.enesaitech.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.CGroup;
import kg.enesaitech.entity.ClassLetter;
import kg.enesaitech.entity.Grade;
import kg.enesaitech.entity.Teacher;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.CGroupService;
import kg.enesaitech.service.ClassLetterService;
import kg.enesaitech.service.GradeService;
import kg.enesaitech.service.StudentService;
import kg.enesaitech.service.TeacherService;
import kg.enesaitech.vo.ClassGroupVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;

@Path("/c_group")
public class CGroupWS {
	CGroupService cGroupService = CGroupService.getInstance();
	GradeService gradeService = GradeService.getInstance();
	ClassLetterService classLetterService = ClassLetterService.getInstance();
	StudentService studentService = StudentService.getInstance();
	TeacherService teacherService = TeacherService.getInstance();

	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	public Response create(ClassGroupVO classGroupVO) {

		boolean result = true;

		CGroup cGroup = mapper.map(classGroupVO, CGroup.class);
		if ((classGroupVO.getGrade() != null)
				&& (classGroupVO.getGrade().getId() != null)) {
			Grade grade = gradeService.findById(classGroupVO.getGrade().getId());
			if (grade == null) {
				result = false;

			} else {
				cGroup.setGrade(grade);
			}
		} else {
			cGroup.setGrade(null);
		}
		if ((classGroupVO.getClassLetter() != null)
				&& (classGroupVO.getClassLetter().getId() != null)) {
			ClassLetter classLetter = classLetterService.findById(classGroupVO
					.getClassLetter().getId());
			if (classLetter == null) {
				result = false;

			} else {
				cGroup.setClassLetter(classLetter);
			}
		} else {
			cGroup.setClassLetter(null);
		}
		if ((classGroupVO.getTeacher() != null)
				&& (classGroupVO.getTeacher().getId() != null)) {
			Teacher teacher = teacherService.findById(classGroupVO.getTeacher()
					.getId());
			if (teacher == null) {
				result = false;

			} else {
				cGroup.setTeacher(teacher);
			}
		} else {
			cGroup.setTeacher(null);
		}
		
		if (result) {
			result = cGroupService.create(cGroup);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(ClassGroupVO classGroupVO, @PathParam("id") int id) {

		boolean result = true;
		CGroup cGroup = cGroupService.findById(id);
		if (cGroup == null) {
			result = false;
		} else {
			cGroup = mapper.map(classGroupVO, CGroup.class);
			if ((classGroupVO.getGrade() != null)
					&& (classGroupVO.getGrade().getId() != null)) {
				Grade grade = gradeService
						.findById(classGroupVO.getGrade().getId());
				if (grade == null) {
					result = false;

				} else {
					cGroup.setGrade(grade);
				}
			} else {
				cGroup.setGrade(null);
			}
			if ((classGroupVO.getClassLetter() != null)
					&& (classGroupVO.getClassLetter().getId() != null)) {
				ClassLetter classLetter = classLetterService.findById(classGroupVO
						.getClassLetter().getId());
				if (classLetter == null) {
					result = false;

				} else {
					cGroup.setClassLetter(classLetter);
				}
			} else {
				cGroup.setClassLetter(null);
			}
			if ((classGroupVO.getTeacher() != null)
					&& (classGroupVO.getTeacher().getId() != null)) {
				Teacher teacher = teacherService.findById(classGroupVO.getTeacher()
						.getId());
				if (teacher == null) {
					result = false;

				} else {
					cGroup.setTeacher(teacher);
				}
			} else {
				cGroup.setTeacher(null);
			}
			if (result) {
				result = cGroupService.update(cGroup);
			}

		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {

		Boolean result = true;
		CGroup cGroup = cGroupService.findById(id);
		if (cGroup == null) {
			result = false;
		} else {
			result = cGroupService.delete(id);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public ClassGroupVO get(@PathParam("id") int id) {
		CGroup cGroup = cGroupService.get(id);
		cGroup.setCourses(null);
		cGroup.setStudents(null);
		if (cGroup.getTeacher() != null) {
			cGroup.getTeacher().setCGroups(null);
			cGroup.getTeacher().setNationality(null);
			cGroup.getTeacher().setCountry(null);
			cGroup.getTeacher().setCourses(null);
			cGroup.getTeacher().setExTeachers(null);
			cGroup.getTeacher().setStatus(null);
		}
		if (cGroup.getGrade() != null) {
			cGroup.getGrade().setCGroups(null);
		}
		if (cGroup.getClassLetter() != null) {
			cGroup.getClassLetter().setCGroups(null);
		}
		ClassGroupVO classGroupVO = mapper.map(cGroup, ClassGroupVO.class);
		return classGroupVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public List<ClassGroupVO> search(SearchParameters searchParameters) {
		List<CGroup> cGroups = cGroupService.getList(searchParameters);

		List<ClassGroupVO> classGroupVOs = new ArrayList<ClassGroupVO>();

		for (int i = 0; i < cGroups.size(); i++) {
			cGroups.get(i).setCourses(null);
			cGroups.get(i).setStudents(null);
			if (cGroups.get(i).getTeacher() != null) {
				cGroups.get(i).getTeacher().setCGroups(null);
				cGroups.get(i).getTeacher().setNationality(null);
				cGroups.get(i).getTeacher().setCountry(null);
				cGroups.get(i).getTeacher().setCourses(null);
				cGroups.get(i).getTeacher().setExTeachers(null);
				cGroups.get(i).getTeacher().setStatus(null);
			}
			if (cGroups.get(i).getGrade() != null) {
				cGroups.get(i).getGrade().setCGroups(null);
			}
			if (cGroups.get(i).getClassLetter() != null) {
				cGroups.get(i).getClassLetter().setCGroups(null);
			}

			classGroupVOs.add(mapper.map(cGroups.get(i), ClassGroupVO.class));
		}

		return classGroupVOs;
	}
}