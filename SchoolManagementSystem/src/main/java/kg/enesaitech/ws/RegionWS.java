package kg.enesaitech.ws;
 
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import kg.enesaitech.entity.Region;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.RegionService;
import kg.enesaitech.vo.RegionVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;
 
@Path("/region")
public class RegionWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	RegionService regionService = RegionService.getInstance();
 
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public RegionVO get(@PathParam("id") int id) {
		Region region = regionService.get(id);
		RegionVO regionVO = mapper.map(region, RegionVO.class);
		return regionVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public List<RegionVO> search(SearchParameters searchParameters) {
		List<Region> regions = regionService.getList(searchParameters);

		List<RegionVO> regionVOs = new ArrayList<RegionVO>();

		for (int i = 0; i < regions.size(); i++) {
			regionVOs.add(mapper.map(regions.get(i), RegionVO.class));
		}

		return regionVOs;
	}
}
