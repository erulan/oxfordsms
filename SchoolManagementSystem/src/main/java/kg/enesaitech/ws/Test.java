package kg.enesaitech.ws;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;


@Aspect
public class Test {
	
	Logger log = Logger.getLogger(Test.class);
 
    @Pointcut("execution(* BookWS.search(..))")
    public void logging() {}
 
    @Around("logging()")
    public Object logging(ProceedingJoinPoint thisJoinPoint) throws Throwable {
    	
    	log.error("Listen Up!!!!");  
 	    log.error("************************");
 	    log.error("************************");
 	    log.error("************************");	
 	    log.error("************************");
 	    log.error("************************");
 	    log.error("************************");
 	    System.out.println("Listen Up!!!!");  
 	    System.out.println("************************");
 	    System.out.println("************************");
 	    System.out.println("************************");
 	    System.out.println("************************");
 	    System.out.println("************************");
 	    System.out.println("************************");
    	
    	
        System.out.println("Before " + thisJoinPoint.getSignature());
        Object ret = thisJoinPoint.proceed();
        System.out.println("After " + thisJoinPoint.getSignature());
 
        return ret;
    }
}

//@Aspect
//public class Test {
//	
//	Logger log = Logger.getLogger(Test.class);
//    
//    @Pointcut("execution(* *.search(..))")
//    public void logBefore(){}  
//
//    @Before("logBefore()")  
//    public void beforeAdvicing(){  
//	    log.error("Listen Up!!!!");  
//	    log.error("************************");
//	    log.error("************************");
//	    log.error("************************");	
//	    log.error("************************");
//	    log.error("************************");
//	    log.error("************************");
//	    System.out.println("Listen Up!!!!");  
//	    System.out.println("************************");
//	    System.out.println("************************");
//	    System.out.println("************************");
//	    System.out.println("************************");
//	    System.out.println("************************");
//	    System.out.println("************************");
//    }  
//
//}