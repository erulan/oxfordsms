
package kg.enesaitech.ws;
 
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import kg.enesaitech.entity.ClassLetter;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.ClassLetterService;
import kg.enesaitech.vo.ClassLetterVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;
 
@Path("/class_letter")
public class ClassLetterWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	ClassLetterService classLetterService = ClassLetterService.getInstance();
 
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public ClassLetterVO get(@PathParam("id") int id) {
		ClassLetter classLetter = classLetterService.get(id);
		classLetter.setCGroups(null);
		ClassLetterVO classLetterVO = mapper.map(classLetter, ClassLetterVO.class);
		return classLetterVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public List<ClassLetterVO> search(SearchParameters searchParameters) {
		List<ClassLetter> classLetters = classLetterService.getList(searchParameters);

		List<ClassLetterVO> classLetterVOs = new ArrayList<ClassLetterVO>();

		for (int i = 0; i < classLetters.size(); i++) {
			classLetters.get(i).setCGroups(null);
			classLetterVOs.add(mapper.map(classLetters.get(i), ClassLetterVO.class));
		}

		return classLetterVOs;
	}
   
}