package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.NoteType;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.NoteTypeService;
import kg.enesaitech.vo.NoteTypeVO;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;

import org.dozer.Mapper;

@Path("/note_type")
public class NoteTypeWS {
	Mapper mapper = DozerMapperProvider.getMapper();
	NoteTypeService noteTypeService = NoteTypeService.getInstance();

	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(NoteTypeVO noteTypeVO) {
		NoteType noteType = mapper.map(noteTypeVO, NoteType.class);
		Boolean created = noteTypeService.create(noteType);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(NoteTypeVO noteTypeVO, @PathParam("id") int id) {
		boolean result = true;
		NoteType noteType = noteTypeService.findById(id);
		if (noteType == null) {
			result = false;
		} else {
			noteType = mapper.map(noteTypeVO, NoteType.class);
			result = noteTypeService.update(noteType);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		NoteType noteType = noteTypeService.findById(id);
		if (noteType == null) {
			result = false;
		} else {
			result = noteTypeService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public NoteTypeVO get(@PathParam("id") int id) {
		NoteType noteType = noteTypeService.get(id);
		noteType.setNoteAttendances(null);
		noteType.setStatus(null);
		NoteTypeVO noteTypeVO = mapper.map(noteType, NoteTypeVO.class);
		return noteTypeVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<NoteTypeVO> search(SearchParameters searchParameters) {
		SearchResult<NoteType> searchResult = noteTypeService.getList(searchParameters);

		SearchResult<NoteTypeVO> searchResultVO = new SearchResult<NoteTypeVO>();

		for (int i = 0; i < searchResult.getResultList().size(); i++) {
			searchResult.getResultList().get(i).setNoteAttendances(null);
			searchResult.getResultList().get(i).setStatus(null);
			searchResultVO.getResultList().add(mapper.map(searchResult.getResultList().get(i), NoteTypeVO.class));
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;
	}
}
