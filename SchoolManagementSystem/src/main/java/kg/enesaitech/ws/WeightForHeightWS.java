package kg.enesaitech.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.WeightForHeight;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.WeightForHeightService;
import kg.enesaitech.vo.SearchParameters;
import kg.enesaitech.vo.SearchResult;
import kg.enesaitech.vo.WeightForHeightVO;

import org.dozer.Mapper;

@Path("/weight_for_height")
public class WeightForHeightWS {
	WeightForHeightService weightForHeightService = WeightForHeightService.getInstance();
	Mapper mapper = DozerMapperProvider.getMapper();

	@POST
	@Path("/create")
	public Response create(WeightForHeightVO weightForHeightVO) {

		WeightForHeight weightForHeight = mapper.map(weightForHeightVO, WeightForHeight.class);
		Boolean created = weightForHeightService.create(weightForHeight);

		return created ? Response.ok().build() : Response.serverError().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(WeightForHeightVO weightForHeightVO, @PathParam("id") int id) {
		boolean result = true;
		WeightForHeight weightForHeight = weightForHeightService.findById(id);
		if (weightForHeight == null) {
			result = false;
		} else {
			weightForHeight = mapper.map(weightForHeightVO, WeightForHeight.class);
			result = weightForHeightService.update(weightForHeight);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		WeightForHeight weightForHeight = weightForHeightService.findById(id);
		if (weightForHeight == null) {
			result = false;
		} else {
			result = weightForHeightService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/{id}")
	@Produces("application/json")
	public WeightForHeightVO get(@PathParam("id") int id) {
		WeightForHeight weightForHeight = weightForHeightService.get(id);
		WeightForHeightVO weightForHeightVO = mapper.map(weightForHeight, WeightForHeightVO.class);
		return weightForHeightVO;
	}

	@POST
	@Path("/list")
	@Produces("application/json")
	public SearchResult<WeightForHeightVO> search(SearchParameters searchParameters) {
		SearchResult<WeightForHeight> weightForHeights = weightForHeightService.getList(searchParameters);

		SearchResult<WeightForHeightVO> weightForHeightVOs = new SearchResult<WeightForHeightVO>();

		for (int i = 0; i < weightForHeights.getResultList().size(); i++) {
			weightForHeightVOs.getResultList().add(mapper.map(weightForHeights.getResultList().get(i), WeightForHeightVO.class));
		}

		return weightForHeightVOs;
	}
}
