package kg.enesaitech.ws;
 
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import kg.enesaitech.entity.BookStatus;
import kg.enesaitech.providers.DozerMapperProvider;
import kg.enesaitech.service.BookStatusService;
import kg.enesaitech.vo.BookStatusVO;
import kg.enesaitech.vo.SearchParameters;

import org.dozer.Mapper;
 
@Path("/book_status")
public class BookStatusWS {

	Mapper mapper = DozerMapperProvider.getMapper();
	BookStatusService bookStatusService = BookStatusService.getInstance();
   
	@POST
	@Path("/create")
	@Produces("text/html")
	public Response create(BookStatusVO bookStatusVO) {
		BookStatus bookStatus = mapper.map(bookStatusVO, BookStatus.class);
		Boolean created = bookStatusService.create(bookStatus);

		return created ? Response.ok().build() : Response.serverError().build();
	}
	
	@POST
	@Path("/update/{id}")
	@Produces("text/html")
	public Response update(BookStatusVO bookStatusVO, @PathParam("id") int id) {
		boolean result = true;
		BookStatus bookStatus = bookStatusService.findById(id);
		if (bookStatus == null) {
			result = false;
		} else {
			bookStatus = mapper.map(bookStatusVO, BookStatus.class);
			result = bookStatusService.update(bookStatus);
		}

		return result ? Response.ok().build() : Response.serverError().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces("text/html")
	public Response delete(@PathParam("id") int id) {
		boolean result = true;
		BookStatus bookStatus = bookStatusService.findById(id);
		if (bookStatus == null) {
			result = false;
		} else {
			result = bookStatusService.delete(id);
		}
		return result ? Response.ok().build() : Response.serverError().build();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public BookStatusVO get(@PathParam("id") int id) {
		BookStatus bookStatus = bookStatusService.get(id);
		bookStatus.setBooks(null);
		BookStatusVO bookStatusVO = mapper.map(bookStatus, BookStatusVO.class);
		return bookStatusVO;
	}
	@POST
	@Path("/list")
	@Produces("application/json")
	public List<BookStatusVO> search(SearchParameters searchParameters) {
		List<BookStatus> educationStatus = bookStatusService.getList(searchParameters);

		List<BookStatusVO> bookStatusVOs = new ArrayList<BookStatusVO>();

		for (int i = 0; i < educationStatus.size(); i++) {
			educationStatus.get(i).setBooks(null);
			bookStatusVOs.add(mapper.map(educationStatus.get(i), BookStatusVO.class));
		}

		return bookStatusVOs;
	}
}
