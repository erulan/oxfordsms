'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('libraryCtrl',  ['$scope', '$modal', 'restService', 'ngTableParams', '$filter', 'Utils', '$translatePartialLoader',
                             function($scope, $modal, restService, ngTableParams, $filter, Utils,$translatePartialLoader){
		
	$translatePartialLoader.addPart('admin');

	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	
	$scope.getTitle = function (title){
		return $("."+title).text();
	}
	
	$scope.deneme = "denemee";	
		
		$scope.tableParams = new ngTableParams({
	        page: 1,            // show first page
	        count: 10, 			// count per page
	        sorting: {
	            id: 'desc'     // initial sorting
	        },				
	    }, {
	        total: 0, // length of data
	        getData: function($defer, params){
	        	Utils.ngTableGetData($defer, params, "book_type");
	        },
	    });
		$scope.deleteBookType = function(id){
			Utils.callDeleteModal("book_type", id);
	};	
		
}]);


