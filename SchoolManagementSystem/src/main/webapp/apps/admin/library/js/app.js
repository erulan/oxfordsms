'use strict'; 

angular.module('libApp', ['controllers','common.apps']).
config(['$routeProvider',   function ($routeProvider) {
					

	$routeProvider.when('/library', {
		templateUrl : 'views/library.html',
		controller : 'libraryCtrl',
	});
	/*$routeProvider.when('/stuff', {
		templateUrl : 'views/stuff.html',
		controller : 'stuffCtrl',
	});*/
	$routeProvider.otherwise({
		redirectTo : '/library'
	});
  			
}]);	


