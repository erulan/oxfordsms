'use strict'; 

angular.module('settingsApp', ['controllers', 'common.apps']).
config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/settings', {
		templateUrl : 'views/settings.html',
		controller : 'settingsCtrl',
	});
	$routeProvider.when('/add_lesson', {
		templateUrl : 'views/add_lesson.html',
		controller : 'add_lessonCtrl',
	});
	$routeProvider.when('/edit_lesson/:id', {
		templateUrl : 'views/edit_lesson.html',
		controller : 'edit_lessonCtrl',
	});
	$routeProvider.when('/edit_category/:id', {
		templateUrl : 'views/edit_book_cat.html',
		controller : 'edit_categoryCtrl',
	});
	$routeProvider.when('/edit_user/:id', {
		templateUrl : 'views/edit_user.html',
		controller : 'edit_userCtrl',
	});
	$routeProvider.when('/users', {
		  templateUrl : 'views/users.html',
		  controller : 'usersCtrl',
	});
	$routeProvider.when('/stuff', {
		  templateUrl : 'views/stuff.html',
		  controller : 'stuffCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/settings'
	});
  			
}]);	


