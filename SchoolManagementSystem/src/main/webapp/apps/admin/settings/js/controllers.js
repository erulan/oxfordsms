'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('settingsCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$location','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$location, $translatePartialLoader){	
	  						
	$translatePartialLoader.addPart('admin');
	
	$scope.deneme = "denemee";	
	$scope.lesson = {};
	$scope.category = {};
	
	$scope.submitLesson = function() {
		restService.one("lesson").customPOST($scope.lesson, "create").then(function(data){
			alert("success");
			$location.path("#/settings");
		});
	};	
	$scope.submitCategory = function() {
		restService.one("category").customPOST($scope.category, "create").then(function(data){
			alert("success");
			$location.path("#/settings");
		});
	};

	restService.all("lesson").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.lessons = results.originalElement;
		console.log($scope.lessons);
	});
	restService.all("category").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.categories = results.originalElement;
	});
	$scope.datepicker = {};
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
		$scope.deleteLesson = function(id){
			Utils.callDeleteModal("lesson", id);
		}
		$scope.deleteCategory = function(id){
			Utils.callDeleteModal("category", id);
		}
}])
.controller('edit_categoryCtrl',  ['$scope', 'restService', '$routeParams', "$filter", "Utils","$location", '$translatePartialLoader',function($scope, restService, $routeParams, $filter, Utils, $location,$translatePartialLoader){
	
	$translatePartialLoader.addPart('admin');

	$scope.categoryId = $routeParams.id;
	$scope.category = {};
	$scope.submitted = false;
	$scope.deneme = "checking";
	
	restService.all("category").customGET($scope.categoryId).then(function(data){
		$scope.category = data.originalElement;
	});
	
	
	
	$scope.update = function(){
		console.log($scope.category);
		$scope.submitted = true;
		restService.one("category").customPOST($scope.category, "update").then(function(data){
			alert("success");
			$location.path("/settings");
		});
	};
	
	$scope.deleteCategory = function(){
		var yesNo = Utils.callDeleteModal("category", $routeParams.id);
	};
	
}])

.controller('edit_book_catCtrl',  ['$scope', 'restService', '$routeParams', "$filter", 'Utils',"$location", '$translatePartialLoader',function($scope, restService, $routeParams, $filter, Utils, $location,$translatePartialLoader){

	$translatePartialLoader.addPart('admin');

	$scope.categoryId = $routeParams.id;
	$scope.category = {};
	$scope.submitted = false;
	$scope.deneme = "checking";
/*	
	restService.all("category").customGET($scope.categoryId).then(function(data){
		$scope.categorys = data.originalElement;
	});*/
	
	$scope.update = function(){
		$scope.submitted = true;
		restService.one("category").customPOST($scope.category, "update/").then(function(data){
			alert("success");
			$location.path("/settings");
		});
	};
	
}])
.controller('edit_lessonCtrl',  ['$scope', 'restService', '$routeParams', '$filter', 'Utils', '$location','$translatePartialLoader', function($scope, restService, $routeParams, $filter, Utils, $location,$translatePartialLoader){

	$translatePartialLoader.addPart('admin');

	$scope.lessonId = $routeParams.id;
	$scope.lesson = {};
	$scope.submitted = false;
	$scope.deneme = "checking";
	
	restService.all("lesson").customGET($scope.lessonId).then(function(data){
		$scope.lessons = data.originalElement;
	});
	
	$scope.update = function(){
		$scope.submitted = true;
		restService.one("lesson").customPOST($scope.lessons, "update/"+$scope.lessonId).then(function(data){
			alert("success");
			$location.path("/settings");
		});
	};
	$scope.deleteLesson = function(){
		var yesNo = Utils.callDeleteModal("lesson", $routeParams.id);
	};
	
}])
  .controller('usersCtrl', ['$scope', '$modal', '$log', 'restService', '$routeParams','ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService,$routeParams, ngTableParams, $filter, Utils,$translatePartialLoader){ 
  
	$translatePartialLoader.addPart('admin');

	  $scope.users = {}
	  $scope.usersId = $routeParams.id
	  
	  	restService.all("users").customGET($scope.usersId).then(function(data){
			$scope.users = data.originalElement;
			alert("here");
			console.log($scope.users);
		});
		
		restService.all("users_role").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
			$scope.users_roles = data.originalElement;
		});
		
		$scope.getTitle = function(title){
			return $("."+title).text();
		}
	  
	  $scope.tableParams = new ngTableParams({
         page: 1,            // show first page
         count: 10,    // count per page
         sorting: {
             userName: 'desc'     // initial sorting
         },    
     }, {
         total: 0, // length of data
         getData: function($defer, params){
          Utils.ngTableGetData($defer, params, "users_role");
         },
     });
  $scope.deleteUser = function(id){
   Utils.callDeleteModal("users_role", id);
  }
  
  }])
.controller('stuffCtrl', ['$scope', '$modal', '$log', 'restService', '$routeParams','$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService,$routeParams, $filter, Utils,$translatePartialLoader){ 
	  
	$translatePartialLoader.addPart('admin');

	  	restService.all("stuff").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
			$scope.stuffs = data.originalElement.resultList;
			$scope.total = data.originalElement.totalRecords;
		});
		restService.all("role").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
			$scope.roles = data.originalElement;
			angular.forEach($scope.roles, function(role, key){
				
				if(angular.equals(role.name, "librarian")){
					$scope.librarianRoleId = role.id;
				}
				if(angular.equals(role.name, "accountant")){
					$scope.accountantId = role.id;
				}
				if(angular.equals(role.name, "secretary")){
					$scope.secretaryId = role.id;
				}
				if(angular.equals(role.name, "deputy_director")){
					$scope.deputyId = role.id;
				}
				if(angular.equals(role.name, "hr_manager")){
					$scope.HRId = role.id;
				}
			});
			
		});
		
		$scope.setAsLibrarian = function(){
			restService.all("users_role").customGET("stuff/" +$scope.selectedId + "/" + $scope.librarianRoleId).then(function(data){
				alert("Success! Username : Stuff NO, Password : Stuff NO + *");
			});
		}

		$scope.setAccountant = function(){
			restService.all("users_role").customGET("stuff/" +$scope.selectedId+ "/" + $scope.accountantId).then(function(data){
				alert("Success! Username : Stuff NO, Password : Stuff NO + *");
			});
		}
		
		$scope.setSecretary = function(){
			restService.all("users_role").customGET("stuff/" +$scope.selectedId+ "/" + $scope.secretaryId).then(function(data){
				alert("Success! Username : Stuff NO, Password : Stuff NO + *");
			});
		}
		
		$scope.setDeputy = function(){
			restService.all("users_role").customGET("stuff/" +$scope.selectedId+ "/" + $scope.deputyId).then(function(data){
				alert("Success! Username : Stuff NO, Password : Stuff NO + *");
			});
		}
		$scope.setHR = function(){
			restService.all("users_role").customGET("stuff/" +$scope.selectedId+ "/" + $scope.HRId).then(function(data){
				alert("Success! Username : Stuff NO, Password : Stuff NO + *");
			});
		}

    
}])	  	
.controller('edit_userCtrl',  ['$scope', '$modal', 'restService', '$routeParams','Utils',  '$location','$translatePartialLoader', function($scope, $modal, restService, $routeParams,  Utils , $location,$translatePartialLoader){
	
	$translatePartialLoader.addPart('admin');

	$scope.userId = $routeParams.id;
	$scope.user = {};
	$scope.submitted = false;
	$scope.deneme = "checking";
	
	restService.all("users").customGET($scope.userId).then(function(data){
		$scope.user = data.originalElement;
	});
	
	restService.all("users_role").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.users_roles = data.originalElement;
	});
	restService.all("status").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.statuss = data.originalElement;
	});

	$scope.update = function(){
		$scope.submitted = true;
		restService.one("users").customPOST($scope.user, "update/" + $scope.userId).then(function(data){
			alert("success");
			$location.path("/users");
		});
	};
	
	$scope.deleteUserRole = function(){
		var yesNo = Utils.callDeleteModal("users_role", $routeParams.id);
	};
	
}]);



