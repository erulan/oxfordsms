'use strict'; 
	
angular.module('controllers', [])		
.controller('editCtrl',['$scope', '$routeParams', 'restService', '$compile','$rootScope','$translatePartialLoader','$location',
                                 function($scope, $routeParams, restService, $compile,$rootScope,$translatePartialLoader, $location) {	
	  						
	$translatePartialLoader.addPart('admin');
	
	$scope.users = {};
	
	$scope.username = $rootScope.userInfo.username;
/*	$scope.studentID = $scope.userInfo.id;
	
	restService.all("users").customGET( $scope.secretaryID ).then(function(data){
		$scope.user = data.originalElement;
	});*/
	
	$scope.save = function(){
		if($scope.newPassword != $scope.confirmPassword){
			alert("Passwords doesn't match");
			return;
		}
		restService.one("security").customGET("change_password/" +  $scope.username +"/"+ $scope.currentPassword +"/"+ $scope.newPassword)
		.then(function(data){
			alert("success");
			$location.path("/edit");
		});
	};
}]);