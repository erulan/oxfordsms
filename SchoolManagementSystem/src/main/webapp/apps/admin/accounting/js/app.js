'use strict'; 

angular.module('accountingApp', ['controllers','common.apps']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/accounting', {
		templateUrl : 'views/accounting.html',
		controller : 'accountingCtrl',
	});
	/*$routeProvider.when('/stuff', {
		templateUrl : 'views/stuff.html',
		controller : 'stuffCtrl',
	});*/
	$routeProvider.otherwise({
		redirectTo : '/accounting'
	});
  			
}]);	


