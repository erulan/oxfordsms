 'use strict';

/* Controllers */
	
angular.module('controllers', [])
.controller('coursesCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){
	
	$translatePartialLoader.addPart('admin');

	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	$scope.getTitle = function (title){
		return $("."+title).text();
	}
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            lesson: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "course");
        },
    });
	  $scope.deleteCourse = function(id){
			Utils.callDeleteModal("course", id);
	};
}])
.controller('add_courseCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$location','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $location,$translatePartialLoader){

	$translatePartialLoader.addPart('admin');

	$scope.course = {};
	$scope.submitted = false;
	$scope.datepicker = {};
	
	restService.all("teacher").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.teacherss = data.originalElement.resultList;
	});
	restService.all("lesson").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.lessons = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});	
	$scope.save = function(){
		if($scope.datepicker.date != null){
			$scope.course.birthDate = new Date($scope.datepicker.date).getTime();
		}
		$scope.submitted = true;
		restService.one("course").customPOST($scope.course, "create").then(function(data){
			alert("Course was successfully created!");
			$location.path("/courses");
		});
	};
}])
.controller('edit_courseCtrl', ['$scope', '$modal', '$routeParams','restService', '$location', '$translatePartialLoader', function ( $scope, $modal, $routeParams, restService, $location,$translatePartialLoader) {	
	
	$translatePartialLoader.addPart('admin');

	$scope.courseId = $routeParams.id;
	$scope.course = {};	
	$scope.submitted = false;
	$scope.datepicker = {};
	$scope.deneme = "checking";
	
	restService.all("course").customGET( $scope.courseId ).then(function(data){
		$scope.course = data.originalElement;
		$scope.datepicker.date = data.originalElement.birthDate;
	});
	restService.all("lesson").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.lessons = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("teacher").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.teacherss = data.originalElement.resultList;
	});
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});
	
	$scope.update = function() {
		$scope.submitted = true;
		restService.all("course").customPOST($scope.course, "update/" + $scope.courseId).then(function(data){
			alert("Course information successfully updated!");
			$location.path("/courses");
		});
	};	  	
	
	$scope.deleteCourse = function(){
		var yesNo = Utils.callDeleteModal("course", $routeParams.id);
	};
}]);

