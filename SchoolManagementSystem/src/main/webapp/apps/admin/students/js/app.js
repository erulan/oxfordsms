'use strict'; 

angular.module('studentsApp', ['controllers', 'common.apps']).
config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/students', {
		templateUrl : 'views/students.html',
		controller : 'studentsCtrl',
	});
	$routeProvider.when('/kids', {
		templateUrl : 'views/kids.html',
		controller : 'kidsCtrl',
	});
	$routeProvider.when('/student', {
		templateUrl : 'views/students.html',
		controller : 'studentsCtrl',
	});
	$routeProvider.when('/add_student', {
		templateUrl : 'views/add_student.html',
		controller : 'add_studentCtrl',
	});
	$routeProvider.when('/add_kid', {
		templateUrl : 'views/add_kid.html',
		controller : 'add_kidCtrl',
	});
	$routeProvider.when('/edit_student/:id', {
		templateUrl : 'views/edit_student.html',
		controller : 'edit_studentCtrl',
	});
	$routeProvider.when('/edit_kid/:id', {
		templateUrl : 'views/edit_kid.html',
		controller : 'edit_kidCtrl',
	});
	$routeProvider.when('/student_profile/:id', {
		templateUrl : 'views/student_profile.html',
		controller : 'student_profileCtrl',
	});
	$routeProvider.when('/kid_profile/:id', {
		templateUrl : 'views/kid_profile.html',
		controller : 'kid_profileCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/students'
	});
	
}]);


