'use strict';
   
/* Controllers */
	
angular.module('controllers', [])		
.controller('studentsCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils',"$location",'$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, location,$translatePartialLoader){
$scope.getStudents = function($defer, params, path) {
    	
		var searchParams = {};
    	var prm = params.parameters();

    	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
    	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
    	
    	searchParams["searchParameters"] = {};
    	angular.forEach(prm.filter, function(value, key){
    		searchParams.searchParameters[key] = value;
    	});

		searchParams.searchParameters["schoolTypeId"] = 1;
    	searchParams["orderParamDesc"] = {};
    	angular.forEach(prm.sorting, function(value, key){
    		searchParams.orderParamDesc[key] = (value == "desc");
    	});
    	
    	restService.all(path).customPOST(searchParams, ['list']).then(function(data){
    		params.total(data.originalElement.totalRecords);
            // set new data
            $defer.resolve(data.originalElement.resultList);
		});
    }

	$translatePartialLoader.addPart('admin');

	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	
	$scope.getTitle = function (title){
		return $("."+title).text();
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "student");
        },
    });
	
	 $scope.deleteStudent = function(id){
			Utils.callDeleteModal("student", id);
			$location.path("/students");
	};

}])

.controller('add_studentCtrl', ['$scope', '$modal', '$routeParams','restService', '$log', '$location', '$translatePartialLoader', function ( $scope, $modal, $routeParams, restService, $log, $location,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('admin');
	$scope.schoolTypeId = 1;
	$scope.student = {};
	$scope.datepicker = {};
	$scope.deneme = "denemee";		
	
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroups = data.originalElement;
	});
	restService.all("status").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.statuss = data.originalElement;
	});
	restService.all("nationality").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.nationalities = data.originalElement;
	});
	restService.all("country").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.countrys = data.originalElement;
	});
	restService.all("oblast").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.oblasts = data.originalElement;
	});
	restService.all("region").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.regions = data.originalElement;
	});
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});
	$scope.save = function() {
		var fd = new FormData();
		
		fd.append('student', new Blob([angular.toJson($scope.student)], {type: "application/json"}));
		fd.append('photo', $scope.photo);
		
		restService.one("student").withHttpConfig({transformRequest: angular.identity})
			.customPOST(fd,'create',undefined,{'Content-Type': undefined}).then(function(data){
			alert("New student was successfully registered!");
			$location.path("/students");
			
		});
	};	  	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
.controller('edit_studentCtrl', ['$scope', '$modal', '$routeParams','restService', '$log','$location', '$translatePartialLoader', function ( $scope, $modal, $routeParams, restService,$log, $location,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('admin');

	$scope.datepicker = {};
	$scope.student = {};
	$scope.studentId = $routeParams.id;
	$scope.submitted = false;


	$scope.deneme = "denemee";	
		
	restService.all("student").customGET( $routeParams.id ).then(function(data){
		$scope.student = data.originalElement;
		$scope.datepicker.date = data.originalElement.birthDate;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000,  searchParameters: {"schoolTypeId": 1}}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("status").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.statuss = data.originalElement;
	});
	restService.all("nationality").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.nationalities = data.originalElement;
	});
	restService.all("country").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.countrys = data.originalElement;
	});
	restService.all("oblast").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.oblasts = data.originalElement;
	});
	restService.all("region").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.regions = data.originalElement;
	});
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});
	
	$scope.submitt = function(){
		$scope.submitted = true;
		
		var fd = new FormData();
		fd.append('student', new Blob([angular.toJson($scope.student)], {type: "application/json"}));
		fd.append('photo', $scope.photo);		
		
		restService.one("student").withHttpConfig({transformRequest: angular.identity})
			.customPOST(fd, "update/" + $scope.studentId, undefined,{'Content-Type': undefined})
				.then(function(data){
					alert("Personal information was successfully updated! ");
					$location.path("/students");
		});
	};

	//this is datepicker
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
.controller('student_profileCtrl', ['$scope', '$modal', '$routeParams','restService', '$log','$translatePartialLoader',  function ( $scope, $modal, $routeParams, restService,$log,$translatePartialLoader) {	

	$translatePartialLoader.addPart('admin');

	$scope.student = {};
	
	restService.all("student").customGET( $routeParams.id ).then(function(data){
		$scope.student = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("studAccountings").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.studAccountingses = data.originalElement;
	});
}])
.controller('kidsCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils',"$location", '$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $location,$translatePartialLoader){
	$translatePartialLoader.addPart('admin');
	
	$scope.getKids = function($defer, params, path) {
    	
		var searchParams = {};
    	var prm = params.parameters();

    	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
    	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
    	
    	searchParams["searchParameters"] = {};
    	angular.forEach(prm.filter, function(value, key){
    		searchParams.searchParameters[key] = value;
    	});

		searchParams.searchParameters["schoolTypeId"] = 2;
    	searchParams["orderParamDesc"] = {};
    	angular.forEach(prm.sorting, function(value, key){
    		searchParams.orderParamDesc[key] = (value == "desc");
    	});
    	
    	restService.all(path).customPOST(searchParams, ['list']).then(function(data){
    		params.total(data.originalElement.totalRecords);
            // set new data
            $defer.resolve(data.originalElement.resultList);
		});
    }
	
	$scope.schoolTypeId = 2;
	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	$scope.getTitle = function (title){
		return $("."+title).text();
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	$scope.getKids($defer, params, "student");
        },
    });
	
	 $scope.deleteStudent = function(id){
			Utils.callDeleteModal("student", id);
			$location.path("/kids");
	};

}])
.controller('add_kidCtrl', ['$scope', '$modal', '$routeParams','restService', '$log', '$location', '$translatePartialLoader', function ( $scope, $modal, $routeParams, restService, $log, $location,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('admin');
	$scope.student = {};
	$scope.datepicker = {};
	$scope.deneme = "denemee";		
	
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000,  searchParameters: {"schoolTypeId": 2}}, "list").then(function(data){
		$scope.CGroups = data.originalElement;
	});
	restService.all("school_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.schoolTypes = data.originalElement;
	});
	restService.all("school_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.schoolTypes = data.originalElement;
	});
	restService.all("status").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.statuss = data.originalElement;
	});
	restService.all("nationality").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.nationalities = data.originalElement;
	});
	restService.all("country").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.countrys = data.originalElement;
	});
	/*restService.all("oblast").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.oblasts = data.originalElement;
	});
	restService.all("region").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.regions = data.originalElement;
	});*/
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});
	$scope.save = function() {
		var fd = new FormData();
		
		fd.append('student', new Blob([angular.toJson($scope.student)], {type: "application/json"}));
		fd.append('photo', $scope.photo);
		
		restService.one("student").withHttpConfig({transformRequest: angular.identity})
		.customPOST(fd,'create',undefined,{'Content-Type': undefined}).then(function(data){
		alert("New student was successfully registered!");
		$location.path("/kids");
		
	});
	};	  	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
.controller('edit_kidCtrl', ['$scope', '$modal', '$routeParams','restService', '$log','$location', '$translatePartialLoader', function ( $scope, $modal, $routeParams, restService,$log, $location,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('admin');
	$scope.datepicker = {};
	$scope.student = {};
	$scope.studentId = $routeParams.id;
	$scope.submitted = false;

	$scope.deneme = "denemee";	
		
	restService.all("student").customGET( $routeParams.id ).then(function(data){
		$scope.student = data.originalElement;
		$scope.datepicker.date = data.originalElement.birthDate;
	});
	
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000, searchParameters: {"schoolTypeId": 2}}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("status").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.statuss = data.originalElement;
	});
	restService.all("nationality").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.nationalities = data.originalElement;
	});
	restService.all("country").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.countrys = data.originalElement;
	});
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});
	
	$scope.submitt = function(){
		$scope.submitted = true;		
		var fd = new FormData();
		fd.append('student', new Blob([angular.toJson($scope.student)], {type: "application/json"}));
		fd.append('photo', $scope.photo);		
		
		restService.one("student").withHttpConfig({transformRequest: angular.identity})
			.customPOST(fd, "update/" + $scope.studentId, undefined,{'Content-Type': undefined})
				.then(function(data){
					alert("Personal information was successfully updated! ");
					$location.path("/kids");
		});
	};

	//this is datepicker
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
.controller('kid_profileCtrl', ['$scope', '$modal', '$routeParams','restService', '$log','$translatePartialLoader',  function ( $scope, $modal, $routeParams, restService,$log,$translatePartialLoader) {	

	$translatePartialLoader.addPart('admin');

	$scope.student = {};
	
	restService.all("student").customGET( $routeParams.id ).then(function(data){
		$scope.student = data.originalElement;
	});
	
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("studAccountings").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.studAccountingses = data.originalElement;
	});
}]);


