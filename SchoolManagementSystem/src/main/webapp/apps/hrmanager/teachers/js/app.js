'use strict'; 

angular.module('teachersApp', ['controllers', 'common.apps']).
config(['$routeProvider', '$locationProvider',  function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/list_teacher', {
		templateUrl : 'views/teachers.html',
		controller : 'teachersCtrl',
	});
	$routeProvider.when('/teacher', {
		templateUrl : 'views/teachers.html',
		controller : 'teachersCtrl',
	});
	$routeProvider.when('/add_teacher', {
		templateUrl : 'views/add_teacher.html',
		controller : 'add_teacherCtrl',
	});
	$routeProvider.when('/edit_teacher/:id', {
		templateUrl : 'views/edit_teacher.html',
		controller : 'edit_teacherCtrl',
	});
	$routeProvider.when('/teacher_profile/:id', {
		templateUrl : 'views/teacher_profile.html',
		controller : 'teacher_profileCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/list_teacher'
	});
  			
}]);	


