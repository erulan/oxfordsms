'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('teachersCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){	
	  					
	$translatePartialLoader.addPart('hrmanager');
	
	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	
	$scope.getTitle = function(title){
		return $("."+title).text();
	}
	
	$scope.deneme = "denemee";	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
        	name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "teacher");
        },
    });
	$scope.datepicker = {};
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	$scope.deleteTeacher = function(id){
		Utils.callDeleteModal("teacher", id);
};
	
}])
.controller('add_teacherCtrl', ['$scope',  '$routeParams', 'restService', '$location','$translatePartialLoader', function ( $scope, $routeParams, restService, $location,$translatePartialLoader) {	
	
	$translatePartialLoader.addPart('hrmanager');

	$scope.teacher = {};			
	$scope.submitted = false;
	$scope.datepicker = {};
	$scope.datepicker2 = {};
	$scope.teacherCatId = {};
	$scope.deneme = "checking";	
	
	restService.all("nationality").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.nationalities = data.originalElement;
	});
	restService.all("country").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.countrys = data.originalElement;
	});
	restService.all("teacher_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.teacherCategories = data.originalElement;
	});
	$scope.save = function(){
		if($scope.datepicker.date != null){
			$scope.teacher.birthDate = new Date($scope.datepicker.date).getTime();
		}
		var fd = new FormData();
		fd.append('teacher', new Blob([angular.toJson($scope.teacher)], {type: "application/json"}));
		fd.append('photo', $scope.photo);
		
		restService.one("teacher").withHttpConfig({transformRequest: angular.identity})
		.customPOST(fd,'create',undefined,{'Content-Type': undefined}).then(function(data){
			alert("New teacher was successfully registered!");
			$location.path("/list_teacher");
		});
	};
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
  $scope.datepicker2.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker2.opened = true;
	  };
	  
	  $scope.datepicker2.format = 'dd/MM/yyyy';

	  $scope.datepicker2.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	$scope.showTeacher = function() {
			console.log($scope.teacher);
	};		
}])
.controller('edit_teacherCtrl', ['$scope',  '$routeParams','restService', '$location','Utils','$translatePartialLoader', function ( $scope,  $routeParams, restService, $location, Utils,$translatePartialLoader) {	
	
	$translatePartialLoader.addPart('hrmanager');

	$scope.teacherId = $routeParams.id;
	$scope.teacher = {};
	$scope.submitted = false;
	$scope.datepicker = {};	
	$scope.deneme = "checking";
	
	$scope.teacherCatId = {};

	restService.all("teacher").customGET( $scope.teacherId ).then(function(data){
		$scope.teacher = data.originalElement;
		$scope.datepicker.date = data.originalElement.birthDate;
	});
	restService.all("teacher_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.teacherCategories = data.originalElement;
	});
	restService.all("nationality").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.nationalities = data.originalElement;
	});
	restService.all("country").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.countrys = data.originalElement;
	});
	$scope.update = function(){
		var fd = new FormData();
		fd.append('teacher', new Blob([angular.toJson($scope.teacher)], {type: "application/json"}));
		fd.append('photo', $scope.photo);		
		
		restService.one("teacher").withHttpConfig({transformRequest: angular.identity})
		.customPOST(fd, "update/" + $scope.teacherId, undefined,{'Content-Type': undefined}).then(function(data){
			alert("Personal information was successfully updated! ");
			$location.path("/list_teacher");
		});
	};
	
	$scope.deleteTeacher = function(){
		var yesNo = Utils.callDeleteModal("teacher", $routeParams.id);
	};
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	$scope.showTeacher = function() {
			console.log($scope.teacher);
	};	  	
	
	
}])
.controller('teacher_profileCtrl', ['$scope', '$modal', '$routeParams','restService', '$log', '$translatePartialLoader', function ( $scope, $modal, $routeParams, restService,$log, $translatePartialLoader) {		
	
	$translatePartialLoader.addPart('hrmanager');

	$scope.teacher = {};
	
	restService.all("teacher").customGET( $routeParams.id ).then(function(data){
		$scope.teacher = data.originalElement;
	});		
}])