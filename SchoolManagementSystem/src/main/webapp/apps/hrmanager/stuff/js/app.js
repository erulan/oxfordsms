'use strict'; 

angular.module('stuffApp', ['common.apps', 'controllers']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/list_stuff', {
		templateUrl : 'views/stuff_list.html',
		controller : 'StuffListCtrl',
	});
	$routeProvider.when('/stuff', {
		templateUrl : 'views/stuff_list.html',
		controller : 'StuffListCtrl',
	});
	$routeProvider.when('/stuff_profile/:id', {
		templateUrl : 'views/stuff_profile.html',
		controller : 'stuff_profileCtrl',
	});
	$routeProvider.when('/add_stuff', {
		templateUrl : 'views/add_stuff.html',
		controller : 'AddStuffCtrl',
	});
	$routeProvider.when('/edit_stuff/:id', {
		templateUrl : 'views/edit_stuff.html',
		controller : 'EditStuffCtrl',
	});
	/*$routeProvider.when('/stuff', {
		templateUrl : 'views/stuff.html',
		controller : 'stuffCtrl',
	});*/
	$routeProvider.otherwise({
		redirectTo : '/list_stuff'
	});
  			
}]);	




