'use strict'; 

angular.module('CategoryApp', ['controllers', 'common.apps']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/list', {
		templateUrl : 'views/categoryList.html',
		controller : 'listCtrl',
	});/*
	$routeProvider.when('/addNewSyllabys', {
		templateUrl : 'views/addNewCategory.html',
		controller : 'addNewCategoryCtrl',
	});*/
	$routeProvider.when('/addNewCategory', {
		templateUrl : 'views/addNewCategory.html',
		controller : 'addNewCategoryCtrl',
	});
	$routeProvider.when('/editSyllabysCategory/:id', {
		templateUrl : 'views/editCategory.html',
		controller : 'editSyllabysCategoryCtrl',
	});
	$routeProvider.when('/infoSyllabysCategory/:id', {
		templateUrl : 'views/infoSyllabysCategory.html',
		controller : 'infoSyllabysCategoryCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/list'
	});
  			
}]);	


