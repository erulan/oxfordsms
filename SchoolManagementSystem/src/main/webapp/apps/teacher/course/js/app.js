'use strict'; 

angular.module('courseApp', ['controllers', 'common.apps']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/list', {
		templateUrl : 'views/list.html',
		controller : 'listCtrl',
	});
	$routeProvider.when('/start_course/:id', {
		templateUrl : 'views/start_course.html',
		controller : 'start_courseCtrl',
	});
	$routeProvider.when('/exam/:id', {
		templateUrl : 'views/exam.html',
		controller : 'examCtrl',
	});
	/*$routeProvider.when('/journal_course', {
		templateUrl : 'views/journal_course.html',
		controller : 'journal_courseCtrl',
	});*/
	$routeProvider.when('/info_course/:id', {
		templateUrl : 'views/info_course.html',
		controller : 'info_courseCtrl',
	});
	$routeProvider.when('/student_profile/:id', {
		templateUrl : 'views/student_profile.html',
		controller : 'studentsCtrl',
	});
	$routeProvider.when('/journal_course/:id', {
		templateUrl : 'views/journal_course.html',
		controller : 'journal_courseCtrl',
	});
	
	$routeProvider.otherwise({
		redirectTo : '/list'
	});
  			
}]);	


