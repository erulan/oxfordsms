'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('listCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){						
	
	$translatePartialLoader.addPart('teacher');

	$scope.getTitle = function(title){
		return $("."+title).text();
	}
	$scope.deneme = "denemee";	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            lesson: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "course");
        },
    });
}])

.controller('start_courseCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$routeParams','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $routeParams,$translatePartialLoader){	 						
	
	$translatePartialLoader.addPart('teacher');

	$scope.course = {};
	$scope.datepicker = {};
	$scope.note_system_id = {};
	$scope.courseId = $routeParams.id;
	$scope.noteAttendances = [];
	var datefilter = $filter('date');
	$scope.datepicker.date = datefilter(new Date(), 'dd/MM/yyyy');
	
	restService.all("syllabus").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {courseId: $routeParams.id}}, "list"  ).then(function(data){
		$scope.syllabuses = data.originalElement.resultList;
		for(var i = 0; data.originalElement.resultList.length; i++){
			if(data.originalElement.resultList[i].completed == false){
				$scope.selectedSyllabusId = data.originalElement.resultList[i].id;
				break;
			}
		}
	}); 
	$scope.save = function(){
		if($scope.datepicker.date != null){
			$scope.syllabus.completedDate = new Date($scope.datepicker.date).getTime();
		}
		angular.forEach($scope.courses.classGroup.students, function(value, key){
			$scope.noteAttendances.push({"student": {"id": value.id}, "note": value.note, "attended": value.attended, "noteSystem": {"id" :$scope.note_system_id}});
		});
		var object = {"syllabus": $scope.syllabus, "noteAttendances": $scope.noteAttendances};
		restService.one("note_attendance").customPOST(object, "create_note_att").then(function(data){
			alert("success");
		});
	};
	restService.all("course").customGET('students/' + $scope.courseId).then(function(results){
		$scope.courses = results.originalElement;
	});
	restService.all("note_system").customPOST({startIndex: 0, resultQuantity: 10000}, "list"  ).then(function(data){
		$scope.note_systems = data.originalElement.resultList;
		$scope.selectednotetypeId = data.originalElement.resultList[0].id;
	}); 
	/*restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.courses = data.originalElement.resultList;
	});*/
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            id: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
            	
    			var searchParams = {};
            	var prm = params.parameters();

            	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
            	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
            	
            	searchParams["searchParameters"] = {};
            	angular.forEach(prm.filter, function(value, key){
            		searchParams.searchParameters[key] = value;
            	});
            	
            	searchParams["orderParamDesc"] = {};
            	angular.forEach(prm.sorting, function(value, key){
            		searchParams.orderParamDesc[key] = (value == "desc");
            	});
            	
            	
            	
            	/*restService.all("course").customPOST(searchParams, 'students/' + $scope.courseId).then(function(data){
            		params.total(data.originalElement.totalRecords);
                    // set new data
                    $defer.resolve(data.originalElement.resultList);
        		});*/
        },
    });
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  $scope.datepicker.format = 'dd/MM/yyyy';
	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
.controller('examCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$routeParams', '$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $routeParams ,$translatePartialLoader){	 						
	
	$translatePartialLoader.addPart('teacher');

	$scope.course = {};
	$scope.datepicker = {};
	$scope.courseId = $routeParams.id;
	$scope.noteAttendances = [];
	var datefilter = $filter('date');
	$scope.datepicker.date = datefilter(new Date(), 'dd/MM/yyyy');
	
	
	restService.all("syllabus").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {courseId: $routeParams.id}}, "list"  ).then(function(data){
		$scope.syllabuses = data.originalElement.resultList;
	}); 
	restService.all("note_type").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {courseId: $routeParams.id}}, "list"  ).then(function(data){
		$scope.noteTypes = data.originalElement.resultList;
		angular.forEach($scope.noteTypes, function(value, key){
			if(value.name == 'exam'){
				$scope.noteTypeId = value.id;
			}
		});
	}); 
	$scope.save = function(){
		if($scope.datepicker.date != null){
			$scope.syllabus.completedDate = new Date($scope.datepicker.date).getTime();
		}
		angular.forEach($scope.courses.classGroup.students, function(value, key){
			$scope.noteAttendances.push({"student": {"id": value.id}, "noteType": {"id": $scope.noteTypeId}, "note": value.note, "attended": value.attended, "noteSystem": {"id" :$scope.note_system_id}});
		});
		var object = {"syllabus": $scope.syllabus, "noteAttendances": $scope.noteAttendances};
		restService.one("note_attendance").customPOST(object, "create_note_att").then(function(data){
			alert("success");
		});
	};
	restService.all("course").customGET('students/' + $scope.courseId).then(function(results){
		$scope.courses = results.originalElement;
	});
	restService.all("note_system").customPOST({startIndex: 0, resultQuantity: 10000}, "list"  ).then(function(data){
		$scope.note_systems = data.originalElement.resultList;
	});
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            id: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
            	
    			var searchParams = {};
            	var prm = params.parameters();

            	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
            	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
            	
            	searchParams["searchParameters"] = {};
            	angular.forEach(prm.filter, function(value, key){
            		searchParams.searchParameters[key] = value;
            	});
            	
            	searchParams["orderParamDesc"] = {};
            	angular.forEach(prm.sorting, function(value, key){
            		searchParams.orderParamDesc[key] = (value == "desc");
            	});
        },
    });
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  $scope.datepicker.format = 'dd/MM/yyyy';
	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
.controller('studentsCtrl', ['$scope', '$modal', '$log', 'restService', '$filter', 'Utils', '$routeParams', '$translatePartialLoader',function($scope, $modal, $log, restService, $filter, Utils, $routeParams ,$translatePartialLoader){	 

	$translatePartialLoader.addPart('teacher');

	$scope.student = {};
	$scope.datepicker = {};
	$scope.submitted = false;
	$scope.noteAttendance = {};
	
	restService.all("student").customGET( $routeParams.id ).then(function(data){
		$scope.student = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    $scope.datepicker.opened = true;
	  };
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  $scope.save = function(){
			if($scope.datepicker.date != null){
				$scope.syllabus.date = new Date($scope.datepicker.date).getTime();
			}
			$scope.submitted = true;
			restService.one("noteAttendance").customPOST($scope.noteAttendance, "create").then(function(data){
				alert("success");
			});
		};
	
}])	

		
.controller('journal_courseCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$routeParams', '$timeout','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $routeParams, $timeout,$translatePartialLoader){
	
	$translatePartialLoader.addPart('teacher');
	
	$scope.syllabus = {};
	$scope.course = {};
	$scope.syllabusTable = [];
	$scope.courseIdx = $routeParams.id;
	
	restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.courses = data.originalElement.resultList;
		$scope.courseId = $scope.courses[0].id;
		$scope.querySyllabus();
	});
	restService.all("quarter").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.quarters = data.originalElement;
		$scope.quarterId = $scope.quarters[0].id;
		$scope.querySyllabus();
	});
	
	$scope.querySyllabus = function(){
		restService.all("syllabus").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"courseId": $scope.courseId, "quarterId": $scope.quarterId}}, "notes"  ).then(function(data){
			$scope.syllabuses = data.originalElement.resultList;
			$scope.syllabusTable = [];
			$scope.setArray($scope.syllabuses, $scope.courseId);
		});
	};
	restService.all("course").customGET( $scope.courseIdx ).then(function(data){
		$scope.course = data.originalElement;
	});
	$scope.setArray = function(syllabuses, courseId){
		var classGroupId = null;
		angular.forEach($scope.courses, function(course, c_key){
			if(course.id == courseId){
				classGroupId = course.classGroup.id;
			}
		});
		restService.all("student").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"cGroupId": classGroupId}}, "list"  ).then(function(data){
			var students = data.originalElement.resultList;
			angular.forEach(students, function(student, st_key){
				var newObject = [];
				newObject[0] = student;
				$scope.syllabusTable.push(newObject);
			});
			angular.forEach(syllabuses, function(syllabus, syl_key){
				angular.forEach($scope.syllabusTable, function(value, st_key){
					var note = null;
					angular.forEach(syllabus.noteAttendances, function(noteAttendance, na_key){
						if(value[0].id == noteAttendance.student.id){
							note = noteAttendance.note;
						}
					});
					value[syl_key+1] = {"note": note};
				});
			});
		});
		
	};	 
}])

.controller('info_courseCtrl', ['$scope',  '$routeParams', '$translatePartialLoader',function ( $scope, $routeParams, $translatePartialLoader) {	 		
	
	$translatePartialLoader.addPart('teacher');

	$scope.deneme = "denemee44";		
}]);
