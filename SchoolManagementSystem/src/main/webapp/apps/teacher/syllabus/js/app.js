'use strict'; 

angular.module('syllabusApp', ['controllers', 'common.apps']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/list', {
		templateUrl : 'views/sylist.html',
		controller : 'listCtrl',
	});
	$routeProvider.when('/addNewSyllabys', {
		templateUrl : 'views/addNewSyllabys.html',
		controller : 'addNewSyllabysCtrl',
	});
	$routeProvider.when('/editSyllabys/:id', {
		templateUrl : 'views/editSyllabys.html',
		controller : 'editSyllabysCtrl',
	});
	$routeProvider.when('/infoSyllabys/:id', {
		templateUrl : 'views/infoSyllabys.html',
		controller : 'infoSyllabysCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/list'
	});
  			
}]);