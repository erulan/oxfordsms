'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('listCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){		
	
	$translatePartialLoader.addPart('teacher');
	
	$scope.getTitle = function(title){
		return $('.'+title).text();
	}
	$scope.deneme = "denemee";		
	$scope.datepicker = {};
	$scope.syllabus_category = {};
	$scope.course = {};
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
        	id: 'desc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "syllabus");
        },
    });
	$scope.deleteSyllabusCategory = function(id){
		Utils.callDeleteModal("syllabus", id);
};/*
restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
	$scope.courses = data.originalElement.resultList;
});*/
restService.all("syllabus").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
	$scope.syllabuses = data.originalElement.resultList;
});
restService.all("syllabus_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
	$scope.syllabuses = data.originalElement.resultList;
});
restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
	$scope.years = data.originalElement;
});
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
.controller('addNewSyllabysCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$location', '$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $location,$translatePartialLoader){							
	
	$translatePartialLoader.addPart('teacher');

	$scope.datepicker = {};
	$scope.syllabus = {};
	$scope.submitted = false;
	restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.courses = data.originalElement.resultList;
	});
	restService.all("syllabus_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.syllabus_categories = data.originalElement.resultList;
	});
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  $scope.save = function(){
		  if($scope.datepicker.date != null){
				$scope.syllabus.plannedDate = new Date($scope.datepicker.date).getTime();
			}
			$scope.submitted = true;
			restService.one("syllabus").customPOST($scope.syllabus, "create").then(function(data){
				alert("success");
				$location.path("/list");
			});
		};
}])
.controller('editSyllabysCtrl', ['$scope', '$modal', '$routeParams','restService', '$location', '$translatePartialLoader', function ( $scope, $modal, $routeParams, restService, $location,$translatePartialLoader) {						
	
	$translatePartialLoader.addPart('teacher');

	$scope.syllabusId = $routeParams.id;	
	$scope.datepicker = {};
	$scope.syllabus = {};
	$scope.submitted = false;
	restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.courses = data.originalElement.resultList;
	});
	restService.all("syllabus").customGET($scope.syllabusId).then(function(data){
		$scope.syllabus = data.originalElement;
		$scope.datepicker.plannedDate = data.originalElement.plannedDate;
	});
	restService.all("syllabus_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.syllabus_categories = data.originalElement.resultList;
	});
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';/*
	  $scope.datepicker2.format2 = 'dd/MM/yyyy';*/
	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  
	  $scope.update = function() {
		  if($scope.datepicker.plannedDate != null){
				$scope.syllabus.plannedDate = new Date($scope.datepicker.plannedDate).getTime();
		  /*if($scope.datepicker.date != null){
			  $scope.syllabus.date = Date.parse($scope.datepicker.date);*/
		  }
			$scope.submitted = true;
			restService.all("syllabus").customPOST($scope.syllabus, "update/" + $scope.syllabusId).then(function(data){
				alert("success");
				$location.path("/list");
			});
		};
}])
.controller('infoSyllabysCtrl', ['$scope', '$modal', '$routeParams','restService', '$location','$translatePartialLoader',  function ( $scope, $modal, $routeParams, restService, $location,$translatePartialLoader) {								
	
	$translatePartialLoader.addPart('teacher');

	$scope.syllabusId = $routeParams.id;	
	$scope.datepicker = {};
	$scope.syllabus = {};
	$scope.submitted = false;
	
	restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.courses = data.originalElement.resultList;
	});
	restService.all("syllabus").customGET($scope.syllabusId).then(function(data){
		$scope.syllabus = data.originalElement;
		$scope.datepicker.date = data.originalElement.date;
	});
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  
	  $scope.update = function() {
		  if($scope.datepicker.date != null){
			  $scope.syllabus.date = Date.parse($scope.datepicker.date);
		  }
			$scope.submitted = true;
			restService.all("syllabus").customPOST($scope.syllabus, "update/").then(function(data){
				alert("success");
				$location.path("/list");
			});
		};		
}])
	
	.controller('TestTab', function($scope){
		var tabClasses;
		
		 
		  function initTabs() {
		    tabClasses = ["","",""];
		  }
		  
		  $scope.getTabClass = function (tabNum) {
		    return tabClasses[tabNum];
		  };
		  
		  $scope.getTabPaneClass = function (tabNum) {
		    return "tab-pane " + tabClasses[tabNum];
		  }
		  
		  $scope.setActiveTab = function (tabNum) {
		    initTabs();
		    tabClasses[tabNum] = "active";
		  };
		  /*
		  $scope.tab1 = "This is first section";
		  $scope.tab2 = "This is SECOND section";
		  $scope.tab3 = "This is THIRD section";*/
		  
		  //Initialize 
		  initTabs();
		  $scope.setActiveTab(1);
		});

