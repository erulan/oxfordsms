'use strict'; 

angular.module('annualApp', ['controllers', 'common.apps']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/examlist', {
		templateUrl : 'views/list.html',
		controller : 'listCtrl',
	});
	$routeProvider.when('/entryexam/:id', {
		templateUrl : 'views/entryexam.html',
		controller : 'entryexamCtrl',
	});
	$routeProvider.when('/govexam/:id', {
		templateUrl : 'views/govexam.html',
		controller : 'govexamCtrl',
	});
	/*$routeProvider.when('/journal_course', {
		templateUrl : 'views/journal_course.html',
		controller : 'journal_courseCtrl',
	});*/
	$routeProvider.when('/info_course/:id', {
		templateUrl : 'views/info_course.html',
		controller : 'info_courseCtrl',
	});
	$routeProvider.when('/student_profile/:id', {
		templateUrl : 'views/student_profile.html',
		controller : 'studentsCtrl',
	});
	$routeProvider.when('/last_yearexam/:id', {
		templateUrl : 'views/lastyearexam.html',
		controller : 'lastyearCtrl',
	});
	$routeProvider.when('/quarterexam/:id', {
		templateUrl : 'views/quarterexam.html',
		controller : 'quarterexamCtrl',
	});
	
	$routeProvider.otherwise({
		redirectTo : '/list'
	});
  			
}]);	


