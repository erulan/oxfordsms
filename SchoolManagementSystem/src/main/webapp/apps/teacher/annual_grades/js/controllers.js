'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('listCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){						
	
	$translatePartialLoader.addPart('teacher');

	$scope.getTitle = function(title){
		return $("."+title).text();
	}
	$scope.deneme = "denemee";	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            lesson: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "course");
        },
    });
}])

.controller('entryexamCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$routeParams','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $routeParams,$translatePartialLoader){	 						
	
	$translatePartialLoader.addPart('teacher');

	$scope.course = {};
	$scope.datepicker = {};
	$scope.courseId = $routeParams.id;
	$scope.noteAttendances = [];
	var datefilter = $filter('date');
	$scope.datepicker.date = datefilter(new Date(), 'dd/MM/yyyy');
	
	restService.all("note_type").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"name": "entry exam"}},  "list"  ).then(function(data){
		$scope.note_type_id = data.originalElement.resultList[0].id;
	}); 
	restService.all("note_system").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"name": "5-system"}},  "list"  ).then(function(data){
		$scope.note_system_id = data.originalElement.resultList[0].id;
	}); 
	
	$scope.save = function(){
		if($scope.datepicker.date != null){
			$scope.completedDate = new Date($scope.datepicker.date).getTime();
		}
		angular.forEach($scope.courses.classGroup.students, function(value, key){
			$scope.noteAttendances.push({"student": {"id": value.id}, "note": value.note, "noteSystem": {"id" :$scope.note_system_id}, "noteType": {"id" :$scope.note_type_id}});
		});
		var object = {"syllabus": {"completedDate": $scope.completedDate, "syllabusCategory":{"course":{"id": $scope.courseId}}}, "noteAttendances": $scope.noteAttendances};
		restService.one("note_attendance").customPOST(object, "create_exam_note").then(function(data){
			alert("success");
		});
	};
	restService.all("course").customGET('students/' + $scope.courseId).then(function(results){
		$scope.courses = results.originalElement;
	});
	restService.all("note_system").customPOST({startIndex: 0, resultQuantity: 10000}, "list"  ).then(function(data){
		$scope.note_systems = data.originalElement.resultList;
	}); 
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            id: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
            	
    			var searchParams = {};
            	var prm = params.parameters();

            	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
            	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
            	
            	searchParams["searchParameters"] = {};
            	angular.forEach(prm.filter, function(value, key){
            		searchParams.searchParameters[key] = value;
            	});
            	
            	searchParams["orderParamDesc"] = {};
            	angular.forEach(prm.sorting, function(value, key){
            		searchParams.orderParamDesc[key] = (value == "desc");
            	});
        },
    });
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  $scope.datepicker.format = 'dd/MM/yyyy';
	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
.controller('lastyearCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$routeParams','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $routeParams,$translatePartialLoader){ 						


	$translatePartialLoader.addPart('teacher');

	$scope.course = {};
	$scope.datepicker = {};
	$scope.courseId = $routeParams.id;
	$scope.noteAttendances = [];
	var datefilter = $filter('date');
	$scope.datepicker.date = datefilter(new Date(), 'dd/MM/yyyy');
	
	restService.all("note_type").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"name": "last year"}},  "list"  ).then(function(data){
		$scope.note_type_id = data.originalElement.resultList[0].id;
	}); 
	restService.all("note_system").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"name": "5-system"}},  "list"  ).then(function(data){
		$scope.note_system_id = data.originalElement.resultList[0].id;
	}); 
	
	$scope.save = function(){
		if($scope.datepicker.date != null){
			$scope.completedDate = new Date($scope.datepicker.date).getTime();
		}
		angular.forEach($scope.courses.classGroup.students, function(value, key){
			$scope.noteAttendances.push({"student": {"id": value.id}, "note": value.note, "noteSystem": {"id" :$scope.note_system_id}, "noteType": {"id" :$scope.note_type_id}});
		});
		var object = {"syllabus": {"completedDate": $scope.completedDate, "syllabusCategory":{"course":{"id": $scope.courseId}}}, "noteAttendances": $scope.noteAttendances};
		restService.one("note_attendance").customPOST(object, "create_exam_note").then(function(data){
			alert("success");
		});
	};
	restService.all("course").customGET('students/' + $scope.courseId).then(function(results){
		$scope.courses = results.originalElement;
	});
	restService.all("note_system").customPOST({startIndex: 0, resultQuantity: 10000}, "list"  ).then(function(data){
		$scope.note_systems = data.originalElement.resultList;
	}); 
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            id: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
            	
    			var searchParams = {};
            	var prm = params.parameters();

            	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
            	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
            	
            	searchParams["searchParameters"] = {};
            	angular.forEach(prm.filter, function(value, key){
            		searchParams.searchParameters[key] = value;
            	});
            	
            	searchParams["orderParamDesc"] = {};
            	angular.forEach(prm.sorting, function(value, key){
            		searchParams.orderParamDesc[key] = (value == "desc");
            	});
        },
    });
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  $scope.datepicker.format = 'dd/MM/yyyy';
	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
.controller('govexamCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$routeParams', '$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $routeParams ,$translatePartialLoader){	 						

	$translatePartialLoader.addPart('teacher');

	$scope.course = {};
	$scope.datepicker = {};
	$scope.courseId = $routeParams.id;
	$scope.noteAttendances = [];
	var datefilter = $filter('date');
	$scope.datepicker.date = datefilter(new Date(), 'dd/MM/yyyy');
	
	restService.all("note_type").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"name": "GOS exam"}},  "list"  ).then(function(data){
		$scope.note_type_id = data.originalElement.resultList[0].id;
	}); 
	restService.all("note_system").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"name": "5-system"}},  "list"  ).then(function(data){
		$scope.note_system_id = data.originalElement.resultList[0].id;
	}); 
	
	$scope.save = function(){
		if($scope.datepicker.date != null){
			$scope.completedDate = new Date($scope.datepicker.date).getTime();
		}
		angular.forEach($scope.courses.classGroup.students, function(value, key){
			$scope.noteAttendances.push({"student": {"id": value.id}, "note": value.note, "noteSystem": {"id" :$scope.note_system_id}, "noteType": {"id" :$scope.note_type_id}});
		});
		var object = {"syllabus": {"completedDate": $scope.completedDate, "syllabusCategory":{"course":{"id": $scope.courseId}}}, "noteAttendances": $scope.noteAttendances};
		restService.one("note_attendance").customPOST(object, "create_exam_note").then(function(data){
			alert("success");
		});
	};
	restService.all("course").customGET('students/' + $scope.courseId).then(function(results){
		$scope.courses = results.originalElement;
	});
	restService.all("note_system").customPOST({startIndex: 0, resultQuantity: 10000}, "list"  ).then(function(data){
		$scope.note_systems = data.originalElement.resultList;
	}); 
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            id: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
            	
    			var searchParams = {};
            	var prm = params.parameters();

            	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
            	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
            	
            	searchParams["searchParameters"] = {};
            	angular.forEach(prm.filter, function(value, key){
            		searchParams.searchParameters[key] = value;
            	});
            	
            	searchParams["orderParamDesc"] = {};
            	angular.forEach(prm.sorting, function(value, key){
            		searchParams.orderParamDesc[key] = (value == "desc");
            	});
        },
    });
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  $scope.datepicker.format = 'dd/MM/yyyy';
	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
.controller('studentsCtrl', ['$scope', '$modal', '$log', 'restService', '$filter', 'Utils', '$routeParams', '$translatePartialLoader',function($scope, $modal, $log, restService, $filter, Utils, $routeParams ,$translatePartialLoader){	 

	$translatePartialLoader.addPart('teacher');

	$scope.student = {};
	$scope.datepicker = {};
	$scope.submitted = false;
	$scope.noteAttendance = {};
	
	restService.all("student").customGET( $routeParams.id ).then(function(data){
		$scope.student = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    $scope.datepicker.opened = true;
	  };
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  $scope.save = function(){
			if($scope.datepicker.date != null){
				$scope.syllabus.date = new Date($scope.datepicker.date).getTime();
			}
			$scope.submitted = true;
			restService.one("noteAttendance").customPOST($scope.noteAttendance, "create").then(function(data){
				alert("success");
			});
		};
	
}])	
.controller('quarterexamCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$routeParams','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $routeParams,$translatePartialLoader){	 						
	
	$translatePartialLoader.addPart('teacher');

	$scope.course = {};
	$scope.datepicker = {};
	$scope.courseId = $routeParams.id;
	$scope.quarterId = {};
	$scope.noteAttendances = [];
	var datefilter = $filter('date');
	$scope.datepicker.date = datefilter(new Date(), 'dd/MM/yyyy');
	
	restService.all("quarter").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.quarters = data.originalElement;
	});
	$scope.save = function(){
		if($scope.datepicker.date != null){
			$scope.completedDate = new Date($scope.datepicker.date).getTime();
		}
		angular.forEach($scope.courses.classGroup.students, function(value, key){
			$scope.noteAttendances.push({"student": {"id": value.id}, "note": value.note});
		});
		var object = {"syllabus": {"completedDate": $scope.completedDate, "syllabusCategory":{"course":{"id": $scope.courseId}}}, "noteAttendances": $scope.noteAttendances};
		restService.one("note_attendance").customPOST(object, "create_quarter_exam/"+ $scope.quarterId).then(function(data){
			alert("success");
		});
	};
	restService.all("course").customGET('students/' + $scope.courseId).then(function(results){
		$scope.courses = results.originalElement;
	});
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            id: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
            	
    			var searchParams = {};
            	var prm = params.parameters();

            	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
            	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
            	
            	searchParams["searchParameters"] = {};
            	angular.forEach(prm.filter, function(value, key){
            		searchParams.searchParameters[key] = value;
            	});
            	
            	searchParams["orderParamDesc"] = {};
            	angular.forEach(prm.sorting, function(value, key){
            		searchParams.orderParamDesc[key] = (value == "desc");
            	});
        },
    });
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  $scope.datepicker.format = 'dd/MM/yyyy';
	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
.controller('info_courseCtrl', ['$scope',  '$routeParams', '$translatePartialLoader',function ( $scope, $routeParams, $translatePartialLoader) {	 		
	
	$translatePartialLoader.addPart('teacher');

	$scope.deneme = "denemee44";		
}]);
