'use strict'; 

angular.module('settingsApp', ['controllers']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/list', {
		templateUrl : 'views/list.html',
		controller : 'listCtrl',
	});
	$routeProvider.when('/add', {
		templateUrl : 'views/add.html',
		controller : 'addCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/list'
	});
  			
}]);	


