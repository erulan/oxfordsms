'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('editCtrl',['$scope', '$routeParams', 'restService', '$compile','$rootScope','$translatePartialLoader','$location',
                                 function($scope, $routeParams, restService, $compile,$rootScope,$translatePartialLoader, $location) {	
	  						
	$translatePartialLoader.addPart('teacher');

	$scope.timetable = {};
	
	$scope.teacherID = $scope.userInfo.id;
	$scope.username = $rootScope.userInfo.username;

	
	restService.all("teacher").customGET( $scope.teacherID ).then(function(data){
		$scope.teacher = data.originalElement;
	});
	$scope.save = function(){
		if($scope.newPassword != $scope.confirmPassword){
			alert("Passwords doesn't match");
			return;
		}
		restService.one("security").customGET("change_password/" + $scope.username +"/"+ $scope.currentPassword +"/"+ $scope.newPassword)
		.then(function(data){
			alert("success");
			$location.path("#/edit");
		});
	};
	$scope.update = function(id){
		$scope.submitted = true;
		restService.one("teacher").customPOST($scope.teacher, "update/"+id)
		.then(function(data){
			alert("success");
			$location.path("#/edit");
		});
	};
	
	
}])

.controller('addCtrl', ['$scope', '$routeParams','$translatePartialLoader', function ( $scope, $routeParams,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('teacher');

	$scope.deneme = "denemee22";		
	  			
}]);

/*'use strict';

 Controllers 
	
angular.module('controllers', [])		
.controller('editCtrl',['$scope', '$routeParams', 'restService', '$compile','$rootScope','$translatePartialLoader','$location',
                                 function($scope, $routeParams, restService, $compile,$rootScope,$translatePartialLoader, $location) {	
	  						
	$translatePartialLoader.addPart('teacher');
	
	$scope.timetable = {};
	
	$scope.userInfo = $rootScope.userInfo;
	$scope.teacherId = $scope.userInfo.id;
	
	restService.all("teacher").customGET( $scope.teacherId ).then(function(data){
		$scope.teachers = data.originalElement.resultList;
	});
	
	$scope.save = function(){
		if($scope.newPassword != $scope.confirmPassword){
			alert("Passwords doesn't match");
			return;
		}
		restService.one("security").customGET("change_password/" + $scope.userInfo.username +"/"+ $scope.currentPassword +"/"+ $scope.newPassword)
		.then(function(data){
			alert("success");
			$location.path("#/edit");
		});
	};
	
	$scope.update = function(id){
		$scope.submitted = true;
		restService.one("teacher").customPOST($scope.teacher, "update/"+id).then(function(data){
			alert("success");
			$location.path("#/edit");
		});
	};
	
	
}])

.controller('addCtrl', ['$scope', '$routeParams','$translatePartialLoader', function ( $scope, $routeParams,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('student');

	$scope.deneme = "denemee22";		
	  			
}]);
*/