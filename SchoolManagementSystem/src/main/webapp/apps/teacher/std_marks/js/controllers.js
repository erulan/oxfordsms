'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('listCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$routeParams', '$timeout','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $routeParams, $timeout,$translatePartialLoader){

	$translatePartialLoader.addPart('teacher');

	$scope.syllabus = {};
	$scope.course = {};
	$scope.syllabusTable = [];
	
	restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.courses = data.originalElement.resultList;
		$scope.courseId = $scope.courses[0].id;
		$scope.querySyllabus();
	});
	restService.all("quarter").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.quarters = data.originalElement;
		$scope.quarterId = $scope.quarters[0].id;
		$scope.querySyllabus();
	});
	
	$scope.querySyllabus = function(){
		restService.all("syllabus").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"courseId": $scope.courseId, "quarterId": $scope.quarterId}}, "notes"  ).then(function(data){
			$scope.syllabuses = data.originalElement.resultList;
			$scope.syllabusTable = [];
			$scope.setArray($scope.syllabuses, $scope.courseId);
		});
	};
	
//	restService.all("syllabus").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
//			$scope.syllabuses = data.originalElement;
//			$scope.quarterId = $scope.quarters[0].id;
//			$scope.querySyllabus();
//		});
	
	
	$scope.setArray = function(syllabuses, courseId){
		
		var classGroupId = null;
		angular.forEach($scope.courses, function(course, c_key){
			if(course.id == courseId){
				classGroupId = course.classGroup.id;
			}
		});
		restService.all("student").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"cGroupId": classGroupId}}, "list"  ).then(function(data){
			var students = data.originalElement.resultList;
			angular.forEach(students, function(student, st_key){
				var newObject = [];
				newObject[0] = student;
				$scope.syllabusTable.push(newObject);
			});
			angular.forEach(syllabuses, function(syllabus, syl_key){
				angular.forEach($scope.syllabusTable, function(value, st_key){
					var note = null;
					angular.forEach(syllabus.noteAttendances, function(noteAttendance, na_key){
						if(value[0].id == noteAttendance.student.id){
							note = noteAttendance.note;
						}
					});
					value[syl_key+1] = {"note": note};
				});
			});
		});
		
	};	 
}])

.controller('editCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$routeParams','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $routeParams,$translatePartialLoader){	 						

	$translatePartialLoader.addPart('teacher');

	$scope.datepicker = {};
	$scope.syllabusId = $routeParams.id;
	$scope.objectsList = [];
	$scope.noteAttendances = [];
    	
	$scope.save = function(){
		angular.forEach($scope.objectsList, function(value, key){
//			$scope.noteAttendances.push("dddd");
			$scope.noteAttendances.push({"student": {"id": value.student.id}, "note": value.noteAttendance != null ? value.noteAttendance.note : null, "attended": value.noteAttendance != null ? value.noteAttendance.attended : null});
		});
		var object = {"syllabus": {"id": $scope.syllabusId}, "noteAttendances": $scope.noteAttendances};
		restService.one("note_attendance").customPOST(object, "update_note_att").then(function(data){
			alert("success");
		});
	};
 
	restService.all("syllabus").customGET( $scope.syllabusId ).then(function(data){
		$scope.syllabus = data.originalElement;
	});
	restService.all("student").customPOST( {startIndex: 0, resultQuantity: 10000, searchParameters: {"cGroupId": 12}}, "list" ).then(function(data){
		$scope.students = data.originalElement.resultList;
		restService.all("note_attendance").customPOST( {startIndex: 0, resultQuantity: 10000, searchParameters: {"syllabusId": $scope.syllabusId}}, "list" ).then(function(data){
			$scope.noteAttendacnes = data.originalElement.resultList;
			angular.forEach($scope.students, function(student, st_key){
				var object = {};
				object.student = student;
				angular.forEach($scope.noteAttendacnes, function(noteAttendance, na_key){
					if(student.id == noteAttendance.student.id){
						object.noteAttendance = noteAttendance;
					}
				});
				$scope.objectsList.push(object);
			});
		});
	});
	/*restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.courses = data.originalElement.resultList;
	});*/
	
    }])
.controller('addCtrl', ['$scope',  '$routeParams','$translatePartialLoader', function ( $scope, $routeParams,$translatePartialLoader) {	
	  				
	$translatePartialLoader.addPart('teacher');

	$scope.deneme = "denemee22";		
	  			
}]);
