'use strict'; 

angular.module('studentMarkApp', ['controllers', 'common.apps']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/list', {
		templateUrl : 'views/student_marks2.html',
		controller : 'listCtrl',
	});
	$routeProvider.when('/add', {
		templateUrl : 'views/add.html',
		controller : 'addCtrl',
	});
	$routeProvider.when('/edit_marks/:id', {
		templateUrl : 'views/edit_marks.html',
		controller : 'editCtrl',
	});
	/*$routeProvider.when('/student_marks', {
		templateUrl : 'views/student_marks2.html',
		controller : 'studentsCtrl',
	});*/
	$routeProvider.otherwise({
		redirectTo : '/list'
	});
  			
}]);	


