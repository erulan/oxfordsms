'use strict'; 

angular.module('exampleApp', [ 'common.apps', 'controllers']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/ajax_rest', {
		templateUrl : 'views/ajax_rest.html',
		controller : 'ExampleListCtrl',
	});
	$routeProvider.when('/modal', {
		templateUrl : 'views/modal.html',
		controller : 'ModalCtrl',
		
	});
	$routeProvider.when('/ex_table', {
		templateUrl : 'views/ex_table.html',
		controller : 'ExampleTableCtrl',
	});
	$routeProvider.when('/datepicker', {
		templateUrl : 'views/datepicker.html',
		controller : 'DatepickerCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/ajax_rest'
	});
  			
}]);	


