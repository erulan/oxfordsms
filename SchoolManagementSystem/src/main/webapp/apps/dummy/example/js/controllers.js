'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('ExampleListCtrl', ['$scope', 'restService',  '$location', '$translate', '$translatePartialLoader', function ($scope, restService, $location, $translate, $translatePartialLoader) {	
	  				
	
	$translatePartialLoader.addPart('dummy');
	

	$scope.deneme = "denemee list CTRL";
	$scope.blood = "";
	
	$scope.getBlood = function(){
		restService.all("blood").customGET("1" ).then(function(data){
			$scope.blood = data.originalElement;
		});
	};
	
}])

.controller('DatepickerCtrl', ['$scope',  function ($scope) {	
	$scope.datepicker = {};
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };	  			
}])

.controller('ExampleTableCtrl', ['$scope', 'restService', 'ngTableParams', '$filter', 'Utils', function($scope, restService, ngTableParams, $filter, Utils){
	
	$scope.listBook = function(){
		var searchParams = {"resultQuantity":200,"startIndex":0, "searchParameters": {"name":"a","author":""}, "orderParamDesc": {"availableAmount":true}};
		
		restService.all("book").customPOST(searchParams, ['list']).then(function(data){
			$scope.listhere = data.originalElement;
		});
	};
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
        }, {
        total: 0, // length of data
        getData: function($scope, params){
        	Utils.ngTableGetData($scope, params, "book");
        },
    });
}])

.controller('ModalCtrl', ['$scope', '$modal', '$log', function ($scope, $modal, $log) {	
	  						
	$scope.deneme = "denemee add CTRL";		
	$scope.items = ['item1', 'item2', 'item3'];
	
	$scope.openModal = function(size){
		var modalInstance = $modal.open({
		      templateUrl: 'views/myModalContent.html',
		      controller: 'ModalInstanceCtrl',
		      size: size,
		      resolve: {
		        items: function () {
		          return $scope.items;
		        }
		      }
		    });

	    modalInstance.result.then(function (selectedItem) {
	      $scope.selected = selectedItem;
	    }, function () {
	      $log.info('Modal dismissed at: ' + new Date());
	    });
	}
	  			
}])

.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', 'items', function ($scope, $modalInstance, items) {

	  $scope.items = items;
	  $scope.selected = {
	    item: $scope.items[0]
	  };

	  $scope.ok = function () {
	    $modalInstance.close($scope.selected.item);
	  };

	  $scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	  };
	}]);


