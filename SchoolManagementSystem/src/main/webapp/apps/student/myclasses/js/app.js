'use strict'; 

angular.module('appClass', ['common.apps', 'controllers']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/student_class', {
		templateUrl : 'views/student_class.html',
		controller : 'student_classCtrl',
	});
	
	$routeProvider.otherwise({
		redirectTo : '/student_class'
	});
  			
}]);	


