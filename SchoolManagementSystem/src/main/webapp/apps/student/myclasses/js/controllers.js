'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('student_classCtrl', ['$scope', 'restService', 'ngTableParams','$location', '$routeParams','$rootScope','$translatePartialLoader',
                                  function($scope, restService, ngTableParams, $location, $routeParams,$rootScope,$translatePartialLoader) {	
	
	$translatePartialLoader.addPart('student');

	$scope.student = {};
	$scope.stNum = $rootScope.userInfo.id;
	$scope.deneme = "denemee";
	
	restService.all("student").customGET( $scope.stNum ).then(function(data){
		$scope.student = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("teacher").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.teacher = data.originalElement;
	});
	  			
}]);
