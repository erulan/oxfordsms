'use strict';

/* Controllers */
	
angular.module('controllers', [ ])		
.controller('timetableCtrl', ['$scope', '$routeParams', 'restService', '$compile','$rootScope','$translatePartialLoader', function($scope, $routeParams, restService, $compile, $rootScope ,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('student');
	
	$scope.timetable = {};
	$scope.timetableArray = [];
	
	restService.all("week_days").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.weekDays = data.originalElement;
	});
	$scope.timetableArray = [];
	$scope.course = [];
	$scope.classGroup = {};
	
	restService.one("security").customGET("current_user").then(function(data){
		 var userInfo = data.originalElement;
		 restService.all("student").customGET( userInfo.id ).then(function(data){
			$scope.student = data.originalElement;
			$scope.classHourTypeId = $scope.student.classGroup.classHourType.id;
			$scope.classGroup.id = $scope.student.classGroup.id;
			restService.all("class_hours").customPOST({startIndex: 0, resultQuantity: 1000, searchParameters: {"classHourTypeId": $scope.classHourTypeId}},  "list").then(function(data){
				$scope.classHours = data.originalElement;
				$scope.getTimetable();
			});
		});	
	 });
	
	
	$scope.getTimetable = function(){
		console.log("calling");
		if($scope.classGroup.id == null){return false;}		
		if($scope.classHourTypeId == null){return false;}
		$scope.timetableArray = [];
		restService.all("timetable").customPOST( {"startIndex": 0, resultQuantity: 1000, searchParameters: 
		{"cGroupId": $scope.classGroup.id, "classHourTypeId": $scope.classHourTypeId }}, "list").then(function(data){
			$scope.timetables = data.originalElement.resultList;
			
			angular.forEach($scope.classHours.resultList, function(classHour, ch_key){
				var object = [];
				object[0] = classHour;
				angular.forEach($scope.weekDays, function(weekDay, wd_key){
					var timetableObj = {};
					timetableObj.course = {};
					angular.forEach($scope.timetables, function(timetable, tt_key){
						if(timetable.weekDays.id == weekDay.id && timetable.classHours.id == classHour.id){
							timetableObj = timetable;
						}
					});
					timetableObj.weekDays = weekDay;
					timetableObj.classHours = classHour;
					object[wd_key + 1] = timetableObj;
				});
				$scope.timetableArray.push(object);
			});
			restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000, searchParameters: {"cGroupId": $scope.classGroup.id }}, "list").then(function(data){
				$scope.courses = data.originalElement.resultList;
			});
		});
	};  				
}]);
//'restService',restService