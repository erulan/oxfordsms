'use strict'; 

angular.module('appTable', ['common.apps','controllers']).
config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/timetable', {
		templateUrl : 'views/timetable.html',
		controller : 'timetableCtrl',
	});
	
	$routeProvider.otherwise({
		redirectTo : '/timetable'
	});
  			
}]);	


//'common.apps'