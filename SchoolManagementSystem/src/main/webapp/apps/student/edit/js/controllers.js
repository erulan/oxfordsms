'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('editCtrl',['$scope', '$routeParams', 'restService', '$compile','$rootScope','$translatePartialLoader','$location',
                                 function($scope, $routeParams, restService, $compile,$rootScope,$translatePartialLoader, $location) {	
	  						
	$translatePartialLoader.addPart('student');
	
	$scope.getLanguage = function(){
		return $translate.use();
	} 
	
	$scope.timetable = {};
	
	$scope.userInfo = $rootScope.userInfo;
	$scope.studentID = $scope.userInfo.id;
	
	restService.all("student").customGET( $scope.studentID ).then(function(data){
		$scope.student = data.originalElement;
	});
	
	
	$scope.save = function(){
		if($scope.newPassword != $scope.confirmPassword){
			alert("Passwords doesn't match");
			return;
		}
		restService.one("security").customGET("change_password/" + $scope.userInfo.username +"/"+ $scope.currentPassword +"/"+ $scope.newPassword).then(function(data){
			alert("success");
			$location.path("#/edit");
		});
	};
	
	$scope.update = function(id){
		$scope.submitted = true;
		restService.one("student").customPOST($scope.student, "update/"+id)
		.then(function(data){
			alert("success");
			$location.path("#/edit");
		});
	};
	
	
}])

.controller('addCtrl', ['$scope', '$routeParams','$translatePartialLoader', function ( $scope, $routeParams,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('student');

	$scope.deneme = "denemee22";		
	  			
}]);
