'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('stAccountingCtrl', ['$scope', 'restService', 'ngTableParams','$location', '$routeParams', '$rootScope','$translatePartialLoader','$translate',
                                  function($scope, restService, ngTableParams, $location, $routeParams, $rootScope,$translatePartialLoader,$translate) {	
	
	$translatePartialLoader.addPart('student');
	
	$scope.getLanguage = function(){
		return $translate.use();
	}
	$scope.getTitle = function(title){
		console.log(title);
		return $('.'+title).text();
	}
	
	
	$scope.student = {};
	$scope.stud_accounting = {};
	$scope.deneme = "denemee2";	
	

	/*restService.all("stud_accounting").customPOST( $routeParams.id ).then(function(data){
		$scope.studentAccountings = data.originalElement;
	});*/	
	restService.all("stud_accounting").customPOST( 
			{startIndex: 0, resultQuantity: 1000, "searchParameters": {"studentId": $rootScope.userInfo.id }}, "list" )
			.then(function(data){
		$scope.studentAccountings = data.originalElement.resultList;
	});	
	
}])

/*.controller('addCtrl', ['$scope',  '$routeParams','$translatePartialLoader', function ( $scope, $routeParams,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('student');

	$scope.getTitle = function(title){
		console.log(title);
		return $('.'+title).text();
	}
	$scope.deneme = "denemee22";		
	  			
}])*/;
