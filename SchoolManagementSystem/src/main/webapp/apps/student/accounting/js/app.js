'use strict'; 

angular.module('appAccounting', ['common.apps','controllers']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/stAccounting', {
		templateUrl : 'views/stAccounting.html',
		controller : 'stAccountingCtrl',
	});
	$routeProvider.when('/add', {
		templateUrl : 'views/add.html',
		controller : 'addCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/list'
	});
  			
}]);	


