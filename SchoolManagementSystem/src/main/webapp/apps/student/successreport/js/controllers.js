'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('successReportCtrl',['$scope', '$routeParams', 'restService', '$compile','$rootScope','$translatePartialLoader','$translate',
                                 function($scope, $routeParams, restService, $compile, $rootScope ,$translatePartialLoader,$translate) {	
	  						
	$translatePartialLoader.addPart('student');
	
	$scope.getLanguage = function(){
		return $translate.use();
	}

	$scope.timetable = {};
	$scope.stNum = $rootScope.userInfo.id;
	
	restService.all("student").customGET( $scope.stNum ).then(function(data){
		$scope.student = data.originalElement;
	});
		restService.all("note_attendance").customPOST( {"startIndex": 0, resultQuantity: 1000, searchParameters: 
				{"studentId": $scope.stNum}}, "notes").then(function(data){
		$scope.noteAttendances = data.originalElement.resultList;
	});
	
	
/*	$scope.$watch('note_attendance', function(newValue, oldValue) {
		$scope.$apply('successReport');
		});
	
	restService.all("quarter1").customPOST({startIndex: 0, resultQuantity: 1000, searchParameters: {"studentId": $scope.stNum}}, "notes").then(function(data){
		$scope.quarter1 = data.originalElement;
	});
	restService.all("qarter2").customPOST({startIndex: 0, resultQuantity: 1000}, "notes").then(function(data){
		$scope.quarter2 = data.originalElement;
	});
	restService.all("quarter3").customPOST({startIndex: 0, resultQuantity: 1000}, "notes").then(function(data){
		$scope.quarter3 = data.originalElement;
	});
	restService.all("qarter4").customPOST({startIndex: 0, resultQuantity: 1000}, "notes").then(function(data){
		$scope.quarter4 = data.originalElement;
	});
	
	$scope.getQuarter = function(classHourId, weekDayId){
		angular.forEach($scope.timetables, function(value){
			if(value.classHours.id == classHourId && value.weekDays.id == weekDayId){
				return value.course.lesson.name;
			}
		});
	
	}
	*/
}])

.controller('addCtrl', ['$scope',  '$routeParams','$translatePartialLoader', function ( $scope, $routeParams,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('student');

	$scope.deneme = "denemee22";		
	  			
}]);
