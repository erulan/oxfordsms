'use strict'; 

angular.module('appSuccess', ['common.apps','controllers']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/successReport', {
		templateUrl : 'views/successReport.html',
		controller : 'successReportCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/successReport'
	});
  			
}]);	


