'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('courseCtrl', ['$scope', 'restService', 'ngTableParams','$location', '$routeParams','$rootScope','$translatePartialLoader','$translate',
                                  function($scope, restService, ngTableParams, $location, $routeParams, $rootScope,$translatePartialLoader,$translate) {	
	
	$translatePartialLoader.addPart('student');

	$scope.getLanguage = function(){
		return $translate.use();
	}
	
	$scope.getTitle = function(title){
		console.log(title);
		return $('.'+title).text();
	}

	$scope.student = {};
	$scope.stNum = $rootScope.userInfo.id;
	$scope.deneme = "denemee";
	
	restService.all("student").customGET( $scope.stNum ).then(function(data){
		$scope.student = data.originalElement;
		
		restService.all("course").customPOST
		({startIndex: 0, resultQuantity: 1000, "searchParameters": {"cGroupId": $scope.student.classGroup.id}}, "list")
		.then(function(data){
			$scope.courses = data.originalElement.resultList;
		});
	});
	
}])

.controller('modalCtrl', ['$scope',  '$routeParams','$translatePartialLoader', function ( $scope, $routeParams,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('student');

	$scope.getTitle = function(title){
		console.log(title);
		return $('.'+title).text();
	}

	$scope.deneme = "denemee22";		
	  			
}]);
