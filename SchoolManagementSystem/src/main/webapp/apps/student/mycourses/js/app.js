'use strict'; 

angular.module('appMycourses', ['common.apps','controllers']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/course', {
		templateUrl : 'views/course.html',
		controller : 'courseCtrl',
	});
	$routeProvider.when('/modal',{
		templateUrl : 'views/modal.html',
		controller : 'modalCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/course'
	});
  			
}]);	


