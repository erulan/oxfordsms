'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('annual_feeCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){	
			
	$translatePartialLoader.addPart('accountant');

	$scope.getStudents = function($defer, params, path) {
	    	
			var searchParams = {};
	    	var prm = params.parameters();

	    	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
	    	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
	    	
	    	searchParams["searchParameters"] = {};
	    	angular.forEach(prm.filter, function(value, key){
	    		searchParams.searchParameters[key] = value;
	    	});

			searchParams.searchParameters["schoolTypeId"] = 1;
	    	searchParams["orderParamDesc"] = {};
	    	angular.forEach(prm.sorting, function(value, key){
	    		searchParams.orderParamDesc[key] = (value == "desc");
	    	});
	    	
	    	restService.all(path).customPOST(searchParams, ['list']).then(function(data){
	    		params.total(data.originalElement.totalRecords);
	            // set new data
	            $defer.resolve(data.originalElement.resultList);
			});
	    }
		
		$scope.$watch("count", function (newVal, oldVal) {
	        $scope.tableParams.count(newVal);
	    });
		$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
			$scope.count = newVal;
		});
		$scope.getTitle = function (title){
			return $("."+title).text();
		}
		
		$scope.tableParams = new ngTableParams({
	        page: 1,            // show first page
	        count: 10, 			// count per page
	        sorting: {
	            name: 'asc'     // initial sorting
	        },				
	    }, {
	        total: 0, // length of data
	        getData: function($defer, params){
	        	$scope.getStudents($defer, params, "stud_accounting");
	        },
	    });
	
	  			
}])
.controller('kids_feeCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){	
			
	$translatePartialLoader.addPart('accountant');
	
$scope.getStudents = function($defer, params, path) {
    	
		var searchParams = {};
    	var prm = params.parameters();

    	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
    	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
    	
    	searchParams["searchParameters"] = {};
    	angular.forEach(prm.filter, function(value, key){
    		searchParams.searchParameters[key] = value;
    	});

		searchParams.searchParameters["schoolTypeId"] = 2;
    	searchParams["orderParamDesc"] = {};
    	angular.forEach(prm.sorting, function(value, key){
    		searchParams.orderParamDesc[key] = (value == "desc");
    	});
    	
    	restService.all(path).customPOST(searchParams, ['list']).then(function(data){
    		params.total(data.originalElement.totalRecords);
            // set new data
            $defer.resolve(data.originalElement.resultList);
		});
    }
	
	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	$scope.getTitle = function (title){
		return $("."+title).text();
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	$scope.getStudents($defer, params, "stud_accounting");
        },
    });
}])
.controller('edit_annual_feeCtrl', ['$scope', '$modal', '$log',  '$routeParams', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log,  $routeParams, restService, ngTableParams, $filter, Utils,$translatePartialLoader){
	  						
	$translatePartialLoader.addPart('accountant');

	$scope.deneme = "denemee2";	
	$scope.stud_accounting = {};
	$scope.editFee = $routeParams.id

	restService.all("stud_accounting").customGET( $scope.editFee).then(function(data){
		$scope.stud_accounting = data.originalElement;
	});
	
	$scope.submitted = function() {
		restService.one("stud_accounting").customPOST($scope.stud_accounting, "update/" + $scope.editFee).then(function(data){
			alert("success");
		});
	};
		
}]);


