'use strict'; 

angular.module('accountingApp', ['controllers','common.apps']).
config(['$routeProvider', '$locationProvider',  function ($routeProvider, $locationProvider) {
					
/*
	$routeProvider.when('/accounting', {
		templateUrl : 'views/accounting.html',
		controller : 'accountingCtrl',
	});
	
	$routeProvider.when('/new_payment/:id', {
		templateUrl : 'views/new_payment.html',
		controller : 'new_paymentCtrl',
	});*/
	$routeProvider.when('/annual_fee', {
		templateUrl : 'views/annual_fee.html',
		controller : 'annual_feeCtrl',
	});
	$routeProvider.when('/kids_fee', {
		templateUrl : 'views/kids_fee.html',
		controller : 'kids_feeCtrl',
	});
	$routeProvider.when('/edit_annualfee/:id', {
		templateUrl : 'views/edit_annualfee.html',
		controller : 'edit_annual_feeCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/annual_fee'
	});
  			
}]);	


