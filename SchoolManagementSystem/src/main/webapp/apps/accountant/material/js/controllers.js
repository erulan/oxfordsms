'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('materialCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){	

	$scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Inventory_Report.xls");
    };
	
	$scope.getMaterials= function($defer, params, path) {
    	
		var searchParams = {};
    	var prm = params.parameters();

    	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
    	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
    	
    	searchParams["searchParameters"] = {};
    	angular.forEach(prm.filter, function(value, key){
    		searchParams.searchParameters[key] = value;
    	});
    	searchParams["orderParamDesc"] = {};
    	angular.forEach(prm.sorting, function(value, key){
    		searchParams.orderParamDesc[key] = (value == "desc");
    	});

		searchParams.searchParameters["materialStatusId"] = 1;    	
    	restService.all(path).customPOST(searchParams, ['list']).then(function(data){
    		params.total(data.originalElement.totalRecords);
            // set new data
            $defer.resolve(data.originalElement.resultList);
		});
    }

	$translatePartialLoader.addPart('accountant');

	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	
	$scope.getTitle = function(title){
		return $('.'+title).text();
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            id: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	$scope.getMaterials($defer, params, "material");
        },
    });
	
	$scope.deleteMaterial= function(id){
		Utils.callDeleteModal("material", id);
	}	
		
	  			
}])
.controller('deleted_materialCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){	
	
$scope.getMaterials= function($defer, params, path) {
    	
		var searchParams = {};
    	var prm = params.parameters();

    	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
    	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
    	
    	searchParams["searchParameters"] = {};
    	angular.forEach(prm.filter, function(value, key){
    		searchParams.searchParameters[key] = value;
    	});

		searchParams.searchParameters["materialStatusId"] = 2;    	
    	restService.all(path).customPOST(searchParams, ['list']).then(function(data){
    		params.total(data.originalElement.totalRecords);
            // set new data
            $defer.resolve(data.originalElement.resultList);
		});
    }

	$translatePartialLoader.addPart('accountant');

	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	
	$scope.getTitle = function(title){
		return $('.'+title).text();
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            id: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	$scope.getMaterials($defer, params, "material");
        },
    });
	
	$scope.deleteMaterial= function(id){
		Utils.callDeleteModal("material", id);
}
	  			
}])
.controller('edit_materialCtrl',  ['$scope', 'restService', '$routeParams', "$filter", "Utils","$location", '$translatePartialLoader',function($scope, restService, $routeParams, $filter, Utils, $location,$translatePartialLoader){
	
	$translatePartialLoader.addPart('accountant');

	$scope.materialId = $routeParams.id;
	$scope.status = {};
	$scope.category = {};
	$scope.material = {};
	$scope.category = {};
	$scope.type = {};
	$scope.place = {};
	$scope.owner = {};
	$scope.ownerType = {};
	$scope.submitted = false;
	
	restService.all("material_category").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.categories = results.resultList;
	});
	restService.all("material_status").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.statuses = results;
	});
	restService.all("place").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.places = results.resultList;
	});
	restService.all("material_type").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.types = results.resultList;
	});
	restService.all("owner_type").customPOST({"startIndex": 0, "resultQuantity": 1000, "searchParameters": {"notName": "student"}}, ["list"]).then(function(results){
		$scope.ownerTypes = results;
	});	
	restService.all("material").customGET($scope.materialId).then(function(data){
		$scope.material = data.originalElement;

		console.log($scope.material.ownerType.id);
		if($scope.material.ownerType.id == 2){
			restService.all("teacher").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
				$scope.owners = results.resultList;
				console.log("teacher");
			});
		}else if($scope.material.ownerType.id == 3){
			restService.all("stuff").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
				$scope.owners = results.resultList;
				console.log("staff");
			});
		}
	});
	
	$scope.getMtype = function(){
		restService.all("material_type").customPOST({"startIndex": 0, "resultQuantity": 1000, "searchParameters": {"categoryId": $scope.category.id}}, ["list"]).then(function(results){
		$scope.types = results.resultList;
		});
	}
	$scope.getOwnerList = function(){
		if($scope.material.ownerType.id == 2){
			restService.all("teacher").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
				$scope.owners = results.resultList;
				console.log("teacher");
			});
		}else if($scope.material.ownerType.id == 3){
			restService.all("stuff").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
				$scope.owners = results.resultList;
				console.log("staff");
			});
		}
	}
	$scope.update = function(){
		console.log($scope.material);
		$scope.submitted = true;
		restService.one("material").customPOST($scope.material, "update/"+$scope.materialId	).then(function(data){
			alert("Inventory was successfully updated!");
			$location.path("/material");
		});
	};
	
	$scope.datepicker = {};
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	
}])
.controller('del_materialCtrl',  ['$scope', 'restService', '$routeParams', "$filter", "Utils","$location", '$translatePartialLoader',function($scope, restService, $routeParams, $filter, Utils, $location,$translatePartialLoader){
	
	$translatePartialLoader.addPart('accountant');

	$scope.materialId = $routeParams.id;
	$scope.submitted = false;
	
	
	restService.all("material").customGET($scope.materialId).then(function(data){
		$scope.material = data.originalElement;
	});
	
	$scope.update = function(){
		console.log($scope.material);
		$scope.material.materialStatus.id=2;
		$scope.submitted = true;
		restService.one("material").customPOST($scope.material, "update/"+$scope.materialId	).then(function(data){
			alert("Inventory was successfully deleted!");
			$location.path("/material");
		});
	};
	
	$scope.datepicker = {};
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	
}])
.controller('add_materialCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader','$location', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader,$location){	
			
	$translatePartialLoader.addPart('material');

	$scope.datepicker = {};
	$scope.category = {};
	$scope.status = {};
	$scope.type = {};
	$scope.place = {};
	$scope.owner = {};
	$scope.ownerType = {};
	$scope.material = {};
	$scope.material.materialStatus={};
	$scope.material.materialStatus.id=1;
	
	restService.all("material_category").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.categories = results.resultList;
	});
	restService.all("material_status").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.statuses = results;
	});
	restService.all("place").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.places = results.resultList;
	});
	restService.all("owner_type").customPOST({"startIndex": 0, "resultQuantity": 1000, "searchParameters": {"notName": "student"}}, ["list"]).then(function(results){
		$scope.ownerTypes = results;
	});

	$scope.getMtype = function(){
		restService.all("material_type").customPOST({"startIndex": 0, "resultQuantity": 1000, "searchParameters": {"categoryId": $scope.category.id}}, ["list"]).then(function(results){
		$scope.types = results.resultList;
		});
	}
	
	$scope.getOwnerList = function(){
		if($scope.material.ownerType.id == 2){
			restService.all("teacher").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
				$scope.owners = results.resultList;
				console.log("teacher");
			});
		}else if($scope.material.ownerType.id == 3){
			restService.all("stuff").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
				$scope.owners = results.resultList;
				console.log("staff");
			});
		}
	}
	
	$scope.submitMaterial = function() {
		console.log($scope);
		restService.one("material").customPOST($scope.material, "create").then(function(data){
			alert("New inventory successfully created!");
			$location.path('/material');
		});
	};
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	
}])
.controller('material_categoryCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$location','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$location, $translatePartialLoader){	
	  						
	$translatePartialLoader.addPart('accountant');

	$scope.category = {};
	
	$scope.submitCategory = function() {
		restService.one("material_category").customPOST($scope.category, "create").then(function(data){
			alert("New inventory category was successfully created!");
			$location.path("#/material_category");
		});
	};

	restService.all("material_category").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.categories = results.resultList;
	});

	$scope.deleteMcategory = function(id){
			Utils.callDeleteModal("material_category", id);
	}
}])

.controller('edit_mCategoryCtrl',  ['$scope', 'restService', '$routeParams', "$filter", "Utils","$location", '$translatePartialLoader',function($scope, restService, $routeParams, $filter, Utils, $location,$translatePartialLoader){
	
	$translatePartialLoader.addPart('accountant');

	$scope.categoryId = $routeParams.id;
	$scope.category = {};
	$scope.submitted = false;
	
	restService.all("material_category").customGET($scope.categoryId).then(function(data){
		$scope.category = data.originalElement;
	});
	
	
	$scope.update = function(){
		console.log($scope.category);
		$scope.submitted = true;
		restService.one("material_category").customPOST($scope.category, "update/"+$scope.categoryId	).then(function(data){
			alert("Inventory category was successfully updated!");
			$location.path("/material_category");
		});
	};
	
}])

.controller('material_typeCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$location','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$location, $translatePartialLoader){	
	  						
	$translatePartialLoader.addPart('accountant');

	$scope.category = {};
	$scope.type = {};
	
	$scope.submitType = function() {
		console.log($scope.type);
		restService.one("material_type").customPOST($scope.type, "create").then(function(data){
			alert("New inventory category type was successfully created!");
			$location.path("#/material_type");
		});
	};

	restService.all("material_type").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.types = results.resultList;
	
	});
	
	restService.all("material_category").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.categories = results.resultList;
		console.log($scope.categories);
	});

	$scope.deleteType = function(id){
		Utils.callDeleteModal("material_type", id);
	}
}])

.controller('edit_mTypeCtrl',  ['$scope', 'restService', '$routeParams', "$filter", "Utils","$location", '$translatePartialLoader',function($scope, restService, $routeParams, $filter, Utils, $location,$translatePartialLoader){
	
	$translatePartialLoader.addPart('accountant');

	$scope.typeId = $routeParams.id;
	$scope.type = {};
	$scope.submitted = false;
	
	restService.all("material_type").customGET($scope.typeId).then(function(data){
		$scope.type = data.originalElement;
	});
	restService.all("material_category").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.categories = results.resultList;
	});
	
	
	$scope.update = function(){
		$scope.submitted = true;
		restService.one("material_type").customPOST($scope.type, "update/"+$scope.typeId).then(function(data){
			alert("Inventory category type information was successfully updated!");
			$location.path("/material_type");
		});
	};
	
}])

.controller('placeCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$location','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$location, $translatePartialLoader){	
	  						
	$translatePartialLoader.addPart('accountant');

	$scope.place = {};
	
	$scope.submitPlace = function() {
		console.log($scope.place);
		restService.one("place").customPOST($scope.place, "create").then(function(data){
			alert("success");
			$location.path("#/place");
		});
	};
	
	restService.all("place").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.places = results.resultList;
	});

		$scope.deletePlace = function(id){
			Utils.callDeleteModal("place", id);
		}
}])

.controller('edit_placeCtrl',  ['$scope', 'restService', '$routeParams', "$filter", "Utils","$location", '$translatePartialLoader',function($scope, restService, $routeParams, $filter, Utils, $location,$translatePartialLoader){
	
	$translatePartialLoader.addPart('accountant');

	$scope.placeId = $routeParams.id;
	$scope.place = {};
	$scope.submitted = false
	
	restService.all("place").customGET($scope.placeId).then(function(data){
		$scope.place = data.originalElement;
	});
	
	
	$scope.update = function(){
		$scope.submitted = true;
		restService.one("place").customPOST($scope.place, "update/"+$scope.placeId).then(function(data){
			alert("success");
			$location.path("/place");
		});
	};
	
}])
