'use strict'; 

angular.module('materialApp', ['controllers', 'common.apps']).
config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/material', {
		templateUrl : 'views/material.html',
		controller : 'materialCtrl',
	});
	$routeProvider.when('/deleted_material', {
		templateUrl : 'views/deleted_material.html',
		controller : 'deleted_materialCtrl',
	});
	$routeProvider.when('/add_material', {
		templateUrl : 'views/add_material.html',
		controller : 'add_materialCtrl',
	});
	$routeProvider.when('/edit_material/:id', {
		templateUrl : 'views/edit_material.html',
		controller : 'edit_materialCtrl',
	});
	$routeProvider.when('/del_material/:id', {
		templateUrl : 'views/del_material.html',
		controller : 'del_materialCtrl',
	});
	$routeProvider.when('/material_category', {
		templateUrl : 'views/material_category.html',
		controller : 'material_categoryCtrl',
	});
	$routeProvider.when('/edit_mCategory/:id', {
		templateUrl : 'views/edit_category.html',
		controller : 'edit_mCategoryCtrl',
	});
	$routeProvider.when('/material_type', {
		templateUrl : 'views/material_type.html',
		controller : 'material_typeCtrl',
	});
	$routeProvider.when('/edit_mType/:id', {
		templateUrl : 'views/edit_type.html',
		controller : 'edit_mTypeCtrl',
	});
	$routeProvider.when('/place', {
		templateUrl : 'views/place.html',
		controller : 'placeCtrl',
	});
	$routeProvider.when('/edit_place/:id', {
		templateUrl : 'views/edit_place.html',
		controller : 'edit_placeCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/material'
	});
  			
}]);	


