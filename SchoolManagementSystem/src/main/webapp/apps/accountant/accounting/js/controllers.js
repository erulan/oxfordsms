'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('accountingCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){	
			
	$translatePartialLoader.addPart('accountant');
	
	$scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Report.xls");
    };
	
	
$scope.getStudents = function($defer, params, path) {
    	
		var searchParams = {};
    	var prm = params.parameters();

    	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
    	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
    	
    	searchParams["searchParameters"] = {};
    	angular.forEach(prm.filter, function(value, key){
    		searchParams.searchParameters[key] = value;
    	});

		searchParams.searchParameters["schoolTypeId"] = 1;
    	searchParams["orderParamDesc"] = {};
    	angular.forEach(prm.sorting, function(value, key){
    		searchParams.orderParamDesc[key] = (value == "desc");
    	});
    	
    	restService.all(path).customPOST(searchParams, ['list']).then(function(data){
    		params.total(data.originalElement.totalRecords);
            // set new data
            $defer.resolve(data.originalElement.resultList);
		});
    }
	
	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	$scope.getTitle = function (title){
		return $("."+title).text();
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	$scope.getStudents($defer, params, "stud_accounting");
        },
    });


	  			
}])
.controller('kidsCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){	
			
	$translatePartialLoader.addPart('accountant');
	
	$scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Report.xls");
    };
    
$scope.getStudents = function($defer, params, path) {
    	
		var searchParams = {};
    	var prm = params.parameters();

    	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
    	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
    	
    	searchParams["searchParameters"] = {};
    	angular.forEach(prm.filter, function(value, key){
    		searchParams.searchParameters[key] = value;
    	});

		searchParams.searchParameters["schoolTypeId"] = 2;
    	searchParams["orderParamDesc"] = {};
    	angular.forEach(prm.sorting, function(value, key){
    		searchParams.orderParamDesc[key] = (value == "desc");
    	});
    	
    	restService.all(path).customPOST(searchParams, ['list']).then(function(data){
    		params.total(data.originalElement.totalRecords);
            // set new data
            $defer.resolve(data.originalElement.resultList);
		});
    }
	
	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	$scope.getTitle = function (title){
		return $("."+title).text();
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	$scope.getStudents($defer, params, "stud_accounting");
        },
    });
}])
.controller('edit_accountingCtrl', ['$scope', '$modal', '$log',  '$routeParams', 'restService', 'ngTableParams', '$filter', 'Utils', '$translatePartialLoader',function($scope, $modal, $log,  $routeParams, restService, ngTableParams, $filter, Utils,$translatePartialLoader){
	  						

	$translatePartialLoader.addPart('accountant');

	$scope.stud_accounting = {};
	$scope.deneme = "denemee2";	

	restService.all("stud_accounting").customGET( $routeParams.id ).then(function(data){
		$scope.stud_accounting = data.originalElement;
	});
	
	$scope.submitted = function() {
		restService.one("stud_accounting").customPOST($scope.stud_accounting, "update").then(function(data){
			alert("success");
		});
	};
		
}])
.controller('new_paymentCtrl', ['$scope', '$modal', '$log',  '$routeParams', 'restService', 'ngTableParams', '$filter', 'Utils', '$route','$translatePartialLoader', function($scope, $modal, $log,  $routeParams, restService, ngTableParams, $filter, Utils, $route,$translatePartialLoader){
	
	$translatePartialLoader.addPart('accountant');

	$scope.stud_accounting = {};
	$scope.studentAccId = $routeParams.id;
	
	restService.all("stud_accounting").customGET($scope.studentAccId).then(function(data){
		$scope.stud_accounting = data.originalElement;
	});
	/*
	$scope.studentId = $routeParams.id;*/
	$scope.submitted = function() {
		restService.one("stud_accounting").customGET("pay/" + $scope.studentAccId + "/" + $scope.payed ).then(function(data){
			alert("You are seccessfully updated!");
			$route.reload();
		});
	};
}])
.controller('annual_feeCtrl', ['$scope', '$modal', '$log',  '$routeParams', 'restService', 'ngTableParams', '$filter', 'Utils', '$route','$translatePartialLoader', function($scope, $modal, $log,  $routeParams, restService, ngTableParams, $filter, Utils, $route,$translatePartialLoader){
	
	$translatePartialLoader.addPart('accountant');

	$scope.stud_accounting = {};
	var studentAccId = $routeParams.id;
	
	restService.all("stud_accounting").customGET(studentAccId).then(function(data){
		$scope.stud_accounting = data.originalElement;
	});
	/*
	$scope.studentId = $routeParams.id;*/
	$scope.submitted = function() {
		restService.one("stud_accounting").customGET("pay/" + studentAccId + "/" + $scope.payed ).then(function(data){
			alert("success");
			$route.reload();
		});
	};
	}]);


