'use strict'; 

angular.module('accountingApp', ['controllers','common.apps']).
config(['$routeProvider', '$locationProvider',  function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/accounting', {
		templateUrl : 'views/accounting.html',
		controller : 'accountingCtrl',
	});
	$routeProvider.when('/kids', {
		templateUrl : 'views/kids.html',
		controller : 'kidsCtrl',
	});
	$routeProvider.when('/edit_accounting/:id', {
		templateUrl : 'views/edit_accounting.html',
		controller : 'edit_accountingCtrl',
	});
	$routeProvider.when('/new_payment/:id', {
		templateUrl : 'views/new_payment.html',
		controller : 'new_paymentCtrl',
	});
	$routeProvider.when('/annual_fee', {
		templateUrl : 'views/annual_fee.html',
		controller : 'annual_feeCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/accounting'
	});
  			
}]);	


