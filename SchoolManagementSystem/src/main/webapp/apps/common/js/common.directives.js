'use strict';

/* Directives */

angular.module('common.directives', [])

.directive('nksOnlyNumber', function () {
    return {
      restrict: 'EA',
      require: 'ngModel',
      link: function (scope, element, attrs, ngModel) {   
        scope.$watch(attrs.ngModel, function(newValue, oldValue) {
          var spiltArray = String(newValue).split("");
          if (spiltArray.length === 0) return;
          if (spiltArray.length === 1 
               && (spiltArray[0] == '-' 
               || spiltArray[0] === '.' )) return;
          if (spiltArray.length === 2 
               && newValue === '-.') return;

          /*Check it is number or not.*/
          if (isNaN(newValue)) {
               ngModel.$setViewValue(oldValue);
               ngModel.$render();
          }
        });
      }
    };
  })


.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
;