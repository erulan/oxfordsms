'use strict';

/* Controllers */
	
angular.module('common.controllers', [])


.controller('DeleteModalCtrl', ['$scope', '$modalInstance', '$rootScope', '$translate', function ($scope, $modalInstance, $rootScope, $translate) {

	  $scope.ok = function () {
	    $modalInstance.close('ok');
	  };

	  $scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	  };
	  
	  
}]);


