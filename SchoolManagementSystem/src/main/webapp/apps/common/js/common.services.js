'use strict';

/* Services */

angular.module('common.services', ['restangular'])
.factory("restService", function(Restangular, $rootScope){
	Restangular.setBaseUrl("/SchoolManagementSystem/rest");

	 Restangular.setErrorInterceptor(
        function(response) {
            if (response.status == 405) {
                alert(response.data);
//                $window.location.href='/login';
            }
        }
    );
	 
	Restangular.setResponseExtractor(function (response, operation, what, url) {
		
        response.originalElement = angular.copy(response);
        $rootScope.requestElement = null;
//        
        return response;		
    });	
		
		
	
	
	return Restangular;
})
.factory("Utils", function(restService, $modal, $log, $location, $route){
	return {
		callDeleteModal: function(deletepath, id){
			var modalInstance = $modal.open({
			      templateUrl: '../../common/templates/deleteModal.html',
			      controller: 'DeleteModalCtrl',
			      size: 'sm'
			    });

			    modalInstance.result.then(function (selectedItem) {
			    	restService.one(deletepath).customGET("delete/" + id).then(function(data){
			    		alert("Successfully deleted!");
			    		$location.path("/" + deletepath);
			    		$route.reload();
			    	});
			    }, function () {
			      return;
			    });
		},
		ngTableGetData: function($defer, params, path) {
        	
			var searchParams = {};
        	var prm = params.parameters();

        	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
        	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
        	
        	searchParams["searchParameters"] = {};
        	angular.forEach(prm.filter, function(value, key){
        		searchParams.searchParameters[key] = value;
        	});
        	
        	searchParams["orderParamDesc"] = {};
        	angular.forEach(prm.sorting, function(value, key){
        		searchParams.orderParamDesc[key] = (value == "desc");
        	});
        	
        	restService.all(path).customPOST(searchParams, ['list']).then(function(data){
        		params.total(data.originalElement.totalRecords);
                // set new data
                $defer.resolve(data.originalElement.resultList);
    		});
        }
	}
});