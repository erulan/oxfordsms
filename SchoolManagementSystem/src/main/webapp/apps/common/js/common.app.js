'use strict'; 

angular.module('common.apps', ['common.controllers', 'common.directives', 'common.services', 'common.controllers', 'common.filters',
                               'ngRoute', 'ui.bootstrap', 'ngTable', 'pascalprecht.translate', 'LocalStorageModule'])
                               

                               
                               
.config(function($translateProvider, $translatePartialLoaderProvider, localStorageServiceProvider) {
	  
	localStorageServiceProvider
	    .setPrefix('')
	    .setStorageType('sessionStorage')
	    .setNotify(true, true)
	    .setStorageCookie(30, '/SchoolManagementSystem/');
	
	$translatePartialLoaderProvider.addPart('common');
	$translateProvider.useLoader('$translatePartialLoader', {
	  urlTemplate: '/SchoolManagementSystem/apps/{part}/translate/{lang}.json'
	});

	$translateProvider.preferredLanguage("en");
	 
})

.run(function($rootScope, localStorageService, restService, $translate) {
	$rootScope.$on('$translatePartialLoaderStructureChanged', function () {
		var language = "en";
		if(localStorageService.isSupported) {
			  language = localStorageService.get("language");
		}
		$translate.use(language);
	    $translate.refresh();
	});
	
	$rootScope.getCurrentLanguage = function(){
	  return $translate.use();
	}
	
	$rootScope.translate = function(key){
		if(localStorageService.isSupported) {
			localStorageService.set("language", key);
		}
		$translate.use(key);
	}
	 $rootScope.getUserInfo = function() {
		 var userInfo = null;
		 if(localStorageService.isSupported) {
			  userInfo = localStorageService.get("userInfo");
			  if(userInfo == null || userInfo.id == null){
				  restService.one("security").customGET("current_user").then(function(data){
					  userInfo = data.originalElement;
					  localStorageService.set("userInfo", userInfo);
				  });
			  }
		 }
		 else{
			 restService.one("security").customGET("current_user").then(function(data){
				 userInfo = data.originalElement;
			 });
		 }
		 $rootScope.userInfo = userInfo;
		 return userInfo;
     };
     $rootScope.getUserInfo();
     $rootScope.logout = function(){
    	 localStorageService.clearAll();
    	 window.location = "/SchoolManagementSystem/logout";
     }
});


	
