'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('lessonCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$location','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $location,$translatePartialLoader){
	  						
	$translatePartialLoader.addPart('secretary');

	$scope.lesson = {};
	$scope.deneme = "denemee";	
	
	$scope.submitLesson = function() {
		console.log($scope.lesson);
		restService.one("lesson").customPOST($scope.lesson, "create").then(function(data){
			alert("New lesson was successfully created!");
			$location.path("#/lesson");
		});
	};

	restService.all("lesson").customPOST({"startIndex": 0, "resultQuantity": 1000}, "list").then(function(results){
		$scope.lessons = results.originalElement;
	});
	$scope.datepicker = {};
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  
	$scope.deleteLesson = function(id){
		Utils.callDeleteModal("lesson", id);
	}
}])
.controller('edit_lessonCtrl',  ['$scope', 'restService', '$routeParams', '$filter', 'Utils', '$location','$log','$translatePartialLoader', function($scope, restService, $routeParams, $filter, Utils,  $location, $log, $translatePartialLoader){
	
	$translatePartialLoader.addPart('secretary');

	$scope.lessonId = $routeParams.id;
	$scope.lesson = {};
	$scope.submitted = false;
	$scope.deneme = "checking";
	
	restService.all("lesson").customGET($scope.lessonId).then(function(data){
		$scope.lessons = data.originalElement;
	});
	
	$scope.update = function(){
		console.log($scope.lesson.name+$scope.lessons.name);
		$scope.submitted = true;
		restService.one("lesson").customPOST($scope.lessons, "update/"+ $scope.lessonId).then(function(data){
			alert("Lesson information was successfully updated!");
			$location.path("/lesson");
		});
	};	
}])