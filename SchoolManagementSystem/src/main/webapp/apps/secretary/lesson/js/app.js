'use strict';

angular.module('lessonApp', ['common.apps', 'controllers']).
config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
	
	
	$routeProvider.when('/lesson', {
		templateUrl : 'views/lesson.html',
		controller : 'lessonCtrl',
	});
	$routeProvider.when('/edit_lesson/:id', {
		templateUrl : 'views/edit_lesson.html',
		controller : 'edit_lessonCtrl',
	});
	
	$routeProvider.otherwise({
		redirectTo : '/lesson'
	});
}])