'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('classHoursCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$location','$translatePartialLoader', '$routeParams', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $location,$translatePartialLoader, $routeParams){
	  						
	$translatePartialLoader.addPart('secretary');
	
	$scope.classhour = {};
	$scope.classHourTypeId = 1;
	$scope.classHourTypeId = $routeParams.id != null ? $routeParams.id : 1;
	
	$scope.submitClassTime = function() {
		$scope.classhour.classHourType = {"id": $scope.classHourTypeId};
		$scope.classhour.startHour = $scope.classhour.startHour.getTime();
		$scope.classhour.endHour = $scope.classhour.endHour.getTime();
		if($scope.classhour.startHour>$scope.classhour.endHour){
			alert("Wrong input! Class Start time should be smaller than Class End time.");
			$location.path("/class_hours/" + $scope.classHourTypeId);
		}else{
			
			restService.all("class_hours").customPOST({"startIndex": 0, "resultQuantity": 1000, "searchParameters": {"classHourTypeId": $scope.classHourTypeId}}, "list").then(function(results){
				$scope.classhour.orderNum = results.resultList.length+1;
				restService.one("class_hours").customPOST($scope.classhour, "create").then(function(data){
					alert("New class hour was successfully created!");
					$location.path("/class_hours/" + $scope.classHourTypeId);
				});
			});	
		}
	};
	
	$scope.$watch('classHourTypeId',function(newValue, oldValue) {
		restService.all("class_hours").customPOST({"startIndex": 0, "resultQuantity": 1000, "searchParameters": {"classHourTypeId": newValue}}, "list").then(function(results){
			$scope.classhours = results.resultList;		
		});
	});
	
	restService.all("class_hour_type").customPOST({"startIndex": 0, "resultQuantity": 1000}, "list").then(function(results){
		$scope.classHourTypes = results;
	});
	
	$scope.classhour.startHour = {};
	  var tstart = new Date();
	  tstart.setHours('08');
	  tstart.setMinutes( '00' );
	$scope.classhour.startHour = tstart;
	$scope.hstep = 1;
	$scope.mstep = 5;
	$scope.ismeridian = false;
	$scope.isreadonly = true;
	
	$scope.classhour.endHour = {};
	  var tend = new Date();
	  tend.setHours('00');
	  tend.setMinutes( '00' );
	$scope.classhour.endHour = tend;
	
	
	$scope.datepicker = {};
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  $scope.datepicker.format = 'dd/MM/yyyy';
	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  
	  
	$scope.deleteClassH= function(id){
		var searchParam = {"startIndex": 0, "resultQuantity": 10, "searchParameters": {"classHoursId": id}};
		restService.all("timetable").customPOST(searchParam, "list").then(function(data){
			if(data.totalRecords == 0){
				Utils.callDeleteModal("class_hours", id);
			}else{
				alert("You have courses during this lesson hours! To be able to delete Lesson hour please delete all courses on this hours!");
			
			}
		});
	}
}])
.controller('edit_classHCtrl',  ['$scope', 'restService', '$routeParams', '$filter', 'Utils', '$location','$log','$translatePartialLoader', function($scope, restService, $routeParams, $filter, Utils,  $location, $log, $translatePartialLoader){
	
	$translatePartialLoader.addPart('secretary');

	$scope.classhId = $routeParams.id;
	$scope.classh = {};
	$scope.submitted = false;	
	$scope.classhour = {};
	$scope.classhour.startHour = {};
		var tstart = new Date();
	$scope.classhour.startHour = tstart;
		var tend = new Date();
	$scope.classhour.endHour = tend;
	
	restService.all("class_hours").customGET($scope.classhId).then(function(data){
		$scope.classhour = data.originalElement;
	});
	
	$scope.update = function(){
		$scope.submitted = true;
		restService.one("class_hours").customPOST($scope.classhour, "update/"+ $scope.classhId).then(function(data){
			alert("Lesson Hour was successfully updated!");
			$location.path("#/class_hours");
		});
	};	
	
	$scope.hstep = 1;
	$scope.mstep = 5;
	$scope.ismeridian = false;
	$scope.isreadonly = true;
	
	
}])