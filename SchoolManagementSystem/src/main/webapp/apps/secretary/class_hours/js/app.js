'use strict';

angular.module('classhourApp', ['common.apps', 'controllers']).
config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
	
	
	$routeProvider.when('/class_hours', {
		templateUrl : 'views/class_hours.html',
		controller : 'classHoursCtrl',
	});
	$routeProvider.when('/class_hours/:id', {
		templateUrl : 'views/class_hours.html',
		controller : 'classHoursCtrl',
	});
	$routeProvider.when('/edit_classH/:id', {
		templateUrl : 'views/edit_classH.html',
		controller : 'edit_classHCtrl',
	});
	
	$routeProvider.otherwise({
		redirectTo : '/class_hours'
	});
}])