'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('groupCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$location','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $location,$translatePartialLoader){
	  						
	$translatePartialLoader.addPart('secretary');

	$scope.group = {};
	$scope.grade = {};
	$scope.letter = {};
	$scope.teacher = {};
	$scope.submitGroup = function() {
		var searchParam = {"startIndex": 0, "resultQuantity": 2, "searchParameters": {"gradeId": $scope.group.grade.id, "classLetterId": $scope.group.classLetter.id}};
		restService.all("c_group").customPOST(searchParam, "list").then(function(data){
			if(data.length > 0){
				alert("You already have class of this letter and grade");
			}else{
				restService.one("c_group").customPOST($scope.group, "create").then(function(data){
					alert("New class was successfully created!");
					$location.path("#/group");
				});
			}
		});
		
	};

	restService.all("class_hour_type").customPOST({startIndex: 0, resultQuantity: 1000},  "list").then(function(data){
		$scope.classHoursTypes = data.originalElement;
	});

	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000, "orderParamDesc": {"grade": false, "classLetter": false}}, "list").then(function(results){
		$scope.groups = results.originalElement;
	});
	/*restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroups = data.originalElement;
	});*/
	restService.all("grade").customPOST({"startIndex": 0, "resultQuantity": 1000}, "list").then(function(results){
		$scope.grades = results.originalElement;
	});
	restService.all("class_letter").customPOST({"startIndex": 0, "resultQuantity": 1000}, "list").then(function(results){
		$scope.letters = results.originalElement;
	});
	restService.all("teacher").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.teachers = data.originalElement.resultList;
	});
	
	$scope.datepicker = {};
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  
	$scope.deleteGroup = function(id){
		Utils.callDeleteModal("c_group", id);
	}
}])
.controller('edit_groupCtrl',  ['$scope', 'restService', '$routeParams', '$filter', 'Utils', '$location','$log','$translatePartialLoader', function($scope, restService, $routeParams, $filter, Utils,  $location, $log, $translatePartialLoader){
	
	$translatePartialLoader.addPart('secretary');
	$scope.teacher = {};
	$scope.group = {};
	$scope.groupId = $routeParams.id;
	$scope.submitted = false;
	
	restService.all("c_group").customGET($scope.groupId).then(function(data){
		$scope.group = data.originalElement;
	});
	restService.all("teacher").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.teachers = data.originalElement.resultList;
	});
	restService.all("class_hour_type").customPOST({startIndex: 0, resultQuantity: 1000},  "list").then(function(data){
		$scope.classHoursTypes = data.originalElement;
	});
	
	$scope.update = function(){
		$scope.submitted = true;
		restService.one("c_group").customPOST($scope.group, "update/"+ $scope.groupId).then(function(data){
			alert("Class information was successfully updated!");
			$location.path("#/grade");
		});
	};	
}])