'use strict';

angular.module('groupApp', ['common.apps', 'controllers']).
config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
	
	
	$routeProvider.when('/group', {
		templateUrl : 'views/group.html',
		controller : 'groupCtrl',
	});
	$routeProvider.when('/c_group', {
		templateUrl : 'views/group.html',
		controller : 'groupCtrl',
	});
	$routeProvider.when('/edit_group/:id', {
		templateUrl : 'views/edit_group.html',
		controller : 'edit_groupCtrl',
	});
	
	$routeProvider.otherwise({
		redirectTo : '/group'
	});
}])