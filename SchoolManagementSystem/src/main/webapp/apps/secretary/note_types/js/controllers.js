'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('notetypeCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$location','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $location,$translatePartialLoader){
	  						
	$translatePartialLoader.addPart('secretary');

	$scope.note_type = {};
	
	$scope.submitNoteType = function() {
		console.log($scope.note_type);
		restService.one("note_type").customPOST($scope.note_type, "create").then(function(data){
			alert("New note type was successfully created!");
			$location.path("#/notetypes");
		});
	};

	restService.all("note_type").customPOST({"startIndex": 0, "resultQuantity": 1000}, "list").then(function(results){
		$scope.note_types = results.originalElement.resultList;
	});
	
	  
	$scope.deleteNoteType = function(id){
		Utils.callDeleteModal("note_type", id);
	}
}])
.controller('edit_notetypeCtrl',  ['$scope', 'restService', '$routeParams', '$filter', 'Utils', '$location','$log','$translatePartialLoader', function($scope, restService, $routeParams, $filter, Utils,  $location, $log, $translatePartialLoader){
	
	$translatePartialLoader.addPart('secretary');

	$scope.note_typeId = $routeParams.id;
	$scope.note_type = {};
	$scope.submitted = false;
	$scope.deneme = "checking";
	
	restService.all("note_type").customGET($scope.note_typeId).then(function(data){
		$scope.note_type = data.originalElement;
	});
	
	$scope.update = function(){
		console.log($scope.note_type.name);
		$scope.submitted = true;
		restService.one("note_type").customPOST($scope.note_type, "update/"+ $scope.note_typeId).then(function(data){
			alert("Note type information was successfully updated!");
			$location.path("/notetypes");
		});
	};	
}])