'use strict';

angular.module('notetypeApp', ['common.apps', 'controllers']).
config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
	
	
	$routeProvider.when('/notetypes', {
		templateUrl : 'views/notetypes.html',
		controller : 'notetypeCtrl',
	});
	$routeProvider.when('/edit_notetype/:id', {
		templateUrl : 'views/edit_notetype.html',
		controller : 'edit_notetypeCtrl',
	});
	
	$routeProvider.otherwise({
		redirectTo : '/notetypes'
	});
}])