'use strict'; 

angular.module('libApp', ['controllers', 'common.apps']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/library', {
		templateUrl : 'views/library.html',
		controller : 'libraryCtrl',
	});
	$routeProvider.when('/add_book', {
		templateUrl : 'views/add_book.html',
		controller : 'libraryCtrl',
	});
	/*$routeProvider.when('/stuff', {
		templateUrl : 'views/stuff.html',
		controller : 'stuffCtrl',
	});*/
	$routeProvider.otherwise({
		redirectTo : '/library'
	});
  			
}]);	


