'use strict'; 

angular.module('coursesApp', ['controllers', 'common.apps']).
config(['$routeProvider',   function ($routeProvider) {
					

	$routeProvider.when('/courses', {
		templateUrl : 'views/courses.html',
		controller : 'coursesCtrl',
	});
	$routeProvider.when('/course', {
		templateUrl : 'views/courses.html',
		controller : 'coursesCtrl',
	});
	$routeProvider.when('/add_course', {
		templateUrl : 'views/add_course.html',
		controller : 'add_courseCtrl',
	});
	$routeProvider.when('/add_course', {
		templateUrl : 'views/add_course.html',
		controller : 'add_courseCtrl',
	});	
	$routeProvider.when('/edit_course/:id', {
		templateUrl : 'views/edit_course.html',
		controller : 'edit_courseCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/courses'
	});
  			
}]);	


