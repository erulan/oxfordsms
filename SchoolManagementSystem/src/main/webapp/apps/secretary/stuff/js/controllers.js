'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('StuffListCtrl', ['$scope', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope,  restService, ngTableParams, $filter, Utils,$translatePartialLoader){		
		
	$translatePartialLoader.addPart('secretary');

	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});

	$scope.getTitle = function (title){
		return $("."+title).text();
	}
	
	$scope.deneme = "denemee";
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
        	name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "stuff");
        },
    });
	$scope.datepicker = {};
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  
	  $scope.deleteStuff = function(id){
			Utils.callDeleteModal("stuff", id);
	};
	
	  			
}])
	
.controller('AddStuffCtrl', ['$scope', 'restService', '$location','$translatePartialLoader', function ($scope, restService, $location,$translatePartialLoader) {
	
	$translatePartialLoader.addPart('secretary');

	$scope.stuff = {};
	$scope.submitted = false;
	$scope.datepicker = {};
	$scope.deneme = "checking";
	
	$scope.submitt = function(){
		if($scope.datepicker.date != null){
			$scope.stuff.birthDate = new Date($scope.datepicker.date).getTime();
		}
		$scope.submitted = true;
		var fd = new FormData();
		fd.append('stuff', new Blob([angular.toJson($scope.stuff)], {type: "application/json"}));
		fd.append('photo', $scope.photo);
		restService.one("stuff").withHttpConfig({transformRequest: angular.identity})
		.customPOST(fd, 'create',undefined,{'Content-Type': undefined}).then(function(data){
			alert("Staff was successfully registered!");
			$location.path("/list_stuff");
		});
	};
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	$scope.datepicker.format = 'dd/MM/yyyy';

	$scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])

.controller('EditStuffCtrl', ['$scope', 'restService', 'Utils', '$routeParams', '$location','$translatePartialLoader', function ($scope, restService, Utils, $routeParams, $location,$translatePartialLoader) {

	$translatePartialLoader.addPart('secretary');

	$scope.stuffId = $routeParams.id;
	$scope.stuff = {};
	$scope.submitted = false;
	$scope.datepicker = {};
	$scope.deneme = "checking";
	
	restService.one("stuff").customGET($scope.stuffId).then(function(data){
		$scope.stuff = data.originalElement;
		$scope.datepicker.date = data.originalElement.birthDate;
	});
	
	$scope.submitt = function(){
		$scope.submitted = true;
		
		var fd = new FormData();
		fd.append('stuff', new Blob([angular.toJson($scope.stuff)], {type: "application/json"}));
		fd.append('photo', $scope.photo);		
		
		restService.one("stuff").withHttpConfig({transformRequest: angular.identity})
		.customPOST(fd, "update/" + $scope.stuffId, undefined,{'Content-Type': undefined}).then(function(data){
			alert("Staff information was successfully updated!");
			$location.path("/list_stuff");
		});
	};
	
	$scope.deleteStuff = function(){
		var yesNo = Utils.callDeleteModal("stuff", $routeParams.id);
	};
	
	
	
	//this is datepicker
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	$scope.datepicker.format = 'dd/MM/yyyy';

	$scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
.controller('stuff_profileCtrl', ['$scope', '$modal', '$routeParams','restService', '$log','$translatePartialLoader',  function ( $scope, $modal, $routeParams, restService,$log,$translatePartialLoader) {	

	$translatePartialLoader.addPart('secretary');

	$scope.stuff = {};
	
	restService.all("stuff").customGET( $routeParams.id ).then(function(data){
		$scope.stuff = data.originalElement;
		console.log($scope.stuff);
	});
	
}]);








