'use strict'; 

angular.module('appTable', ['common.apps','controllers']).
config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
	
	$routeProvider.when('/addTimetable',{
		templateUrl : 'views/addTimetable.html',
		controller : 'addTimetableCtrl',
	});
	
/*	$routeProvider.when('/addTimetableSecondary',{
		templateUrl : 'views/addTimetable2.html',
		controller : 'addTimetable2Ctrl',
	});*/

	$routeProvider.otherwise({
		redirectTo : '/addTimetable'
	});
  			
}]);	


//'common.apps'