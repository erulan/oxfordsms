'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('addTimetableCtrl',['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){	
	
	$translatePartialLoader.addPart('secretary');	
	$scope.timetable = {};
	$scope.classHourTypeId = 1;

	
//	$scope.$watch("count", function (newVal, oldVal) {
//        $scope.tableParams.count(newVal);
//    });

	$scope.getTitle = function(title){
		return $('.'+title).text();
	}
//	$scope.stNum = $rootScope.userInfo.id;
//	$scope.timetableArray = [];
	$scope.course = [];
	$scope.classGroup = {};
	$scope.classGroup.id = 1;
	
	$scope.$watch('classGroup.id',function(newValue, oldValue) {
		restService.all("c_group").customGET( $scope.classGroup.id ).then(function(data){
			$scope.oneClass = data.originalElement;
			$scope.classHourTypeId = $scope.oneClass.classHourType.id;
		});
//		$scope.getTimetable();
	});
	
	$scope.$watch('classHourTypeId',function(newValue, oldValue) {
		restService.all("class_hours").customPOST({startIndex: 0, resultQuantity: 1000, searchParameters: {"classHourTypeId": newValue}},  "list").then(function(data){
			$scope.classHours = data.originalElement;
			$scope.getTimetable();
		});
	});
	
	restService.all("week_days").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.weekDays = data.originalElement;
	});
	restService.all("class_hour_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.classHourTypes = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroups = data.originalElement;
		$scope.classGroup.id = $scope.CGroups[0].id != null ? $scope.CGroups[0].id : null;
	});
	
	
	$scope.getTimetable = function(){
		if($scope.classGroup.id == null){return false;}		
		if($scope.classHourTypeId == null){return false;}
		$scope.timetableArray = [];
		restService.all("timetable").customPOST( {"startIndex": 0, resultQuantity: 1000, searchParameters: 
		{"cGroupId": $scope.classGroup.id, "classHourTypeId": $scope.classHourTypeId }}, "list").then(function(data){
			$scope.timetables = data.originalElement.resultList;
			
			angular.forEach($scope.classHours.resultList, function(classHour, ch_key){
				var object = [];
				object[0] = classHour;
				angular.forEach($scope.weekDays, function(weekDay, wd_key){
					var timetableObj = {};
					timetableObj.course = {};
					angular.forEach($scope.timetables, function(timetable, tt_key){
						if(timetable.weekDays.id == weekDay.id && timetable.classHours.id == classHour.id){
							timetableObj = timetable;
						}
					});
					timetableObj.weekDays = weekDay;
					timetableObj.classHours = classHour;
					object[wd_key + 1] = timetableObj;
				});
				$scope.timetableArray.push(object);
			});
			
		});
		
		restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000, searchParameters: {"cGroupId": $scope.classGroup.id }}, "list").then(function(data){
			$scope.courses = data.originalElement.resultList;
		});
		
	}

	$scope.save = function(){
		$scope.timetableSave = [];
		angular.forEach($scope.timetableArray, function(timetableRow, row_key){
			angular.forEach(timetableRow, function(timetableCell, cell_key){
				if(cell_key != 0 && timetableCell.course.id != null && timetableCell.course.id != "noclass"){
					$scope.timetableSave.push({ "classHours": {"id":timetableCell.classHours.id}, "course": {"id":timetableCell.course.id}, "weekDays": {"id" :timetableCell.weekDays.id}});
				}
			});
		});
		var object = {"classGroup": {"id": $scope.classGroup.id}, "timetables": $scope.timetableSave};
		restService.one("timetable").customPOST(object, "create_timetable").then(function(data){
			alert("success");
			$scope.getTimetable($scope.classGroup.id);
		});
	};
}])

.controller('addTimetable2Ctrl', ['$scope', '$routeParams', 'restService', '$compile', 'Utils', '$rootScope', "$route",'$translatePartialLoader', function($scope, $routeParams, restService, $compile, Utils, $rootScope, $route,$translatePartialLoader) {	

	$translatePartialLoader.addPart('secretary');	
	$scope.timetable = {};

	
//	$scope.$watch("count", function (newVal, oldVal) {
//        $scope.tableParams.count(newVal);
//    });

	$scope.getTitle = function(title){
		return $('.'+title).text();
	}
//	$scope.stNum = $rootScope.userInfo.id;
	$scope.timetableArray = [];
	$scope.course = [];
	$scope.classGroup = {};
	
	$scope.$watch('classGroup.id',function(newValue, oldValue) {
		$scope.getTimetable(newValue);
	});
	
	restService.all("week_days").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.weekDays = data.originalElement;
	});
	restService.all("class_hours").customPOST({startIndex: 0, resultQuantity: 1000, searchParameters: {"classHourTypeId": 1}},  "list").then(function(data){
		$scope.classHours = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroups = data.originalElement;
		$scope.classGroup.id = $scope.CGroups[0].id != null ? $scope.CGroups[0].id : null;
	});
	
	
	$scope.getTimetable = function(id){
		if(id == null){return false;}
		
		$scope.timetableArray = [];
		restService.all("timetable").customPOST( {"startIndex": 0, resultQuantity: 1000, searchParameters: 
		{"cGroupId": id, "classHourTypeId": 2 }}, "list").then(function(data){
			$scope.timetables = data.originalElement.resultList;
			
			angular.forEach($scope.classHours, function(classHour, ch_key){
				var object = [];
				object[0] = classHour;
				angular.forEach($scope.weekDays, function(weekDay, wd_key){
					var timetableObj = {};
					timetableObj.course = {};
					angular.forEach($scope.timetables, function(timetable, tt_key){
						if(timetable.weekDays.id == weekDay.id && timetable.classHours.id == classHour.id){
							timetableObj = timetable;
						}
					});
					timetableObj.weekDays = weekDay;
					timetableObj.classHours = classHour;
					object[wd_key + 1] = timetableObj;
				});
				$scope.timetableArray.push(object);
			});
			
		});
		
		restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000, searchParameters: {"cGroupId": id }}, "list").then(function(data){
			$scope.courses = data.originalElement.resultList;
		});
		
	}

	$scope.save = function(){
		$scope.timetableSave = [];
		angular.forEach($scope.timetableArray, function(timetableRow, row_key){
			angular.forEach(timetableRow, function(timetableCell, cell_key){
				if(cell_key != 0 && timetableCell.course.id != null && timetableCell.course.id != "noclass"){
					$scope.timetableSave.push({ "classHours": {"id": 2 }, "course": {"id":timetableCell.course.id}, "weekDays": {"id" :timetableCell.weekDays.id}});
				}
			});
		});
		var object = {"classGroup": {"id": $scope.classGroup.id}, "timetables": $scope.timetableSave};
		restService.one("timetable").customPOST(object, "create_timetable").then(function(data){
			alert("success");
			$scope.getTimetable($scope.classGroup.id);
		});
	};
}]);
//'restService',restService