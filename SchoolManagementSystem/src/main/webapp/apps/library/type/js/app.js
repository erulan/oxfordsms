'use strict'; 

angular.module('libApp', ['controllers', 'common.apps']).
config(['$routeProvider',   function ($routeProvider) {
					

	$routeProvider.when('/type', {
		templateUrl : 'views/types.html',
		controller : 'typeCtrl',
	});
	$routeProvider.when('/add_type', {
		templateUrl : 'views/add_type.html',
		controller : 'addTypeCtrl',
	});
	$routeProvider.when('/edit_type/:id', {
		templateUrl : 'views/edit_type.html',
		controller : 'editTypeCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/type'
	});
  			
}]);	


