'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('typeCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){
	  						
	$translatePartialLoader.addPart('library');


$scope.deneme = "denemee";	

$scope.$watch("count", function (newVal, oldVal) {
    $scope.tableParams.count(newVal);
});
$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
	$scope.count = newVal;
});
$scope.getTitle = function(title){
	return $('.'+title).text();
}

	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            id: 'desc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "book_type");
        },
    });
	$scope.deleteBookType = function(id){
		Utils.callDeleteModal("book_type", id);
	};	
		
}])
.controller('addTypeCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$location','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $location,$translatePartialLoader){
	
	$translatePartialLoader.addPart('library');
	
	$scope.getTitle = function(title){
		return $('.'+title).text();
	}
	
	
	$scope.save = false;
	
	restService.all("book_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.books = data.originalElement;
	});
	
	restService.all("category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.categories = data.originalElement;
	});
	
	$scope.save = function(){
		restService.one("book_type").customPOST($scope.book_type, "create").then(function(data){
			alert("success");
			$location.path("/type");
		});
		};
}])

.controller('editTypeCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$routeParams', '$filter', 'Utils', '$location','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $routeParams, $filter, Utils, $location,$translatePartialLoader){
	
	$translatePartialLoader.addPart('library');

	$scope.getTitle = function(title){
		return $('.'+title).text();
	}
	
	
	$scope.typeId = $routeParams.id;
	$scope.book_type = {};
	$scope.save = false;
	
	restService.all("book").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.books = data.originalElement.resultList;
	});
	
	restService.one("book_type").customGET($scope.typeId).then(function(data){
		$scope.book_type = data.originalElement;
	});
	restService.all("book_status").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.statuses = data.originalElement;
	});
	restService.all("book_status").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.statuses = data.originalElement;
	});
	restService.all("category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.categories = data.originalElement;
	});
	
	$scope.save = function(){
		$scope.save = true;
		restService.one("book_type").customPOST($scope.book_type, "update/"+$scope.typeId).then(function(data){
			alert("Обновлено!");
			$location.path("/type");
		});
	};
	
	}]);
	


