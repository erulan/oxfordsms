'use strict'; 

angular.module('libApp', ['controllers', 'common.apps']).
config(['$routeProvider',   function ($routeProvider) {
					
		$routeProvider.when('/settings', {
		templateUrl : 'views/settings.html',
		controller : 'settingsCtrl',
	});  
		$routeProvider.otherwise({
			redirectTo : '/settings'
		});
}]);	


