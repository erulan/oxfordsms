'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('bookCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){
	
	$translatePartialLoader.addPart('library');

	$scope.$watch("count", function (newVal, oldVal) {
	    $scope.tableParams.count(newVal);
	});
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	$scope.getTitle = function(title){
		return $('.'+title).text();
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            id: 'desc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "book");
        },
    });
	$scope.deleteBook = function(id){
		Utils.callDeleteModal("book", id);
	};	
}])

.controller('addBookCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$location','$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $location,$translatePartialLoader){
		
	$translatePartialLoader.addPart('library');
	
	restService.all("book_type").customPOST({startIndex: 0, resultQuantity: 100000}, "list").then(function(data){
		$scope.books = data.originalElement.resultList;
	});
	
	restService.all("category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.categories = data.originalElement;
	});
	restService.all("book_status").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.statuses = data.originalElement;
	});
	
	$scope.submitted = function(){
	restService.one("book").customPOST($scope.book, "create").then(function(data){
		alert("success");
		$location.path("/books");
	});
	};
}])
.controller('editBookCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$routeParams', '$filter', 'Utils', '$location','$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $routeParams, $filter, Utils, $location,$translatePartialLoader){

	$translatePartialLoader.addPart('library');

	
	$scope.bookId = $routeParams.id;
	$scope.book = {};
	$scope.submitt = false;
	
	restService.all("book_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.books = data.originalElement.resultList;
	});
	
	restService.one("book").customGET($scope.bookId).then(function(data){
		$scope.book = data.originalElement;
	});

	restService.all("book_status").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.statuses = data.originalElement;
	});
	
	$scope.submitted = function(){
		$scope.submitted = true;
		restService.one("book").customPOST($scope.book, "update/"+$scope.bookId).then(function(data){
			alert("Обновлено!");
			$location.path("/books");
		});
	};
	
	$scope.deleteBook = function(){
		var yesNo = Utils.callDeleteModal("book", $routeParams.id);
	};	
	
}]);


