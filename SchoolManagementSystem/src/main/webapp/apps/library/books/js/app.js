'use strict'; 

angular.module('libApp', ['controllers', 'common.apps']).
config(['$routeProvider',   function ($routeProvider) {
					

	$routeProvider.when('/book', {
		templateUrl : 'views/books.html',
		controller : 'bookCtrl',
	});
	$routeProvider.when('/add_book', {
		templateUrl : 'views/add_book.html',
		controller : 'addBookCtrl',
	});
	$routeProvider.when('/edit_book/:id', {
		templateUrl : 'views/edit_book.html',
		controller : 'editBookCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/book'
	});
  			
}]);	


