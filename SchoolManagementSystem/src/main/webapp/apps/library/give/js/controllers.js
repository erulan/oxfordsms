'use strict';

/* Controllers */
	
angular.module('controllers', [])	
.controller('give_listCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){
	
	$translatePartialLoader.addPart('library');
	$scope.ownerId = 24;
	$scope.$watch("count", function (newVal, oldVal) {
	    $scope.tableParamsList.count(newVal);
	});
	$scope.$watch("tableParamsList.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	$scope.getTitle = function(title){
		return $('.'+title).text();
	}
	
	$scope.tableParamsList = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            id: 'desc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "book_in_use");
        },
    });
	$scope.deleteBook = function(id){
		Utils.callDeleteModal("book_in_use", id);
	};		
	restService.all("student").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"id": $scope.ownerId}}, "list"  ).then(function(data){
		$scope.owners = data.originalElement.resultList;
	});
	
	
}])
.controller('giveCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader','$location',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader,$location){
	
	$translatePartialLoader.addPart('library');

	/*restService.all("book").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.books = data.originalElement.resultList;
	});*/
	
	
	$scope.$watch("count", function (newVal, oldVal) {
	    $scope.tableParamsBook.count(newVal);
	});
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	$scope.getTitle = function(title){
		return $('.'+title).text();
	}
	$scope.ngTableGetData = function($defer, params, path) {
    	
		var searchParams = {};
    	var prm = params.parameters();

    	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
    	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
    	
    	searchParams["searchParameters"] = {};
    	angular.forEach(prm.filter, function(value, key){
    		searchParams.searchParameters[key] = value;
    	});
		searchParams.searchParameters["bookStatusId"] = 1;

    	searchParams["orderParamDesc"] = {};
    	angular.forEach(prm.sorting, function(value, key){
    		searchParams.orderParamDesc[key] = (value == "desc");
    	});

    	restService.all(path).customPOST(searchParams, ['list']).then(function(data){
    		params.total(data.originalElement.totalRecords);
            // set new data
            $defer.resolve(data.originalElement.resultList);
		});
    },
	$scope.tableParamsBook = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            id: 'desc'     // initial sorting
        },				
    }, {
        total: 0, // length of data        
        getData: function($defer, params){
        	$scope.ngTableGetData($defer, params, "book");
        },
    });
	
//	$scope.$watch("count", function (newVal, oldVal) {
//	    $scope.tableParamsNew.count(newVal);
//	});
	
	$scope.deleteBook = function(id){
		Utils.callDeleteModal("book_in_use", id);
		$location.path("/give_list");
	};		
	

	$scope.owner = {};
	$scope.ownerType = {};
	$scope.book_in_use = {};
	$scope.book_in_use.ownerType = {id: 1};
	$scope.datepicker = {};
	
	restService.all("owner_type").customPOST({"startIndex": 0, "resultQuantity": 1000, "searchParameters": {"notName": "no owner"}}, ["list"]).then(function(results){
		$scope.ownerTypes = results;
		console.log(results);
	});	
	
	$scope.tableParamsStudent = getTableParams("student");
	$scope.tableParamsTeacher = getTableParams("teacher");
	$scope.tableParamsStuff = getTableParams("stuff");
	
	 function getTableParams(ownerType){
		return new ngTableParams({
	        page: 1,            // show first page
	        count: 10, 			// count per page
	        sorting: {
	            id: 'desc'     // initial sorting
	        },				
	    }, {
	        total: 0, // length of data
	        getData: function($defer, params){
	        	Utils.ngTableGetData($defer, params, ownerType);
	        },
	    });
	}
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.open = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	
		$scope.datepicker.open = function($event) {
		    $event.preventDefault();
		    $event.stopPropagation();
	
		    $scope.datepicker.opened = true;
		  };
		  
		  $scope.datepicker.format = 'dd/MM/yyyy';
	
		  $scope.datepicker.dateOptions = {
		    formatYear: 'yy',
		    startingDay: 1,
		    language: 'ru'
		  };
		
	
		  
	$scope.saveGiveBook = function(){
		console.log($scope);
		restService.one("book_in_use").customPOST($scope.book_in_use, "create").then(function(data){
			alert("New book successfully will given!");
			$location.path("/give_list");
		});
	};
}])

/*.controller('give_returnCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', '$routeParams','$location',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader, $routeParams, $location){
		
		$translatePartialLoader.addPart('library');
		
		$scope.datepicker = {};
		$scope.book_in_use = {};
		$scope.book_in_useId = $routeParams.id;
		
		restService.all("book_in_use").customGET($scope.book_in_useId).then(function(data){
			$scope.book_in_use = data.originalElement;
		});
		
		
		
	$scope.datepicker.open = function($event) {
		
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  }
	  
	  $scope.update = function() {
			restService.one("book_in_use").customPOST($scope.book_in_use, "update/" + $scope.book_in_useId).then(function(data){
				alert("Successfully updated!");
				$location.path("/return_list");
				
			});
		};	
}])*/;


