'use strict'; 

angular.module('libGiveApp', ['controllers', 'common.apps']).
config(['$routeProvider',   function ($routeProvider) {
					

	$routeProvider.when('/give', {
		templateUrl : 'views/give.html',
		controller : 'giveCtrl',
	});
	$routeProvider.when('/give_list', {
		templateUrl : 'views/give_list.html',
		controller : 'give_listCtrl',
	});
	$routeProvider.when('/give_return/:id', {
		templateUrl : 'views/give_return.html',
		controller : 'give_returnCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/give_list'
	});
  			
}]);	


