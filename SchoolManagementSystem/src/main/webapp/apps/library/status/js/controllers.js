'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('statusCtrl', ['$scope', '$modal', '$log', 'restService', 'Utils','$translatePartialLoader','$location', function($scope, $modal, $log, restService, Utils,$translatePartialLoader, $location){
	  					
	$translatePartialLoader.addPart('library');

	$scope.deneme = "denemee";	
	restService.all("book_status").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.statuses = results.originalElement;
	});		
	
	$scope.save = function(){
		restService.one("book_status").customPOST($scope.status, "create").then(function(data){
			alert("New status successfully added!");
			$location.path("#/status");
		});
	};
	
	$scope.deleteStatus = function(id){
		 Utils.callDeleteModal("book_status", id);
	};
	  			
}])

.controller('editStatusCtrl',  ['$scope', 'restService', '$routeParams', "$filter", "Utils","$location", function($scope, restService, $routeParams, $filter, Utils, $location){
	$scope.statusId = $routeParams.id;
	$scope.status = {};
	$scope.submitStatus = false;
	
	restService.all("book_status").customGET($scope.statusId).then(function(data){
		$scope.status = data.originalElement;
	});
		
	$scope.update = function(){
		$scope.submitStatus = true;
		restService.one("book_status").customPOST($scope.status, "update/"+$scope.statusId).then(function(data){
			alert("Status successfully updated!");
			$location.path("/status");
		});
	};
	
}]);


