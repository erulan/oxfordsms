'use strict'; 

angular.module('libApp', ['controllers', 'common.apps']).
config(['$routeProvider',   function ($routeProvider) {
					

	$routeProvider.when('/status', {
		templateUrl : 'views/statuses.html',
		controller : 'statusCtrl',
	});
	$routeProvider.when('/book_status', {
		templateUrl : 'views/statuses.html',
		controller : 'statusCtrl',
	});
	$routeProvider.when('/add_status', {
		templateUrl : 'views/add_status.html',
		controller : 'addStatusCtrl',
	});
	$routeProvider.when('/edit_status/:id', {
		templateUrl : 'views/edit_status.html',
		controller : 'editStatusCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/status'
	});
  			
}]);	


