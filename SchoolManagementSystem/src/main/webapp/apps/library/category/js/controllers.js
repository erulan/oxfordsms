'use strict';
	
angular.module('controllers', [])		
.controller('categoryCtrl', ['$scope', '$modal', 'restService', '$routeParams', 'Utils', "$location","$translatePartialLoader",function($scope, $modal, restService, $routeParams,  Utils,$location,$translatePartialLoader){
	
	$translatePartialLoader.addPart('library');
	
	$scope.deneme = "denemee";		
	restService.all("category").customPOST({"startIndex": 0, "resultQuantity": 1000}, "list").then(function(results){
		$scope.categories = results.originalElement;
	});	
	
	$scope.save = function(){
		alert($scope.category.name);
		console.log('scope is', $scope);
		restService.one("category").customPOST($scope.category, "create").then(function(data){
			alert("New category successfully added!");
			$location.path("#/category");
		});
	};
	$scope.deleteCategory = function(id){
			Utils.callDeleteModal("category", id);
			};	

			
}])

.controller('editCategoryCtrl',  ['$scope', 'restService', '$routeParams', "Utils","$location",'$translatePartialLoader', function($scope, restService, $routeParams, Utils, $location,$translatePartialLoader){
	
	$translatePartialLoader.addPart('library');
	
	$scope.categoryId = $routeParams.id;
	$scope.category = {};
	$scope.submitt = false;
	
	restService.all("category").customGET($scope.categoryId).then(function(data){
		$scope.category = data.originalElement;
	});

	$scope.update = function(){
		$scope.submitt= true;
		restService.one("category").customPOST($scope.category, "update/"+$scope.categoryId).then(function(data){
			alert("Category name was successfully updated!");
			$location.path("/category");
		});
	};
	
	$scope.deleteCategory = function(){
		var yesNo = Utils.callDeleteModal("category", $routeParams.id);
	};
	
}]);


