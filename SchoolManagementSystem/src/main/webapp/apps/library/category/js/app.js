'use strict'; 

angular.module('libApp', ['controllers', 'common.apps']).
config(['$routeProvider',   function ($routeProvider) {
					

	$routeProvider.when('/category', {
		templateUrl : 'views/category.html',
		controller : 'categoryCtrl',
	});
	$routeProvider.when('/add_category', {
		templateUrl : 'views/add_category.html',
		controller : 'addCategoryCtrl',
	});
	$routeProvider.when('/edit_category/:id', {
		templateUrl : 'views/edit_category.html',
		controller : 'editCategoryCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/category'
	});
  			
}]);	


