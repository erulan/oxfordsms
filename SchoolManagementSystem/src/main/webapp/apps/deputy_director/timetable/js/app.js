'use strict'; 

angular.module('appTable', ['common.apps','controllers']).
config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/addTimetable', {
		templateUrl : 'views/timetable.html',
		controller : 'timetableCtrl',
	});
	
	$routeProvider.when('/addTimetable',{
		templateUrl : 'views/addTimetable.html',
		controller : 'addTimetableCtrl',
	});

	$routeProvider.when('/editTimetable/:id',{
		templateUrl : 'views/editTimetable.html',
		controller : 'editTimetableCtrl',
	});
	
	$routeProvider.when('/choose_timetable',{
		templateUrl : 'views/choose_timetable.html',
		controller : 'chooseTimetableCtrl',
	});
	
	$routeProvider.otherwise({
		redirectTo : '/addTimetable'
	});
  			
}]);	


//'common.apps'