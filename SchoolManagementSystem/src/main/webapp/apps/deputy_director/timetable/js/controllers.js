'use strict';

/* Controllers */
	
angular.module('controllers', [ ])		
.controller('timetableCtrl', ['$scope', '$routeParams', 'restService', '$compile', '$translatePartialLoader',function($scope, $routeParams, restService, $compile,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('secretary');

	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });

	$scope.getTitle = function(title){
		return $('.'+title).text();
	}
	$scope.timetable = {};
	$scope.stNum = 10;
	
	restService.all("student").customGET( $scope.stNum ).then(function(data){
		$scope.student = data.originalElement;
		
		restService.all("timetable").customPOST( {"startIndex": 0, resultQuantity: 1000, searchParameters: {"cGroupId": $scope.student.classGroup.id}}, "list").then(function(data){
			$scope.timetables = data.originalElement.resultList;
		});
	});
	
//	$scope.$watch('timetables', function(newValue, oldValue) {
//		$scope.$apply('timetables');
//		});
	
	restService.all("week_days").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.weekDays = data.originalElement;
	});
	restService.all("class_hours").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.classHours = data.originalElement;
	});
	
	$scope.getCourseName = function(classHourId, weekDayId){
		angular.forEach($scope.timetables, function(value){
			if(value.classHours.id == classHourId && value.weekDays.id == weekDayId){
				return value.course.lesson.name;
			}
		});
		return "No Course";
	};
	
	  				
}])
.controller('addTimetableCtrl', ['$scope', '$routeParams', 'restService', '$compile', 'Utils', '$rootScope', "$route",'$translatePartialLoader', function($scope, $routeParams, restService, $compile, Utils, $rootScope, $route,$translatePartialLoader) {	
		
	
	$translatePartialLoader.addPart('secretary');

	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });

	$scope.getTitle = function(title){
		return $('.'+title).text();
	}
	$scope.timetable = {};
	$scope.stNum = $rootScope.userInfo.id;
	$scope.timetableArray = [];
	$scope.course = [];
	$scope.classGroup = {};
	
	$scope.$watch('classGroup.id',function(newValue, oldValue) {
		$scope.getTimetable(newValue);
	});
	
	restService.all("week_days").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.weekDays = data.originalElement;
	});
	restService.all("class_hours").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.classHours = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroups = data.originalElement;
		$scope.classGroup.id = $scope.CGroups[0].id != null ? $scope.CGroups[0].id : null;
	});
	
	$scope.getTimetable = function(id){
		if(id == null){return false;}
		
		$scope.timetableArray = [];
		restService.all("timetable").customPOST( {"startIndex": 0, resultQuantity: 1000, searchParameters: 
		{"cGroupId": id}}, "list").then(function(data){
			$scope.timetables = data.originalElement.resultList;
			
			angular.forEach($scope.classHours, function(classHour, ch_key){
				var object = [];
				object[0] = classHour;
				angular.forEach($scope.weekDays, function(weekDay, wd_key){
					var timetableObj = {};
					timetableObj.course = {};
					angular.forEach($scope.timetables, function(timetable, tt_key){
						if(timetable.weekDays.id == weekDay.id && timetable.classHours.id == classHour.id){
							timetableObj = timetable;
						}
					});
					timetableObj.weekDays = weekDay;
					timetableObj.classHours = classHour;
					object[wd_key + 1] = timetableObj;
				});
				$scope.timetableArray.push(object);
			});
			
		});
		
		restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000, searchParameters: {"cGroupId": id }}, "list").then(function(data){
			$scope.courses = data.originalElement.resultList;
		});
		
	}

	$scope.save = function(){
		$scope.timetableSave = [];
		angular.forEach($scope.timetableArray, function(timetableRow, row_key){
			angular.forEach(timetableRow, function(timetableCell, cell_key){
				if(cell_key != 0 && timetableCell.course.id != null && timetableCell.course.id != "noclass"){
					$scope.timetableSave.push({ "classHours": {"id":timetableCell.classHours.id}, "course": {"id":timetableCell.course.id}, "weekDays": {"id" :timetableCell.weekDays.id}});
				}
			});
		});
		var object = {"classGroup": {"id": $scope.classGroup.id}, "timetables": $scope.timetableSave};
		restService.one("timetable").customPOST(object, "create_timetable").then(function(data){
			alert("success");
			$scope.getTimetable($scope.classGroup.id);
		});
	};
}])

.controller('chooseTimetableCtrl', ['$scope', '$routeParams', 'restService', '$compile',function($scope, $routeParams, restService, $compile ) {	

	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroups = data.originalElement;
	});				
}])

.controller('editTimetableCtrl', ['$scope', '$routeParams', 'restService', '$compile', 'Utils', function($scope, $routeParams, restService, $compile, Utils ) {	
	  						
	$scope.timetable = {};
	$scope.stNum = 10;
	
	restService.all("student").customGET( $scope.stNum ).then(function(data){
		$scope.student = data.originalElement;
		
		restService.all("timetable").customPOST( {"startIndex": 0, resultQuantity: 1000, searchParameters: {"cGroupId": $scope.student.classGroup.id}}, "list").then(function(data){
			$scope.timetables = data.originalElement.resultList;
		});
	});
	$scope.$watch('timetables', function(newValue, oldValue) {
		$scope.$apply('timetables');
		});
	
	restService.all("week_days").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.weekDays = data.originalElement;
	});
	restService.all("class_hours").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.classHours = data.originalElement;
	});
	restService.all("lesson").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.lessons = data.originalElement;
	});
	restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.courses = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroups = data.originalElement;
	});
	
	$scope.getCourseName = function(classHourId, weekDayId){
		angular.forEach($scope.timetables, function(value){
			if(value.classHours.id == classHourId && value.weekDays.id == weekDayId){
				return value.course.lesson.name;
			}
		});
		return "No course";
	};
	
	$scope.save = function(){
		angular.forEach($scope.courses.classGroup.students, function(value, key){
			$scope.timeTable.push({ "classHours": {"id":classHour.id}, "course": {"id":lesson.id}, "weekDays": {"id" :weekDay.id}});
		});
		var object = {"classGroup": $scope.classGroup, "timetables": $scope.timeTable};
		restService.one("timetable").customPOST(object, "create_timetable").then(function(data){
			alert("success");
		});
	};
	  				
}]);
//'restService',restService