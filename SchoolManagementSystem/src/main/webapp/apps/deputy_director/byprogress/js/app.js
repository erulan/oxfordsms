'use strict'; 

angular.module('byprogressApp', ['controllers','common.apps']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/byprogress', {
		templateUrl : 'views/byprogress.html',
		controller : 'byprogressCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/byprogress'
	});
  			
}]);	


