'use strict'; 

angular.module('teacherAppS', ['common.apps', 'controllers']).
config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/teacher', {
		templateUrl : 'views/teacher.html',
		controller : 'teachersCtrl',
	});
	$routeProvider.when('/add_teacher', {
		templateUrl : 'views/add_teacher.html',
		controller : 'add_teacherCtrl',
	});
	$routeProvider.when('/edit_teacher/:id', {
		templateUrl : 'views/edit_teacher.html',
		controller : 'edit_teacherCtrl',
	});
	$routeProvider.when('/teacher_profile/:id', {
		templateUrl : 'views/teacher_profile.html',
		controller : 'teacher_profileCtrl',
	});
	/*$routeProvider.when('/delete', {
		templateUrl : 'views/modal.html',
		controller : 'ModalInstanceCtrl',
	}); */       
	$routeProvider.otherwise({
		redirectTo : '/teacher'
	});
	
}]);


