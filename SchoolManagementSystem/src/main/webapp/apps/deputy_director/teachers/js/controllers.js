'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('teachersCtrl',  ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){	
	  					
	$translatePartialLoader.addPart('secretary');

	
	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	$scope.getTitle = function(title){
		return $("."+title).text();
	}
	
	
	$scope.deneme = "denemee";	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
        	name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "teacher");
        },
    });
	$scope.datepicker = {};
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  $scope.deleteTeacher = function(id){
			Utils.callDeleteModal("teacher", id);
	};
			
}])
.controller('add_teacherCtrl', ['$scope',  '$routeParams', 'restService','$location','$translatePartialLoader', function ( $scope, $routeParams, restService, $location,$translatePartialLoader) {	
	  				
	$translatePartialLoader.addPart('secretary');

	$scope.datepicker = {};
	$scope.teacher = {};
	$scope.deneme = "denemee";	
	
	restService.all("blood").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.bloods = data.originalElement;
	});
	restService.all("nationality").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.nationalities = data.originalElement;
	});
	restService.all("country").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.countrys = data.originalElement;
	});
	restService.all("oblast").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.oblasts = data.originalElement;
	});
	restService.all("region").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.regions = data.originalElement;
	});
	$scope.save = function() {
		restService.one("teacher").customPOST($scope.teacher, "create").then(function(data){
			alert("New teacher was successfully registered!");
			$location.path("/teacher");
		});
	};

	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	$scope.showTeacher = function() {
			console.log($scope.teacher);
	};		
}])
.controller('edit_teacherCtrl', ['$scope',  '$routeParams','restService','$location','$translatePartialLoader', function ( $scope, $routeParams, restService, $location,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('secretary');

	$scope.datepicker = {};	
	$scope.teacher = {};

	$scope.teacherId = $routeParams.id;
	$scope.deneme = "denemee";	
	
	restService.all("teacher").customGET( $routeParams.id ).then(function(data){
		$scope.teacher = data.originalElement;
	});
	
	restService.all("blood").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.bloods = data.originalElement;
	});
	restService.all("nationality").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.nationalities = data.originalElement;
	});
	restService.all("country").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.countrys = data.originalElement;
	});
	restService.all("oblast").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.oblasts = data.originalElement;
	});
	restService.all("region").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.regions = data.originalElement;
	});
	$scope.update = function() {
		restService.one("teacher").customPOST($scope.teacher, "update/"+ $scope.teacherId).then(function(data){
			alert("Personal information was successfully updated! ");
			$location.path("/teacher");
		});
	};
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	$scope.showTeacher = function() {
			console.log($scope.teacher);
	};	  			
}])
.controller('teacher_profileCtrl', ['$scope', '$modal', '$routeParams','restService', '$log','$translatePartialLoader',  function ( $scope, $modal, $routeParams, restService,$log, $translatePartialLoader) {		
	
	$translatePartialLoader.addPart('secretary');

	$scope.teacher = {};
	
	restService.all("teacher").customGET( $routeParams.id ).then(function(data){
		$scope.teacher = data.originalElement;
	});
	
	  			
}])
