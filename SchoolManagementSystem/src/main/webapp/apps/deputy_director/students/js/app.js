'use strict'; 

angular.module('studentsApp', ['controllers', 'common.apps']).
config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/students', {
		templateUrl : 'views/students.html',
		controller : 'studentsCtrl',
	});
	$routeProvider.when('/student', {
		templateUrl : 'views/students.html',
		controller : 'studentsCtrl',
	});
	$routeProvider.when('/add_student', {
		templateUrl : 'views/add_student.html',
		controller : 'add_studentCtrl',
	});
	$routeProvider.when('/edit_student/:id', {
		templateUrl : 'views/edit_student.html',
		controller : 'edit_studentCtrl',
	});
	$routeProvider.when('/student_profile/:id', {
		templateUrl : 'views/student_profile.html',
		controller : 'student_profileCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/students'
	});
	
}]);


