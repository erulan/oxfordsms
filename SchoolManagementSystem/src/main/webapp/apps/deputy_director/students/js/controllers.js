'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('studentsCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils',"$location", '$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $location,$translatePartialLoader){
	
	$translatePartialLoader.addPart('secretary');

	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});
	$scope.getTitle = function (title){
		return $("."+title).text();
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "student");
        },
    });
	
	 $scope.deleteStudent = function(id){
			Utils.callDeleteModal("student", id);
			$location.path("/students");
	};

}])

.controller('add_studentCtrl', ['$scope', '$modal', '$routeParams','restService', '$log', '$location', '$translatePartialLoader', function ( $scope, $modal, $routeParams, restService, $log, $location,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('secretary');

	$scope.student = {};
	$scope.datepicker = {};
	$scope.deneme = "denemee";		
	
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroups = data.originalElement;
	});
	restService.all("blood").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.bloods = data.originalElement;
	});
	restService.all("status").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.statuss = data.originalElement;
	});
	restService.all("nationality").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.nationalities = data.originalElement;
	});
	restService.all("country").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.countrys = data.originalElement;
	});
	restService.all("oblast").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.oblasts = data.originalElement;
	});
	restService.all("region").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.regions = data.originalElement;
	});
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});
	$scope.save = function() {
		restService.one("student").customPOST($scope.student, "create").then(function(data){
			alert("New student was successfully registered!");
			$location.path("/students");
			
		});
	};	  	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
.controller('edit_studentCtrl', ['$scope', '$modal', '$routeParams','restService', '$log','$location', '$translatePartialLoader', function ( $scope, $modal, $routeParams, restService,$log, $location,$translatePartialLoader) {	
	  						
	$translatePartialLoader.addPart('secretary');

	$scope.datepicker = {};
	$scope.student = {};
	$scope.studentId = $routeParams.id;
	$scope.submitted = false;

	$scope.deneme = "denemee";	
		
	restService.all("student").customGET( $routeParams.id ).then(function(data){
		$scope.student = data.originalElement;
		$scope.datepicker.date = data.originalElement.birthDate;
	});
	
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("blood").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.bloods = data.originalElement;
	});
	restService.all("status").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.statuss = data.originalElement;
	});
	restService.all("nationality").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.nationalities = data.originalElement;
	});
	restService.all("country").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.countrys = data.originalElement;
	});
	restService.all("oblast").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.oblasts = data.originalElement;
	});
	restService.all("region").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.regions = data.originalElement;
	});
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});
	
	$scope.submitt = function(){
		$scope.submitted = true;
		restService.one("student").customPOST($scope.student, "update/" + $scope.studentId).then(function(data){
			alert("Personal information was successfully updated! ");
			$location.path("/students");
		});
	};

	//this is datepicker
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
.controller('student_profileCtrl', ['$scope', '$modal', '$routeParams','restService', '$log','$translatePartialLoader',  function ( $scope, $modal, $routeParams, restService,$log,$translatePartialLoader) {	

	$translatePartialLoader.addPart('secretary');

	$scope.student = {};
	
	restService.all("student").customGET( $routeParams.id ).then(function(data){
		$scope.student = data.originalElement;
	});
	
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("studAccountings").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.studAccountingses = data.originalElement;
	});
}])


