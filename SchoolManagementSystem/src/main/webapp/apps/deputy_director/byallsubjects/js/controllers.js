'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('byallsubjectsCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){
	
	$translatePartialLoader.addPart('deputy_director');
	$scope.lesson = {};
	$scope.c_group = {};
	$scope.quarter = {};

	restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.courses = data.originalElement;
	});
	restService.all("lesson").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.lessons = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("quarter").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.quarters = data.originalElement;
	});
	restService.all("student").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.students = data.originalElement.resultList;
	});
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});	
	restService.all("teacher").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.teachers = data.originalElement;
	});	

	
}]);


