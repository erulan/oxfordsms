'use strict'; 

angular.module('byallsubjectsApp', ['controllers','common.apps']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/byallsubjects', {
		templateUrl : 'views/byall.html',
		controller : 'byallsubjectsCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/byallsubjects'
	});
  			
}]);	


