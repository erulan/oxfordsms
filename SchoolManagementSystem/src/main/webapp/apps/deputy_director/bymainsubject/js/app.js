'use strict'; 

angular.module('bymainsubjectApp', ['controllers','common.apps']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/bymainsubject', {
		templateUrl : 'views/mainsubject.html',
		controller : 'bymainsubjectCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/bymainsubject'
	});
  			
}]);	


