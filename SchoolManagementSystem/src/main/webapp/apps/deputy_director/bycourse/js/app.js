'use strict'; 

angular.module('bycourseApp', ['controllers','common.apps']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/bycourse/#/mon_by_course', {
		templateUrl : 'views/example.html',
		controller : 'bycourseCtrl',
	});
	$routeProvider.when('/mon_by_course', {
		templateUrl : 'views/monitoring_by_course.html',
		controller : 'mon_by_courseCtrl',
	});
	$routeProvider.when('/mon_by_class', {
		templateUrl : 'views/monitoring_by_class.html',
		controller : 'mon_by_classCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/bycourse/#/mon_by_course'
	});
  			
}]);	


