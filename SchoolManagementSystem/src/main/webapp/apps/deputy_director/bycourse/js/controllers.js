'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('mon_by_courseCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){
	
	$translatePartialLoader.addPart('deputy_director');
	$scope.lesson = {};
	$scope.c_group = {};
	$scope.quarter = {};
	$scope.yearId = {};
	$scope.courseId = {};
	$scope.cGroupId = {};
		
	
	restService.all("lesson").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.lessons = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});
	
//	restService.all("course").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"c_groupId": $scope.c_groupId}}, "list"  ).then(function(data){
//		$scope.courses = data.originalElement.resultList;
//	});
	
	$scope.$watch('course.classGroup.id', function(newValue, oldValue) {
		if (newValue === oldValue) { return; }
		restService.all("course").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"cGroupId": newValue}}, "list"  ).then(function(data){
			$scope.courses = data.originalElement.resultList;
		});
   });

	$scope.sortList = function(){
		restService.all("deputy_director").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"courseId": $scope.courseId, "cGroupId": $scope.c_groupId, "yearId": $scope.yearId}}, "by_course"  ).then(function(data){
			$scope.coursenotes = data.originalElement.resultList;
		});
	};
}])
.controller('mon_by_classCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){
	
	$translatePartialLoader.addPart('deputy_director');
	$scope.lesson = {};
	$scope.c_group = {};
	$scope.quarter = {};
	$scope.yearId = {};
	$scope.courseId = {};
	$scope.cGroupId = {};
	$scope.quarterId = {};
	
	restService.all("lesson").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.lessons = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});
	restService.all("quarter").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.quarters = data.originalElement;
	});
	
	$scope.$watch('c_groupId', function(newValue, oldValue) {
		if (newValue === oldValue) { return; }
		restService.all("course").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"cGroupId": newValue}}, "list"  ).then(function(data){
			$scope.courses = data.originalElement.resultList;
		});
   });

	$scope.sortList = function(){
		restService.all("deputy_director").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"quarterId": $scope.quarterId, "cGroupId": $scope.cGroupId,  "yearId": $scope.yearId}}, "by_c_group"  ).then(function(data){
			$scope.classnotes = data.originalElement.resultList;
		});
	};
	
}]);


