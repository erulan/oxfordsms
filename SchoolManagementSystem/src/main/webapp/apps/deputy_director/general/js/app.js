'use strict'; 

angular.module('generalApp', ['controllers','common.apps']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/general', {
		templateUrl : 'views/general.html',
		controller : 'GeneralCtrl',
	});
	$routeProvider.when('/charts', {
		templateUrl : 'views/charts.html',
		controller : 'chartsCtrl',
	});
	$routeProvider.when('/rating', {
		templateUrl : 'views/rating.html',
		controller : 'ratingCtrl',
	});
	$routeProvider.when('/recommend', {
		templateUrl : 'views/recommend.html',
		controller : 'recommendCtrl',
	});

	$routeProvider.when('/tasks', {
		templateUrl : 'views/tasks.html',
		controller : 'tasksCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/general'
	});
  			
}]);	


