'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('GeneralCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', 
                    function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $translatePartialLoader){
	
/*	$translatePartialLoader.addPart('deputy_director');*/
	$scope.lesson = {};
	$scope.c_group = {};
	$scope.quarterId = {};

	restService.all("quarter").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.quarters = data.originalElement;
	});
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});	
	$scope.sortList = function(){
		restService.all("deputy_director").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"quarterId": $scope.quarterId, "yearId": $scope.yearId}}, "general"  ).then(function(data){
			$scope.generalnotes = data.originalElement.resultList;
		});
	};
	
}])
.controller('chartsCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $translatePartialLoader){
	
	/*$translatePartialLoader.addPart('deputy_director');*/
	
	/*var sucPercent = 68;
	var left = 100 - sucPercent;*/
	 $(document).ready(function(){
	$("#sparkline").sparkline([55,12], {
	    type: 'pie',
	    width: '200',
	    height: '200'
	});
	
	$("#sparkline2").sparkline([20,70], {
	    type: 'pie',
	    width: '200',
	    height: '200'
	});
	
	$("#sparkline3").sparkline([40,10], {
	    type: 'pie',
	    width: '200',
	    height: '200'
	});
	 });
	
}])
.controller('ratingCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $translatePartialLoader){
	
	/*$translatePartialLoader.addPart('deputy_director');*/
	$scope.lesson = {};
	$scope.c_group = {};
	$scope.quarter = {};

	restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.courses = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("quarter").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.quarters = data.originalElement;
	});
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});	
}])
.controller('recommendCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $translatePartialLoader){
	
	/*$translatePartialLoader.addPart('deputy_director');*/
	$scope.lesson = {};
	$scope.c_group = {};
	$scope.quarter = {};

	restService.all("course").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.courses = data.originalElement;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("quarter").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.quarters = data.originalElement;
	});
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});	
}])
//.controller('recommendCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $translatePartialLoader){
//	
//	/*$translatePartialLoader.addPart('deputy_director');*/
//	
//}])
.controller('tasksCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils, $translatePartialLoader){
	
	/*$translatePartialLoader.addPart('deputy_director');*/
	
}]);


