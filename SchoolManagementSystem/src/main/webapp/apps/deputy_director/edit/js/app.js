'use strict'; 

angular.module('appEdit', ['common.apps','controllers']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/edit', {
		templateUrl : 'views/edit.html',
		controller : 'editCtrl',
	});

	$routeProvider.otherwise({
		redirectTo : '/edit'
	});
  			
}]);	


