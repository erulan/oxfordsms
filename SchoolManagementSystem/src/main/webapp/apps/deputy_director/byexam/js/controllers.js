'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('byexamCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){
	
	$translatePartialLoader.addPart('deputy_director');
	$scope.cGroupId = {};
	$scope.yearId = {};


	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.CGroupes = data.originalElement;
	});
	restService.all("year").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.years = data.originalElement;
	});	
	
	$scope.sortList = function(){
		restService.all("deputy_director").customPOST({startIndex: 0, resultQuantity: 10000, searchParameters: {"cGroupId": $scope.cGroupId, "yearId": $scope.yearId}}, "by_exam"  ).then(function(data){
			$scope.examnotes = data.originalElement.resultList;
		});
	};
}]);


