'use strict'; 

angular.module('byexamApp', ['controllers','common.apps']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/byexam', {
		templateUrl : 'views/byexam.html',
		controller : 'byexamCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/byexam'
	});
  			
}]);	


