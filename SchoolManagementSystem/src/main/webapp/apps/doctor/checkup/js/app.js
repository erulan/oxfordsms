'use strict'; 

angular.module('teachersApp', ['controllers', 'common.apps']).
config(['$routeProvider', '$locationProvider',  function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/checkup', {
		templateUrl : 'views/checkup.html',
		controller : 'checkupCtrl',
	});
	$routeProvider.when('/checkupResults', {
		templateUrl : 'views/checkupResults.html',
		controller : 'checkupResultsCtrl',
	});
	$routeProvider.when('/edit_checkup/:id', {
		templateUrl : 'views/edit_checkup.html',
		controller : 'EditCheckupCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/checkup'
	});
  			
}]);	


