'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('checkupCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){	
	  					
	$translatePartialLoader.addPart('doctor');
	
	$scope.period = {};
	$scope.group = {};
	$scope.student = {};
	$scope.students = {};
	$scope.schoolType = {};
	$scope.schoolType.id = 2;
	
	restService.all("check_period").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.periods = results.resultList;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000,  searchParameters: {"schoolTypeId": 2}}, "list").then(function(data){
		$scope.groups = data.originalElement;
	});	
	restService.all("school_type").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.schoolTypes = results.resultList;
	});
	
//	$scope.getGroupList = function(){
//		if($scope.schoolType.id != null){
//			restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000,  searchParameters: {"schoolTypeId": $scope.schoolType.id}}, "list").then(function(data){
//				$scope.groups = data.originalElement;
//			});
//		}
//	}

	$scope.getStudentList = function(){
		if($scope.period.id != null && $scope.group.id != null){
			restService.all("checkup").customPOST({"startIndex": 0, "resultQuantity": 1000, "searchParameters": {"periodId": $scope.period.id, "CGroupId" : $scope.group.id}}, ["list"]).then(function(results){
					$scope.students = results.resultList;
					console.log($scope.students);
					});
			restService.all("checkup").customPOST({"startIndex": 0, "resultQuantity": 1000, "searchParameters": {"periodId": $scope.period.id, "CGroupId" : $scope.group.id}}, ["list"]).then(function(results){
				$scope.students2 = results.resultList;
				for (var i = 0; i < $scope.students2.length; i++) {																																																																																																																																																																		
					if($scope.students2[i].weight!=0 || $scope.students2[i].height!=0 || $scope.students2[i].weightFH!=0 ){
//						console.log("got u!");
//						$('.check_button_'+$scope.students2[i].student.id).hide();
//						$('.check_icon_'+$scope.students2[i].student.id).show();
					 
					}
			 }
				});	
		}
	}

	
	$scope.submitResult = function(checkupId, studentId,h,w,wfh){
		$scope.checkup={};
		$scope.checkup.student ={};
		$scope.checkup.period={};
		$scope.getStudent = {};
		if($scope.period.id!=null){
			if(h!=null){
				if(w!=null){
						$scope.checkup.student.id = studentId;
						$scope.checkup.height = h;
						$scope.checkup.weight = w;
						$scope.checkup.weightFH = wfh;
						$scope.checkup.period.id = $scope.period.id;
						if(checkupId!= null){
							$scope.checkup.id = checkupId;
							restService.one("checkup").customPOST($scope.checkup, "update/"+checkupId).then(function(data){
								console.log("update");
								alert("successfully updated!");
								$('.check_button_'+studentId).hide(1000);
								$('.check_icon_'+studentId).show(1000);
							});
						}else{
							restService.one("checkup").customPOST($scope.checkup, "create").then(function(data){
								alert("successfully created!");
							});
							console.log("create");
							$('.check_button_'+studentId).hide(1000);
							$('.check_icon_'+studentId).show(1000);
						}
						
				}else{
					alert("Please enter weight!");
				}
			}else{
				alert("Please enter height!");
			}
		}else{
			alert("Please Select check up period!");
		}
	}
	
}])
.controller('checkupResultsCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$translatePartialLoader',function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader){	
	  					
	$translatePartialLoader.addPart('doctor');
	
	$scope.period = {};
	$scope.group = {};
	$scope.student = {};
	$scope.students = {};
	$scope.schoolType = {};
	$scope.schoolType.id = 2;
	
	restService.all("check_period").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.periods = results.resultList;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000,  searchParameters: {"schoolTypeId": 2}}, "list").then(function(data){
		$scope.groups = data.originalElement;
	});	
	restService.all("school_type").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.schoolTypes = results.resultList;
	});
	
//	$scope.getGroupList = function(){
//		if($scope.schoolType.id != null){
//			restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000,  searchParameters: {"schoolTypeId": $scope.schoolType.id}}, "list").then(function(data){
//				$scope.groups = data.originalElement;
//			});
//		}
//	}

	$scope.getStudentList = function(){
		if($scope.period.id != null && $scope.group.id != null){
			restService.all("checkup").customPOST({"startIndex": 0, "resultQuantity": 1000, "searchParameters": {"periodId": $scope.period.id, "CGroupId" : $scope.group.id}}, ["list"]).then(function(results){
					$scope.students = results.resultList;
					console.log($scope.students);
					});
			restService.all("checkup").customPOST({"startIndex": 0, "resultQuantity": 1000, "searchParameters": {"periodId": $scope.period.id, "CGroupId" : $scope.group.id}}, ["list"]).then(function(results){
				$scope.students2 = results.resultList;
				for (var i = 0; i < $scope.students2.length; i++) {																																																																																																																																																																		
					if($scope.students2[i].heightComment!=null || $scope.students2[i].weightComment!=null || $scope.students2[i].weightFHComment!=null){
//						$('.check_button_'+$scope.students2[i].student.id).hide();
//						$('.check_icon_'+$scope.students2[i].student.id).show();
					 
					}
			 }
				});	
		}
	}
	
	$scope.submitResult = function(student){
						$scope.submitted = true;
						if(student.id!=null){
							restService.one("checkup").customPOST(student, "update/"+student.id	).then(function(data){
								alert("Comments was successfully saved!");
							});
							console.log(student.student.id);
							$('.check_button_'+student.student.id).hide(1000);
							$('.check_icon_'+student.student.id).show(1000);
	
						}else{
							alert("Firstly Please enter weight and height of children!");
						}
	}
	
}])
.controller('EditCheckupCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils', '$translatePartialLoader','$routeParams','$location', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$translatePartialLoader, $routeParams, $location){	
		
	$translatePartialLoader.addPart('doctor');
	
	$scope.checkupId = $routeParams.id;
	$scope.student = {};
	$scope.period = {};
	$scope.group = {};
	$scope.submitted = false;
	
	restService.all("check_period").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		$scope.periods = results.resultList;
	});
	restService.all("c_group").customPOST({startIndex: 0, resultQuantity: 1000,  searchParameters: {"schoolTypeId": 2}}, "list").then(function(data){
		$scope.groups = data.originalElement;
	});
	restService.one("checkup").customGET($scope.checkupId).then(function(data){
		$scope.student = data.originalElement;
	});
	
	$scope.update = function(){
		console.log("here");
		$scope.submitted = true;
		restService.one("checkup").customPOST($scope.student, "update/"+$scope.checkupId).then(function(data){
			console.log("here");
			alert("successfully updated!");
			$location.path("/checkupResults");
		});
	};
	
}]);
