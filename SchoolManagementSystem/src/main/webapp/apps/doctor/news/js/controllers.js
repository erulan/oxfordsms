'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('NewsListCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$location','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$location, $translatePartialLoader){	
	  						
	$translatePartialLoader.addPart('doctor');

	$scope.news = {};
	
	$scope.submitNews= function() {
		console.log($scope.news);
		if($scope.news.date!=null){
			if($scope.news.title!=null){
				if($scope.news.text!=null){
					restService.one("news").customPOST($scope.news, "create").then(function(data){
						alert("News was successfully created!");
						$location.path("#/news");
					});
				}else{
					alert("Please enter content");
				}
			}else{
				alert("Please enter title");
			}
		}else{
			alert("Please enter date");
		}
	};
	
	restService.all("news").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		console.log(results);
		$scope.newses = results.resultList;
	});

	$scope.deleteNews = function(id){
			Utils.callDeleteModal("news", id);
	}
	
	$scope.datepicker = {};
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])

.controller('EditNewsCtrl',  ['$scope', 'restService', '$routeParams', "$filter", "Utils","$location", '$translatePartialLoader',function($scope, restService, $routeParams, $filter, Utils, $location,$translatePartialLoader){
	
	$translatePartialLoader.addPart('doctor');

	$scope.newsId = $routeParams.id;
	$scope.news = {};
	$scope.submitted = false;
	
	restService.all("news").customGET($scope.newsId).then(function(data){
		$scope.news = data.originalElement;
	});
	
	
	$scope.update = function(){
		console.log($scope.news);
		$scope.submitted = true;
		restService.one("news").customPOST($scope.news, "update/"+$scope.newsId	).then(function(data){
			alert("News was successfully updated!");
			$location.path("/news");
		});
	};
	
	$scope.datepicker = {};
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	
}])







