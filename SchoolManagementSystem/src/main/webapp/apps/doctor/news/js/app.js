'use strict'; 

angular.module('stuffApp', ['common.apps', 'controllers']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/news', {
		templateUrl : 'views/list_news.html',
		controller : 'NewsListCtrl',
	});
	$routeProvider.when('/edit_news/:id', {
		templateUrl : 'views/edit_news.html',
		controller : 'EditNewsCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/news'
	});
  			
}]);	




