'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('HeightToAgeListCtrl', ['$scope', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader','$timeout','$translate', function($scope,  restService, ngTableParams, $filter, Utils,$translatePartialLoader,$timeout, $translate){		
	
	$translatePartialLoader.addPart('doctor');
	
	$scope.getLanguage = function(){
		return $translate.use();
	}
	$scope.gender = 1;
	$scope.standard = {};
	
	$scope.getStandards = function($defer, params, path) {
    	
		var searchParams = {};
    	var prm = params.parameters();

    	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
    	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
    	
    	searchParams["searchParameters"] = {};
    	angular.forEach(prm.filter, function(value, key){
    		searchParams.searchParameters[key] = value;
    	});

		searchParams.searchParameters["isMale"] = $scope.gender;
    	searchParams["orderParamDesc"] = {};
    	angular.forEach(prm.sorting, function(value, key){
    		searchParams.orderParamDesc[key] = (value == "desc");
    	});
    	
    	restService.all(path).customPOST(searchParams, ['list']).then(function(data){
    		params.total(data.originalElement.totalRecords);
            // set new data
            $defer.resolve(data.originalElement.resultList);
		});
    }

	$scope.getDataByGender =function(){
		$scope.tableParams.reload();
		
	}
	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});

	$scope.getTitle = function (title){
		return $("."+title).text();
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
        	month: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	$scope.getStandards($defer, params, "height_standard");
//        	Utils.ngTableGetData($defer, params, "height_standard");
        },
    });
	
	$scope.submitStandard = function(){
		if($scope.standard.isMale!=null){
			if($scope.standard.month!=null){
				if($scope.standard.cat1!=null && $scope.standard.cat2!=null && $scope.standard.cat3!=null && $scope.standard.cat4!=null && $scope.standard.cat5!=null && $scope.standard.cat6!=null && $scope.standard.cat7!=null){
					restService.one("height_standard").customPOST($scope.standard, "create").then(function(data){
						alert("Standard was successfully registered!");
					    $scope.tableParams.reload();
						$scope.standard={};
					});
				}else{
					alert("Please check categories!");
				}
			}else{
				alert("Please enter month!");
			}
		}else{
			alert("Please select gender!");
		}
	}
	
	$scope.getById = function(id){
		console.log(id);
		restService.one("height_standard").customGET(id).then(function(data){
			$scope.standard = data.originalElement;
			$(".check_button").hide(1000);
			$(".update_button").show(1000);
		});
	}
	
	$scope.submitted = false;
	$scope.updateStandard = function(){
		$scope.submitted = true;
		if($scope.standard.isMale!=null){
			if($scope.standard.month!=null){
				if($scope.standard.cat1!=null && $scope.standard.cat2!=null && $scope.standard.cat3!=null && $scope.standard.cat4!=null && $scope.standard.cat5!=null && $scope.standard.cat6!=null && $scope.standard.cat7!=null){
console.log("a"+$scope.standard.cat3);
					restService.one("height_standard").customPOST($scope.standard, "update/" + $scope.standard.id).then(function(data){
						alert("Height Standard was successfully updated!");
					    $scope.tableParams.reload();
						$(".check_button").show(1000);
						$(".update_button").hide(1000);
						$scope.standard = {};

						});

				}else{
					alert("Please check categories!");
				}
			}else{
				alert("Please enter month!");
			}
		}else{
			alert("Please select gender!");
		}
	}	
		  
	  $scope.deleteStuff = function(id){
			Utils.callDeleteModal("height_standard", id);
	};
	
	  			
}])
	
.controller('WeightToAgeListCtrl', ['$scope', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader','$timeout', '$translate',function($scope,  restService, ngTableParams, $filter, Utils,$translatePartialLoader,$timeout,$translate){		
	
	$translatePartialLoader.addPart('doctor');
	$scope.getLanguage = function(){
		return $translate.use();
	}
	$scope.standard = {};
	$scope.gender = 1;
	
	$scope.getStandards = function($defer, params, path) {
    	
		var searchParams = {};
    	var prm = params.parameters();

    	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
    	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
    	
    	searchParams["searchParameters"] = {};
    	angular.forEach(prm.filter, function(value, key){
    		searchParams.searchParameters[key] = value;
    	});

		searchParams.searchParameters["isMale"] = $scope.gender;
    	searchParams["orderParamDesc"] = {};
    	angular.forEach(prm.sorting, function(value, key){
    		searchParams.orderParamDesc[key] = (value == "desc");
    	});
    	
    	restService.all(path).customPOST(searchParams, ['list']).then(function(data){
    		params.total(data.originalElement.totalRecords);
            // set new data
            $defer.resolve(data.originalElement.resultList);
		});
    }

	
	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});

	$scope.getTitle = function (title){
		return $("."+title).text();
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
        	month: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	$scope.getStandards($defer, params, "weight_standard");
//        	Utils.ngTableGetData($defer, params, "weight_standard");
        },
    });
	
	$scope.getDataByGender = function(){
		$scope.tableParams.reload();
	}
	
	$scope.submitStandard = function(){
		if($scope.standard.isMale!=null){
			if($scope.standard.month!=null){
				if($scope.standard.cat1!=null && $scope.standard.cat2!=null && $scope.standard.cat3!=null && $scope.standard.cat4!=null && $scope.standard.cat5!=null && $scope.standard.cat6!=null && $scope.standard.cat7!=null){
					restService.one("weight_standard").customPOST($scope.standard, "create").then(function(data){
						alert("Standard was successfully registered!");
					    $scope.tableParams.reload();
						$scope.standard={};
					});
				}else{
					alert("Please check categories!");
				}
			}else{
				alert("Please enter month!");
			}
		}else{
			alert("Please select gender!");
		}
	}
	
	$scope.getById = function(id){
		console.log(id);
		restService.one("weight_standard").customGET(id).then(function(data){
			$scope.standard = data.originalElement;
			$(".check_button").hide(1000);
			$(".update_button").show(1000);
		});
	}
	
	$scope.submitted = false;
	$scope.updateStandard = function(){
		$scope.submitted = true;
		if($scope.standard.isMale!=null){
			if($scope.standard.month!=null){
				if($scope.standard.cat1!=null && $scope.standard.cat2!=null && $scope.standard.cat3!=null && $scope.standard.cat4!=null && $scope.standard.cat5!=null && $scope.standard.cat6!=null && $scope.standard.cat7!=null){
console.log("a"+$scope.standard.cat3);
					restService.one("weight_standard").customPOST($scope.standard, "update/" + $scope.standard.id).then(function(data){
						alert("Weight Standard was successfully updated!");
					    $scope.tableParams.reload();
						$(".check_button").show(1000);
						$(".update_button").hide(1000);
						$scope.standard = {};

						});

				}else{
					alert("Please check categories!");
				}
			}else{
				alert("Please enter month!");
			}
		}else{
			alert("Please select gender!");
		}
	}	
		  
	  $scope.deleteStuff = function(id){
			Utils.callDeleteModal("weight_standard", id);
	};
	
	  			
}])
.controller('WeightForHeightListCtrl', ['$scope', 'restService', 'ngTableParams', '$filter', 'Utils','$translatePartialLoader','$timeout', '$translate',function($scope,  restService, ngTableParams, $filter, Utils,$translatePartialLoader,$timeout,$translate){		
	
	$translatePartialLoader.addPart('doctor');
	$scope.getLanguage = function(){
		return $translate.use();
	}
	
	$scope.standard = {};
	$scope.gender = 1;
	
	$scope.getStandards = function($defer, params, path) {
    	
		var searchParams = {};
    	var prm = params.parameters();

    	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
    	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
    	
    	searchParams["searchParameters"] = {};
    	angular.forEach(prm.filter, function(value, key){
    		searchParams.searchParameters[key] = value;
    	});

		searchParams.searchParameters["isMale"] = $scope.gender;
    	searchParams["orderParamDesc"] = {};
    	angular.forEach(prm.sorting, function(value, key){
    		searchParams.orderParamDesc[key] = (value == "desc");
    	});
    	
    	restService.all(path).customPOST(searchParams, ['list']).then(function(data){
    		params.total(data.originalElement.totalRecords);
            // set new data
            $defer.resolve(data.originalElement.resultList);
		});
    }

	
	$scope.$watch("count", function (newVal, oldVal) {
        $scope.tableParams.count(newVal);
    });
	$scope.$watch("tableParams.$params.count", function (newVal, oldVal) {
		$scope.count = newVal;
	});

	$scope.getTitle = function (title){
		return $("."+title).text();
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
        	height: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	$scope.getStandards($defer, params, "weight_for_height");
//        	Utils.ngTableGetData($defer, params, "weight_for_height");
        },
    });
	
	$scope.getDataByGender = function(){
		$scope.tableParams.reload();
	}
	
	$scope.submitStandard = function(){
		if($scope.standard.isMale!=null){
			if($scope.standard.height!=null){
				if($scope.standard.cat1!=null && $scope.standard.cat2!=null && $scope.standard.cat3!=null && $scope.standard.cat4!=null && $scope.standard.cat5!=null && $scope.standard.cat6!=null && $scope.standard.cat7!=null){
					restService.one("weight_for_height").customPOST($scope.standard, "create").then(function(data){
						alert("Standard was successfully registered!");
					    $scope.tableParams.reload();
						$scope.standard={};
					});
				}else{
					alert("Please check categories!");
				}
			}else{
				alert("Please enter height!");
			}
		}else{
			alert("Please select gender!");
		}
	}
	
	$scope.getById = function(id){
		console.log(id);
		restService.one("weight_for_height").customGET(id).then(function(data){
			$scope.standard = data.originalElement;
			$(".check_button").hide(1000);
			$(".update_button").show(1000);
		});
	}
	
	$scope.submitted = false;
	$scope.updateStandard = function(){
		$scope.submitted = true;
		if($scope.standard.isMale!=null){
			if($scope.standard.height!=null){
				if($scope.standard.cat1!=null && $scope.standard.cat2!=null && $scope.standard.cat3!=null && $scope.standard.cat4!=null && $scope.standard.cat5!=null && $scope.standard.cat6!=null && $scope.standard.cat7!=null){
console.log("a"+$scope.standard.cat3);
					restService.one("weight_for_height").customPOST($scope.standard, "update/" + $scope.standard.id).then(function(data){
						alert("Weight Standard For Height was successfully updated!");
					    $scope.tableParams.reload();
						$(".check_button").show(1000);
						$(".update_button").hide(1000);
						$scope.standard = {};

						});

				}else{
					alert("Please check categories!");
				}
			}else{
				alert("Please enter month!");
			}
		}else{
			alert("Please select gender!");
		}
	}	
		  
	  $scope.deleteStuff = function(id){
			Utils.callDeleteModal("weight_for_height", id);
	};
	
	  			
}])