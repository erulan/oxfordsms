'use strict'; 

angular.module('stuffApp', ['common.apps', 'controllers']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/list_height_to_age', {
		templateUrl : 'views/list_height_to_age.html',
		controller : 'HeightToAgeListCtrl',
	});
	$routeProvider.when('/height_standard', {
		templateUrl : 'views/list_height_to_age.html',
		controller : 'HeightToAgeListCtrl',
	});
	$routeProvider.when('/list_weight_to_age', {
		templateUrl : 'views/list_weight_to_age.html',
		controller : 'WeightToAgeListCtrl',
	});
	$routeProvider.when('/list_weight_for_height', {
		templateUrl : 'views/list_weight_for_height.html',
		controller : 'WeightForHeightListCtrl',
	});
	$routeProvider.when('/stuff', {
		templateUrl : 'views/stuff_list.html',
		controller : 'StuffListCtrl',
	});
	$routeProvider.when('/stuff_profile/:id', {
		templateUrl : 'views/stuff_profile.html',
		controller : 'stuff_profileCtrl',
	});
	$routeProvider.when('/add_stuff', {
		templateUrl : 'views/add_stuff.html',
		controller : 'AddStuffCtrl',
	});
	$routeProvider.when('/edit_stuff/:id', {
		templateUrl : 'views/edit_stuff.html',
		controller : 'EditStuffCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/list_height_to_age'
	});
  			
}]);	




