'use strict'; 

angular.module('stuffApp', ['common.apps', 'controllers']).
config(['$routeProvider', '$locationProvider',   function ($routeProvider, $locationProvider) {
					

	$routeProvider.when('/check_period', {
		templateUrl : 'views/list_period.html',
		controller : 'ListPeriodCtrl',
	});
	$routeProvider.when('/edit_period/:id', {
		templateUrl : 'views/edit_period.html',
		controller : 'EditPeriodCtrl',
	});
	$routeProvider.otherwise({
		redirectTo : '/check_period'
	});
  			
}]);	




