'use strict';

/* Controllers */
	
angular.module('controllers', [])		
.controller('ListPeriodCtrl', ['$scope', '$modal', '$log', 'restService', 'ngTableParams', '$filter', 'Utils','$location','$translatePartialLoader', function($scope, $modal, $log, restService, ngTableParams, $filter, Utils,$location, $translatePartialLoader){	
	  						
	$translatePartialLoader.addPart('doctor');

	$scope.period = {};
	
	$scope.submitPeriod= function() {
		if($scope.period.name!=null){
			if($scope.period.dateFrom!=null){
				if($scope.period.dateTo!=null){
					if($scope.period.description!=null){
						if($scope.period.dateFrom < $scope.period.dateTo){

							restService.one("check_period").customPOST($scope.period, "create").then(function(data){
								alert("Check Period was successfully created!");
								$location.path("#/check_period");
							});
						}else{
							alert("ERROR! Date From is bigger than Date To");
						}
				}else{
					alert("Please enter description");
				}
			}else{
				alert("Please enter dateTo");
			}
		}else{
			alert("Please enter dateFrom");
		}
	}else{
		alert("Please enter name");
	}
	};
	
	restService.all("check_period").customPOST({"startIndex": 0, "resultQuantity": 1000}, ["list"]).then(function(results){
		console.log(results);
		$scope.periods = results.resultList;
	});

	$scope.deletePeriod = function(id){
		console.log(id);
			Utils.callDeleteModal("check_period", id);
	}
	
	$scope.datepicker1 = {};
	$scope.datepicker1.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker1.opened = true;
	  };
	  
	  $scope.datepicker1.format = 'dd/MM/yyyy';

	  $scope.datepicker1.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  
		$scope.datepicker2 = {};
		$scope.datepicker2.open = function($event) {
		    $event.preventDefault();
		    $event.stopPropagation();

		    $scope.datepicker2.opened = true;
		  };
		  
		  $scope.datepicker2.format = 'dd/MM/yyyy';

		  $scope.datepicker2.dateOptions = {
		    formatYear: 'yy',
		    startingDay: 1,
		    language: 'ru'
		  };
}])

.controller('EditPeriodCtrl',  ['$scope', 'restService', '$routeParams', "$filter", "Utils","$location", '$translatePartialLoader',function($scope, restService, $routeParams, $filter, Utils, $location,$translatePartialLoader){
	
	$translatePartialLoader.addPart('doctor');

	$scope.periodId = $routeParams.id;
	$scope.period = {};
	$scope.submitted = false;
	
	restService.all("check_period").customGET($scope.periodId).then(function(data){
		$scope.period = data.originalElement;
	});
	
	
	$scope.update = function(){
		$scope.submitted = true;
		if($scope.period.name!=null){
			if($scope.period.dateFrom!=null){
				if($scope.period.dateTo!=null){
					if($scope.period.description!=null){
						if($scope.period.dateFrom < $scope.period.dateTo){

							restService.one("check_period").customPOST($scope.period, "update/"+$scope.periodId	).then(function(data){
								alert("Period was successfully updated!");
								$location.path("/check_period");
							});
						}else{
							alert("ERROR! Date From is bigger than Date To");
						}
				}else{
					alert("Please enter description");
				}
			}else{
				alert("Please enter dateTo");
			}
		}else{
			alert("Please enter dateFrom");
		}
	}else{
		alert("Please enter name");
	}
	};
	
	$scope.datepicker1 = {};
	$scope.datepicker1.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker1.opened = true;
	  };
	  
	  $scope.datepicker1.format = 'dd/MM/yyyy';

	  $scope.datepicker1.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  
		$scope.datepicker2 = {};
		$scope.datepicker2.open = function($event) {
		    $event.preventDefault();
		    $event.stopPropagation();

		    $scope.datepicker2.opened = true;
		  };
		  
		  $scope.datepicker2.format = 'dd/MM/yyyy';

		  $scope.datepicker2.dateOptions = {
		    formatYear: 'yy',
		    startingDay: 1,
		    language: 'ru'
		  };	
}])







