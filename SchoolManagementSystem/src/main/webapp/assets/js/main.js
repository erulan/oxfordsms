function loadJS(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function() {
        var state = s.readyState;
        if (!callback.done && (!state || /loaded|complete/.test(state))) {
            callback.done = true;
            callback();
        }
    };
    document.getElementsByTagName('head')[0].appendChild(s);
}


function loadCSS(src, callback) {
    var s = document.createElement('link');
    s.type = "text/css";
    s.rel = "stylesheet";		
    s.href = src;
    s.async = false;
    s.onreadystatechange = s.onload = function() {
        var state = s.readyState;
        if (!callback.done && (!state || /loaded|complete/.test(state))) {
            callback.done = true;
            callback();
        }
    };
    document.getElementsByTagName('head')[0].appendChild(s);
}

var jsList = {};
jsList["_angular_"] = "../../../assets/lib/angular/angular.min.js";


jsList["_angular_route_"] = "../../../assets/lib/angular/angular-route.min.js";
jsList["_app_"] = "js/app.js";
jsList["_controllers_"] = "js/controllers.js";
jsList["_directives_"] = "js/directives.js";
jsList["_services_"] = "js/services.js";
jsList["_filters_"] = "js/filters.js";

jsList["_common_app_"] = "../../common/js/common.app.js";
jsList["_common_controllers_"] = "../../common/js/common.controllers.js";
jsList["_common_directives_"] = "../../common/js/common.directives.js";
jsList["_common_services_"] = "../../common/js/common.services.js";
jsList["_common_filters_"] = "../../common/js/common.filters.js";

jsList["_jquery_"] = "../../../assets/lib/jquery/jquery-2.1.1.min.js";
jsList["_bootstrap_"] = "../../../assets/lib/bootstrap/js/bootstrap.min.js";

jsList["_lodash_"] = "../../../assets/lib/lodash/lodash.min.js";
jsList["_restangular_"] = "../../../assets/lib/angular/restangular.min.js";

jsList["_ui_bootstrap_tpls_"] = "../../../assets/lib/angular/ui-bootstrap/ui-bootstrap-tpls-0.9.0.min.js";
jsList["_ng_table_"] = "../../../assets/lib/ng-table/ng-table.min.js";
jsList["_export_excel_"] = "../../../assets/lib/ng-table/ng-table-export-excel.js";

jsList["_fuelux_"] = "../../../assets/lib/fuelux/fuelux.js";
jsList["_theme_app_"] = "../../../assets/js/app.js";
jsList["_theme_plugin_"] = "../../../assets/js/app.plugin.js";
jsList["_theme_app_data_"] = "../../../assets/js/app.data.js";
jsList["_translate_"] = "../../../assets/lib/angular-translate/angular-translate.min.js";
jsList["_translate-loader-static-files_"] = "../../../assets/lib/angular-translate/angular-translate-loader-static-files.min.js";
jsList["_translate-loader-partial_"] = "../../../assets/lib/angular-translate/angular-translate-loader-partial.min.js";
jsList["_angular-local-storage_"] = "../../../assets/lib/angular-local-storage.min.js";
	
loadJS(jsList["_angular_"], function() {

	loadJS(jsList["_jquery_"], function() {
		loadJS(jsList["_fuelux_"], function() {});
		loadJS(jsList["_theme_app_"], function() {});
		loadJS(jsList["_theme_plugin_"], function() {});
		loadJS(jsList["_theme_app_data_"], function() {});
	});
	loadJS(jsList["_angular_route_"], function() {});
	loadJS(jsList["_bootstrap_"], function() {});
	loadJS(jsList["_lodash_"], function() {
		loadJS(jsList["_restangular_"], function() {});
	});		
	
	loadJS(jsList["_ui_bootstrap_tpls_"], function() {});
	loadJS(jsList["_ng_table_"], function() {});
	loadJS(jsList["_export_excel_"], function() {});
	loadJS(jsList["_translate_"], function() {
		loadJS(jsList["_translate-loader-static-files_"], function() {});
		loadJS(jsList["_translate-loader-partial_"], function() {});
	});
	loadJS(jsList["_angular-local-storage_"], function() {});

	loadJS(jsList["_common_app_"], function() {});
	loadJS(jsList["_common_controllers_"], function() {});
	loadJS(jsList["_common_services_"], function() {});
	loadJS(jsList["_common_directives_"], function() {});
	loadJS(jsList["_common_filters_"], function() {});
					
	loadJS(jsList["_app_"], function() {});
	loadJS(jsList["_services_"], function() {});
	loadJS(jsList["_controllers_"], function() {});
	loadJS(jsList["_filters_"], function() {});
	loadJS(jsList["_directives_"], function() {});
});	

var cssList = {};
cssList["_mystyle_"] ="../../common/css/mystyle.css";
cssList["_bootstrap_"] ="../../../assets/lib/bootstrap/css/bootstrap.min.css";
cssList["_bootstrap_glyphicon-woff_"] ="../../../assets/lib/bootstrap/fonts/glyphicons-halflings-regular.woff";
cssList["_font_awesome_"] ="../../../assets/css/font-awesome.min.css";
cssList["_theme_font_"] ="../../../assets/css/font.css";
cssList["_theme_plugin_"] ="../../../assets/css/plugin.css";
cssList["_theme_app_"] ="../../../assets/css/app.css";
cssList["_datatables_"] ="../../../assets/js/datatables/datatables.css"; 
cssList["_fuelux_"] ="../../../assets/lib/fuelux/fuelux.css";	
cssList["_ng_table_"] ="../../../assets/lib/ng-table/ng-table.min.css"; 	

loadCSS(cssList["_mystyle_"], function() {});
loadCSS(cssList["_bootstrap_"], function() {
	loadCSS(cssList["_bootstrap_glyphicon-woff_"], function() {});
});
loadCSS(cssList["_font_awesome_"], function() {});
loadCSS(cssList["_theme_font_"], function() {});		
loadCSS(cssList["_theme_app_"], function() {});
loadCSS(cssList["_datatables_"], function() {});
loadCSS(cssList["_plugin_"], function() {});			
loadCSS(cssList["_fuelux_"], function() {});
loadCSS(cssList["_ng_table_"], function() {});




